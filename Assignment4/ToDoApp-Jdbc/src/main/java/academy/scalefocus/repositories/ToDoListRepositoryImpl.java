package academy.scalefocus.repositories;

import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.tools.ConsoleManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ToDoListRepositoryImpl implements ToDoListRepository {
    private static final String CONNECTION_URL = "jdbc:oracle:thin:@localhost:1521:xe";
    private static final String USER = "cveta";
    private static final String PASSWORD = "admin_password";


    @Override
    public ToDoList getById(int id) {
        ToDoList toDoList = new ToDoList();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM todolists WHERE" +
                     " todolist_id=?")) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    toDoList = createToDoList(resultSet);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return toDoList;
    }

    private ToDoList createToDoList(ResultSet resultSet) throws SQLException {
        ToDoList toDoList = new ToDoList();

        toDoList.setId(resultSet.getInt("todolist_id"));
        toDoList.setCreatorId(resultSet.getInt("creator_id"));
        toDoList.setTitle(resultSet.getString("title"));
        toDoList.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        toDoList.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());
        toDoList.setModifierId(resultSet.getInt("modifier_id"));

        return toDoList;
    }

    @Override
    public List<ToDoList> getAllToDoLists() {
        List<ToDoList> lists = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM todolists");
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                ToDoList toDoList = createToDoList(resultSet);
                lists.add(toDoList);
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return lists;
    }

    @Override
    public void add(ToDoList toDoList) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO todolists " +
                     "(creator_id, title, date_created, date_modified, modifier_id) " +
                     "VALUES (?,?,?,?,?)")) {

            preparedStatement.setInt(1, toDoList.getCreatorId());
            preparedStatement.setString(2, toDoList.getTitle());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(toDoList.getDateCreated()));
            preparedStatement.setTimestamp(4, Timestamp.valueOf(toDoList.getDateModified()));
            preparedStatement.setInt(5, toDoList.getModifierId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void edit(ToDoList toDoList, int todoId) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("UPDATE todolists SET title=?, " +
                     "date_modified=?, modifier_id=? " +
                     "WHERE todolist_id=?")) {

            preparedStatement.setString(1, toDoList.getTitle());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(toDoList.getDateModified()));
            preparedStatement.setInt(3, toDoList.getModifierId());
            preparedStatement.setInt(4, todoId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void delete(ToDoList toDoList, int loggedUserId) {

        if(toDoList.getCreatorId() == loggedUserId) {

            try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                    USER, PASSWORD);
                 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM todolists WHERE todolist_id=?");
            ) {

                preparedStatement.setInt(1, toDoList.getId());

                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                ConsoleManager.writeLine(e.getMessage());
            }
        } else {

            try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                    USER, PASSWORD);
                 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM userslists WHERE user_id=? AND " +
                         "todolist_id=?");
            ) {

                preparedStatement.setInt(1, loggedUserId);
                preparedStatement.setInt(2, toDoList.getId());

                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                ConsoleManager.writeLine(e.getMessage());
            }
        }
    }

    @Override
    public List<ToDoList> getAllToDoLists(int userId) {
        List<ToDoList> lists = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM todolists WHERE " +
                     "creator_id=?")) {

            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    ToDoList toDoList = createToDoList(resultSet);
                    lists.add(toDoList);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM userslists WHERE" +
                     " user_id=?")) {

            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    ToDoList toDoList = createToDoList(resultSet);
                    lists.add(toDoList);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return lists;
    }

    @Override
    public void share(int userId, int todoId) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO userslists " +
                     "(list_id, user_id) VALUES (?,?)")) {

            preparedStatement.setInt(1, todoId);
            preparedStatement.setInt(2, userId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }
}
