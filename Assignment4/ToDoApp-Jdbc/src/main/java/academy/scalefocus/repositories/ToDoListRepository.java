package academy.scalefocus.repositories;

import academy.scalefocus.entities.ToDoList;

import java.util.List;

public interface ToDoListRepository {

    ToDoList getById(int id);

    List<ToDoList> getAllToDoLists();

    void add(ToDoList toDoList);

    void edit(ToDoList toDoList, int id);

    void delete(ToDoList toDoList, int loggedUserId);

    List<ToDoList> getAllToDoLists(int parentId);

    void share(int userId, int todoId);
}
