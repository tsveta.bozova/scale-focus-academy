package academy.scalefocus.repositories;

import academy.scalefocus.entities.User;
import academy.scalefocus.tools.ConsoleManager;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    private static final String CONNECTION_URL = "jdbc:oracle:thin:@localhost:1521:xe";
    private static final String USER = "cveta";
    private static final String PASSWORD = "admin_password";


    public UserRepositoryImpl() {

        User user = new User();

        user.setUsername("admin");
        user.setPassword("adminpass");
        user.setFirstName("Administrator");
        user.setLastName("Administrator");
        user.setIsAdmin(true);
        user.setDateCreated(LocalDateTime.now());
        user.setCreatorId(1);
        user.setModifierId(1);
        user.setDateModified(LocalDateTime.now());

        if (getAll().isEmpty()) {
            add(user);
        }
    }


    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users");
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                User user = createUser(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return users;
    }

    @Override
    public User getById(int id) {
        User user = new User();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE" +
                     " user_id=?")) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    user = createUser(resultSet);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return user;
    }

    @Override
    public void add(User user) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users " +
                     "(username, password, first_name, last_name, is_admin, date_created, creator_id, modifier_id, date_modified) " +
                     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getIsAdminAsString());
            preparedStatement.setTimestamp(6, Timestamp.valueOf(user.getDateCreated()));
            preparedStatement.setInt(7, user.getCreatorId());
            preparedStatement.setInt(8, user.getModifierId());
            preparedStatement.setTimestamp(9, Timestamp.valueOf(user.getDateModified()));

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void edit(User user, int id) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET username=?, " +
                             "password=?, first_name=?, last_name=?, is_admin=?, modifier_id=?, date_modified=? " +
                     "WHERE user_id=?")) {

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getIsAdminAsString());
            preparedStatement.setInt(6, user.getModifierId());
            preparedStatement.setTimestamp(7, Timestamp.valueOf(user.getDateModified()));
            preparedStatement.setInt(8, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM users WHERE user_id=?")
        ) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public User getByUserNameAndPassword(String userName, String password) {
        User user = new User();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE" +
                     " username=? AND password=?")) {

            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, password);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    user = createUser(resultSet);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return user;
    }

    private User createUser(ResultSet resultSet) throws SQLException {
        User user = new User();

        user.setId(resultSet.getInt("user_id"));
        user.setUsername(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setIsAdminFromString(resultSet.getString("is_admin"));
        user.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        user.setCreatorId(resultSet.getInt("creator_id"));
        user.setModifierId(resultSet.getInt("modifier_id"));
        user.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());

        return user;
    }
}
