package academy.scalefocus.repositories;

import academy.scalefocus.entities.Task;
import academy.scalefocus.tools.ConsoleManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryImpl implements TaskRepository {
    private static final String CONNECTION_URL = "jdbc:oracle:thin:@localhost:1521:xe";
    private static final String USER = "cveta";
    private static final String PASSWORD = "admin_password";


    @Override
    public Task getById(int id) {
        Task task = new Task();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM tasks WHERE" +
                     " task_id=?")) {

            preparedStatement.setInt(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    task = createTask(resultSet);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }

        return task;
    }

    private Task createTask(ResultSet resultSet) throws SQLException {
        Task task = new Task();

        task.setId(resultSet.getInt("task_id"));
        task.setToDoListId(resultSet.getInt("todolist_id"));
        task.setTitle(resultSet.getString("title"));
        task.setDescription(resultSet.getString("description"));
        task.setIsCompleteFromString(resultSet.getString("is_complete"));
        task.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        task.setCreatorId(resultSet.getInt("creator_id"));
        task.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());
        task.setModifierId(resultSet.getInt("modifier_id"));

        return task;
    }


    @Override
    public void add(Task task) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO tasks " +
                     "(todolist_id, title, description, is_complete, date_created, creator_id, date_modified, modifier_id) " +
                     "VALUES (?,?,?,?,?,?,?,?)")) {

            preparedStatement.setInt(1, task.getToDoListId());
            preparedStatement.setString(2, task.getTitle());
            preparedStatement.setString(3, task.getDescription());
            preparedStatement.setString(4, task.getIsCompleteAsString());
            preparedStatement.setTimestamp(5, Timestamp.valueOf(task.getDateCreated()));
            preparedStatement.setInt(6, task.getCreatorId());
            preparedStatement.setTimestamp(7, Timestamp.valueOf(task.getDateModified()));
            preparedStatement.setInt(8, task.getModifierId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void edit(Task task) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("UPDATE tasks SET title=?, " +
                     "description=?, date_modified=?, modifier_id=? " +
                     "WHERE task_id=?")) {

            preparedStatement.setString(1, task.getTitle());
            preparedStatement.setString(2, task.getDescription());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(task.getDateModified()));
            preparedStatement.setInt(4, task.getModifierId());
            preparedStatement.setInt(5, task.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void delete(Task task, int loggedUserId) {

        if(task.getCreatorId() == loggedUserId) {

            try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                    USER, PASSWORD);
                 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM tasks WHERE task_id=?")
            ) {

                preparedStatement.setInt(1, task.getId());
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                ConsoleManager.writeLine(e.getMessage());
            }
        } else {

            try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                    USER, PASSWORD);
                 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM userstasks WHERE user_id=? AND " +
                         "task_id=?")
            ) {

                preparedStatement.setInt(1, loggedUserId);
                preparedStatement.setInt(2, task.getId());
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                ConsoleManager.writeLine(e.getMessage());
            }
        }
    }

    @Override
    public List<Task> getAll(int toDoListId) {
        List<Task> tasks = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL,
                USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM tasks WHERE" +
                     " todolist_id=?")) {

            preparedStatement.setInt(1, toDoListId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Task task = createTask(resultSet);
                    tasks.add(task);
                }
            }
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
        return tasks;
    }

    @Override
    public void assign(int userId, int taskId) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO userstasks " +
                     "(task_id, user_id) VALUES (?,?)")) {

            preparedStatement.setInt(1, taskId);
            preparedStatement.setInt(2, userId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public void complete(Task task) {

        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement("UPDATE tasks SET is_complete=?, " +
                     "date_modified=?, modifier_id=? " +
                     "WHERE task_id=?")) {

            preparedStatement.setString(1, task.getIsCompleteAsString());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(task.getDateModified()));
            preparedStatement.setInt(3, task.getModifierId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }
}
