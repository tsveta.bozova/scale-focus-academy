package academy.scalefocus.entities;

import java.time.LocalDateTime;

public class User {
    private int id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private boolean isAdmin;
    private LocalDateTime dateCreated;
    private int creatorId;
    private int modifierId;
    private LocalDateTime dateModified;



    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setIsAdminFromString(String isAdminString) {
        this.isAdmin = "1".equals(isAdminString);
    }
    public String getIsAdminAsString() {
        return this.isAdmin ? "1" : "0";
    }

    public void setIsAdmin(boolean admin) {
        this.isAdmin = admin;
    }

    public boolean getIsAdmin () {
        return this.isAdmin;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getModifierId() {
        return modifierId;
    }

    public void setModifierId(int modifierId) {
        this.modifierId = modifierId;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }
}
