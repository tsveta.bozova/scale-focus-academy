package academy.scalefocus.views;

import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

public class AuthenticationView {

    public void run()  {

        while (AuthenticationService.getInstance().getLoggedUser() == null) {

            ConsoleManager.clear();

            ConsoleManager.write("Username: ");
            String username = ConsoleManager.readLine();

            ConsoleManager.write("Password: ");
            String password = ConsoleManager.readLine();

            AuthenticationService.getInstance().authenticateUser(username, password);
        }

    }
}
