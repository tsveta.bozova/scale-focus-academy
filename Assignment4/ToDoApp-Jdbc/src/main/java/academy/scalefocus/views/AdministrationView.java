package academy.scalefocus.views;

import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

public class AdministrationView {

    public void run() {

        while (true) {
            ConsoleManager.clear();

            if (AuthenticationService.getInstance().getLoggedUser().getIsAdmin()) {
                ConsoleManager.writeLine("[U]ser Management");
            }
            ConsoleManager.writeLine("To[D]oLIst Management");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");
            String choice = ConsoleManager.readLine();

            if ("U".equalsIgnoreCase(choice)) {
                usersManagement();
                break;
            } else if ("D".equalsIgnoreCase(choice)) {
                toDoListsManagement();
                break;
            } else if ("X".equalsIgnoreCase(choice)) {
                return;
            } else {
                ConsoleManager.clear();
                ConsoleManager.writeLine("Invalid choice!");
                ConsoleManager.writeLine("Press [Enter] to continue");
                ConsoleManager.readLine();
                break;
            }
        }
    }

    private void usersManagement() {

        UserManagementView view = new UserManagementView();
        view.run();
    }

    private void toDoListsManagement() {
        ToDoListManagementView view = new ToDoListManagementView();
        view.run();
    }
}
