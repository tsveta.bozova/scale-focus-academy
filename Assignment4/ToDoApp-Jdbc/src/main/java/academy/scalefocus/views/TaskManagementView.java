package academy.scalefocus.views;

import academy.scalefocus.entities.Task;
import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.TaskRepository;
import academy.scalefocus.repositories.TaskRepositoryImpl;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class TaskManagementView {
    private ToDoList parent;
    private int loggedUserId;
    private TaskRepository taskRepo;
    private UserRepository userRepo;

    public TaskManagementView(ToDoList parent) {
        this.parent = parent;
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.taskRepo = new TaskRepositoryImpl();
        this.userRepo = new UserRepositoryImpl();
    }

    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            switch (choice) {
                case LIST:
                    list();
                    break;
                case ADD:
                    add();
                    break;
                case EDIT:
                    edit();
                    break;
                case COMPLETE:
                    complete();
                    break;
                case ASSIGN:
                    assign();
                    break;
                case DELETE:
                    delete();
                    break;
                case EXIT:
                    return;
                default:
                    break;
            }
        }
    }


    private MenuChoices renderMenu() {

        while (true) {

            ConsoleManager.clear();

            ConsoleManager.writeLine("[L]ist Tasks");
            ConsoleManager.writeLine("[A]dd Task");
            ConsoleManager.writeLine("[E]dit Task");
            ConsoleManager.writeLine("[C]omplete task");
            ConsoleManager.writeLine("Assig[n] a Task");
            ConsoleManager.writeLine("[D]elete Task");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");

            String choice = ConsoleManager.readLine();

            switch (choice.toUpperCase()) {
                case "L":
                    return MenuChoices.LIST;
                case "A":
                    return MenuChoices.ADD;
                case "E":
                    return MenuChoices.EDIT;
                case "C":
                    return MenuChoices.COMPLETE;
                case "N":
                    return MenuChoices.ASSIGN;
                case "D":
                    return MenuChoices.DELETE;
                case "X":
                    return MenuChoices.EXIT;
                default:
                    ConsoleManager.clear();
                    ConsoleManager.writeLine("Invalid choice!");
                    ConsoleManager.writeLine("Press [Enter] to continue");
                    ConsoleManager.readLine();
                    break;
            }
        }
    }

    private void add() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add Task####");

        Task task = new Task();

        ConsoleManager.write("Title: ");

        task.setToDoListId(parent.getId());
        task.setTitle(ConsoleManager.readLine());

        ConsoleManager.write("Description: ");
        task.setDescription(ConsoleManager.readLine());

        task.setDateCreated(LocalDateTime.now());
        task.setIsComplete(false);
        task.setDateCreated(LocalDateTime.now());
        task.setCreatorId(this.loggedUserId);
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        this.taskRepo.add(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item added successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void list() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####List All Tasks####");

        List<Task> tasks = this.taskRepo.getAll(this.parent.getId());

        for (Task task : tasks) {

            ConsoleManager.write("ID: ");
            ConsoleManager.writeLine(task.getId());
            ConsoleManager.write("Title: ");
            ConsoleManager.writeLine(task.getTitle());
            ConsoleManager.write("ToDoList ID: ");
            ConsoleManager.writeLine(task.getToDoListId());
            ConsoleManager.write("Description :");
            ConsoleManager.writeLine(task.getDescription());
            ConsoleManager.write("Complete:");
            ConsoleManager.writeLine(task.getIsComplete());
            ConsoleManager.write("Created: ");
            ConsoleManager.writeLine(task.getDateCreated());
            ConsoleManager.write("Creator ID: ");
            ConsoleManager.writeLine(task.getCreatorId());
            ConsoleManager.write("Last User made changes: ");
            ConsoleManager.writeLine(task.getModifierId());
            ConsoleManager.write("Date of last change: ");
            ConsoleManager.writeLine(task.getDateModified());
            ConsoleManager.writeLine("---------------------------------");
        }

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void assign() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Assign Task####");

        List<Task> tasks = this.taskRepo.getAll(this.loggedUserId);

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Tasks: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();

        List<User> users = this.userRepo.getAll();

        for (User user : users) {

            ConsoleManager.write(user.getUsername() + " ( " + user.getId() + " )\n");

        }

        ConsoleManager.write("Assign to: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        this.taskRepo.assign(userId, taskId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();

    }

    private void edit() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parent.getId());

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.write("Title ( " + task.getTitle() + " ): ");
        task.setTitle(ConsoleManager.readLine());

        ConsoleManager.write("Description ( " + task.getDescription() + " ): ");
        task.setDescription(ConsoleManager.readLine());

        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        this.taskRepo.edit(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void complete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Complete Task####");

        List<Task> tasks = this.taskRepo.getAll(parent.getId());

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = taskRepo.getById(taskId);

        ConsoleManager.write("Title ( " + task.getIsComplete() + " ): ");
        task.setIsComplete(Boolean.parseBoolean(ConsoleManager.readLine()));
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        this.taskRepo.complete(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();

    }

    private void delete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parent.getId());

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(id);
        this.taskRepo.delete(task, this.loggedUserId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item deleted successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }
}
