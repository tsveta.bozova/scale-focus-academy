package academy.scalefocus.views;

import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.ToDoListRepository;
import academy.scalefocus.repositories.ToDoListRepositoryImpl;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class ToDoListManagementView {
    private int loggedUserId;
    private ToDoListRepository todoRepo;
    private UserRepository userRepo;

    public ToDoListManagementView() {
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.todoRepo = new ToDoListRepositoryImpl();
        this.userRepo = new UserRepositoryImpl();
    }

    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            switch (choice) {

                case LIST:
                    list();
                    break;
                case ADD:
                    add();
                    break;
                case EDIT:
                    edit();
                    break;
                case DELETE:
                    delete();
                    break;
                case SHARE:
                    share();
                    break;
                case VIEW:
                    view();
                    break;
                case EXIT:
                    return;
                default:
                    break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {

            ConsoleManager.clear();

            ConsoleManager.writeLine("[L]ist ToDoLists");
            ConsoleManager.writeLine("[A]dd ToDoLIst");
            ConsoleManager.writeLine("[E]dit ToDoList");
            ConsoleManager.writeLine("[D]elete ToDoList");
            ConsoleManager.writeLine("[S]hare ToDoList");
            ConsoleManager.writeLine("[V]iew ToDoList");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");

            String choice = ConsoleManager.readLine();

            switch (choice.toUpperCase()) {
                case "L":
                    return MenuChoices.LIST;
                case "A":
                    return MenuChoices.ADD;
                case "E":
                    return MenuChoices.EDIT;
                case "D":
                    return MenuChoices.DELETE;
                case "S":
                    return MenuChoices.SHARE;
                case "V":
                    return MenuChoices.VIEW;
                case "X":
                    return MenuChoices.EXIT;
                default:
                    ConsoleManager.clear();
                    ConsoleManager.writeLine("Invalid choice!");
                    ConsoleManager.writeLine("Press [Enter] to continue");
                    ConsoleManager.readLine();
                    break;
            }
        }
    }

    private void add() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add ToDoList####");

        ToDoList toDoList = new ToDoList();

        ConsoleManager.write("Title: ");

        toDoList.setCreatorId(this.loggedUserId);
        toDoList.setTitle(ConsoleManager.readLine());
        toDoList.setDateCreated(LocalDateTime.now());
        toDoList.setDateModified(LocalDateTime.now());
        toDoList.setModifierId(this.loggedUserId);

        this.todoRepo.add(toDoList);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item added successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void list() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####List ToDoLists####");

        List<ToDoList> lists = this.todoRepo.getAllToDoLists(this.loggedUserId);

        if(lists.isEmpty()) {
            ConsoleManager.writeLine("You don't have ToDoLists");
            ConsoleManager.writeLine("Press [Enter] to continue");
            ConsoleManager.readLine();
            return;
        }

        for (ToDoList list : lists) {

            ConsoleManager.write("ID: ");
            ConsoleManager.writeLine(list.getId());
            ConsoleManager.write("Title: ");
            ConsoleManager.writeLine(list.getTitle());
            ConsoleManager.write("Created: ");
            ConsoleManager.writeLine(list.getDateCreated());
            ConsoleManager.write("Creator ID: ");
            ConsoleManager.writeLine(list.getCreatorId());
            ConsoleManager.write("Date of last change: ");
            ConsoleManager.writeLine(list.getDateCreated());
            ConsoleManager.write("Modifier ID: ");
            ConsoleManager.writeLine(list.getModifierId());
            ConsoleManager.writeLine("---------------------------------");
        }

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void edit() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit ToDoList####");

        List<ToDoList> lists = this.todoRepo.getAllToDoLists();

        for (ToDoList toDoList : lists) {

            ConsoleManager.write(toDoList.getTitle() + " ( " + toDoList.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int todoId = Integer.parseInt(ConsoleManager.readLine());

        ToDoList list = this.todoRepo.getById(todoId);

        ConsoleManager.write("Title ( " + list.getTitle() + " ): ");
        list.setTitle(ConsoleManager.readLine());

        list.setDateModified(LocalDateTime.now());
        list.setModifierId(this.loggedUserId);

        this.todoRepo.edit(list, todoId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void delete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete ToDoList####");


        List<ToDoList> lists = this.todoRepo.getAllToDoLists();

        for (ToDoList list : lists) {

            ConsoleManager.write(list.getTitle() + " ( " + list.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int todoId = Integer.parseInt(ConsoleManager.readLine());

        ToDoList list = this.todoRepo.getById(todoId);

        this.todoRepo.delete(list, this.loggedUserId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item deleted successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void share() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Share ToDoList####");

        List<ToDoList> lists = this.todoRepo.getAllToDoLists(this.loggedUserId);

        for (ToDoList list : lists) {

            ConsoleManager.write(list.getTitle() + " ( " + list.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int todoId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();

        List<User> users = this.userRepo.getAll();

        for (User user : users) {

            ConsoleManager.write(user.getUsername() + " ( " + user.getId() + " )\n");

        }

        ConsoleManager.write("Share to: ");
        int userToShareId = Integer.parseInt(ConsoleManager.readLine());

        this.todoRepo.share(userToShareId, todoId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }
    private void view() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####View ToDoList####");

        List<ToDoList> toDoLists = this.todoRepo.getAllToDoLists(this.loggedUserId);

        for (ToDoList toDoList : toDoLists) {
            ConsoleManager.write(toDoList.getTitle() + " ( " + toDoList.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int todoId = Integer.parseInt(ConsoleManager.readLine());

        ToDoList toDoList = this.todoRepo.getById(todoId);

        ConsoleManager.clear();

        TaskManagementView taskView = new TaskManagementView(toDoList);
        taskView.run();
    }
}
