package academy.scalefocus.views;

import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class UserManagementView {
    private int loggedUserId;
    private UserRepository userRepo;

    public UserManagementView() {
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.userRepo = new UserRepositoryImpl();
    }

    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            switch (choice) {
                case LIST:
                    list();
                    break;
                case ADD:
                    add();
                    break;
                case EDIT:
                    edit();
                    break;
                case DELETE:
                    delete();
                    break;
                case EXIT:
                    return;
                default:
                    break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.clear();

            ConsoleManager.writeLine("[L]ist Users");
            ConsoleManager.writeLine("[A]dd user");
            ConsoleManager.writeLine("[E]dit User");
            ConsoleManager.writeLine("[D]elete User");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");
            String choice = ConsoleManager.readLine();

            if ("L".equalsIgnoreCase(choice)) {
                return MenuChoices.LIST;
            } else if ("A".equalsIgnoreCase(choice)) {
                return MenuChoices.ADD;
            } else if ("E".equalsIgnoreCase(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equalsIgnoreCase(choice)) {
                return MenuChoices.DELETE;
            } else if ("X".equalsIgnoreCase(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.clear();
                ConsoleManager.writeLine("Invalid choice!");
                ConsoleManager.writeLine("Press [Enter] to continue");
                ConsoleManager.readLine();
            }
        }
    }

    private void add() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add User####");

        User user = new User();

        ConsoleManager.write("Username: ");
        user.setUsername(ConsoleManager.readLine());

        ConsoleManager.write("Password: ");
        user.setPassword(ConsoleManager.readLine());


        ConsoleManager.write("First Name: ");
        user.setFirstName(ConsoleManager.readLine());

        ConsoleManager.write("Last Name: ");
        user.setLastName(ConsoleManager.readLine());

        ConsoleManager.write("Is Admin: ");
        user.setIsAdmin(Boolean.parseBoolean(ConsoleManager.readLine()));

        user.setDateCreated(LocalDateTime.now());
        user.setCreatorId(this.loggedUserId);
        user.setModifierId(this.loggedUserId);
        user.setDateModified(LocalDateTime.now());

        this.userRepo.add(user);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item added successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void list() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####List Users####");

        List<User> users = this.userRepo.getAll();

        for (User user : users) {

            ConsoleManager.write("ID: ");
            ConsoleManager.writeLine(user.getId());
            ConsoleManager.write("Username: ");
            ConsoleManager.writeLine(user.getUsername());
            ConsoleManager.write("Password: ");
            ConsoleManager.writeLine(user.getPassword());
            ConsoleManager.write("First Name: ");
            ConsoleManager.writeLine(user.getFirstName());
            ConsoleManager.write("Last Name: ");
            ConsoleManager.writeLine(user.getLastName());
            ConsoleManager.write("Is Admin: ");
            ConsoleManager.writeLine(user.getIsAdmin());
            ConsoleManager.write("Date of creation: ");
            ConsoleManager.writeLine(user.getDateCreated());
            ConsoleManager.write("Creator ID: ");
            ConsoleManager.writeLine(user.getCreatorId());
            ConsoleManager.write("ModifierID: ");
            ConsoleManager.writeLine(user.getModifierId());
            ConsoleManager.write("Date of last change: ");
            ConsoleManager.writeLine(user.getDateModified());
            ConsoleManager.writeLine("---------------------------------");
        }

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void edit() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit User####");

        List<User> users = this.userRepo.getAll();

        for (User user : users) {
            ConsoleManager.write(user.getUsername() + " ( " + user.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of user: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        User user = this.userRepo.getById(id);

        ConsoleManager.write("Username ( " + user.getUsername() + " ): ");
        user.setUsername(ConsoleManager.readLine());

        ConsoleManager.write("Password ( " + user.getPassword() + " ) : ");
        user.setPassword(ConsoleManager.readLine());

        ConsoleManager.write("First Name ( " + user.getFirstName() + " ): ");
        user.setFirstName(ConsoleManager.readLine());

        ConsoleManager.write("Last Name: ( " + user.getLastName() + " ): ");
        user.setLastName(ConsoleManager.readLine());

        ConsoleManager.write("Is Admin: ( " + user.getIsAdmin() + " ): ");
        user.setIsAdmin(Boolean.parseBoolean(ConsoleManager.readLine()));

        user.setModifierId(this.loggedUserId);
        user.setDateModified(LocalDateTime.now());

        this.userRepo.edit(user, id);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void delete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete User####");

        List<User> users = this.userRepo.getAll();

        for (User user : users) {

            ConsoleManager.write(user.getUsername() + " ( " + user.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of user: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        this.userRepo.delete(id);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item deleted successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }
}
