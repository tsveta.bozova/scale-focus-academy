package academy.scalefocus.views;

public enum MenuChoices {

    LIST, ADD, EDIT, DELETE, COMPLETE, ASSIGN, VIEW, SHARE, EXIT
}
