package academy.scalefocus.services;

import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;

public class AuthenticationService {

    private static AuthenticationService instance = null;


    public static AuthenticationService getInstance() {

        if (AuthenticationService.instance == null)
            AuthenticationService.instance = new AuthenticationService();

        return AuthenticationService.instance;
    }

    private AuthenticationService() {

    }

    private User authenticatedUser = null;

    public User getLoggedUser() {
        return authenticatedUser;
    }

    public void authenticateUser(String username, String password) {

        UserRepository userRepo = new UserRepositoryImpl();


        this.authenticatedUser = userRepo.getByUserNameAndPassword(username, password);
    }
}
