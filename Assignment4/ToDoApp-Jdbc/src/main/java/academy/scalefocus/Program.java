package academy.scalefocus;

import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import academy.scalefocus.views.AdministrationView;
import academy.scalefocus.views.AuthenticationView;

public class Program {

    public static void main(String[] args) {
        AuthenticationView authView = new AuthenticationView();
        authView.run();


        if (AuthenticationService.getInstance().getLoggedUser() != null) {

            AdministrationView view = new AdministrationView();
            view.run();


            ConsoleManager.clear();
            ConsoleManager.writeLine("Good bye");
        }
    }
}
