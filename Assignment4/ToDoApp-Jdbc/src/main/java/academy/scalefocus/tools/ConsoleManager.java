package academy.scalefocus.tools;

import java.util.Scanner;

public class ConsoleManager {

    private static Scanner scanner = new Scanner(System.in);

    public static void clear() {

        for (int i = 0; i < 5; i++) {

            System.out.println("\n\n\n\n\n\n\n\n\n");
        }
    }

    public static String readLine() {

        return scanner.nextLine();
    }

    public static void writeLine() {

        System.out.println();
    }

    public static <T> void writeLine(T value) {

        System.out.println(value);
    }

    public static <T> void write(T value) {

        System.out.print(value);
    }
}
