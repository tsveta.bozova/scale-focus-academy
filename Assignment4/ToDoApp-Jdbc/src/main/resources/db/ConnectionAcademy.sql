
CREATE TABLE users (
        user_id               INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
        username              VARCHAR(255) NOT NULL,
        password              VARCHAR(255) NOT NULL,
        first_name            VARCHAR(255) NOT NULL,
        last_name             VARCHAR(255) NOT NULL,
        is_admin              CHAR(1)      NOT NULL,
        date_created          TIMESTAMP         NOT NULL,
        creator_id            INT          NOT NULL,
        modifier_id           INT          NOT NULL,
        date_modified         TIMESTAMP         NOT NULL,
		
		CONSTRAINT user_creator_fk
		FOREIGN KEY (creator_id)
		REFERENCES users(user_id),
		
		CONSTRAINT user_modifier_fk
		FOREIGN KEY (modifier_id)
		REFERENCES users(user_id)
    );
    
CREATE TABLE todolists (
        todolist_id                 INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
        creator_id            INT          NOT NULL,
        title                 VARCHAR(255) NOT NULL,
        date_created          TIMESTAMP         NOT NULL,
        date_modified         TIMESTAMP         NOT NULL,
        modifier_id           INT          NOT NULL,
		
		CONSTRAINT todolist_creator_fk
		FOREIGN KEY (creator_id)
        REFERENCES users (user_id),
		
		CONSTRAINT todolist_modifier_fk
		FOREIGN KEY (modifier_id)
		REFERENCES users(user_id)
    );
    
CREATE TABLE tasks (
        task_id                 INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
        todolist_id           INT          NOT NULL,
        title                 VARCHAR(255) NOT NULL,
        description           VARCHAR(255) NOT NULL,
        is_complete           CHAR(1)      NOT NULL,
        date_created          TIMESTAMP         NOT NULL,
        creator_id            INT          NOT NULL,
        date_modified         TIMESTAMP         NOT NULL,
        modifier_id           INT          NOT NULL,
		
        CONSTRAINT task_creator_fk
		FOREIGN KEY (creator_id)
        REFERENCES users (user_id),
		
        CONSTRAINT todolist_fk
		FOREIGN KEY (todolist_id)
        REFERENCES todolists (todolist_id) ON DELETE CASCADE,

		CONSTRAINT task_modifier_fk
		FOREIGN KEY (modifier_id)
		REFERENCES users(user_id)
    );

  
CREATE TABLE userstasks (
    task_id  INTEGER NOT NULL,
    user_id  INTEGER,
	
	CONSTRAINT assigned_task_fK
	FOREIGN KEY (task_id)
	REFERENCES tasks(task_id) ON DELETE CASCADE,
	
	CONSTRAINT assigned_users_fk
	FOREIGN KEY (user_id)
	REFERENCES users(user_id)ON DELETE CASCADE
);

CREATE TABLE userslists (
    list_id INTEGER NOT NULL,
    user_id INTEGER,
	
	CONSTRAINT shared_list_fk 
	FOREIGN KEY (list_id)
	REFERENCES todolists(todolist_id) ON DELETE CASCADE,
	
	CONSTRAINT shared_users_fk
	FOREIGN KEY (user_id)
	REFERENCES users(user_id)ON DELETE CASCADE
);