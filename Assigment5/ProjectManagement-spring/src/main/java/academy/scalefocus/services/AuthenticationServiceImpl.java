package academy.scalefocus.services;

import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private UserRepository userRepo;
    private User authenticatedUser = null;

    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User getLoggedUser() {
        return this.authenticatedUser;
    }

    @Override
    public void authenticateUser(String username, String password) {
        this.authenticatedUser = this.userRepo.getByUserNameAndPassword(username, password);
    }
}



