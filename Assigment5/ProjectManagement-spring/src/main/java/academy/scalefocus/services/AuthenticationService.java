package academy.scalefocus.services;

import academy.scalefocus.entities.User;

public interface AuthenticationService {

    User getLoggedUser();

    void authenticateUser(String username, String password);
}
