package academy.scalefocus.services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service
public class DatabaseService {
    private static final Logger logger = Logger.getLogger(DatabaseService.class);
    private static final String CONNECTION_URL = "jdbc:oracle:thin:@localhost:1521:xe";
    private static final String USER = "cveta";
    private static final String PASSWORD = "admin_password";

    private Connection connection;

    public Connection getConnection() {

        if(this.connection == null) {
            try {
                this.connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
            } catch (SQLException e) {
                logger.info("Can not connect to the database", e);
            }
        }
        return connection;
    }
}
