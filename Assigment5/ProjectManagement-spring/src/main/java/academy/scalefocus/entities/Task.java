package academy.scalefocus.entities;

import academy.scalefocus.enums.TaskStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class Task {
    private int id;
    private int projectId;
    private int assigneeId;
    private String title;
    private String description;
    private TaskStatus status;
    private LocalDateTime dateCreated;
    private int creatorId;
    private LocalDateTime dateModified;
    private int modifierId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(int assigneeId) {
        this.assigneeId = assigneeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }

    public int getModifierId() {
        return modifierId;
    }

    public void setModifierId(int modifierId) {
        this.modifierId = modifierId;
    }

    public void fromResultSet (ResultSet resultSet, Task task) throws SQLException {
        task.setId(resultSet.getInt("id"));
        task.setProjectId(resultSet.getInt("project_id"));
        task.setAssigneeId(resultSet.getInt("assignee_id"));
        task.setTitle(resultSet.getString("title"));
        task.setDescription(resultSet.getString("description"));
        task.setStatus(Enum.valueOf(TaskStatus.class, resultSet.getString("status").toUpperCase()));
        task.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        task.setCreatorId(resultSet.getInt("creator_id"));
        task.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());
        task.setModifierId(resultSet.getInt("modifier_id"));
    }

    @Override
    public String toString() {
        StringBuilder sb =  new StringBuilder();
        sb.append("ID: ").append(this.id).append(System.lineSeparator()).append("Project ID: ").append(this.projectId)
                .append(System.lineSeparator()).append("Assignee ID: ").append(this.assigneeId).append(System.lineSeparator())
                .append("Title: ").append(this.title).append(System.lineSeparator()).append("Description: ").append(this.description)
                .append(System.lineSeparator()).append("Status: ").append(this.status).append(System.lineSeparator())
                .append("Date of creation: ").append(this.dateCreated).append(System.lineSeparator()).append("Creator ID: ")
                .append(this.creatorId).append(System.lineSeparator()).append("Date of last change: ").append(this.dateModified)
                .append(System.lineSeparator()).append("Modifier ID: ").append(this.modifierId).append(System.lineSeparator())
                .append("---------------------------------");
        return sb.toString();
    }
}
