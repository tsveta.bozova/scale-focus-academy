package academy.scalefocus.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class WorkLog {
    private int id;
    private int taskId;
    private int userId;
    private int hoursSpent;
    private LocalDate date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getHoursSpent() {
        return hoursSpent;
    }

    public void setHoursSpent(int hoursSpent) {
        this.hoursSpent = hoursSpent;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void fromResultSet(ResultSet resultSet, WorkLog workLog) throws SQLException {
        workLog.setId(resultSet.getInt("id"));
        workLog.setTaskId(resultSet.getInt("task_id"));
        workLog.setUserId(resultSet.getInt("user_id"));
        workLog.setHoursSpent(resultSet.getInt("hours_spent"));
        workLog.setDate(resultSet.getDate("date_worked").toLocalDate());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(this.id).append(System.lineSeparator()).append("Task ID ").append(this.taskId)
                .append(System.lineSeparator()).append("User ID ").append(this.userId).append(System.lineSeparator())
                .append("Time: ").append(this.hoursSpent).append(System.lineSeparator()).append("Date: ")
                .append(this.date).append(System.lineSeparator()).append("---------------------------------");
        return sb.toString();
    }
}
