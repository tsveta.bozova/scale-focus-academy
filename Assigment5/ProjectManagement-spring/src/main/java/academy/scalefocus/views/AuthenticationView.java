package academy.scalefocus.views;

import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationView {
    private AuthenticationService authenticationService;
    private AdministrationView adminView;

    @Autowired
    public AuthenticationView(AuthenticationService authenticationServices, AdministrationView adminView) {
        this.authenticationService = authenticationServices;
        this.adminView = adminView;
    }

    public void run()  {

        while (this.authenticationService.getLoggedUser() == null) {

            ConsoleManager.clear();

            ConsoleManager.write("Username: ");
            String username = ConsoleManager.readLine();

            ConsoleManager.write("Password: ");
            String password = ConsoleManager.readLine();

            this.authenticationService.authenticateUser(username, password);

        }

        if (this.authenticationService.getLoggedUser() != null) {
            this.adminView.run();

            ConsoleManager.clear();
            ConsoleManager.writeLine("Good bye");
        }
    }
}
