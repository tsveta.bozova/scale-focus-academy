package academy.scalefocus.views;

import academy.scalefocus.entities.Project;
import academy.scalefocus.entities.Team;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.*;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Component
public class ProjectManagementView {
    private TeamRepository teamRepo;
    private ProjectRepository projectRepo;
    private TaskManagementView taskView;
    private AuthenticationService authenticationService;

    @Autowired
    public ProjectManagementView(TeamRepository teamRepo, ProjectRepository projectRepo,
                                 TaskManagementView taskView, AuthenticationService authenticationService) {
        this.teamRepo = teamRepo;
        this.projectRepo = projectRepo;
        this.taskView = taskView;
        this.authenticationService = authenticationService;
    }

    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            if (choice.equals(MenuChoices.LIST)) {
                list();
            } else if (choice.equals(MenuChoices.CREATE)) {
                add();
            } else if (choice.equals(MenuChoices.EDIT)) {
                edit();
            } else if (choice.equals(MenuChoices.DELETE)) {
                delete();
            } else if (choice.equals(MenuChoices.ASSIGN)) {
                assignToTeam();
            } else if (choice.equals(MenuChoices.VIEW)) {
                view();
            } else if (choice.equals(MenuChoices.BROWSE)) {
                browse();
            } else if (choice.equals(MenuChoices.EXIT)) {
                return;
            } else {
                break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printProjectMenu();

            String choice = ConsoleManager.readLine().toUpperCase();

            if ("L".equals(choice)) {
                return MenuChoices.LIST;
            } else if ("C".equals(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equalsIgnoreCase(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equalsIgnoreCase(choice)) {
                return MenuChoices.DELETE;
            } else if ("A".equals(choice)) {
                return MenuChoices.ASSIGN;
            } else if ("V".equals(choice)) {
                return MenuChoices.VIEW;
            } else if ("B".equals(choice)) {
                return MenuChoices.BROWSE;
            } else if ("X".equalsIgnoreCase(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    private void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Create Project####");

        Project project = new Project();

        ConsoleManager.write("Title: ");
        project.setTitle(ConsoleManager.readLine());
        ConsoleManager.write("Description: ");
        project.setDescription(ConsoleManager.readLine());
        project.setDateCreated(LocalDateTime.now());
        project.setCreatorId(authenticationService.getLoggedUser().getId());
        project.setDateModified(LocalDateTime.now());
        project.setModifierId(authenticationService.getLoggedUser().getId());

        this.projectRepo.add(project);

        ConsoleManager.printSuccessfulMessage();
    }

    private void list() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List Projects####");

        Map<Integer, Project> projects = this.projectRepo.getAllRelatedProjects(this.authenticationService.getLoggedUser().getId());

        if (projects.isEmpty()) {
            ConsoleManager.printMessageForNoProjects();
            return;
        }

        projects.entrySet().forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Project####");

        List<Project> projects = this.projectRepo.getAllProjects();

        if (projects.isEmpty()) {
            ConsoleManager.printMessageForNoProjects();
            return;
        }

        projects.stream()
                .filter(p -> p.getCreatorId() == this.authenticationService.getLoggedUser().getId())
                .forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        Project project = this.projectRepo.getById(projectId);

        ConsoleManager.write("Title ( " + project.getTitle() + " ): ");
        project.setTitle(ConsoleManager.readLine());
        ConsoleManager.writeLine("Description ( " + project.getDescription() + " ): ");
        project.setDescription(ConsoleManager.readLine());
        project.setDateModified(LocalDateTime.now());
        project.setModifierId(this.authenticationService.getLoggedUser().getId());

        this.projectRepo.edit(project, projectId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Project####");

        List<Project> projects = this.projectRepo.getAllProjects();

        if (projects.isEmpty()) {
            ConsoleManager.printMessageForNoProjects();
            return;
        }

        projects.stream()
                .filter(p -> p.getCreatorId() == this.authenticationService.getLoggedUser().getId())
                .forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        this.projectRepo.delete(projectId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void assignToTeam() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Assign Project####");

        List<Project> projects = this.projectRepo.getAllProjects();

        if (projects.isEmpty()) {
            ConsoleManager.printMessageForNoProjects();
            return;
        }

        projects.stream()
                .filter(p -> p.getCreatorId() == this.authenticationService.getLoggedUser().getId())
                .forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();

        List<Team> teams = this.teamRepo.getAllTeams();

        teams.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.write("Assign to: ");
        int teamToAssignId = Integer.parseInt(ConsoleManager.readLine());

        this.projectRepo.assignToTeam(teamToAssignId, projectId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void view() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####View Project####");

        Map<Integer, Project> projects = this.projectRepo.getAllRelatedProjects(this.authenticationService.getLoggedUser().getId());

        if (projects.isEmpty()) {
            ConsoleManager.printMessageForNoProjects();
            return;
        }

        projects.forEach((key, value) -> ConsoleManager.writeLine(value.getTitle() + " ( " + value.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        Project project = this.projectRepo.getById(projectId);

        ConsoleManager.clear();

        this.taskView.run(project);
    }

    private void browse() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Browse Project####");

        List<Project> projects = this.projectRepo.getAllProjects();

        if (projects.isEmpty()) {
            ConsoleManager.printMessageForNoProjects();
            return;
        }

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();

        this.taskView.list(projectId);
    }
}
