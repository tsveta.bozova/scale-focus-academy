package academy.scalefocus.views;

import academy.scalefocus.entities.User;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class UserManagementView {
    private AuthenticationService authenticationService;
    private UserRepository userRepo;

    @Autowired
    public UserManagementView(AuthenticationService authenticationService, UserRepository userRepo) {
        this.authenticationService = authenticationService;
        this.userRepo = userRepo;
    }


    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            if (choice.equals(MenuChoices.LIST)) {
                list();
            } else if (choice.equals(MenuChoices.CREATE)) {
                add();
            } else if (choice.equals(MenuChoices.EDIT)) {
                edit();
            } else if (choice.equals(MenuChoices.DELETE)) {
                delete();
            } else if (choice.equals(MenuChoices.EXIT)) {
                return;
            } else {
                break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printUserManagementMenu();

            String choice = ConsoleManager.readLine();

            if ("L".equalsIgnoreCase(choice)) {
                return MenuChoices.LIST;
            } else if ("A".equalsIgnoreCase(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equalsIgnoreCase(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equalsIgnoreCase(choice)) {
                return MenuChoices.DELETE;
            } else if ("X".equalsIgnoreCase(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    private void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add User####");

        User user = new User();

        ConsoleManager.write("Username: ");
        user.setUsername(ConsoleManager.readLine());
        ConsoleManager.write("Password: ");
        user.setPassword(ConsoleManager.readLine());
        ConsoleManager.write("First Name: ");
        user.setFirstName(ConsoleManager.readLine());
        ConsoleManager.write("Last Name: ");
        user.setLastName(ConsoleManager.readLine());
        ConsoleManager.write("Is Admin: ");
        user.setIsAdmin(Boolean.parseBoolean(ConsoleManager.readLine()));
        user.setDateCreated(LocalDateTime.now());
        user.setCreatorId(this.authenticationService.getLoggedUser().getId());
        user.setModifierId(this.authenticationService.getLoggedUser().getId());
        user.setDateModified(LocalDateTime.now());

        this.userRepo.add(user);

        ConsoleManager.printSuccessfulMessage();
    }

    private void list() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List Users####");

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit User####");

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of user: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        User user = this.userRepo.getById(userId);

        ConsoleManager.write("Username ( " + user.getUsername() + " ): ");
        user.setUsername(ConsoleManager.readLine());
        ConsoleManager.write("Password ( " + user.getPassword() + " ) : ");
        user.setPassword(ConsoleManager.readLine());
        ConsoleManager.write("First Name ( " + user.getFirstName() + " ): ");
        user.setFirstName(ConsoleManager.readLine());
        ConsoleManager.write("Last Name: ( " + user.getLastName() + " ): ");
        user.setLastName(ConsoleManager.readLine());
        ConsoleManager.write("Is Admin: ( " + user.getIsAdmin() + " ): ");
        user.setIsAdmin(Boolean.parseBoolean(ConsoleManager.readLine()));

        user.setModifierId(this.authenticationService.getLoggedUser().getId());
        user.setDateModified(LocalDateTime.now());

        this.userRepo.edit(user, userId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete User####");

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of user: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        this.userRepo.delete(userId);

        ConsoleManager.printSuccessfulMessage();
    }
}
