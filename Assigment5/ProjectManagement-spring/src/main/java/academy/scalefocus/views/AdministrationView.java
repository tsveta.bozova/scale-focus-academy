package academy.scalefocus.views;

import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdministrationView {
    private TeamManagementView teamView;
    private UserManagementView userView;
    private ProjectManagementView projectView;
    private AuthenticationService authenticationService;

    @Autowired
    public AdministrationView(TeamManagementView teamView, UserManagementView userView,
                              ProjectManagementView projectView, AuthenticationService authenticationService) {
        this.teamView = teamView;
        this.userView = userView;
        this.projectView = projectView;
        this.authenticationService = authenticationService;
    }

    public void run() {

        while (true) {
            ConsoleManager.clear();

            if (this.authenticationService.getLoggedUser().getIsAdmin()) {
                ConsoleManager.writeLine("[U]ser Management");
                ConsoleManager.writeLine("[T]eam Management");
            }
            ConsoleManager.writeLine("[P]roject Management");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");
            String choice = ConsoleManager.readLine();


            if ("U".equalsIgnoreCase(choice)) {
                if(this.authenticationService.getLoggedUser().getIsAdmin()) {
                    usersManagement();
                }
                break;
            }  else if ("T".equalsIgnoreCase(choice)) {
                if(this.authenticationService.getLoggedUser().getIsAdmin()) {
                    teamManagement();
                }
                break;
            } else if ("P".equalsIgnoreCase(choice)) {
                projectManagement();
                break;
            } else if ("X".equalsIgnoreCase(choice)) {
                return;
            } else {
                ConsoleManager.printInvalidMessage();
                break;
            }
        }
    }

    private void teamManagement() {
        this.teamView.run();
    }

    private void usersManagement() {
        this.userView.run();
    }

    private void projectManagement() {
        this.projectView.run();
    }
}
