package academy.scalefocus.views;

import academy.scalefocus.entities.Project;
import academy.scalefocus.entities.Task;
import academy.scalefocus.entities.User;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.enums.TaskStatus;
import academy.scalefocus.repositories.*;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class TaskManagementView {
    private ProjectRepository projectRepo;
    private TaskRepository taskRepo;
    private UserRepository userRepo;
    private TaskDetailView detailView;
    private AuthenticationService authenticationService;
    private Project parentProject;

    @Autowired
    public TaskManagementView(ProjectRepository projectRepo, TaskRepository taskRepo,
                              UserRepository userRepo, TaskDetailView detailView,
                              AuthenticationService authenticationService) {
        this.projectRepo = projectRepo;
        this.taskRepo = taskRepo;
        this.userRepo = userRepo;
        this.detailView = detailView;
        this.authenticationService = authenticationService;
    }

    public void run(Project project) {

        this.parentProject = project;

        while (true) {
            MenuChoices choice = renderMenu();

            if (choice.equals(MenuChoices.CREATE)) {
                if(this.parentProject.getCreatorId() == this.authenticationService.getLoggedUser().getId()) {
                    add();
                }
            } else if (choice.equals(MenuChoices.EDIT)) {
                if(this.parentProject.getCreatorId() == this.authenticationService.getLoggedUser().getId()) {
                    edit();
                }
            } else if (choice.equals(MenuChoices.DELETE)) {
                if(this.parentProject.getCreatorId() == this.authenticationService.getLoggedUser().getId()) {
                    delete();
                }
            } else if (choice.equals(MenuChoices.VIEW)) {
                view();
            } else if (choice.equals(MenuChoices.ASSIGN)) {
                assign();
            } else if (choice.equals(MenuChoices.STATUS)) {
                changeStatus();
            } else if (choice.equals(MenuChoices.EXIT)) {
                return;
            } else {
                break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.clear();

            if (this.parentProject.getCreatorId() == this.authenticationService.getLoggedUser().getId()) {
                ConsoleManager.writeLine("[C]reate Task");
                ConsoleManager.writeLine("[E]dit Task");
                ConsoleManager.writeLine("[D]elete Task");
            }
            ConsoleManager.writeLine("[Ch]ange assignee");
            ConsoleManager.writeLine("[S]tatus");
            ConsoleManager.writeLine("[V]iew task details");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");

            String choice = ConsoleManager.readLine().toUpperCase();

            if ("L".equals(choice)) {
                return MenuChoices.LIST;
            } else if ("C".equals(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equals(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equals(choice)) {
                return MenuChoices.DELETE;
            } else if ("V".equals(choice)) {
                return MenuChoices.VIEW;
            } else if ("CH".equals(choice)) {
                return MenuChoices.ASSIGN;
            } else if ("S".equals(choice)) {
                return MenuChoices.STATUS;
            } else if ("X".equals(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    private void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add Task####");

        Map<Integer, User> users = this.userRepo.getAllRelatedUsers(this.parentProject.getId());

        users.forEach((key, value) -> ConsoleManager.writeLine(value.getUsername() + " ( " + value.getId() + " )"));

        Task task = new Task();

        ConsoleManager.write("Assign to: ");
        task.setAssigneeId(Integer.parseInt(ConsoleManager.readLine()));
        task.setProjectId(this.parentProject.getId());
        ConsoleManager.write("Title: ");
        task.setTitle(ConsoleManager.readLine());
        ConsoleManager.write("Description: ");
        task.setDescription(ConsoleManager.readLine());
        task.setStatus(TaskStatus.PENDING);
        task.setDateCreated(LocalDateTime.now());
        task.setCreatorId(this.authenticationService.getLoggedUser().getId());
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.authenticationService.getLoggedUser().getId());

        this.taskRepo.add(task);

        ConsoleManager.printSuccessfulMessage();
    }

    public void list(int parentProjectId) {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List All Tasks####");

        List<Task> tasks = this.taskRepo.getAll(parentProjectId);

        tasks.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }


    private void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.write("Assignee ID ( " + task.getAssigneeId() + " ): ");
        task.setAssigneeId(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Title ( " + task.getTitle() + " ): ");
        task.setTitle(ConsoleManager.readLine());
        ConsoleManager.write("Description ( " + task.getDescription() + " ): ");
        task.setDescription(ConsoleManager.readLine());
        ConsoleManager.write("Status ( " + task.getStatus().name() + " ): ");
        task.setStatus(Enum.valueOf(TaskStatus.class, ConsoleManager.readLine().toUpperCase()));
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.authenticationService.getLoggedUser().getId());

        this.taskRepo.edit(task);

        ConsoleManager.printSuccessfulMessage();
    }

    private void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);
        this.taskRepo.delete(task, this.authenticationService.getLoggedUser().getId());

        ConsoleManager.printSuccessfulMessage();
    }

    private void view() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####View Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.clear();

        this.detailView.run(task);
    }

    private void assign() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Change Assignee####");

        ConsoleManager.writeLine();

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        List<Task> assignedTasks = tasks.stream()
                .filter(t -> t.getAssigneeId() == this.authenticationService.getLoggedUser().getId())
                .collect(Collectors.toList());

        if (isEmpty(assignedTasks)) return;

        assignedTasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.write("Task ID: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Users####");

        Map<Integer, User> users = this.userRepo.getAllRelatedUsers(this.parentProject.getId());

        users.forEach((key, value) -> ConsoleManager.writeLine(value.getUsername() + " ( " + value.getId() + " )"));

        ConsoleManager.write("Assign to: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        this.taskRepo.assign(userId, taskId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void changeStatus() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Change Status####");

        ConsoleManager.writeLine();

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        List<Task> assignedTasks = tasks.stream()
                .filter(t -> t.getAssigneeId() == this.authenticationService.getLoggedUser().getId())
                .collect(Collectors.toList());

        if (isEmpty(assignedTasks)) return;

        assignedTasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.write("Task ID: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.write(task.getTitle() + "( " + task.getStatus() + " ): ");
        String newStatus = ConsoleManager.readLine();

        this.taskRepo.changeStatus(newStatus, taskId);

        ConsoleManager.printSuccessfulMessage();
    }

    private boolean isEmpty(List<Task> assignedTasks) {
        if (assignedTasks.isEmpty()) {
            ConsoleManager.writeLine("You don't have assigned Tasks");
            ConsoleManager.writeLine("Press [Enter] to continue");
            ConsoleManager.readLine();
            return true;
        }
        return false;
    }
}
