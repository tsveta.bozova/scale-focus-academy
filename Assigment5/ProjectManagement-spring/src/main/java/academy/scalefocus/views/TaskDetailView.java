package academy.scalefocus.views;

import academy.scalefocus.entities.Task;
import academy.scalefocus.entities.WorkLog;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.*;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class TaskDetailView {
    private WorkLogRepository workLogRepo;
    private AuthenticationService authenticationService;
    private Task parentTask;

    @Autowired
    public TaskDetailView(WorkLogRepository workLogRepo, AuthenticationService authenticationService) {
        this.workLogRepo = workLogRepo;
        this.authenticationService = authenticationService;
    }

    public void run(Task task) {
        this.parentTask = task;

        while (true) {
            MenuChoices choice = renderMenu();

            if (choice.equals(MenuChoices.CREATE)) {
                add();
            } else if (choice.equals(MenuChoices.EDIT)) {
                edit();
            } else if (choice.equals(MenuChoices.DELETE)) {
                delete();
            } else if (choice.equals(MenuChoices.VIEW)) {
                view();
            }  else if (choice.equals(MenuChoices.EXIT)) {
                return;
            } else {
                break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printTaskDetailMenu();

            String choice = ConsoleManager.readLine().toUpperCase();

            if ("L".equals(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equals(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equals(choice)) {
                return MenuChoices.DELETE;
            } else if ("V".equals(choice)) {
                return MenuChoices.VIEW;
            } else if ("X".equals(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    private void add() {
        if (this.authenticationService.getLoggedUser().getId() != this.parentTask.getAssigneeId()) {
            ConsoleManager.printDeniedMessage();
            return;
        }

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Log Work####");

        WorkLog workLog = new WorkLog();

        workLog.setTaskId(this.parentTask.getId());
        workLog.setUserId(this.authenticationService.getLoggedUser().getId());
        ConsoleManager.write("Hours: ");
        workLog.setHoursSpent(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Date: ");
        workLog.setDate(LocalDate.parse(ConsoleManager.readLine()));

        this.workLogRepo.add(workLog);

        ConsoleManager.printSuccessfulMessage();
    }

    private void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Log####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogsByCreator(this.authenticationService.getLoggedUser().getId());

        workLogs.forEach(w -> ConsoleManager.writeLine("Work Log ( " + w.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Log: ");
        int logId = Integer.parseInt(ConsoleManager.readLine());

        WorkLog workLog = this.workLogRepo.getById(logId);

        ConsoleManager.write("Task ID ( " + workLog.getTaskId() + " ): ");
        workLog.setTaskId(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("User ID ( " + workLog.getUserId() + " ): ");
        workLog.setUserId(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Hours ( " + workLog.getHoursSpent() + " ): ");
        workLog.setHoursSpent(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Date ( " + workLog.getDate() + " ): ");
        workLog.setDate(LocalDate.parse(ConsoleManager.readLine()));

        this.workLogRepo.edit(workLog);

        ConsoleManager.printSuccessfulMessage();
    }

    private void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Log####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogsByCreator(this.authenticationService.getLoggedUser().getId());

        workLogs.forEach(w -> ConsoleManager.writeLine("Work Log ( " + w.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Log: ");
        int logId = Integer.parseInt(ConsoleManager.readLine());

        this.workLogRepo.delete(logId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void view() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####View All Logs####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogs(this.parentTask.getId());

        workLogs.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }
}
