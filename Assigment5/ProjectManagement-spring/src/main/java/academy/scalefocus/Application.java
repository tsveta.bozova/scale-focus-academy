package academy.scalefocus;

import academy.scalefocus.views.AuthenticationView;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"academy.scalefocus"})
public class Application {

    public static void main(String[] args) {
        ApplicationContext appContext = new AnnotationConfigApplicationContext(Application.class);

        AuthenticationView authView = appContext.getBean(AuthenticationView.class);
        authView.run();
    }
}
