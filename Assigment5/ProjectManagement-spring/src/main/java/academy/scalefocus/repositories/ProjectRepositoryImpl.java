package academy.scalefocus.repositories;

import academy.scalefocus.tools.ConsoleManager;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.entities.Project;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Repository("projectRepository")
public class ProjectRepositoryImpl implements ProjectRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(ProjectRepositoryImpl.class);

    private DatabaseService connection;

    @Autowired
    public ProjectRepositoryImpl(DatabaseService connection) {
        this.connection = connection;
    }

    @Override
    public Project getById(int projectId) {
        Project project = new Project();

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("SELECT * FROM projects WHERE" +
                " id=?")) {

            preparedStatement.setInt(1, projectId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    project.fromResultSet(resultSet, project);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return project;
    }

    @Override
    public List<Project> getAllProjects() {
        List<Project> projects = new ArrayList<>();

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("SELECT * FROM projects");
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                Project project = new Project();
                project.fromResultSet(resultSet, project);
                projects.add(project);
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return projects;
    }

    @Override
    public void add(Project project) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("INSERT INTO projects " +
                "(title, description, date_created, creator_id, date_modified, modifier_id) " +
                "VALUES (?,?,?,?,?,?)")) {

            preparedStatement.setString(1, project.getTitle());
            preparedStatement.setString(2, project.getDescription());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(project.getDateCreated()));
            preparedStatement.setInt(4, project.getCreatorId());
            preparedStatement.setTimestamp(5, Timestamp.valueOf(project.getDateModified()));
            preparedStatement.setInt(6, project.getModifierId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

    }

    @Override
    public void edit(Project project, int projectId) {

        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement("UPDATE projects SET title=?, " +
                "description=?, date_modified=?, modifier_id=? " +
                "WHERE id=?")) {

            preparedStatement.setString(1, project.getTitle());
            preparedStatement.setString(2, project.getDescription());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(project.getDateModified()));
            preparedStatement.setInt(4, project.getModifierId());
            preparedStatement.setInt(5, projectId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(int projectId) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("DELETE FROM projects WHERE id=?")) {
            preparedStatement.setInt(1, projectId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public Map<Integer, Project> getAllRelatedProjects(int userId) {
        Map<Integer, Project> projects = new TreeMap<>();

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("SELECT * " +
                "FROM projects p " +
                "INNER JOIN teams_projects tp " +
                "ON p.id = tp.project_id " +
                "INNER JOIN team_member tm " +
                "ON tp.team_id = tm.team_id " +
                "WHERE tm.user_id=?")) {
            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Project project = new Project();
                    project.fromResultSet(resultSet, project);
                    projects.putIfAbsent(project.getId(), project);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("SELECT * " +
                "FROM projects WHERE creator_id=?")) {
            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Project project = new Project();
                    project.fromResultSet(resultSet, project);
                    projects.putIfAbsent(project.getId(), project);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return projects;
    }

    @Override
    public void assignToTeam(int teamId, int projectId) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("INSERT INTO teams_projects " +
                "(team_id, project_id) VALUES (?,?)")) {

            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, projectId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }
}
