package academy.scalefocus.repositories;

import academy.scalefocus.entities.Team;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.tools.ConsoleManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository("teamRepository")
public class TeamRepositoryImpl implements TeamRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(TeamRepositoryImpl.class);
    private DatabaseService connection;

    @Autowired
    public TeamRepositoryImpl(DatabaseService connection) {
        this.connection = connection;
    }

    @Override
    public Team getById(int teamId) {
        Team team = new Team();

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("SELECT * FROM teams WHERE" +
                " id=?")) {

            preparedStatement.setInt(1, teamId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    team.fromResultSet(resultSet, team);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return team;
    }

    @Override
    public void add(Team team) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("INSERT INTO teams " +
                "(title, date_created, creator_id, date_modified, modifier_id) " +
                "VALUES (?,?,?,?,?)")) {

            preparedStatement.setString(1, team.getTitle());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(team.getDateCreated()));
            preparedStatement.setInt(3, team.getCreatorId());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(team.getDateModified()));
            preparedStatement.setInt(5, team.getModifierId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

    }

    @Override
    public void edit(Team team, int teamId) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("UPDATE teams SET title=?, " +
                     "date_modified=?, modifier_id=? " +
                     "WHERE id=?")) {

            preparedStatement.setString(1, team.getTitle());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(team.getDateModified()));
            preparedStatement.setInt(3, team.getModifierId());
            preparedStatement.setInt(4, teamId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(int teamId) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("DELETE FROM " +
                "teams WHERE id=?")) {

            preparedStatement.setInt(1, teamId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public List<Team> getAllTeams() {
        List<Team> teams = new ArrayList<>();

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("SELECT * FROM teams");
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                Team team = new Team();
                team.fromResultSet(resultSet, team);
                teams.add(team);
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return teams;
    }

    @Override
    public void assignUser(int userId, int teamId) {

        try (PreparedStatement preparedStatement = this.connection.getConnection().prepareStatement("INSERT INTO team_member " +
                     "(team_id, user_id) VALUES (?,?)")) {

            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, userId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }
}
