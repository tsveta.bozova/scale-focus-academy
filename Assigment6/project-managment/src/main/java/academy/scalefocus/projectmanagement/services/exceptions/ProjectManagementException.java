package academy.scalefocus.projectmanagement.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Has incomplete tasks")
public class ProjectManagementException extends RuntimeException {

    public ProjectManagementException(String message) {
        super(message);
    }
}
