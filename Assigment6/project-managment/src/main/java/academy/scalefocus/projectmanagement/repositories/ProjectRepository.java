package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
