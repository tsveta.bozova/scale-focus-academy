package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.services.exceptions.InvalidCredentialsException;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepo;

    @Autowired
    public UserServiceImpl(UserRepository userRepo) {
        this.userRepo = userRepo;
        if (userRepo.count() == 0) {
            User user = initUser();
            userRepo.save(user);
        }
    }

    private User initUser() {
        User user = new User();
        user.setUsername("admin");
        user.setPassword("adminpass");
        user.setFirstName("administrator");
        user.setLastName("administrator");
        user.setAdmin(true);
        return user;
    }

    @Override
    public User getByUsernameAndPassword(String username, String password) {
        return this.userRepo.findByUsernameAndPassword(username, password)
                .orElseThrow(() -> new InvalidCredentialsException("Invalid login credentials"));
    }

    @Override
    public UserResponseDTO save(UserRequestDTO userRequestDTO, User loggedUser) {
        User user = createUserFromDTO(userRequestDTO, loggedUser);
        return createUserToDisplay(userRepo.save(user));
    }

    private UserResponseDTO createUserToDisplay(User user) {
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(user.getId());
        userResponseDTO.setUsername(user.getUsername());
        userResponseDTO.setFirstName(user.getFirstName());
        userResponseDTO.setLastName(user.getLastName());
        userResponseDTO.setAdmin(user.isAdmin());
        userResponseDTO.setCreatorId(user.getCreator().getId());
        userResponseDTO.setDateCreated(user.getDateCreated());
        if(user.getDateModified() != null) {
            userResponseDTO.setModifierId(user.getModifier().getId());
            userResponseDTO.setDateModified(user.getDateModified());
        }
        return userResponseDTO;
    }

    private User createUserFromDTO(UserRequestDTO userRequestDTO, User loggedUser) {
        User user = new User();
        user.setUsername(userRequestDTO.getUsername());
        user.setPassword(userRequestDTO.getPassword());
        user.setFirstName(userRequestDTO.getFirstName());
        user.setLastName(userRequestDTO.getLastName());
        user.setAdmin(userRequestDTO.isAdmin());
        user.setCreator(loggedUser);
        return user;
    }

    @Override
    @Transactional
    public UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long userId, User loggedUser) {
        User user = this.userRepo.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in database"));

        user.setUsername(userRequestDTO.getUsername());
        user.setPassword(userRequestDTO.getPassword());
        user.setFirstName(userRequestDTO.getFirstName());
        user.setLastName(userRequestDTO.getLastName());
        user.setAdmin(userRequestDTO.isAdmin());
        user.setModifier(loggedUser);
        userRepo.save(user);
        return createUserToDisplay(user);
    }

    @Override
    public void deleteUser(Long userId) {
        User user = this.userRepo.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in database"));

        User admin = userRepo.findByUsernameAndPassword("admin", "adminpass")
                .orElseThrow(() -> new ResourceNotFoundException("User is not in database"));

        userRepo.updateCreatorModifier(user, admin);

        userRepo.deleteById(userId);
    }

    @Override
    public List<UserResponseDTO> getAll() {
        return userRepo.findAll()
                .stream()
                .filter(u -> !u.getUsername().equals("admin"))
                .map(this::createUserToDisplay)
                .collect(Collectors.toList());
    }
}
