package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.teams.TeamRequestDTO;
import academy.scalefocus.projectmanagement.dtos.teams.TeamResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;

import java.util.List;

public interface TeamService {
    List<TeamResponseDTO> getAll();
    TeamResponseDTO save(TeamRequestDTO teamRequestDTO, User loggedUser);
    TeamResponseDTO update(TeamRequestDTO teamRequestDTO, User loggedUser, Long teamId);
    void delete(Long teamId);
    TeamResponseDTO assign(Long teamId, Long userId);
}
