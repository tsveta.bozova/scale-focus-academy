package academy.scalefocus.projectmanagement.dtos;

import java.time.LocalDate;
import java.util.Objects;

public class WorkLogResponseDTO {
    private Long id;
    private Long taskId;
    private Long userId;
    private int hoursSpent;
    private LocalDate date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getHoursSpent() {
        return hoursSpent;
    }

    public void setHoursSpent(int hoursSpent) {
        this.hoursSpent = hoursSpent;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkLogResponseDTO)) return false;
        WorkLogResponseDTO that = (WorkLogResponseDTO) o;
        return hoursSpent == that.hoursSpent &&
                Objects.equals(id, that.id) &&
                Objects.equals(taskId, that.taskId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taskId, userId, hoursSpent, date);
    }
}
