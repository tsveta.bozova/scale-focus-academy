package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;

import java.util.List;

public interface TaskService {
    List<TaskResponseDTO> getAll(Long projectId, User loggedUser);

    TaskResponseDTO save(TaskRequestDTO taskRequestDTO, Long projectId, User loggedUser);

    TaskResponseDTO update(Long taskId, TaskRequestDTO taskRequestDTO, Long projectId, User loggedUser);

    void delete(Long taskId, Long projectId, User loggedUser);

    TaskResponseDTO changeAssignee(Long taskId, Long userId, User loggedUser);

    TaskResponseDTO updateStatus(Long taskId, String status, User loggedUser);
}
