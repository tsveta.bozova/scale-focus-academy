package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;

import java.util.List;

public interface UserService {
    User getByUsernameAndPassword(String username, String password);
    UserResponseDTO save(UserRequestDTO userRequestDTO, User loggedUser);
    UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long userId, User loggedUser);
    void deleteUser(Long userId);
    List<UserResponseDTO> getAll();
}
