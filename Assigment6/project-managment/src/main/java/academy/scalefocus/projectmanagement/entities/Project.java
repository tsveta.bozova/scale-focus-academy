package academy.scalefocus.projectmanagement.entities;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name = "projects")
public class Project {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    @ManyToOne
    @JoinColumn (name = "creator_id", referencedColumnName = "id")
    private User creator;
    @Column(name = "date_created")
    private Instant dateCreated;
    @ManyToOne
    @JoinColumn (name = "modifier_id", referencedColumnName = "id")
    private User modifier;
    @Column(name = "date_modified")
    private Instant dateModified;
    @ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
    @JoinTable(name = "projects_teams",
            joinColumns = @JoinColumn (name = "project_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn (name = "team_id", referencedColumnName = "id"))
    private Set<Team> teams = new HashSet<>();
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Task> tasks = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @PrePersist
    public void setDateCreated() {
        this.dateCreated = Instant.now();
    }

    public User getModifier() {
        return modifier;
    }

    public void setModifier(User modifier) {
        this.modifier = modifier;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    @PreUpdate
    public void setDateModified() {
        this.dateModified = Instant.now();
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
}
