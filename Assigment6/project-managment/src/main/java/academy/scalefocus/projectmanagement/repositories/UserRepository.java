package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>, CustomUserRepository {
    Optional<User> findByUsernameAndPassword(String username, String password);
}
