package academy.scalefocus.projectmanagement.entities;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    @ManyToOne
    @JoinColumn (name = "project_id", referencedColumnName = "id")
    private Project project;
    @ManyToOne
    @JoinColumn (name = "assignee_id", referencedColumnName = "id")
    private User assignee;
    private TaskStatus status = TaskStatus.PENDING;
    @ManyToOne
    @JoinColumn (name = "creator_id", referencedColumnName = "id")
    private User creator;
    @Column(name = "date_created")
    private Instant dateCreated;
    @ManyToOne
    @JoinColumn (name = "modifier_id", referencedColumnName = "id")
    private User modifier;
    @Column(name = "date_modified")
    private Instant dateModified;
    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WorkLog> workLogs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @PrePersist
    public void setDateCreated() {
        this.dateCreated = Instant.now();
    }

    public User getModifier() {
        return modifier;
    }

    public void setModifier(User modifier) {
        this.modifier = modifier;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    @PreUpdate
    public void setDateModified() {
        this.dateModified = Instant.now();
    }

    public Set<WorkLog> getWorkLogs() {
        return workLogs;
    }

    public void setWorkLogs(Set<WorkLog> workLogs) {
        this.workLogs = workLogs;
    }
}
