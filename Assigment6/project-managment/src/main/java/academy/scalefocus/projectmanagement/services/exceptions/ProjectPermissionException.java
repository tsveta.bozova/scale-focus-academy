package academy.scalefocus.projectmanagement.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus (value = HttpStatus.FORBIDDEN, reason = "No permission")
public class ProjectPermissionException  extends RuntimeException{

    public ProjectPermissionException(String message) {
        super(message);
    }
}
