package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class TaskController {
    private TaskService taskService;
    private AuthenticationService authenticationService;

    @Autowired
    public TaskController(TaskService taskService, AuthenticationService authenticationService) {
        this.taskService = taskService;
        this.authenticationService = authenticationService;
    }

    @GetMapping("/projects/{projectId}/tasks")
    public List<TaskResponseDTO> getAll (@PathVariable Long projectId,
                                         @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser = authenticationService.login(authorizationHeader);
        return taskService.getAll(projectId, loggedUser);
    }

    @PostMapping("/projects/{projectId}/tasks")
    public TaskResponseDTO create(@PathVariable Long projectId,
                                  @RequestHeader ("Authorization") String authorizationHeader,
                                  @RequestBody TaskRequestDTO taskRequestDTO) {
        User loggedUser = authenticationService.login(authorizationHeader);
        return taskService.save(taskRequestDTO, projectId, loggedUser);
    }

    @PutMapping("/projects/{projectId}/tasks/{taskId}")
    public TaskResponseDTO update(@PathVariable Long projectId,
                                  @PathVariable Long taskId,
                                  @RequestHeader ("Authorization") String authorizationHeader,
                                  @RequestBody TaskRequestDTO taskRequestDTO) {
        User loggedUser = authenticationService.login(authorizationHeader);
        return taskService.update(taskId, taskRequestDTO, projectId, loggedUser);
    }

    @DeleteMapping("/projects/{projectId}/tasks/{taskId}")
    public void delete (@PathVariable Long projectId,
                        @PathVariable Long taskId,
                        @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser = authenticationService.login(authorizationHeader);
        taskService.delete(taskId, projectId, loggedUser);
    }

    @PutMapping("/tasks/{taskId}/users/{userId}")
    public TaskResponseDTO changeAssignee (@PathVariable Long taskId,
                                           @PathVariable Long userId,
                                           @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser = authenticationService.login(authorizationHeader);
        return taskService.changeAssignee(taskId, userId, loggedUser);
    }

    @PutMapping("/tasks/{taskId}/status")
    public TaskResponseDTO updateStatus (@PathVariable Long taskId,
                                         @RequestBody String status,
                                         @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser = authenticationService.login(authorizationHeader);
        return taskService.updateStatus(taskId, status, loggedUser);
    }
}
