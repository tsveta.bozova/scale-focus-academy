package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
