package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.User;

public interface CustomUserRepository {
    void updateCreatorModifier(User user, User admin);
}
