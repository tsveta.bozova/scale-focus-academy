package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.projects.ProjectRequestDTO;
import academy.scalefocus.projectmanagement.dtos.projects.ProjectResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProjectController {
    private ProjectService projectService;
    private AuthenticationService authenticationService;

    @Autowired
    public ProjectController(ProjectService projectService, AuthenticationService authenticationService) {
        this.projectService = projectService;
        this.authenticationService = authenticationService;
    }

    @GetMapping ("/projects")
    public List<ProjectResponseDTO> getAll (@RequestHeader("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        return projectService.getAll(loggedUser);
    }

    @PostMapping("/projects")
    public ProjectResponseDTO create (@RequestHeader("Authorization") String authorizationHeader,
                                      @RequestBody ProjectRequestDTO projectRequestDTO) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        return projectService.save(projectRequestDTO, loggedUser);
    }

    @PutMapping("/projects/{projectId}/teams/{teamId}")
    public ProjectResponseDTO assignTeamToProject (@PathVariable Long projectId, @PathVariable Long teamId,
                                                   @RequestHeader("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        return projectService.assignTeamToProject(projectId, teamId, loggedUser);
    }

    @PutMapping("/projects/{projectId}")
    public ProjectResponseDTO update(@PathVariable Long projectId,
                                     @RequestHeader("Authorization") String authorizationHeader,
                                     @RequestBody ProjectRequestDTO projectRequestDTO) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        return projectService.update(projectId, loggedUser, projectRequestDTO);
    }

    @DeleteMapping("/projects/{projectId}")
    public void delete(@PathVariable Long projectId,
                       @RequestHeader("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        projectService.delete(projectId, loggedUser);
    }
}
