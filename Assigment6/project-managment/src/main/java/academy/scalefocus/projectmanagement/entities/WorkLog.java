package academy.scalefocus.projectmanagement.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "worklogs")
public class WorkLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn (name = "task_id", referencedColumnName = "id")
    private Task task;
    @ManyToOne
    @JoinColumn (name = "user_id", referencedColumnName = "id")
    private User user;
    @Column(name = "hours_spent")
    private int hoursSpent;
    @Column(name = "date_worked")
    private LocalDate date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getHoursSpent() {
        return hoursSpent;
    }

    public void setHoursSpent(int hoursSpent) {
        this.hoursSpent = hoursSpent;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
