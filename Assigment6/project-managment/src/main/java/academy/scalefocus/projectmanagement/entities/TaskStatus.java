package academy.scalefocus.projectmanagement.entities;

public enum TaskStatus {
    PENDING, IN_PROGRESS, COMPLETED
}
