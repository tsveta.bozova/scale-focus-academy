package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.teams.TeamRequestDTO;
import academy.scalefocus.projectmanagement.dtos.teams.TeamResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamController {
    private TeamService teamService;
    private AuthenticationService authenticationService;

    @Autowired
    public TeamController(TeamService teamService, AuthenticationService authenticationService) {
        this.teamService = teamService;
        this.authenticationService = authenticationService;
    }

    @GetMapping ("/teams")
    public List<TeamResponseDTO> getAll (@RequestHeader("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        return teamService.getAll();
    }

    @PostMapping("/teams")
    public TeamResponseDTO create (@RequestHeader("Authorization") String authorizationHeader,
                        @RequestBody TeamRequestDTO teamRequestDTO) {
        User loggedUser = authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        return teamService.save(teamRequestDTO, loggedUser);
    }

    @PutMapping("/teams/{teamId}")
    public TeamResponseDTO update (@PathVariable Long teamId,
                                   @RequestHeader ("Authorization") String authorizationHeader,
                                   @RequestBody TeamRequestDTO teamRequestDTO) {
        User loggedUser = authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        return teamService.update(teamRequestDTO, loggedUser, teamId);
    }

    @DeleteMapping ("/teams/{teamId}")
    public void delete (@PathVariable Long teamId,
                        @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        teamService.delete(teamId);
    }

    @PutMapping ("/teams/{teamId}/users/{userId}")
    public TeamResponseDTO assignUserToTeam (@PathVariable Long teamId, @PathVariable Long userId,
                                             @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        return teamService.assign(teamId, userId);
    }
}
