package academy.scalefocus.projectmanagement.dtos.projects;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

public class ProjectResponseDTO {
    private Long id;
    private String title;
    private String description;
    private Long creatorId;
    private Instant dateCreated;
    private Long modifierId;
    private Instant dateModified;
    private List<Long> teamsIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    public void setDateModified(Instant dateModified) {
        this.dateModified = dateModified;
    }

    public List<Long> getTeamsIds() {
        return teamsIds;
    }

    public void setTeamsIds(List<Long> teamsIds) {
        this.teamsIds = teamsIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjectResponseDTO)) return false;
        ProjectResponseDTO that = (ProjectResponseDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(modifierId, that.modifierId) &&
                Objects.equals(dateModified, that.dateModified) &&
                Objects.equals(teamsIds, that.teamsIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, creatorId, dateCreated, modifierId, dateModified, teamsIds);
    }
}
