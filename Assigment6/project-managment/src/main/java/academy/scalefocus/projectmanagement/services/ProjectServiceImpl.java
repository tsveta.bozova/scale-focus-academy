package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.projects.ProjectRequestDTO;
import academy.scalefocus.projectmanagement.dtos.projects.ProjectResponseDTO;
import academy.scalefocus.projectmanagement.entities.Project;
import academy.scalefocus.projectmanagement.entities.TaskStatus;
import academy.scalefocus.projectmanagement.entities.Team;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.ProjectRepository;
import academy.scalefocus.projectmanagement.repositories.TeamRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectManagementException;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectPermissionException;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository projectRepo;
    private TeamRepository teamRepo;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepo, TeamRepository teamRepo) {
        this.projectRepo = projectRepo;
        this.teamRepo = teamRepo;
    }

    @Override
    public List<ProjectResponseDTO> getAll(User loggedUser) {
        return projectRepo.findAll()
                .stream()
                .filter(project -> isProjectCreatedByUser(loggedUser, project) || isUserInTeamAssignedToProject(loggedUser, project))
                .map(this::createResponse)
                .collect(Collectors.toList());
    }

    private boolean isUserInTeamAssignedToProject(User loggedUser, Project project) {
        return project.getTeams().stream()
                .anyMatch(team -> isUserInTeam(loggedUser, team));
    }

    private boolean isUserInTeam(User loggedUser, Team team) {
        return team.getMembers().stream()
                .anyMatch(member -> member.getId().equals(loggedUser.getId()));

    }

    @Override
    public ProjectResponseDTO save(ProjectRequestDTO projectRequestDTO, User loggedUser) {
        Project project = createProjectFromDTO(projectRequestDTO, loggedUser);
        return createResponse(projectRepo.save(project));
    }

    @Override
    @Transactional
    public ProjectResponseDTO assignTeamToProject(Long projectId, Long teamId, User loggedUser) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(()-> new ResourceNotFoundException("The project is not in the database"));

        Team team = teamRepo.findById(teamId)
                .orElseThrow(()-> new ResourceNotFoundException("The team is not in the database"));

        if (!isProjectCreatedByUser(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        project.getTeams().add(team);
        teamRepo.save(team);

        return createResponse(projectRepo.save(project));
    }

    @Override
    @Transactional
    public ProjectResponseDTO update(Long projectId, User loggedUser, ProjectRequestDTO projectRequestDTO) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(()-> new ResourceNotFoundException("The project is not in the database"));

        if (!isProjectCreatedByUser(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        project.setTitle(projectRequestDTO.getTitle());
        project.setDescription(projectRequestDTO.getDescription());
        project.setModifier(loggedUser);

        return createResponse(projectRepo.save(project));
    }

    @Override
    @Transactional
    public void delete(Long projectId, User loggedUser) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(()-> new ResourceNotFoundException("The project is not in the database"));

        if(projectHasIncompleteTasks(project)) {
            throw new ProjectManagementException("Please, complete all tasks");
        }

        if (!isProjectCreatedByUser(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        projectRepo.deleteById(projectId);
    }

    private boolean projectHasIncompleteTasks(Project project) {
        return project.getTasks()
                .stream()
                .anyMatch(task -> !task.getStatus().equals(TaskStatus.COMPLETED));
    }

    private Project createProjectFromDTO(ProjectRequestDTO projectRequestDTO, User loggedUser) {
        Project project = new Project();
        project.setTitle(projectRequestDTO.getTitle());
        project.setDescription(projectRequestDTO.getDescription());
        project.setCreator(loggedUser);
        return project;
    }

    private ProjectResponseDTO createResponse(Project project) {
        ProjectResponseDTO projectResponseDTO = new ProjectResponseDTO();
        projectResponseDTO.setId(project.getId());
        projectResponseDTO.setTitle(project.getTitle());
        projectResponseDTO.setDescription(project.getDescription());
        projectResponseDTO.setCreatorId(project.getCreator().getId());
        projectResponseDTO.setDateCreated(project.getDateCreated());
        if (project.getModifier() != null) {
            projectResponseDTO.setModifierId(project.getModifier().getId());
            projectResponseDTO.setDateModified(project.getDateModified());
        }
        projectResponseDTO.setTeamsIds(project.getTeams()
                .stream()
                .map(Team::getId)
                .collect(Collectors.toList()));
        return projectResponseDTO;
    }

    private boolean isProjectCreatedByUser(User loggedUser, Project project) {
        return project.getCreator().getId().equals(loggedUser.getId());
    }
}
