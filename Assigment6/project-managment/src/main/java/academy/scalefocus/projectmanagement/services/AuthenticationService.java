package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.entities.User;

public interface AuthenticationService {
    User login(String authorizationHeader);

    void checkIfUserIsAdmin(User loggedUser);
}
