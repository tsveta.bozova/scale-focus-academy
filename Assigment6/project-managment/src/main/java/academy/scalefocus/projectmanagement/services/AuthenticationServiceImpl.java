package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.services.exceptions.InvalidAuthorizationHeaderException;
import academy.scalefocus.projectmanagement.services.exceptions.InvalidCredentialsException;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private UserRepository userRepository;

    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public User login(String authorizationHeader) {
        String[] authorizationParts = extractAuthorizationParts(authorizationHeader);
        String username = authorizationParts[0];
        String password = authorizationParts[1];
        return  userRepository.findByUsernameAndPassword(username, password)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in database"));
    }

    private String[] extractAuthorizationParts(String authorizationHeader) {
        String[] authorizationParts = authorizationHeader.split(",");
        if (authorizationParts.length != 2) {
            throw new InvalidAuthorizationHeaderException("Invalid Authorization header");
        }
        return authorizationParts;
    }

    @Override
    public void checkIfUserIsAdmin(User loggedUser) {
        if (!loggedUser.isAdmin()) {
            throw new InvalidCredentialsException("User doesn't have access");
        }
    }
}
