package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private UserService userService;
    private AuthenticationService authenticationService;

    @Autowired
    public UserController(UserService userService,
                          AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @PostMapping("/users")
    public UserResponseDTO createUser(@RequestHeader("Authorization") String authorizationHeader,
                           @RequestBody UserRequestDTO userRequestDTO) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(authenticationService.login(authorizationHeader));
        return userService.save(userRequestDTO, loggedUser);
    }

    @GetMapping("/users")
    public List<UserResponseDTO> getAll(@RequestHeader("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        return userService.getAll();
    }

    @PutMapping("/users/{userId}")
    public UserResponseDTO updateUser(@PathVariable Long userId,
                                      @RequestHeader("Authorization") String authorizationHeader,
                                      @RequestBody UserRequestDTO userRequestDTO) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        return userService.updateUser(userRequestDTO, userId, loggedUser);
    }

    @DeleteMapping("/users/{userId}")
    public void deleteUser(@PathVariable Long userId,
                           @RequestHeader ("Authorization") String authorizationHeader) {
        User loggedUser =  authenticationService.login(authorizationHeader);
        authenticationService.checkIfUserIsAdmin(loggedUser);
        userService.deleteUser(userId);
    }
}
