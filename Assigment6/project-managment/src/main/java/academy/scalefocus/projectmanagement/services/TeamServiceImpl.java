package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.teams.TeamRequestDTO;
import academy.scalefocus.projectmanagement.dtos.teams.TeamResponseDTO;
import academy.scalefocus.projectmanagement.entities.Team;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.TeamRepository;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {
    private TeamRepository teamRepo;
    private UserRepository userRepo;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepo, UserRepository userRepo) {
        this.teamRepo = teamRepo;
        this.userRepo = userRepo;
    }

    @Override
    public List<TeamResponseDTO> getAll() {
        return teamRepo.findAll()
                .stream()
                .map(this::createResponse)
                .collect(Collectors.toList());
    }

    private TeamResponseDTO createResponse(Team team) {
        TeamResponseDTO teamResponseDTO = new TeamResponseDTO();
        teamResponseDTO.setId(team.getId());
        teamResponseDTO.setTitle(team.getTitle());
        teamResponseDTO.setCreatorId(team.getCreator().getId());
        teamResponseDTO.setDateCreated(team.getDateCreated());
        if(team.getDateModified() != null) {
            teamResponseDTO.setModifierId(team.getModifier().getId());
            teamResponseDTO.setDateModified(team.getDateModified());
        }
        teamResponseDTO.setMembersIds(team.getMembers()
                .stream()
                .map(User::getId)
                .collect(Collectors.toList()));
        return teamResponseDTO;
    }


    @Override
    public TeamResponseDTO save(TeamRequestDTO teamRequestDTO, User loggedUser) {
        Team team = createTeamFromDTO(teamRequestDTO, loggedUser);
        return createResponse(teamRepo.save(team));
    }

    private Team createTeamFromDTO(TeamRequestDTO teamRequestDTO, User loggedUser) {
        Team team = new Team();
        team.setTitle(teamRequestDTO.getTitle());
        team.setCreator(loggedUser);
        return team;
    }


    @Override
    @Transactional
    public TeamResponseDTO update(TeamRequestDTO teamRequestDTO, User loggedUser, Long teamId) {
        Team team = teamRepo.findById(teamId)
                .orElseThrow(() -> new ResourceNotFoundException("Team is not in the database"));

        team.setTitle(teamRequestDTO.getTitle());
        team.setModifier(loggedUser);

        return createResponse(teamRepo.save(team));
    }

    @Override
    public void delete(Long teamId) {
        teamRepo.deleteById(teamId);
    }

    @Override
    @Transactional
    public TeamResponseDTO assign(Long teamId, Long userId) {
        Team team = teamRepo.findById(teamId)
                .orElseThrow(() -> new ResourceNotFoundException("Team is not in the database"));

        User user = userRepo.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in the database"));

        team.getMembers().add(user);
        userRepo.save(user);

        return createResponse(teamRepo.save(team));
    }
}
