package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.projects.ProjectRequestDTO;
import academy.scalefocus.projectmanagement.dtos.projects.ProjectResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;

import java.util.List;

public interface ProjectService {

    List<ProjectResponseDTO> getAll(User loggedUser);

    ProjectResponseDTO save(ProjectRequestDTO projectRequestDTO, User loggedUser);

    ProjectResponseDTO assignTeamToProject(Long projectId, Long teamId, User loggedUser);

    ProjectResponseDTO update(Long projectId, User loggedUser, ProjectRequestDTO projectRequestDTO);

    void delete(Long projectId, User loggedUser);
}
