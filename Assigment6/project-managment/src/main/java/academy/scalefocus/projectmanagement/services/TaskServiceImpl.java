package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.entities.*;
import academy.scalefocus.projectmanagement.repositories.ProjectRepository;
import academy.scalefocus.projectmanagement.repositories.TaskRepository;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectPermissionException;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {
    private TaskRepository taskRepo;
    private ProjectRepository projectRepo;
    private UserRepository userRepo;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepo, ProjectRepository projectRepo, UserRepository userRepo) {
        this.taskRepo = taskRepo;
        this.projectRepo = projectRepo;
        this.userRepo = userRepo;
    }

    @Override
    public List<TaskResponseDTO> getAll(Long projectId, User loggedUser) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project is not in the database"));

        if (!isUserCreatorOfProject(loggedUser, project) &&
                !isUserInTeam(loggedUser, project.getTeams())) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        return project.getTasks()
                .stream()
                .map(this::createResponse)
                .collect(Collectors.toList());
    }

    @Override
    public TaskResponseDTO save(TaskRequestDTO taskRequestDTO, Long projectId, User loggedUser) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project is not in the database"));

        if (!isUserCreatorOfProject(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        Task task = createTaskFromDTO(taskRequestDTO, loggedUser, project);

        return createResponse(taskRepo.save(task));
    }

    @Override
    @Transactional
    public TaskResponseDTO update(Long taskId, TaskRequestDTO taskRequestDTO, Long projectId, User loggedUser) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project is not in the database"));

        if (!isUserCreatorOfProject(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        User assignee = userRepo.findById(taskRequestDTO.getAssigneeId())
                .orElseThrow(() -> new ResourceNotFoundException("User is not in the database"));

        task.setTitle(taskRequestDTO.getTitle());
        task.setDescription(taskRequestDTO.getDescription());
        task.setAssignee(assignee);
        task.setStatus(taskRequestDTO.getStatus());
        task.setModifier(loggedUser);
        return createResponse(taskRepo.save(task));
    }

    @Override
    @Transactional
    public void delete(Long taskId, Long projectId, User loggedUser) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project is not in the database"));

        if (!isUserCreatorOfProject(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        taskRepo.deleteById(taskId);
    }

    @Override
    public TaskResponseDTO changeAssignee(Long taskId, Long userId, User loggedUser) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        Project project = task.getProject();

        if (!isUserCreatorOfProject(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        User user = userRepo.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in the database"));

        if (!isUserInTeam(user, project.getTeams())) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        task.setAssignee(user);
        task.setModifier(loggedUser);

        return createResponse(taskRepo.save(task));
    }

    @Override
    public TaskResponseDTO updateStatus(Long taskId, String status, User loggedUser) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        Project project = task.getProject();

        if (!isUserCreatorOfProject(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        task.setStatus(TaskStatus.valueOf(status.toUpperCase()));
        task.setModifier(loggedUser);

        return createResponse(taskRepo.save(task));
    }

    private Task createTaskFromDTO(TaskRequestDTO taskRequestDTO, User loggedUser, Project project) {
        Task task = new Task();
        User assignee = userRepo.findById(taskRequestDTO.getAssigneeId())
                .orElseThrow(() -> new ResourceNotFoundException("User is not in the database"));
        task.setTitle(taskRequestDTO.getTitle());
        task.setDescription(taskRequestDTO.getDescription());
        task.setProject(project);
        task.setAssignee(assignee);
        task.setCreator(loggedUser);
        return task;
    }

    private boolean isUserInTeam(User loggedUser, Set<Team> teams) {
        return teams.stream()
                .anyMatch(team -> team.getMembers()
                        .stream()
                        .anyMatch(member -> member.getId().equals(loggedUser.getId())));
    }

    private TaskResponseDTO createResponse(Task task) {
        TaskResponseDTO taskResponseDTO = new TaskResponseDTO();
        taskResponseDTO.setId(task.getId());
        taskResponseDTO.setTitle(task.getTitle());
        taskResponseDTO.setDescription(task.getDescription());
        taskResponseDTO.setProjectId(task.getProject().getId());
        taskResponseDTO.setCreatorId(task.getProject().getId());
        taskResponseDTO.setAssigneeId(task.getAssignee().getId());
        taskResponseDTO.setStatus(task.getStatus());
        taskResponseDTO.setCreatorId(task.getCreator().getId());
        taskResponseDTO.setDateCreated(task.getDateCreated());
        if (task.getModifier() != null) {
            taskResponseDTO.setModifierId(task.getModifier().getId());
            taskResponseDTO.setDateModified(task.getDateModified());
        }
        return taskResponseDTO;
    }

    private boolean isUserCreatorOfProject(User loggedUser, Project project) {
        return project.getCreator().getId().equals(loggedUser.getId());
    }
}
