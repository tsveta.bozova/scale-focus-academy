package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.WorkLogRequestDTO;
import academy.scalefocus.projectmanagement.dtos.WorkLogResponseDTO;
import academy.scalefocus.projectmanagement.entities.*;
import academy.scalefocus.projectmanagement.repositories.TaskRepository;
import academy.scalefocus.projectmanagement.repositories.WorkLogRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectPermissionException;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkLogServiceImpl implements WorkLogService {
    private WorkLogRepository workLogRepo;
    private TaskRepository taskRepo;

    @Autowired
    public WorkLogServiceImpl(WorkLogRepository workLogRepo, TaskRepository taskRepo) {
        this.workLogRepo = workLogRepo;
        this.taskRepo = taskRepo;
    }

    @Override
    public List<WorkLogResponseDTO> getAll(Long taskId, User loggedUser) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        Project project = task.getProject();
        if (!isProjectCreatedByUser(loggedUser, project)
                && !isUserInTeamAssignedToProject(loggedUser, project)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        return task.getWorkLogs()
                .stream()
                .map(this::createResponse)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public WorkLogResponseDTO save(WorkLogRequestDTO workLogRequestDTO, Long taskId, User loggedUser) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        Project project = task.getProject();
        if (!checkForPermissionForProject(loggedUser, project, task)) {
            throw new ProjectPermissionException("User doesn't have access");
        }
        WorkLog workLog = createWorkLogFromDTO(workLogRequestDTO, task, loggedUser);

        return createResponse(workLogRepo.save(workLog));
    }

    @Override
    @Transactional
    public WorkLogResponseDTO update(WorkLogRequestDTO workLogRequestDTO, Long workLogId, Long taskId, User loggedUser) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        Project project = task.getProject();
        if (!checkForPermissionForProject(loggedUser, project, task)) {
            throw new ProjectPermissionException("User doesn't have access");
        }
        WorkLog workLog = workLogRepo.findById(workLogId)
                .orElseThrow(() -> new ResourceNotFoundException("Work Log is not in the database"));
        workLog.setHoursSpent(workLogRequestDTO.getHoursSpent());
        workLog.setDate(workLogRequestDTO.getDate());

        return createResponse(workLogRepo.save(workLog));
    }

    @Override
    @Transactional
    public void delete(Long taskId, Long workLogId, User loggedUser) {
        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Task is not in the database"));

        Project project = task.getProject();
        if (!checkForPermissionForProject(loggedUser, project, task)) {
            throw new ProjectPermissionException("User doesn't have access");
        }

        workLogRepo.deleteById(workLogId);
    }

    private WorkLog createWorkLogFromDTO(WorkLogRequestDTO workLogRequestDTO, Task task, User loggedUser) {
        WorkLog workLog = new WorkLog();
        workLog.setTask(task);
        workLog.setUser(loggedUser);
        workLog.setHoursSpent(workLogRequestDTO.getHoursSpent());
        workLog.setDate(workLogRequestDTO.getDate());
        return workLog;
    }

    private WorkLogResponseDTO createResponse(WorkLog workLog) {
        WorkLogResponseDTO workLogResponseDTO = new WorkLogResponseDTO();
        workLogResponseDTO.setId(workLog.getId());
        workLogResponseDTO.setTaskId(workLog.getTask().getId());
        workLogResponseDTO.setUserId(workLog.getUser().getId());
        workLogResponseDTO.setHoursSpent(workLog.getHoursSpent());
        workLogResponseDTO.setDate(workLog.getDate());
        return workLogResponseDTO;
    }

    private boolean isProjectCreatedByUser(User loggedUser, Project project) {
        return project.getCreator().getId().equals(loggedUser.getId());
    }

    private boolean isUserInTeamAssignedToProject(User loggedUser, Project project) {
        return project.getTeams().stream()
                .anyMatch(team -> isUserInTeam(loggedUser, team));
    }

    private boolean isUserInTeam(User loggedUser, Team team) {
        return team.getMembers().stream()
                .anyMatch(member -> member.getId().equals(loggedUser.getId()));
    }

    private boolean checkForPermissionForProject(User loggedUser, Project project, Task task) {
        return isProjectCreatedByUser(loggedUser, project)
                || isUserInTeamAssignedToProject(loggedUser, project)
                || task.getAssignee().getId().equals(loggedUser.getId());
    }
}
