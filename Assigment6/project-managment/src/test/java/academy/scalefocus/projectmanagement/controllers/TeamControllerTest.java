package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.teams.TeamRequestDTO;
import academy.scalefocus.projectmanagement.dtos.teams.TeamResponseDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.Team;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.TeamService;
import academy.scalefocus.projectmanagement.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TeamController.class)
class TeamControllerTest {
    private static final Long TEAM_ID = 1L;
    private static final String GET_ALL_JSON = "[{\"id\":1,\"title\":\"team\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"membersIds\":[]}]";
    private static final String CREATE_TEAM_JSON = "{\"id\":1,\"title\":\"team\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"membersIds\":[]}";
    private static final String UPDATED_TEAM_JSON = "{\"id\":1,\"title\":\"test team\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"membersIds\":[]}";
    private static final String ASSIGN_TEAM_JSON = "{\"id\":1,\"title\":\"team\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"membersIds\":[1]}";
    private static final String TITLE = "team";
    private static final Instant INSTANT_DATE = Instant.parse("2020-04-02T20:20:20.00Z");
    private static final String AUTHORIZATION_HEADER = "admin,adminpass";
    private static final String REQUEST_BODY = "{\"title\": \"team\"}";
    private static final String UPDATED_REQUEST_BODY = "{\"title\": \"test team\"}";
    private static final String AUTHORIZATION = "Authorization";
    private static final String POST_GET_URL = "/teams";
    private static final Long ADMIN_ID = 100L;
    private static final String UPDATED_TITLE = "test team";
    private static final String UPDATE_DELETE_URL = "/teams/{teamId}";
    private static final long MEMBER_ID = 1L;
    private static final String ASSIGN_URL = "/teams/{teamId}/users/{userId}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    @MockBean
    private AuthenticationService authenticationService;

    private TeamResponseDTO teamResponseDTO;
    private User loggedUser;

    @BeforeEach
    private void setUp() {
        loggedUser = new User();
        loggedUser.setId(ADMIN_ID);
        loggedUser.setUsername("admin");
        loggedUser.setPassword("adminpass");
        loggedUser.setFirstName("administrator");
        loggedUser.setLastName("administrator");
        loggedUser.setAdmin(true);

        when(authenticationService.login(AUTHORIZATION_HEADER))
                .thenReturn(loggedUser);
        doNothing().when(authenticationService)
                .checkIfUserIsAdmin(loggedUser);

        teamResponseDTO = new TeamResponseDTO();
        teamResponseDTO.setId(TEAM_ID);
        teamResponseDTO.setTitle(TITLE);
        teamResponseDTO.setCreatorId(loggedUser.getId());
        teamResponseDTO.setDateCreated(INSTANT_DATE);
        teamResponseDTO.setModifierId(loggedUser.getId());
        teamResponseDTO.setDateModified(INSTANT_DATE);
        teamResponseDTO.setMembersIds(new ArrayList<>());
    }

    @Test
    void getAll() throws Exception {
        List<TeamResponseDTO> teams = new ArrayList<>();

        teams.add(teamResponseDTO);

        when(teamService.getAll())
                .thenReturn(teams);


        mockMvc.perform(get(POST_GET_URL)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_ALL_JSON));

        verify(teamService)
                .getAll();
    }

    @Test
    void create() throws Exception {
        TeamRequestDTO teamRequestDTO= new TeamRequestDTO();
        teamRequestDTO.setTitle(TITLE);

        when(teamService.save(teamRequestDTO, loggedUser))
                .thenReturn(teamResponseDTO);

        mockMvc.perform(post(POST_GET_URL)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CREATE_TEAM_JSON));

        verify(teamService)
                .save(teamRequestDTO, loggedUser);
    }

    @Test
    void delete() throws Exception {
        doNothing().when(teamService)
                .delete(TEAM_ID);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(UPDATE_DELETE_URL, TEAM_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk());

        verify(teamService)
                .delete(TEAM_ID);
    }

    @Test
    void update() throws Exception {
        TeamRequestDTO teamRequestDTO = new TeamRequestDTO();
        teamRequestDTO.setTitle(UPDATED_TITLE);

        teamResponseDTO.setTitle(UPDATED_TITLE);

        when(teamService.update(teamRequestDTO, loggedUser, TEAM_ID))
                .thenReturn(teamResponseDTO);

        mockMvc.perform(put(UPDATE_DELETE_URL, TEAM_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_TEAM_JSON));

        verify(teamService)
                .update(teamRequestDTO, loggedUser, TEAM_ID);
    }

    @Test
    void assignUserToTeam() throws Exception {
        List<Long> membersIds = new ArrayList<>();
        membersIds.add(MEMBER_ID);
        teamResponseDTO.setMembersIds(membersIds);

        when(teamService.assign(TEAM_ID, MEMBER_ID))
                .thenReturn(teamResponseDTO);

        mockMvc.perform(put(ASSIGN_URL, TEAM_ID, MEMBER_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(ASSIGN_TEAM_JSON));

        verify(teamService)
                .assign(TEAM_ID, MEMBER_ID);
    }
}