package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.entities.TaskStatus;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TaskController.class)
class TaskControllerTest {
    private static final String AUTHORIZATION_HEADER = "admin,adminpass";
    private static final long TASK_ID = 1L;
    private static final String DESCRIPTION = "description";
    private static final String TITLE = "task";
    private static final long PROJECT_ID = 1L;
    private static final long ASSIGNEE_ID = 1L;
    private static final Instant INSTANT_DATE = Instant.parse("2020-04-02T20:20:20.00Z");
    private static final String POST_GET_URL = "/projects/{projectId}/tasks";
    private static final String AUTHORIZATION = "Authorization";
    private static final String GET_ALL_JSON = "[{\"id\":1,\"title\":\"task\",\"description\":\"description\",\"projectId\":1,\"assigneeId\":1,\"status\":\"PENDING\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\"}]";
    private static final String REQUEST_BODY = "{\"title\" : \"task\",\"description\" : \"description\",\"assigneeId\" : 1 }";
    private static final String CREATE_TASK_JSON = "{\"id\":1,\"title\":\"task\",\"description\":\"description\",\"projectId\":1,\"assigneeId\":1,\"status\":\"PENDING\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\"}";
    private static final String UPDATED_TITLE = "test task";
    private static final String UPDATED_DESCRIPTION = "test description";
    private static final String UPDATE_DELETE_URL = "/projects/{projectId}/tasks/{taskId}";
    private static final String UPDATED_REQUEST_BODY = "{\"title\" : \"test task\",\"description\" : \"test description\",\"assigneeId\" : 1 }";
    private static final String UPDATED_TASK_JSON = "{\"id\":1,\"title\":\"test task\",\"description\":\"test description\",\"projectId\":1,\"assigneeId\":1,\"status\":\"PENDING\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\"}";
    public static final String UPDATED_STATUS = "IN_PROGRESS";
    public static final String UPDATE_STATUS_URL = "/tasks/{taskId}/status";
    public static final String UPDATED_STATUS_RESPONSE_JSON = "{\"id\":1,\"title\":\"task\",\"description\":\"description\",\"projectId\":1,\"assigneeId\":1,\"status\":\"IN_PROGRESS\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\"}";
    public static final long UPDATED_ASSIGNEE_ID = 2L;
    public static final String UPDATED_ASSIGNEE_JSON = "{\"id\":1,\"title\":\"task\",\"description\":\"description\",\"projectId\":1,\"assigneeId\":2,\"status\":\"PENDING\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\"}";
    public static final String TASK_CHANGE_ASSIGNEE_URL = "/tasks/{taskId}/users/{userId}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    @MockBean
    private AuthenticationService authenticationService;

    private TaskResponseDTO taskResponseDTO;
    private User loggedUser;

    @BeforeEach
    private void setUp() {
        loggedUser = new User();
        loggedUser.setId(100L);
        loggedUser.setUsername("admin");
        loggedUser.setPassword("adminpass");
        loggedUser.setFirstName("administrator");
        loggedUser.setLastName("administrator");
        loggedUser.setAdmin(true);

        when(authenticationService.login(AUTHORIZATION_HEADER))
                .thenReturn(loggedUser);

        taskResponseDTO = new TaskResponseDTO();
        taskResponseDTO.setId(TASK_ID);
        taskResponseDTO.setTitle(TITLE);
        taskResponseDTO.setDescription(DESCRIPTION);
        taskResponseDTO.setProjectId(PROJECT_ID);
        taskResponseDTO.setAssigneeId(ASSIGNEE_ID);
        taskResponseDTO.setStatus(TaskStatus.PENDING);
        taskResponseDTO.setCreatorId(loggedUser.getId());
        taskResponseDTO.setDateCreated(INSTANT_DATE);
        taskResponseDTO.setModifierId(loggedUser.getId());
        taskResponseDTO.setDateModified(INSTANT_DATE);
    }

    @Test
    void getAll() throws Exception {
        List<TaskResponseDTO> tasks = new ArrayList<>();

        tasks.add(taskResponseDTO);

        when(taskService.getAll(PROJECT_ID, loggedUser))
                .thenReturn(tasks);


        mockMvc.perform(get(POST_GET_URL, PROJECT_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_ALL_JSON));

        verify(taskService)
                .getAll(PROJECT_ID, loggedUser);
    }

    @Test
    void create() throws Exception {
        TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
        taskRequestDTO.setTitle(TITLE);
        taskRequestDTO.setDescription(DESCRIPTION);
        taskRequestDTO.setAssigneeId(ASSIGNEE_ID);

        when(taskService.save(taskRequestDTO, PROJECT_ID, loggedUser))
                .thenReturn(taskResponseDTO);

        mockMvc.perform(post(POST_GET_URL, PROJECT_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CREATE_TASK_JSON));

        verify(taskService)
                .save(taskRequestDTO, PROJECT_ID, loggedUser);
    }

    @Test
    void update() throws Exception {
        TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
        taskRequestDTO.setTitle(UPDATED_TITLE);
        taskRequestDTO.setDescription(UPDATED_DESCRIPTION);
        taskRequestDTO.setAssigneeId(ASSIGNEE_ID);

        taskResponseDTO.setTitle(UPDATED_TITLE);
        taskResponseDTO.setDescription(UPDATED_DESCRIPTION);
        taskResponseDTO.setAssigneeId(ASSIGNEE_ID);

        when(taskService.update(TASK_ID, taskRequestDTO, PROJECT_ID, loggedUser))
                .thenReturn(taskResponseDTO);

        mockMvc.perform(put(UPDATE_DELETE_URL, PROJECT_ID, TASK_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_TASK_JSON));

        verify(taskService)
                .update(TASK_ID, taskRequestDTO, PROJECT_ID, loggedUser);
    }

    @Test
    void delete() throws Exception {
        doNothing().when(taskService)
                .delete(TASK_ID, PROJECT_ID, loggedUser);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(UPDATE_DELETE_URL, PROJECT_ID, TASK_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk());

        verify(taskService)
                .delete(TASK_ID, PROJECT_ID, loggedUser);
    }

    @Test
    void changeAssignee() throws Exception {
        taskResponseDTO.setAssigneeId(UPDATED_ASSIGNEE_ID);

        when(taskService.changeAssignee(TASK_ID, UPDATED_ASSIGNEE_ID, loggedUser))
                .thenReturn(taskResponseDTO);

        mockMvc.perform(put(TASK_CHANGE_ASSIGNEE_URL, TASK_ID, UPDATED_ASSIGNEE_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_ASSIGNEE_JSON));

        verify(taskService)
                .changeAssignee(TASK_ID, UPDATED_ASSIGNEE_ID, loggedUser);
    }

    @Test
    void updateStatus() throws Exception {
        taskResponseDTO.setStatus(TaskStatus.valueOf(UPDATED_STATUS));

        when(taskService.updateStatus(TASK_ID, UPDATED_STATUS, loggedUser))
                .thenReturn(taskResponseDTO);

        mockMvc.perform(put(UPDATE_STATUS_URL, TASK_ID, UPDATED_STATUS)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(UPDATED_STATUS)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_STATUS_RESPONSE_JSON));

        verify(taskService)
                .updateStatus(TASK_ID, UPDATED_STATUS, loggedUser);
    }
}