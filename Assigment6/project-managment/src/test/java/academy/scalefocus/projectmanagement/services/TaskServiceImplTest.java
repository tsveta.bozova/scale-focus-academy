package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.entities.*;
import academy.scalefocus.projectmanagement.repositories.ProjectRepository;
import academy.scalefocus.projectmanagement.repositories.TaskRepository;
import academy.scalefocus.projectmanagement.repositories.TeamRepository;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectPermissionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class TaskServiceImplTest {
    private static final String TITLE = "Task";
    private static final String UPDATED_TITLE = "Test task";
    private static final String DESCRIPTION = "description";
    private static final String UPDATED_DESCRIPTION = "updated description";
    private static final Long PROJECT_ID = 1L;
    private static final String PROJECT_TITLE = "team";
    private static final String PROJECT_PERMISSION_EXCEPTION_MESSAGE = "User doesn't have access";
    private static final long TASK_ID = 1L;
    private static final Long TEAM_ID = 1L;
    private static final String TEAM_TITLE = "team";
    public static final String UPDATED_STATUS = "IN_PROGRESS";

    @MockBean
    private TaskRepository taskRepository;

    @MockBean
    private TeamRepository teamRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private TaskServiceImpl taskService;

    private User loggedUserMember;
    private User loggedUserCreator;
    private User loggedUserWithoutAccess;
    private Team team;
    private Set<Team> teams = new HashSet<>();
    private Set<User> members = new HashSet<>();
    private Set<Task> tasks = new HashSet<>();
    private Task expectedTask;

    @BeforeEach
    void setUp() {
        loggedUserCreator = createUser();
        loggedUserMember = createMember();
        loggedUserWithoutAccess = createUserWithoutAccess();
        team = createTeam();
        expectedTask = createTask();
    }

    private User createUserWithoutAccess() {
        User user = new User();
        user.setId(3L);
        user.setUsername("asd");
        return user;
    }

    private Task createTask() {
        Task task = new Task();
        task.setId(TASK_ID);
        task.setTitle(TITLE);
        task.setDescription(DESCRIPTION);
        task.setAssignee(loggedUserCreator);
        task.setCreator(loggedUserCreator);
        task.setDateCreated();
        task.setModifier(loggedUserCreator);
        task.setDateModified();
        return task;
    }

    private User createMember() {
        User user = new User();
        user.setId(2L);
        user.setTeams(teams);
        user.setUsername("stoyan");
        return user;
    }

    private Team createTeam() {
        Team team = new Team();
        team.setId(TEAM_ID);
        team.setTitle(TEAM_TITLE);
        members.add(loggedUserMember);
        team.setMembers(members);
        return team;
    }

    private User createUser() {
        User user = new User();
        user.setId(1L);
        user.setUsername("cveta");
        user.setTeams(new HashSet<>());
        return user;
    }

    private Project createProject() {
        Project project = new Project();
        project.setId(PROJECT_ID);
        project.setTitle(PROJECT_TITLE);
        project.setCreator(loggedUserCreator);
        project.setDateCreated();
        project.setModifier(loggedUserCreator);
        project.setDateModified();
        teams.add(team);
        project.setTeams(teams);
        tasks.add(expectedTask);
        project.setTasks(tasks);
        return project;
    }

    @Test
    void getAll() {
        String message = "";
        List<Task> tasks = new ArrayList<>();

        Project project = createProject();
        expectedTask.setProject(project);
        tasks.add(expectedTask);

        when(taskRepository.findAll())
                .thenReturn(tasks);
        when(projectRepository.findById(PROJECT_ID))
                .thenReturn(java.util.Optional.of(project));


        List<TaskResponseDTO> taskResponseDTOS = project.getTasks().stream()
                .map(this::createResponse)
                .collect(Collectors.toList());

        try {
            taskService.getAll(PROJECT_ID, loggedUserWithoutAccess);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        List<TaskResponseDTO> actualResponsesByCreator = taskService.getAll(PROJECT_ID, loggedUserCreator);
        List<TaskResponseDTO> actualResponsesByMember = taskService.getAll(PROJECT_ID, loggedUserMember);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualResponsesByMember),
                () -> assertNotNull(actualResponsesByCreator),
                () -> assertEquals(taskResponseDTOS, actualResponsesByCreator),
                () -> assertEquals(taskResponseDTOS, actualResponsesByMember)
        );

        verify(projectRepository, times(3))
                .findById(PROJECT_ID);
    }

    private TaskResponseDTO createResponse(Task task) {
        TaskResponseDTO taskResponseDTO = new TaskResponseDTO();
        taskResponseDTO.setId(expectedTask.getId());
        taskResponseDTO.setTitle(expectedTask.getTitle());
        taskResponseDTO.setDescription(expectedTask.getDescription());
        taskResponseDTO.setCreatorId(expectedTask.getCreator().getId());
        taskResponseDTO.setDateCreated(expectedTask.getDateCreated());
        if(task.getModifier() != null) {
            taskResponseDTO.setModifierId(expectedTask.getModifier().getId());
            taskResponseDTO.setDateModified(expectedTask.getDateModified());
        }
        taskResponseDTO.setAssigneeId(expectedTask.getAssignee().getId());
        taskResponseDTO.setStatus(expectedTask.getStatus());
        taskResponseDTO.setProjectId(expectedTask.getProject().getId());
        return taskResponseDTO;
    }

    @Test
    void save() {
        String message = "";
        Project project = createProject();
        expectedTask.setProject(project);
        TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
        taskRequestDTO.setTitle(TITLE);
        taskRequestDTO.setDescription(DESCRIPTION);
        taskRequestDTO.setAssigneeId(loggedUserCreator.getId());

        when(projectRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.of(project));
        when(taskRepository.save(any(Task.class)))
                .thenReturn(expectedTask);
        when(userRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(loggedUserCreator));

        TaskResponseDTO taskResponseDTO = createResponse(expectedTask);

        try {
            taskService.save(taskRequestDTO, PROJECT_ID, loggedUserMember);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        TaskResponseDTO actualTask = taskService.save(taskRequestDTO, PROJECT_ID, loggedUserCreator);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualTask),
                () -> assertEquals(taskResponseDTO, actualTask)
        );

        verify(taskRepository)
                .save(any(Task.class));
    }

    @Test
    void update() {
        String message = "";
        Project project = createProject();
        TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
        taskRequestDTO.setTitle(UPDATED_TITLE);
        taskRequestDTO.setDescription(UPDATED_DESCRIPTION);
        taskRequestDTO.setAssigneeId(loggedUserCreator.getId());
        taskRequestDTO.setStatus(TaskStatus.PENDING);

        expectedTask.setTitle(UPDATED_TITLE);
        expectedTask.setDescription(UPDATED_DESCRIPTION);
        expectedTask.setAssignee(loggedUserCreator);
        expectedTask.setProject(project);
        expectedTask.setStatus(TaskStatus.PENDING);

        when(taskRepository.save(any(Task.class)))
                .thenReturn(expectedTask);
        when(projectRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(project));
        when(taskRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTask));
        when(userRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(loggedUserCreator));

        TaskResponseDTO responseTask = createResponse(expectedTask);

        TaskResponseDTO actualTask = taskService.update(TASK_ID, taskRequestDTO, PROJECT_ID, loggedUserCreator);

        try {
            taskService.update(TASK_ID, taskRequestDTO,PROJECT_ID, loggedUserMember);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualTask),
                () -> assertEquals(responseTask, actualTask)
        );
        verify(taskRepository)
                .save(any(Task.class));
        verify(projectRepository, times(2))
                .findById(any(Long.class));
    }

    @Test
    void delete() {
        Project project = createProject();
        String message = "";

        when(projectRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(project));

        try {
            taskService.delete(TASK_ID, PROJECT_ID, loggedUserMember);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        taskService.delete(TASK_ID, PROJECT_ID, loggedUserCreator);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);

        verify(taskRepository)
                .deleteById(anyLong());
        verify(projectRepository, times(2))
                .findById(anyLong());
    }

    @Test
    void changeAssignee() {
        String message = "";
        String messageUserNotMember = "";
        Project project = createProject();

        expectedTask.setAssignee(loggedUserMember);
        expectedTask.setProject(project);

        when(taskRepository.save(any(Task.class)))
                .thenReturn(expectedTask);
        when(taskRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTask));
        when(userRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(loggedUserMember));

        TaskResponseDTO responseTask = createResponse(expectedTask);

        TaskResponseDTO actualTask = taskService.changeAssignee(TASK_ID, loggedUserMember.getId(), loggedUserCreator);

        try {
            taskService.changeAssignee(TASK_ID, loggedUserMember.getId(), loggedUserMember);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        try {
            taskService.changeAssignee(TASK_ID, loggedUserWithoutAccess.getId(), loggedUserMember);
        } catch (ProjectPermissionException e) {
            messageUserNotMember = e.getMessage();
        }

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, messageUserNotMember);
        assertAll(
                () -> assertNotNull(actualTask),
                () -> assertEquals(responseTask, actualTask)
        );
        verify(taskRepository)
                .save(any(Task.class));

    }

    @Test
    void updateStatus() {
        String message = "";
        Project project = createProject();

        expectedTask.setStatus(TaskStatus.valueOf(UPDATED_STATUS));
        expectedTask.setProject(project);
        expectedTask.setModifier(loggedUserCreator);

        when(taskRepository.save(any(Task.class)))
                .thenReturn(expectedTask);
        when(taskRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTask));

        TaskResponseDTO responseTask = createResponse(expectedTask);

        TaskResponseDTO actualTask = taskService.updateStatus(TASK_ID, UPDATED_STATUS, loggedUserCreator);

        try {
            taskService.updateStatus(TASK_ID, UPDATED_STATUS, loggedUserMember);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }


        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualTask),
                () -> assertEquals(responseTask, actualTask)
        );
        verify(taskRepository)
                .save(any(Task.class));
    }
}