package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {
    private static final Long USER_ID = 1L;
    private static final String GET_ALL_JSON = "[{\"id\":1,\"username\":\"asd\",\"firstName\":\"Cveta\",\"lastName\":\"Bozova\",\"creatorId\":1,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":1,\"dateModified\":\"2020-04-02T20:20:20Z\",\"admin\":true}]";
    private static final String CREATE_USER_JSON = "{\"id\":1,\"username\":\"asd\",\"firstName\":\"Cveta\",\"lastName\":\"Bozova\",\"creatorId\":1,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":1,\"dateModified\":\"2020-04-02T20:20:20Z\",\"admin\":true}";
    private static final String UPDATED_USER_JSON = "{\"id\":1,\"username\":\"cvetence\",\"firstName\":\"Cveta\",\"lastName\":\"Bozova\",\"creatorId\":1,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":1,\"dateModified\":\"2020-04-02T20:20:20Z\",\"admin\":true}";
    private static final String USERNAME = "asd";
    private static final String FIRST_NAME = "Cveta";
    private static final String LAST_NAME = "Bozova";
    private static final boolean ADMIN = true;
    private static final String PASSWORD = "password";
    private static final String AUTHORIZATION_HEADER = "admin,adminpass";
    private static final Instant INSTANT_DATE = Instant.parse("2020-04-02T20:20:20.00Z");
    private static final String REQUEST_BODY = "{\"username\" : \"asd\", \"password\" : \"password\", \"firstName\" : \"Cveta\", \"lastName\" : \"Bozova\",\"admin\" : true}";
    private static final String UPDATED_REQUEST_BODY = "{\"username\" : \"cvetence\", \"password\" : \"password\", \"firstName\" : \"Cveta\", \"lastName\" : \"Bozova\",\"admin\" : true}";
    private static final String AUTHORIZATION = "Authorization";
    private static final String POST_GET_URL = "/users";
    private static final long ADMIN_ID = 100L;
    private static final String UPDATED_USERNAME = "cvetence";
    private static final String UPDATE_DELETE_URL = "/users/{userId}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private AuthenticationService authenticationService;

    private UserResponseDTO userResponseDTO;
    private User loggedUser;

    @BeforeEach
    private void setUp() {
        loggedUser = new User();
        loggedUser.setId(1L);
        loggedUser.setUsername("admin");
        loggedUser.setPassword("adminpass");
        loggedUser.setFirstName("administrator");
        loggedUser.setLastName("administrator");
        loggedUser.setAdmin(true);

        when(authenticationService.login(AUTHORIZATION_HEADER))
                .thenReturn(loggedUser);
        doNothing().when(authenticationService)
                .checkIfUserIsAdmin(loggedUser);

        userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(USER_ID);
        userResponseDTO.setUsername(USERNAME);
        userResponseDTO.setFirstName(FIRST_NAME);
        userResponseDTO.setLastName(LAST_NAME);
        userResponseDTO.setAdmin(ADMIN);
        userResponseDTO.setCreatorId(loggedUser.getId());
        userResponseDTO.setDateCreated(INSTANT_DATE);
        userResponseDTO.setModifierId(loggedUser.getId());
        userResponseDTO.setDateModified(INSTANT_DATE);
    }

    @DisplayName("Get all should return proper json list of all users")
    @Test
    void getAll() throws Exception {
        List<UserResponseDTO> users = new ArrayList<>();

        users.add(userResponseDTO);

        when(userService.getAll())
                .thenReturn(users);


        mockMvc.perform(get(POST_GET_URL)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_ALL_JSON));

        verify(userService)
                .getAll();

    }

    @DisplayName("Should return proper user in json")
    @Test
    void create() throws Exception {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setUsername(USERNAME);
        userRequestDTO.setPassword(PASSWORD);
        userRequestDTO.setFirstName(FIRST_NAME);
        userRequestDTO.setLastName(LAST_NAME);
        userRequestDTO.setAdmin(ADMIN);

        when(userService.save(userRequestDTO, loggedUser))
                .thenReturn(userResponseDTO);

        mockMvc.perform(post(POST_GET_URL)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CREATE_USER_JSON));

        verify(userService)
                .save(userRequestDTO, loggedUser);
    }

    @Test
    void delete() throws Exception {

        doNothing().when(userService)
                .deleteUser(USER_ID);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(UPDATE_DELETE_URL, USER_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk());

        verify(userService)
                .deleteUser(USER_ID);
    }


    @Test
    void update() throws Exception {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setUsername(UPDATED_USERNAME);
        userRequestDTO.setPassword(PASSWORD);
        userRequestDTO.setFirstName(FIRST_NAME);
        userRequestDTO.setLastName(LAST_NAME);
        userRequestDTO.setAdmin(ADMIN);

        userResponseDTO.setUsername(UPDATED_USERNAME);

        when(userService.updateUser(userRequestDTO, USER_ID, loggedUser))
                .thenReturn(userResponseDTO);

        mockMvc.perform(put(UPDATE_DELETE_URL, USER_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_USER_JSON));

        verify(userService)
                .updateUser(userRequestDTO, USER_ID, loggedUser);
    }
}
