package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.WorkLogRequestDTO;
import academy.scalefocus.projectmanagement.dtos.WorkLogResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.WorkLogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WorkLogController.class)
class WorkLogControllerTest {
    private static final String AUTHORIZATION_HEADER = "admin,adminpass";
    public static final long WORK_LOG_ID = 1L;
    public static final long TASK_ID = 1L;
    public static final long USER_ID = 1L;
    public static final int HOURS_SPENT = 8;
    private static final LocalDate DATE = LocalDate.parse("2020-04-02");
    private static final String POST_GET_URL = "/tasks/{taskId}/work-logs";
    private static final String GET_ALL_JSON = "[{\"id\":1,\"taskId\":1,\"userId\":1,\"hoursSpent\":8,\"date\":\"2020-04-02\"}]";
    private static final String REQUEST_BODY = "{\"hoursSpent\": 8, \"date\": \"2020-04-02\"}";
    private static final String CREATE_WORK_LOG_JSON = "{\"id\":1,\"taskId\":1,\"userId\":1,\"hoursSpent\":8,\"date\":\"2020-04-02\"}";
    private static final int UPDATED_HOURS_SPENT = 10;
    private static final LocalDate UPDATED_DATE = LocalDate.parse("2020-04-03");
    private static final String UPDATE_DELETE_URL = "/tasks/{taskId}/work-logs/{workLogId}";
    private static final String UPDATED_REQUEST_BODY = "{\"hoursSpent\": 10, \"date\": \"2020-04-03\"}";
    private static final String UPDATED_WORK_LOG_JSON = "{\"id\":1,\"taskId\":1,\"userId\":1,\"hoursSpent\":10,\"date\":\"2020-04-03\"}";


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WorkLogService workLogService;

    @MockBean
    private AuthenticationService authenticationService;

    private WorkLogResponseDTO workLogResponseDTO;
    private User loggedUser;

    @BeforeEach
    private void setUp() {
        loggedUser = new User();
        loggedUser.setId(100L);
        loggedUser.setUsername("admin");
        loggedUser.setPassword("adminpass");
        loggedUser.setFirstName("administrator");
        loggedUser.setLastName("administrator");
        loggedUser.setAdmin(true);

        when(authenticationService.login(AUTHORIZATION_HEADER))
                .thenReturn(loggedUser);

        workLogResponseDTO = new WorkLogResponseDTO();
        workLogResponseDTO.setId(WORK_LOG_ID);
        workLogResponseDTO.setTaskId(TASK_ID);
        workLogResponseDTO.setUserId(USER_ID);
        workLogResponseDTO.setHoursSpent(HOURS_SPENT);
        workLogResponseDTO.setDate(DATE);
    }

    @Test
    void getAll() throws Exception {
        List<WorkLogResponseDTO> workLogs = new ArrayList<>();

        workLogs.add(workLogResponseDTO);

        when(workLogService.getAll(TASK_ID, loggedUser))
                .thenReturn(workLogs);

        mockMvc.perform(get(POST_GET_URL, TASK_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_ALL_JSON));

        verify(workLogService)
                .getAll(TASK_ID, loggedUser);
    }

    @Test
    void create() throws Exception {
        WorkLogRequestDTO workLogRequestDTO = new WorkLogRequestDTO();
        workLogRequestDTO.setHoursSpent(HOURS_SPENT);
        workLogRequestDTO.setDate(DATE);

        when(workLogService.save(workLogRequestDTO, TASK_ID, loggedUser))
                .thenReturn(workLogResponseDTO);

        mockMvc.perform(post(POST_GET_URL, TASK_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CREATE_WORK_LOG_JSON));

        verify(workLogService)
                .save(workLogRequestDTO, TASK_ID, loggedUser);
    }

    @Test
    void update() throws Exception {
        WorkLogRequestDTO workLogRequestDTO = new WorkLogRequestDTO();
        workLogRequestDTO.setHoursSpent(UPDATED_HOURS_SPENT);
        workLogRequestDTO.setDate(UPDATED_DATE);

        workLogResponseDTO.setHoursSpent(UPDATED_HOURS_SPENT);
        workLogResponseDTO.setDate(UPDATED_DATE);

        when(workLogService.update(workLogRequestDTO, TASK_ID, WORK_LOG_ID, loggedUser))
                .thenReturn(workLogResponseDTO);

        mockMvc.perform(put(UPDATE_DELETE_URL, TASK_ID, WORK_LOG_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_WORK_LOG_JSON));

        verify(workLogService)
                .update(workLogRequestDTO, TASK_ID, WORK_LOG_ID, loggedUser);
    }

    @Test
    void delete() throws Exception {
        doNothing().when(workLogService)
                .delete(TASK_ID, WORK_LOG_ID, loggedUser);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(UPDATE_DELETE_URL, TASK_ID, WORK_LOG_ID)
                .header(AUTHORIZATION, AUTHORIZATION_HEADER))
                .andExpect(status().isOk());

        verify(workLogService)
                .delete(TASK_ID, WORK_LOG_ID, loggedUser);
    }
}