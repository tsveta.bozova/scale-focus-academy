Project Management
=
Spring Boot Java Application to manage projects and employees inside a company.

Preparation
-
To run the application you open Command Prompt or Bash (depends on the operating system), 
login with your docker account. First you have to pull the two images. 
Write in the console:

docker run -p 1521:1521 --name oracledb cvetabozova/bozova:project-management_oracledb 
- to pull and run the database image
Note: it is important to give the name oracledb, so you can link the database container with
the image container.

docker run -p 8080:8080 --name `container name` --link oracledb:cvetabozova/bozova cvetabozova/myapp-v2:0.0.1 
- to pull and link the application with the database container

You are ready to go!

Endpoints
=

Gaining access token:
-

-Request

POST /users/login HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
```
{
	"username": "admin",
	"password": "adminpass"
}
```

-Response

 ```
{
    "token": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA"
}
```

Note: the first run of the application have to be with username:admin, password:adminpass.


USERS
=
 


#### The user must be ADMIN to acccess /users endpoints


### Create User

 
POST /users HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request 

```
{
    "username": "user",
    "password": "password",
    "firstName": "John",
    "lastName": "Doe",
    "isAdmin": false
}
``` 

- Response

```
{
    "id": 2,
    "username": "user",
    "firstName": "John",
    "lastName": "Doe",
    "creatorId": 1,
    "dateCreated": "2020-05-10T19:41:02.922Z",
    "modifierId": null,
    "dateModified": null,
    "admin": false
}
```

### List all users


GET /users HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Response

```
[
    {
        "id": 2,
        "username": "user",
        "firstName": "John",
        "lastName": "Doe",
        "creatorId": 1,
        "dateCreated": "2020-05-10T19:41:02.922Z",
        "modifierId": null,
        "dateModified": null,
        "admin": false
    }
]
```


### Upadate User

PUT /users/{userId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
    "username": "user",
    "password": "password",
    "firstName": "John",
    "lastName": "Doe",
    "isAdmin": true
}
```

- Response

```
{
    "id": 2,
    "username": "user",
    "firstName": "John",
    "lastName": "Doe",
    "creatorId": 1,
    "dateCreated": "2020-05-10T19:41:02.922Z",
    "modifierId": 1,
    "dateModified": 2020-05-10T19:50:02.922Z,
    "admin": true
}
```

### Delete User

DELETE /users/{userId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json


TEAMS
=


#### The user must be ADMIN to acccess /teams endpoints


### Create Team

 
POST /teams HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request
 
```
{
	"title" : "team"
}
``` 

- Response

```
{
    "id": 1,
    "title": "team",
    "creatorId": 1,
    "dateCreated": "2020-05-10T19:56:52.698Z",
    "modifierId": null,
    "dateModified": null,
    "membersIds": []
}
```

### List all teams


GET /teams HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Response

```
[
    {
        "id": 1,
        "title": "team",
        "creatorId": 1,
        "dateCreated": "2020-05-10T19:56:52.698Z",
        "modifierId": null,
        "dateModified": null,
        "membersIds": []
    }
]
```

### Upadate Team

PUT /teams/{teamId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"title" : "Updated team"
}
```

- Response

```
{
    "id": 1,
    "title": "Updated team",
    "creatorId": 1,
    "dateCreated": "2020-05-10T19:56:52.698Z",
    "modifierId": 1,
    "dateModified": 2020-05-10T20:00:52.698Z,
    "membersIds": []
}
```

### Delete Team

DELETE /teams/{teamId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json


### Assign user to team


PUT /teams/{teamId}/users/{userId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

-Response

```
{
    "id": 1,
    "title": "Updated team",
    "creatorId": 1,
    "dateCreated": "2020-05-10T19:56:52.698Z",
    "modifierId": 1,
    "dateModified": "2020-05-10T19:59:39.611Z",
    "membersIds": [
        2
    ]
}
```


PROJECTS
=


### Create Project

 
POST /projects HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"title" : "Project Management",
	"description" : "to create my first api"
}
``` 

- Response

```
{
    "id": 1,
    "title": "Project Management",
    "description": "to create my first api",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:23:52.899Z",
    "modifierId": null,
    "dateModified": null,
    "teamsIds": []
}
```

### List all projects


GET /projects HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Response

```
[
    {
        "id": 1,
        "title": "Updated Project",
        "description": "to create my first api",
        "creatorId": 1,
        "dateCreated": "2020-05-10T20:23:52.899Z",
        "modifierId": 1,
        "dateModified": "2020-05-10T20:25:59.818Z",
        "teamsIds": []
    }
]
```

### Update Projects

PUT /projects/{projectId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"title" : "Updated Project",
	"description" : "to create my first api"
}
```

- Response

```
{
    "id": 1,
    "title": "Updated Project",
    "description": "to create my first api",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:23:52.899Z",
    "modifierId": 1,
    "dateModified": null,
    "teamsIds": []
}
```

### Delete Project

DELETE /projects/{projectId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json


### Assign team to project


PUT /projects/{projectId}/teams/{teamId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

-Response

```
{
    "id": 1,
    "title": "Updated Project",
    "description": "to create my first api",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:23:52.899Z",
    "modifierId": 1,
    "dateModified": "2020-05-10T20:25:59.818Z",
    "teamsIds": [
        1
    ]
}
```


TASKS
=


### Create Task

 
POST /projects/{projectId}/tasks HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"title" : "Task",
	"description" : "to fix bugs",
	"assigneeId" : 2
}
```

- Response

```
{
    "id": 1,
    "title": "Task",
    "description": "to fix bugs",
    "projectId": 1,
    "assigneeId": 2,
    "status": "PENDING",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:34:02.948Z",
    "modifierId": null,
    "dateModified": null
}
```

### List all tasks


GET /projects/{projectId}/tasks HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Response

```
[
    {
        "id": 1,
        "title": "Task",
        "description": "to fix bugs",
        "projectId": 1,
        "assigneeId": 2,
        "status": "PENDING",
        "creatorId": 1,
        "dateCreated": "2020-05-10T20:34:02.948Z",
        "modifierId": null,
        "dateModified": null
    }
]
```

### Update Task

PUT /projects/{projectId}/tasks/{taskId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"title" : "Updated task",
	"description" : "to fix bugs",
	"assigneeId" : 2
}
```

- Response

```
{
    "id": 1,
    "title": "task test",
    "description": "to fix update bug",
    "projectId": 1,
    "assigneeId": 2,
    "status": "IN_PROGRESS",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:34:02.948Z",
    "modifierId": 1,
    "dateModified": "2020-05-10T21:25:47.425Z"
}
```

### Delete Task

DELETE /projects/{projectId}/tasks{taskId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json


### Change assignee


PUT /tasks/{taskId}/users/{userId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

-Response

```
{
    "id": 1,
    "title": "Task",
    "description": "to fix bugs",
    "projectId": 1,
    "assigneeId": 2,
    "status": "PENDING",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:34:02.948Z",
    "modifierId": 1,
    "dateModified": "2020-05-10T21:02:54.254Z"
}
```

### Update status

PUT /tasks/{taskId}/status HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

-Response 

```
{
    "id": 1,
    "title": "Task",
    "description": "to fix bugs",
    "projectId": 1,
    "assigneeId": 2,
    "status": "IN_PROGRESS",
    "creatorId": 1,
    "dateCreated": "2020-05-10T20:34:02.948Z",
    "modifierId": 1,
    "dateModified": "2020-05-10T21:25:47.425Z"
}
```

WORK LOGS
=


### Create Work Log

 
POST /tasks/{taskId}/work-logs HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"hoursSpent" : 8,
	"date" : "2020-04-28"
}
``` 

- Response

```
{
    "id": 1,
    "taskId": 1,
    "userId": 1,
    "hoursSpent": 8,
    "date": "2020-04-28"
}
```

### List all work logs


GET /tasks/{taskId}/work-logs HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Response

```
[
    {
        "id": 1,
        "taskId": 1,
        "userId": 1,
        "hoursSpent": 8,
        "date": "2020-04-28"
    }
]
```

### Update work log

PUT /tasks/{taskId}/work-logs/{workLogId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

- Request

```
{
	"hoursSpent" : 10,
	"date" : "2020-04-29"
}
```

- Response

```
{
    "id": 1,
    "taskId": 1,
    "userId": 1,
    "hoursSpent": 10,
    "date": "2020-04-29"
}
```

### Delete work log

DELETE /tasks/{taskId}/work-logs/{workLogId} HTTP/1.1
Host: localhost:8080
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTg5MTM2NTQ5fQ.tgO-3xjcWASIEgIkVaoQ5zXDYk_73ysBw6XGgy558SA
Content-Type: application/json

