package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.projects.ProjectRequestDTO;
import academy.scalefocus.projectmanagement.dtos.projects.ProjectResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.JWTTokenService;
import academy.scalefocus.projectmanagement.services.ProjectService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProjectController.class)
class ProjectControllerTest {
    private static final String AUTHORIZATION_HEADER = "admin,adminpass";
    private static final String POST_GET_URL = "/projects";
    private static final String AUTHORIZATION = "Authorization";
    private static final String GET_ALL_JSON = "[{\"id\":1,\"title\":\"project\",\"description\":\"description\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"teamsIds\":[]}]";
    private static final String TITLE = "project";
    private static final String DESCRIPTION = "description";
    private static final String REQUEST_BODY = "{\"title\": \"project\",\"description\": \"description\"}";
    private static final String CREATE_PROJECT_JSON = "{\"id\":1,\"title\":\"project\",\"description\":\"description\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"teamsIds\":[]}";
    private static final String UPDATED_TITLE = "test project";
    private static final long PROJECT_ID = 1L;
    private static final Instant INSTANT_DATE = Instant.parse("2020-04-02T20:20:20.00Z");
    private static final String UPDATE_DELETE_URL = "/projects/{projectId}";
    private static final String UPDATED_REQUEST_BODY = "{\"title\": \"test project\",\"description\": \"test description\"}";
    private static final String UPDATED_TEAM_JSON = "{\"id\":1,\"title\":\"test project\",\"description\":\"test description\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"teamsIds\":[]}";
    private static final String UPDATED_DESCRIPTION = "test description";
    private static final long TEAM_ID = 1L;
    private static final String ASSIGN_URL = "/projects/{projectId}/teams/{teamId}";
    private static final String ASSIGN_TEAM_JSON = "{\"id\":1,\"title\":\"project\",\"description\":\"description\",\"creatorId\":100,\"dateCreated\":\"2020-04-02T20:20:20Z\",\"modifierId\":100,\"dateModified\":\"2020-04-02T20:20:20Z\",\"teamsIds\":[1]}";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private JWTTokenService tokenService;

    @MockBean
    private ProjectService projectService;

    @MockBean
    private AuthenticationService authenticationService;

    private ProjectResponseDTO projectResponseDTO;
    private User loggedUser;
    private String token;

    @BeforeEach
    private void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        loggedUser = new User();
        loggedUser.setId(100L);
        loggedUser.setUsername("admin");
        loggedUser.setPassword("adminpass");
        loggedUser.setFirstName("administrator");
        loggedUser.setLastName("administrator");
        loggedUser.setAdmin(true);

        token = "token";

        when(authenticationService.createLoggedUser())
                .thenReturn(loggedUser);

        projectResponseDTO = new ProjectResponseDTO();
        projectResponseDTO.setId(PROJECT_ID);
        projectResponseDTO.setTitle(TITLE);
        projectResponseDTO.setDescription(DESCRIPTION);
        projectResponseDTO.setCreatorId(loggedUser.getId());
        projectResponseDTO.setDateCreated(INSTANT_DATE);
        projectResponseDTO.setModifierId(loggedUser.getId());
        projectResponseDTO.setDateModified(INSTANT_DATE);
        projectResponseDTO.setTeamsIds(new ArrayList<>());
    }

    @Test
    void getAll() throws Exception {
        List<ProjectResponseDTO> projects = new ArrayList<>();

        projects.add(projectResponseDTO);

        when(projectService.getAll(loggedUser))
                .thenReturn(projects);


        mockMvc.perform(get(POST_GET_URL)
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_ALL_JSON));

        verify(projectService)
                .getAll(loggedUser);
    }

    @Test
    void create() throws Exception {
        ProjectRequestDTO projectRequestDTO = new ProjectRequestDTO();
        projectRequestDTO.setTitle(TITLE);
        projectRequestDTO.setDescription(DESCRIPTION);

        when(projectService.save(projectRequestDTO, loggedUser))
                .thenReturn(projectResponseDTO);

        mockMvc.perform(post(POST_GET_URL)
                .header(AUTHORIZATION, token)
                .content(REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CREATE_PROJECT_JSON));

        verify(projectService)
                .save(projectRequestDTO, loggedUser);
    }

    @Test
    void assignTeamToProject() throws Exception {
        List<Long> teamsIds = new ArrayList<>();
        teamsIds.add(TEAM_ID);
        projectResponseDTO.setTeamsIds(teamsIds);

        when(projectService.assignTeamToProject(PROJECT_ID, TEAM_ID, loggedUser))
                .thenReturn(projectResponseDTO);

        mockMvc.perform(put(ASSIGN_URL, TEAM_ID, PROJECT_ID)
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(ASSIGN_TEAM_JSON));

        verify(projectService)
                .assignTeamToProject(PROJECT_ID, TEAM_ID, loggedUser);
    }

    @Test
    void update() throws Exception {
        ProjectRequestDTO projectRequestDTO = new ProjectRequestDTO();
        projectRequestDTO.setTitle(UPDATED_TITLE);
        projectRequestDTO.setDescription(UPDATED_DESCRIPTION);

        projectResponseDTO.setTitle(UPDATED_TITLE);
        projectResponseDTO.setDescription(UPDATED_DESCRIPTION);

        when(projectService.update(PROJECT_ID, loggedUser, projectRequestDTO))
                .thenReturn(projectResponseDTO);

        mockMvc.perform(put(UPDATE_DELETE_URL, PROJECT_ID)
                .header(AUTHORIZATION, token)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATED_TEAM_JSON));

        verify(projectService)
                .update(PROJECT_ID, loggedUser, projectRequestDTO);
    }

    @Test
    void delete() throws Exception {
        doNothing().when(projectService)
                .delete(PROJECT_ID, loggedUser);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(UPDATE_DELETE_URL, PROJECT_ID)
                .header(AUTHORIZATION, token))
                .andExpect(status().isOk());

        verify(projectService)
                .delete(PROJECT_ID, loggedUser);
    }
}