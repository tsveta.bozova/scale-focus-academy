package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.users.LoginRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.LoginResponseDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
public class UserServiceImplTest {
    private static final String USERNAME = "cvetence";
    private static final String PASSWORD = "123";
    private static final String FIRST_NAME = "Cveta";
    private static final String LAST_NAME = "Bozova";
    private static final String UPDATED_USERNAME = "asd";
    private static final long USER_ID = 1L;
    private static final String TOKEN = "token";

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserServiceImpl userService;

    @MockBean
    private JWTTokenService tokenService;

    private User loggedUser;
    private User expectedUser;

    @BeforeEach
    void setUp() {
        loggedUser = createAdmin();
        expectedUser = createUser(loggedUser);
    }

    private User createAdmin() {
        User user = new User();
        user.setId(100L);
        user.setUsername("admin");
        user.setPassword("adminpass");
        user.setFirstName("administrator");
        user.setLastName("administrator");
        return user;
    }

    @Test
    @DisplayName("Should create user successfully")
    void create() {
        UserRequestDTO userRequestDTO = createUserRequestDTO();

        when(userRepository.save(any(User.class)))
                .thenReturn(expectedUser);

        UserResponseDTO responseUser = createResponse(expectedUser);

        UserResponseDTO actualUser = userService.save(userRequestDTO, loggedUser);

        assertAll(
                () -> assertNotNull(actualUser),
                () -> assertEquals(responseUser, actualUser)
        );

        verify(userRepository, times(2))
                .save(any(User.class));
    }

    private UserResponseDTO createResponse(User expectedUser) {
        UserResponseDTO responseUser = new UserResponseDTO();
        responseUser.setId(USER_ID);
        responseUser.setUsername(expectedUser.getUsername());
        responseUser.setFirstName(expectedUser.getFirstName());
        responseUser.setLastName(expectedUser.getLastName());
        responseUser.setAdmin(expectedUser.isAdmin());
        responseUser.setCreatorId(expectedUser.getCreator().getId());
        responseUser.setDateCreated(expectedUser.getDateCreated());
        responseUser.setModifierId(expectedUser.getModifier().getId());
        responseUser.setDateModified(expectedUser.getDateModified());
        return responseUser;
    }

    private UserRequestDTO createUserRequestDTO() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setUsername(USERNAME);
        userRequestDTO.setPassword(PASSWORD);
        userRequestDTO.setFirstName(FIRST_NAME);
        userRequestDTO.setLastName(LAST_NAME);
        userRequestDTO.setAdmin(true);
        return userRequestDTO;
    }

    private User createUser(User loggedUser) {
        User expectedUser = new User();
        expectedUser.setId(USER_ID);
        expectedUser.setUsername(USERNAME);
        expectedUser.setPassword(PASSWORD);
        expectedUser.setFirstName(FIRST_NAME);
        expectedUser.setLastName(LAST_NAME);
        expectedUser.setAdmin(true);
        expectedUser.setCreator(loggedUser);
        expectedUser.setDateCreated();
        expectedUser.setModifier(loggedUser);
        expectedUser.setDateModified();
        return expectedUser;
    }

    @Test
    @DisplayName("Should return registered user if exists.")
    void login() {
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setUsername("admin");
        loginRequestDTO.setPassword("adminpass");

        when(userRepository.findByUsername(any(String.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedUser));

        when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);

        when(tokenService.generate(any(User.class)))
                .thenReturn(TOKEN);

        LoginResponseDTO responseUser = new LoginResponseDTO();
        responseUser.setToken(TOKEN);

        LoginResponseDTO actualUser = userService.login(loginRequestDTO);

        assertAll(
                () -> assertNotNull(actualUser),
                () -> assertEquals(responseUser, actualUser)
        );
        verify(userRepository)
                .findByUsername(anyString());
    }

    @Test
    void update() {
        UserRequestDTO userRequestDTO = createUserUpdateRequestDTO();
        expectedUser.setUsername(UPDATED_USERNAME);
        when(userRepository.save(any(User.class)))
                .thenReturn(expectedUser);
        when(userRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedUser));

        UserResponseDTO responseUser = createResponse(expectedUser);

        UserResponseDTO actualUser = userService.updateUser(userRequestDTO, USER_ID, loggedUser);

        assertAll(
                () -> assertNotNull(actualUser),
                () -> assertEquals(responseUser, actualUser)
        );
        verify(userRepository)
                .save(any(User.class));
        verify(userRepository)
                .findById(any(Long.class));
    }

    private UserRequestDTO createUserUpdateRequestDTO() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setUsername(UPDATED_USERNAME);
        userRequestDTO.setPassword(PASSWORD);
        userRequestDTO.setFirstName(FIRST_NAME);
        userRequestDTO.setLastName(LAST_NAME);
        userRequestDTO.setAdmin(true);
        return userRequestDTO;
    }

    @Test
    void getAll() {
        List<User> users = new ArrayList<>();
        users.add(expectedUser);

        when(userRepository.findAll())
                .thenReturn(users);

        List<UserResponseDTO> userResponseDTOS = users.stream()
                .map(this::createResponse)
                .collect(Collectors.toList());

        List<UserResponseDTO> actualUsers = userService.getAll();

        assertAll(
                () -> assertNotNull(actualUsers),
                () -> assertEquals(userResponseDTOS, actualUsers)
        );

        verify(userRepository)
                .findAll();
    }

    @Test
    void delete () {
        when(userRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedUser));

        when(userRepository.findByUsername(any(String.class)))
                .thenReturn(java.util.Optional.ofNullable(loggedUser));

        when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);

        userService.deleteUser(anyLong());

        verify(userRepository)
                .deleteById(anyLong());
        verify(userRepository)
                .findById(any(Long.class));
    }
}
