package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.teams.TeamRequestDTO;
import academy.scalefocus.projectmanagement.dtos.teams.TeamResponseDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.Team;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.TeamRepository;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class TeamServiceImplTest {
    private static final String TITLE = "Team";
    private static final String UPDATED_TITLE = "Test team";
    private static final long TEAM_ID = 1L;
    private static final String USERNAME = "cveta";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "cveta";
    private static final String LAST_NAME = "bozova";
    private static final long MEMBER_ID = 1L;

    @MockBean
    private TeamRepository teamRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private TeamServiceImpl teamService;

    private User loggedUser;
    private Team expectedTeam;

    @BeforeEach
    void setUp() {
        loggedUser = createAdmin();
        expectedTeam = createTeam();
    }

    private Team createTeam() {
        Team team = new Team();
        team.setId(TEAM_ID);
        team.setTitle(TITLE);
        team.setCreator(loggedUser);
        team.setDateCreated();
        team.setModifier(loggedUser);
        team.setDateModified();
        team.setMembers(new HashSet<>());
        return team;
    }

    private User createAdmin() {
        User user = new User();
        user.setId(100L);
        user.setUsername("admin");
        user.setPassword("adminpass");
        user.setFirstName("administrator");
        user.setLastName("administrator");
        return user;
    }

    @Test
    void getAll() {
        List<Team> teams = new ArrayList<>();
        teams.add(expectedTeam);

        when(teamRepository.findAll())
                .thenReturn(teams);

        List<TeamResponseDTO> teamResponseDTOS = teams.stream()
                .map(this::createResponse)
                .collect(Collectors.toList());

        List<TeamResponseDTO> actualResponses = teamService.getAll();

        assertAll(
                () -> assertNotNull(actualResponses),
                () -> assertEquals(teamResponseDTOS, actualResponses)
        );

        verify(teamRepository)
                .findAll();
    }

    @Test
    void save() {
        TeamRequestDTO teamRequestDTO = new TeamRequestDTO();
        teamRequestDTO.setTitle(TITLE);

        when(teamRepository.save(any(Team.class)))
                .thenReturn(expectedTeam);

        TeamResponseDTO teamResponseDTO = createResponse(expectedTeam);

        TeamResponseDTO actualTeam = teamService.save(teamRequestDTO, loggedUser);

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(teamResponseDTO, actualTeam)
        );

        verify(teamRepository)
                .save(any(Team.class));
    }

    private TeamResponseDTO createResponse(Team expectedTeam) {
        TeamResponseDTO teamResponseDTO = new TeamResponseDTO();
        teamResponseDTO.setId(expectedTeam.getId());
        teamResponseDTO.setTitle(expectedTeam.getTitle());
        teamResponseDTO.setCreatorId(expectedTeam.getCreator().getId());
        teamResponseDTO.setDateCreated(expectedTeam.getDateCreated());
        teamResponseDTO.setModifierId(expectedTeam.getModifier().getId());
        teamResponseDTO.setDateModified(expectedTeam.getDateModified());
        teamResponseDTO.setMembersIds(expectedTeam.getMembers()
                .stream()
                .map(User::getId)
                .collect(Collectors.toList()));
        return teamResponseDTO;
    }

    @Test
    void update() {
        TeamRequestDTO teamRequestDTO = new TeamRequestDTO();
        teamRequestDTO.setTitle(UPDATED_TITLE);

        expectedTeam.setTitle(UPDATED_TITLE);

        when(teamRepository.save(any(Team.class)))
                .thenReturn(expectedTeam);
        when(teamRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedTeam));

        TeamResponseDTO responseTeam = createResponse(expectedTeam);

        TeamResponseDTO actualTeam = teamService.update(teamRequestDTO, loggedUser, TEAM_ID);

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(responseTeam, actualTeam)
        );
        verify(teamRepository)
                .save(any(Team.class));
        verify(teamRepository)
                .findById(any(Long.class));
    }

    @Test
    void delete() {
        teamService.delete(TEAM_ID);

        verify(teamRepository)
                .deleteById(anyLong());
    }

    @Test
    void assign() {
        User member = createMember();
        expectedTeam.getMembers().add(member);

        when(teamRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedTeam));
        when(teamRepository.save(any(Team.class)))
                .thenReturn(expectedTeam);
        when(userRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(member));

        TeamResponseDTO teamResponseDTO = createResponse(expectedTeam);

        TeamResponseDTO actualTeam = teamService.assign(TEAM_ID, MEMBER_ID);

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(teamResponseDTO, actualTeam)
        );
        verify(teamRepository)
                .findById(any(Long.class));
        verify(teamRepository)
                .save(any(Team.class));
        verify(userRepository)
                .findById(any(Long.class));
        verify(userRepository, times(2))
                .save(any(User.class));
    }

    private User createMember() {
        User user = new User();
        user.setId(MEMBER_ID);
        user.setUsername(USERNAME);
        user.setPassword(PASSWORD);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setAdmin(true);
        user.setCreator(loggedUser);
        user.setDateCreated();
        user.setModifier(loggedUser);
        user.setDateModified();
        return user;
    }
}