package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.projects.ProjectRequestDTO;
import academy.scalefocus.projectmanagement.dtos.projects.ProjectResponseDTO;
import academy.scalefocus.projectmanagement.entities.*;
import academy.scalefocus.projectmanagement.repositories.ProjectRepository;
import academy.scalefocus.projectmanagement.repositories.TeamRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectManagementException;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectPermissionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProjectServiceImplTest {
    private static final String TITLE = "Team";
    private static final String UPDATED_TITLE = "Test team";
    private static final String DESCRIPTION = "description";
    private static final String UPDATED_DESCRIPTION = "updated description";
    private static final long PROJECT_ID = 1L;
    private static final Long TEAM_ID = 1L;
    private static final String TEAM_TITLE = "team";
    private static final String PROJECT_PERMISSION_EXCEPTION_MESSAGE = "User doesn't have access";
    private static final String PROJECT_MANAGEMENT_EXCEPTION_MESSAGE = "Please, complete all tasks";
    private static final long TASK_ID = 1L;
    private static final String TASK_TITLE = "task";
    private static final long TEAM_TO_ASSIGN_ID = 2L;
    private static final String TEAM_TO_ASSIGN_TITLE = "Team2";

    @MockBean
    private TeamRepository teamRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectServiceImpl projectService;

    private Project expectedProject;
    private User loggedUserMember;
    private User loggedUserCreator;
    private Team team;
    private Set<Team> teams = new HashSet<>();
    private Set<User> members = new HashSet<>();
    private Set<Task> tasks = new HashSet<>();
    private Task task;

    @BeforeEach
    void setUp() {
        task = createTask();
        loggedUserCreator = createUser();
        loggedUserMember = createMember();
        team = createTeam();
        expectedProject = createProject();
    }

    private Task createTask() {
        Task task = new Task();
        task.setId(TASK_ID);
        task.setTitle(TASK_TITLE);
        return task;
    }

    private User createMember() {
        User user = new User();
        user.setId(2L);
        user.setTeams(teams);
        user.setUsername("stoyan");
        return user;
    }

    private Team createTeam() {
        Team team = new Team();
        team.setId(TEAM_ID);
        team.setTitle(TEAM_TITLE);
        members.add(loggedUserMember);
        team.setMembers(members);
        return team;
    }

    private User createUser() {
        User user = new User();
        user.setId(1L);
        user.setUsername("cveta");
        user.setTeams(new HashSet<>());
        return user;
    }

    private Project createProject() {
        Project project = new Project();
        project.setId(PROJECT_ID);
        project.setTitle(TITLE);
        project.setDescription(DESCRIPTION);
        project.setCreator(loggedUserCreator);
        project.setDateCreated();
        project.setModifier(loggedUserCreator);
        project.setDateModified();
        teams.add(team);
        project.setTeams(teams);
        tasks.add(task);
        project.setTasks(tasks);
        return project;
    }

    @Test
    void getAll() {
        List<Project> projects = new ArrayList<>();
        projects.add(expectedProject);

        when(projectRepository.findAll())
                .thenReturn(projects);

        List<ProjectResponseDTO> projectResponseDTOS = projects.stream()
                .map(this::createResponse)
                .collect(Collectors.toList());

        List<ProjectResponseDTO> actualResponsesByCreator = projectService.getAll(loggedUserCreator);
        List<ProjectResponseDTO> actualResponsesByMember = projectService.getAll(loggedUserMember);

        assertAll(
                () -> assertNotNull(actualResponsesByMember),
                () -> assertNotNull(actualResponsesByCreator),
                () -> assertEquals(projectResponseDTOS, actualResponsesByCreator),
                () -> assertEquals(projectResponseDTOS, actualResponsesByMember)
        );

        verify(projectRepository, times(2))
                .findAll();
    }


    private ProjectResponseDTO createResponse(Project project) {
        ProjectResponseDTO projectResponseDTO = new ProjectResponseDTO();
        projectResponseDTO.setId(expectedProject.getId());
        projectResponseDTO.setTitle(expectedProject.getTitle());
        projectResponseDTO.setDescription(expectedProject.getDescription());
        projectResponseDTO.setCreatorId(expectedProject.getCreator().getId());
        projectResponseDTO.setDateCreated(expectedProject.getDateCreated());
        projectResponseDTO.setModifierId(expectedProject.getModifier().getId());
        projectResponseDTO.setDateModified(expectedProject.getDateModified());
        projectResponseDTO.setTeamsIds(expectedProject.getTeams()
                .stream()
                .map(Team::getId)
                .collect(Collectors.toList()));
        return projectResponseDTO;
    }

    @Test
    void save() {
        ProjectRequestDTO projectRequestDTO = new ProjectRequestDTO();
        projectRequestDTO.setTitle(TITLE);
        projectRequestDTO.setDescription(DESCRIPTION);

        when(projectRepository.save(any(Project.class)))
                .thenReturn(expectedProject);

        ProjectResponseDTO projectResponseDTO = createResponse(expectedProject);

        ProjectResponseDTO actualProject = projectService.save(projectRequestDTO, loggedUserCreator);

        assertAll(
                () -> assertNotNull(actualProject),
                () -> assertEquals(projectResponseDTO, actualProject)
        );

        verify(projectRepository)
                .save(any(Project.class));
    }

    @Test
    void assignTeamToProject() {
        String message = "";
        Team teamToAssign = new Team();
        teamToAssign.setId(TEAM_TO_ASSIGN_ID);
        teamToAssign.setTitle(TEAM_TO_ASSIGN_TITLE);
        expectedProject.getTeams().add(teamToAssign);

        when(projectRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedProject));
        when(projectRepository.save(any(Project.class)))
                .thenReturn(expectedProject);
        when(teamRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(teamToAssign));

        ProjectResponseDTO projectResponseDTO = createResponse(expectedProject);

        try {
            projectService.assignTeamToProject(PROJECT_ID, TEAM_TO_ASSIGN_ID, loggedUserMember);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        ProjectResponseDTO actualProject = projectService.assignTeamToProject(PROJECT_ID, TEAM_TO_ASSIGN_ID, loggedUserCreator);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualProject),
                () -> assertEquals(projectResponseDTO, actualProject)
        );
        verify(projectRepository, times(2))
                .findById(any(Long.class));
        verify(projectRepository)
                .save(any(Project.class));
        verify(teamRepository, times(2))
                .findById(any(Long.class));
        verify(teamRepository)
                .save(any(Team.class));
    }

    @Test
    void update() {
        String message = "";
        ProjectRequestDTO projectRequestDTO = new ProjectRequestDTO();
        projectRequestDTO.setTitle(UPDATED_TITLE);
        projectRequestDTO.setDescription(UPDATED_DESCRIPTION);

        expectedProject.setTitle(UPDATED_TITLE);
        expectedProject.setDescription(UPDATED_DESCRIPTION);

        when(projectRepository.save(any(Project.class)))
                .thenReturn(expectedProject);
        when(projectRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedProject));

        ProjectResponseDTO responseProject = createResponse(expectedProject);

        ProjectResponseDTO actualProject = projectService.update(PROJECT_ID, loggedUserCreator, projectRequestDTO);

        try {
            projectService.update(PROJECT_ID, loggedUserMember, projectRequestDTO);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualProject),
                () -> assertEquals(responseProject, actualProject)
        );
        verify(projectRepository)
                .save(any(Project.class));
        verify(projectRepository, times(2))
                .findById(any(Long.class));
    }

    @Test
    void delete() {
        String messageManagementException = "";
        String messagePermissionException = "";
        when(projectRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.ofNullable(expectedProject));

        try {
            projectService.delete(PROJECT_ID, loggedUserCreator);
        } catch (ProjectManagementException e) {
            messageManagementException = e.getMessage();
        }

        task.setStatus(TaskStatus.COMPLETED);

        try {
            projectService.delete(PROJECT_ID, loggedUserMember);
        } catch (ProjectPermissionException e) {
            messagePermissionException = e.getMessage();
        }

        projectService.delete(PROJECT_ID, loggedUserCreator);

        assertEquals(PROJECT_MANAGEMENT_EXCEPTION_MESSAGE, messageManagementException);
        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, messagePermissionException);

        verify(projectRepository)
                .deleteById(anyLong());
        verify(projectRepository, times(3))
                .findById(anyLong());
    }
}