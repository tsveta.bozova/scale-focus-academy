package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.dtos.WorkLogRequestDTO;
import academy.scalefocus.projectmanagement.dtos.WorkLogResponseDTO;
import academy.scalefocus.projectmanagement.entities.*;
import academy.scalefocus.projectmanagement.repositories.ProjectRepository;
import academy.scalefocus.projectmanagement.repositories.TaskRepository;
import academy.scalefocus.projectmanagement.repositories.WorkLogRepository;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectManagementException;
import academy.scalefocus.projectmanagement.services.exceptions.ProjectPermissionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class WorkLogServiceImplTest {
    private static final Long WORK_LOG_ID = 1L;
    private static final int HOURS_SPENT = 8;
    private static final LocalDate DATE = LocalDate.parse("2020-04-02");
    private static final LocalDate UPDATED_DATE = LocalDate.parse("2020-04-03");
    private static final long TASK_ID = 1L;
    private static final long PROJECT_ID = 1L;
    private static final long USER_ID = 1L;
    private static final String USERNAME = "cveta";
    private static final long USER_MEMBER_ID = 2L;
    private static final long USER_WITHOUT_ACCESS_ID = 3L;
    private static final long TEAM_ID = 1L;
    private static final String PROJECT_PERMISSION_EXCEPTION_MESSAGE = "User doesn't have access";
    private static final int UPDATED_HOURS_SPENT = 10;


    @MockBean
    private WorkLogRepository workLogRepository;

    @MockBean
    private TaskRepository taskRepository;

    @Autowired
    private WorkLogServiceImpl workLogService;

    private WorkLog expectedWorkLog;
    private Task task;
    private Project project;
    private User loggedUserCreator;
    private User loggedUserMember;
    private User loggedUserWithoutAccess;
    private Team team;
    private Set<User> members = new HashSet<>();
    private Set<Team> teams = new HashSet<>();
    private Set<WorkLog> workLogs = new HashSet<>();

    @BeforeEach
    void setUp() {
        loggedUserCreator = createCreator();
        loggedUserMember = createMember();
        loggedUserWithoutAccess = createUser();
        team = createTeam();
        project = createProject();
        task = createTask();
        expectedWorkLog = createWorkLog();
    }

    private Team createTeam() {
        Team team = new Team();
        team.setId(TEAM_ID);
        members.add(loggedUserMember);
        team.setMembers(members);
        return team;
    }

    private User createUser() {
        User user = new User();
        user.setId(USER_WITHOUT_ACCESS_ID);
        user.setUsername(USERNAME);
        return user;
    }

    private User createMember() {
        User user = new User();
        user.setId(USER_MEMBER_ID);
        user.setUsername(USERNAME);
        return user;
    }

    private User createCreator() {
        User user = new User();
        user.setId(USER_ID);
        user.setUsername(USERNAME);
        return user;
    }

    private Project createProject() {
        Project project = new Project();
        project.setId(PROJECT_ID);
        project.setCreator(loggedUserCreator);
        teams.add(team);
        project.setTeams(teams);
        return project;
    }

    private Task createTask() {
        Task task = new Task();
        task.setId(TASK_ID);
        task.setAssignee(loggedUserMember);
        task.setProject(project);
        return task;
    }

    private WorkLog createWorkLog() {
        WorkLog workLog = new WorkLog();
        workLog.setId(WORK_LOG_ID);
        workLog.setUser(loggedUserCreator);
        workLog.setHoursSpent(HOURS_SPENT);
        workLog.setDate(DATE);
        workLog.setTask(task);
        return workLog;
    }

    @Test
    void getAll() {
        String messagePermissionException = "";
        List<WorkLog> workLogList = new ArrayList<>();

        workLogList.add(expectedWorkLog);
        workLogs.add(expectedWorkLog);
        task.setWorkLogs(workLogs);

        when(workLogRepository.findAll())
                .thenReturn(workLogList);
        when(taskRepository.findById(TASK_ID))
                .thenReturn(java.util.Optional.of(task));


        List<WorkLogResponseDTO> workLogResponseDTOS = task.getWorkLogs().stream()
                .map(this::createResponse)
                .collect(Collectors.toList());

        System.out.println();
        try {
            workLogService.getAll(TASK_ID, loggedUserWithoutAccess);
        } catch (ProjectPermissionException e) {
            messagePermissionException = e.getMessage();
        }


        System.out.println();
        List<WorkLogResponseDTO> actualResponsesByCreator = workLogService.getAll(TASK_ID, loggedUserCreator);
        List<WorkLogResponseDTO> actualResponsesByMember = workLogService.getAll(TASK_ID, loggedUserMember);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, messagePermissionException);
        assertAll(
                () -> assertNotNull(actualResponsesByMember),
                () -> assertNotNull(actualResponsesByCreator),
                () -> assertEquals(workLogResponseDTOS, actualResponsesByCreator),
                () -> assertEquals(workLogResponseDTOS, actualResponsesByMember)
        );

        verify(taskRepository, times(3))
                .findById(anyLong());
    }

    private WorkLogResponseDTO createResponse(WorkLog workLog) {
        WorkLogResponseDTO workLogResponseDTO = new WorkLogResponseDTO();
        workLogResponseDTO.setId(expectedWorkLog.getId());
        workLogResponseDTO.setUserId(expectedWorkLog.getUser().getId());
        workLogResponseDTO.setTaskId(expectedWorkLog.getTask().getId());
        workLogResponseDTO.setHoursSpent(expectedWorkLog.getHoursSpent());
        workLogResponseDTO.setDate(expectedWorkLog.getDate());
        return workLogResponseDTO;
    }

    @Test
    void save() {
        String message = "";
        workLogs.add(expectedWorkLog);
        task.setWorkLogs(workLogs);
        WorkLogRequestDTO workLogRequestDTO = new WorkLogRequestDTO();
        workLogRequestDTO.setHoursSpent(HOURS_SPENT);
        workLogRequestDTO.setDate(DATE);

        when(taskRepository.findById(TASK_ID))
                .thenReturn(java.util.Optional.of(task));
        when(workLogRepository.save(any(WorkLog.class)))
                .thenReturn(expectedWorkLog);

        WorkLogResponseDTO workLogResponseDTO = createResponse(expectedWorkLog);

        try {
            workLogService.save(workLogRequestDTO, TASK_ID, loggedUserWithoutAccess);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        WorkLogResponseDTO actualTask = workLogService.save(workLogRequestDTO, TASK_ID, loggedUserCreator);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualTask),
                () -> assertEquals(workLogResponseDTO, actualTask)
        );

        verify(workLogRepository)
                .save(any(WorkLog.class));
    }

    @Test
    void update() {
        String message = "";
        WorkLogRequestDTO workLogRequestDTO = new WorkLogRequestDTO();
        workLogRequestDTO.setHoursSpent(UPDATED_HOURS_SPENT);
        workLogRequestDTO.setDate(UPDATED_DATE);

        expectedWorkLog.setHoursSpent(UPDATED_HOURS_SPENT);
        expectedWorkLog.setDate(UPDATED_DATE);

        when(workLogRepository.save(any(WorkLog.class)))
                .thenReturn(expectedWorkLog);
        when(taskRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(task));
        when(workLogRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedWorkLog));

        WorkLogResponseDTO responseWorkLog = createResponse(expectedWorkLog);

        WorkLogResponseDTO actualWorkLogByCreator = workLogService.update(workLogRequestDTO, WORK_LOG_ID, TASK_ID, loggedUserCreator);
        WorkLogResponseDTO actualWorkLogByMember = workLogService.update(workLogRequestDTO, WORK_LOG_ID, TASK_ID, loggedUserMember);

        try {
            workLogService.update(workLogRequestDTO, WORK_LOG_ID, TASK_ID, loggedUserWithoutAccess);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);
        assertAll(
                () -> assertNotNull(actualWorkLogByCreator),
                () -> assertNotNull(actualWorkLogByMember),
                () -> assertEquals(responseWorkLog, actualWorkLogByCreator),
                () -> assertEquals(responseWorkLog, actualWorkLogByMember)
        );
        verify(workLogRepository, times(2))
                .save(any(WorkLog.class));
        verify(taskRepository, times(3))
                .findById(any(Long.class));
    }

    @Test
    void delete() {
        String message = "";

        when(taskRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(task));

        try {
            workLogService.delete(TASK_ID, WORK_LOG_ID, loggedUserWithoutAccess);
        } catch (ProjectPermissionException e) {
            message = e.getMessage();
        }

        workLogService.delete(TASK_ID, PROJECT_ID, loggedUserCreator);
        workLogService.delete(TASK_ID, PROJECT_ID, loggedUserMember);

        assertEquals(PROJECT_PERMISSION_EXCEPTION_MESSAGE, message);

        verify(workLogRepository, times(2))
                .deleteById(anyLong());
        verify(taskRepository, times(3))
                .findById(anyLong());
    }
}