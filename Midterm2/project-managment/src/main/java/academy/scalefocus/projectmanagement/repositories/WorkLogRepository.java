package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.WorkLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkLogRepository extends JpaRepository<WorkLog, Long> {
}
