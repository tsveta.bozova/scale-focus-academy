package academy.scalefocus.projectmanagement.dtos.teams;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

public class TeamResponseDTO {
    private Long id;
    private String title;
    private Long creatorId;
    private Instant dateCreated;
    private Long modifierId;
    private Instant dateModified;
    private List<Long> membersIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    public void setDateModified(Instant dateModified) {
        this.dateModified = dateModified;
    }

    public List<Long> getMembersIds() {
        return membersIds;
    }

    public void setMembersIds(List<Long> membersIds) {
        this.membersIds = membersIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TeamResponseDTO)) return false;
        TeamResponseDTO that = (TeamResponseDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(modifierId, that.modifierId) &&
                Objects.equals(dateModified, that.dateModified) &&
                Objects.equals(membersIds, that.membersIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, creatorId, dateCreated, modifierId, dateModified, membersIds);
    }
}
