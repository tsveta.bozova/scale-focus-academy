package academy.scalefocus.projectmanagement.dtos;

import academy.scalefocus.projectmanagement.entities.TaskStatus;

import java.util.Objects;

public class TaskRequestDTO {
    private String title;
    private String description;
    private Long assigneeId;
    private TaskStatus status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Long assigneeId) {
        this.assigneeId = assigneeId;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskRequestDTO)) return false;
        TaskRequestDTO that = (TaskRequestDTO) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(assigneeId, that.assigneeId) &&
                status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description, assigneeId, status);
    }
}
