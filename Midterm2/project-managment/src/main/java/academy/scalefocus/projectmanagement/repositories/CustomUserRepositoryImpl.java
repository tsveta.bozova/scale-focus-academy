package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
public class CustomUserRepositoryImpl implements CustomUserRepository {
    private EntityManager entityManager;

    @Autowired
    public CustomUserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void updateCreatorModifier(User user, User admin) {
        Query query = entityManager.createQuery("Update User u set u.creator = ?1, u.modifier = ?2 " +
                "where u.creator = ?3 or u.modifier = ?4");
        query.setParameter(1, admin);
        query.setParameter(2, admin);
        query.setParameter(3, user);
        query.setParameter(4, user);

        query.executeUpdate();
    }
}
