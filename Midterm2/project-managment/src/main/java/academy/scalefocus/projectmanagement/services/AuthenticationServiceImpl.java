package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.security.CustomUserPrincipal;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private UserRepository userRepository;

    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createLoggedUser() {
        CustomUserPrincipal userPrincipal = (CustomUserPrincipal) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User is not in the database"));
    }
}
