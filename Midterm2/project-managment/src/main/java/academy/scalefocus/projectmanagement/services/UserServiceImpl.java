package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.users.LoginRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.LoginResponseDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.repositories.UserRepository;
import academy.scalefocus.projectmanagement.services.exceptions.InvalidCredentialsException;
import academy.scalefocus.projectmanagement.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepo;
    private final TokenService tokenService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepo, TokenService tokenService,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.tokenService = tokenService;
        this.userRepo = userRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;

        if (userRepo.count() == 0) {
            User user = initUser();
            userRepo.save(user);
        }
    }

    private User initUser() {
        User user = new User();
        user.setUsername("admin");
        user.setPassword(bCryptPasswordEncoder.encode("adminpass"));
        user.setFirstName("administrator");
        user.setLastName("administrator");
        user.setAdmin(true);
        return user;
    }

    @Override
    public LoginResponseDTO login(LoginRequestDTO loginRequestDTO) {
        User user = this.userRepo.findByUsername(loginRequestDTO.getUsername())
                .orElseThrow(() -> new InvalidCredentialsException("Invalid login credentials"));

        if(!bCryptPasswordEncoder.matches(loginRequestDTO.getPassword(), user.getPassword())) {
            throw new InvalidCredentialsException("Invalid login credentials");
        }

        return createLoginResponse(user);
    }

    private LoginResponseDTO createLoginResponse(User user) {
        LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
        loginResponseDTO.setToken(
                tokenService.generate(user)
        );
        return loginResponseDTO;
    }

    @Override
    public UserResponseDTO save(UserRequestDTO userRequestDTO, User loggedUser) {
        User user = createUserFromDTO(userRequestDTO, loggedUser);
        return createUserToDisplay(userRepo.save(user));
    }

    private UserResponseDTO createUserToDisplay(User user) {
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(user.getId());
        userResponseDTO.setUsername(user.getUsername());
        userResponseDTO.setFirstName(user.getFirstName());
        userResponseDTO.setLastName(user.getLastName());
        userResponseDTO.setAdmin(user.isAdmin());
        userResponseDTO.setCreatorId(user.getCreator().getId());
        userResponseDTO.setDateCreated(user.getDateCreated());
        if(user.getDateModified() != null) {
            userResponseDTO.setModifierId(user.getModifier().getId());
            userResponseDTO.setDateModified(user.getDateModified());
        }
        return userResponseDTO;
    }

    private User createUserFromDTO(UserRequestDTO userRequestDTO, User loggedUser) {
        User user = new User();
        String password = bCryptPasswordEncoder.encode(userRequestDTO.getPassword());
        user.setUsername(userRequestDTO.getUsername());
        user.setPassword(password);
        user.setFirstName(userRequestDTO.getFirstName());
        user.setLastName(userRequestDTO.getLastName());
        user.setAdmin(userRequestDTO.isAdmin());
        user.setCreator(loggedUser);
        return user;
    }

    @Override
    @Transactional
    public UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long userId, User loggedUser) {
        User userToUpdate = userRepo.findById(userId)
                .orElseThrow(() ->  new ResourceNotFoundException("User is not in the database"));

        String password = bCryptPasswordEncoder.encode(userRequestDTO.getPassword());

        userToUpdate.setUsername(userRequestDTO.getUsername());
        userToUpdate.setPassword(password);
        userToUpdate.setFirstName(userRequestDTO.getFirstName());
        userToUpdate.setLastName(userRequestDTO.getLastName());
        userToUpdate.setAdmin(userRequestDTO.isAdmin());
        userToUpdate.setModifier(loggedUser);

        userRepo.save(userToUpdate);

        return createUserToDisplay(userToUpdate);
    }

    @Override
    @Transactional
    public void deleteUser(Long userId) {
        User user = this.userRepo.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in database"));

        User admin = userRepo.findByUsername("admin")
                .orElseThrow(() -> new ResourceNotFoundException("User is not in database"));

        if(!bCryptPasswordEncoder.matches("adminpass", user.getPassword())) {
            throw new InvalidCredentialsException("Invalid login credentials");
        }

        userRepo.updateCreatorModifier(user, admin);

        userRepo.deleteById(userId);
    }

    @Override
    public List<UserResponseDTO> getAll() {
        return userRepo.findAll()
                .stream()
                .filter(u -> !u.getUsername().equals("admin"))
                .map(this::createUserToDisplay)
                .collect(Collectors.toList());
    }
}
