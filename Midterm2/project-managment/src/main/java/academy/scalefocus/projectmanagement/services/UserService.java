package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.users.LoginRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.LoginResponseDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserRequestDTO;
import academy.scalefocus.projectmanagement.dtos.users.UserResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;

import java.util.List;

public interface UserService {
    LoginResponseDTO login(LoginRequestDTO loginRequestDTO);
    UserResponseDTO save(UserRequestDTO userRequestDTO, User loggedUser);
    UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long userId, User loggedUser);
    void deleteUser(Long userId);
    List<UserResponseDTO> getAll();
}
