package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.WorkLogRequestDTO;
import academy.scalefocus.projectmanagement.dtos.WorkLogResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.WorkLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WorkLogController {
    private WorkLogService workLogService;
    private AuthenticationService authenticationService;

    @Autowired
    public WorkLogController(WorkLogService workLogService, AuthenticationService authenticationService) {
        this.workLogService = workLogService;
        this.authenticationService = authenticationService;
    }

    @GetMapping("/tasks/{taskId}/work-logs")
    public List<WorkLogResponseDTO> getAll (@PathVariable Long taskId) {
        User loggedUser = authenticationService.createLoggedUser();
        return workLogService.getAll(taskId, loggedUser);
    }

    @PostMapping("/tasks/{taskId}/work-logs")
    public WorkLogResponseDTO create(@PathVariable Long taskId,
                                     @RequestBody WorkLogRequestDTO workLogRequestDTO) {
        User loggedUser = authenticationService.createLoggedUser();
        return workLogService.save(workLogRequestDTO, taskId, loggedUser);
    }

    @PutMapping("/tasks/{taskId}/work-logs/{workLogId}")
    public WorkLogResponseDTO update(@PathVariable Long taskId,
                                     @PathVariable Long workLogId,
                                     @RequestBody WorkLogRequestDTO workLogRequestDTO) {
        User loggedUser = authenticationService.createLoggedUser();
        return workLogService.update(workLogRequestDTO, workLogId, taskId, loggedUser);
    }

    @DeleteMapping("/tasks/{taskId}/work-logs/{workLogId}")
    public void delete(@PathVariable Long taskId,
                       @PathVariable Long workLogId) {
        User loggedUser = authenticationService.createLoggedUser();
        workLogService.delete(taskId, workLogId, loggedUser);
    }
}
