package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.teams.TeamRequestDTO;
import academy.scalefocus.projectmanagement.dtos.teams.TeamResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@PreAuthorize("hasRole('ADMIN')")
public class TeamController {
    private TeamService teamService;
    private AuthenticationService authenticationService;

    @Autowired
    public TeamController(TeamService teamService, AuthenticationService authenticationService) {
        this.teamService = teamService;
        this.authenticationService = authenticationService;
    }

    @GetMapping ("/teams")
    public List<TeamResponseDTO> getAll () {
        return teamService.getAll();
    }

    @PostMapping("/teams")
    public TeamResponseDTO create (@RequestBody TeamRequestDTO teamRequestDTO) {
        User loggedUser = authenticationService.createLoggedUser();
        return teamService.save(teamRequestDTO, loggedUser);
    }

    @PutMapping("/teams/{teamId}")
    public TeamResponseDTO update (@PathVariable Long teamId,
                                   @RequestBody TeamRequestDTO teamRequestDTO) {
        User loggedUser = authenticationService.createLoggedUser();
        return teamService.update(teamRequestDTO, loggedUser, teamId);
    }

    @DeleteMapping ("/teams/{teamId}")
    public void delete (@PathVariable Long teamId) {
        teamService.delete(teamId);
    }

    @PutMapping ("/teams/{teamId}/users/{userId}")
    public TeamResponseDTO assignUserToTeam (@PathVariable Long teamId, @PathVariable Long userId) {
        return teamService.assign(teamId, userId);
    }
}
