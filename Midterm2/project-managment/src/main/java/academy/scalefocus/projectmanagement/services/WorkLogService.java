package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.dtos.WorkLogRequestDTO;
import academy.scalefocus.projectmanagement.dtos.WorkLogResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;

import java.util.List;

public interface WorkLogService {
    List<WorkLogResponseDTO> getAll(Long taskId, User loggedUser);

    WorkLogResponseDTO save(WorkLogRequestDTO workLogRequestDTO, Long taskId, User loggedUser);

    WorkLogResponseDTO update(WorkLogRequestDTO workLogRequestDTO,
                              Long workLogId, Long taskId, User loggedUser);

    void delete(Long taskId, Long workLogId, User loggedUser);
}
