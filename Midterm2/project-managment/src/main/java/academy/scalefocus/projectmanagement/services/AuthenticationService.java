package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.entities.User;

public interface AuthenticationService {
    User createLoggedUser();
}
