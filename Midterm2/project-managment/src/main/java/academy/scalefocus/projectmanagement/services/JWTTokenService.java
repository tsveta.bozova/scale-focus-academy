package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.security.CustomUserPrincipal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class JWTTokenService implements TokenService {

    public static final String JWT_SECRET = "Tw0QWZZXKf4ooPIRcaitzaUBqScRRzAK";

    @Override
    public String generate(User user) {
        Instant expirationDate = Instant.now().plus(1, ChronoUnit.HOURS);
        Date date = Date.from(expirationDate);

        Key key = Keys.hmacShaKeyFor(JWT_SECRET.getBytes());

        String token = Jwts.builder()
                .claim("id", user.getId())
                .claim("sub", user.getUsername())
                .claim("admin", user.isAdmin())
                .setExpiration(date)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
        return "Bearer " + token;
    }

    @Override
    public CustomUserPrincipal parseToken(String token) {
        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(JWT_SECRET.getBytes())
                .build()
                .parseClaimsJws(token);

        Claims body = claimsJws.getBody();

        String username = body
                .getSubject();

        Long id = body
                .get("id", Long.class);

        boolean isAdmin = body
                .get("admin", Boolean.class);

        return new CustomUserPrincipal(id, username, isAdmin);
    }
}
