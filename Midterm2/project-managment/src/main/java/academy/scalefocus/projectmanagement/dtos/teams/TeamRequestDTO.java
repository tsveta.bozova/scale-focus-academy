package academy.scalefocus.projectmanagement.dtos.teams;

import java.util.Objects;

public class TeamRequestDTO {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TeamRequestDTO)) return false;
        TeamRequestDTO that = (TeamRequestDTO) o;
        return Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
