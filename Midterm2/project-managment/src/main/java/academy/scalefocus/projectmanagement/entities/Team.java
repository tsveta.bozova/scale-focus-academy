package academy.scalefocus.projectmanagement.entities;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "teams")
public class Team {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @ManyToOne
    @JoinColumn (name = "creator_id", referencedColumnName = "id")
    private User creator;
    @Column(name = "date_created")
    private Instant dateCreated;
    @ManyToOne
    @JoinColumn (name = "modifier_id", referencedColumnName = "id")
    private User modifier;
    @Column(name = "date_modified")
    private Instant dateModified;
    @ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
    @JoinTable(name = "team_member",
    joinColumns = @JoinColumn (name = "team_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn (name = "user_id", referencedColumnName = "id"))
    private Set<User> members = new HashSet<>();
    @ManyToMany (mappedBy = "teams", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
    private Set<Project> projects = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @PrePersist
    public void setDateCreated() {
        this.dateCreated = Instant.now();
    }

    public User getModifier() {
        return modifier;
    }

    public void setModifier(User modifier) {
        this.modifier = modifier;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    @PreUpdate
    public void setDateModified() {
        this.dateModified = Instant.now();
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
}
