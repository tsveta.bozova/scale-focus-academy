package academy.scalefocus.projectmanagement.controllers;

import academy.scalefocus.projectmanagement.dtos.TaskRequestDTO;
import academy.scalefocus.projectmanagement.dtos.TaskResponseDTO;
import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.services.AuthenticationService;
import academy.scalefocus.projectmanagement.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController {
    private TaskService taskService;
    private AuthenticationService authenticationService;

    @Autowired
    public TaskController(TaskService taskService, AuthenticationService authenticationService) {
        this.taskService = taskService;
        this.authenticationService = authenticationService;
    }

    @GetMapping("/projects/{projectId}/tasks")
    public List<TaskResponseDTO> getAll (@PathVariable Long projectId) {
        User loggedUser = authenticationService.createLoggedUser();
        return taskService.getAll(projectId, loggedUser);
    }

    @PostMapping("/projects/{projectId}/tasks")
    public TaskResponseDTO create(@PathVariable Long projectId,
                                  @RequestBody TaskRequestDTO taskRequestDTO) {
        User loggedUser = authenticationService.createLoggedUser();
        return taskService.save(taskRequestDTO, projectId, loggedUser);
    }

    @PutMapping("/projects/{projectId}/tasks/{taskId}")
    public TaskResponseDTO update(@PathVariable Long projectId,
                                  @PathVariable Long taskId,
                                  @RequestBody TaskRequestDTO taskRequestDTO) {
        User loggedUser = authenticationService.createLoggedUser();
        return taskService.update(taskId, taskRequestDTO, projectId, loggedUser);
    }

    @DeleteMapping("/projects/{projectId}/tasks/{taskId}")
    public void delete (@PathVariable Long projectId,
                        @PathVariable Long taskId) {
        User loggedUser = authenticationService.createLoggedUser();
        taskService.delete(taskId, projectId, loggedUser);
    }

    @PutMapping("/tasks/{taskId}/users/{userId}")
    public TaskResponseDTO changeAssignee (@PathVariable Long taskId,
                                           @PathVariable Long userId) {
        User loggedUser = authenticationService.createLoggedUser();
        return taskService.changeAssignee(taskId, userId, loggedUser);
    }

    @PutMapping("/tasks/{taskId}/status")
    public TaskResponseDTO updateStatus (@PathVariable Long taskId,
                                         @RequestBody String status) {
        User loggedUser = authenticationService.createLoggedUser();
        return taskService.updateStatus(taskId, status, loggedUser);
    }
}
