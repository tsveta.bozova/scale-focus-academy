package academy.scalefocus.projectmanagement.services;

import academy.scalefocus.projectmanagement.entities.User;
import academy.scalefocus.projectmanagement.security.CustomUserPrincipal;

public interface TokenService {
    String generate(User user);

    CustomUserPrincipal parseToken(String token);
}
