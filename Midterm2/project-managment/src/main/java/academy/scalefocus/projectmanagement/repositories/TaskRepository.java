package academy.scalefocus.projectmanagement.repositories;

import academy.scalefocus.projectmanagement.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
