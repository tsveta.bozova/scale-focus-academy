package academy.scalefocus.projectmanagement.dtos;

import java.time.LocalDate;
import java.util.Objects;

public class WorkLogRequestDTO {
    private int hoursSpent;
    private LocalDate date;

    public int getHoursSpent() {
        return hoursSpent;
    }

    public void setHoursSpent(int hoursSpent) {
        this.hoursSpent = hoursSpent;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkLogRequestDTO)) return false;
        WorkLogRequestDTO that = (WorkLogRequestDTO) o;
        return hoursSpent == that.hoursSpent &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hoursSpent, date);
    }
}
