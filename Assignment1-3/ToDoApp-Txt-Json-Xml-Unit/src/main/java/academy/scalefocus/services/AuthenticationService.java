package academy.scalefocus.services;

import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.json.UserRepositoryJsonImpl;
import academy.scalefocus.repositories.txt.UserRepositoryTxtImpl;
import academy.scalefocus.repositories.xml.UserRepositoryXmlImpl;

public class AuthenticationService {

    private static AuthenticationService instance = null;


    public static AuthenticationService getInstance() {

        if (AuthenticationService.instance == null)
            AuthenticationService.instance = new AuthenticationService();

        return AuthenticationService.instance;
    }

    private AuthenticationService() {

    }

    private User authenticatedUser = null;

    public User getLoggedUser() {
        return authenticatedUser;
    }

    public void authenticateUser(String username, String password) {

        UserRepository userRepoTxt = new UserRepositoryTxtImpl("users.txt");
        UserRepository userRepoJson = new UserRepositoryJsonImpl("users.json");
        UserRepository userRepoXml = new UserRepositoryXmlImpl();


        this.authenticatedUser = userRepoXml.getByUserNameAndPassword(username, password);
    }
}

