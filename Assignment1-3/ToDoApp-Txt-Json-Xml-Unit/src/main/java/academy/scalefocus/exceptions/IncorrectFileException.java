package academy.scalefocus.exceptions;

public class IncorrectFileException extends RuntimeException {

    public IncorrectFileException(Exception exception) {
        super("File not found!", exception);
    }
}
