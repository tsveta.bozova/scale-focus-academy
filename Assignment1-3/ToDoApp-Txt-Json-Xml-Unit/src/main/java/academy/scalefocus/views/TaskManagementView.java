package academy.scalefocus.views;


import academy.scalefocus.entities.Task;
import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.TaskRepository;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.json.TaskRepositoryJsonImpl;
import academy.scalefocus.repositories.json.UserRepositoryJsonImpl;
import academy.scalefocus.repositories.txt.TaskRepositoryTxtImpl;
import academy.scalefocus.repositories.txt.UserRepositoryTxtImpl;
import academy.scalefocus.repositories.xml.TaskRepositoryXmlImpl;
import academy.scalefocus.repositories.xml.UserRepositoryXmlImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class TaskManagementView {

    private ToDoList parent;
    private int loggedUserId;

    public TaskManagementView(ToDoList parent) {
        this.parent = parent;
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
    }

    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            switch (choice) {
                case LIST:
                    list();
                    break;
                case ADD:
                    add();
                    break;
                case EDIT:
                    edit();
                    break;
                case COMPLETE:
                    complete();
                    break;
                case ASSIGN:
                    assign();
                    break;
                case DELETE:
                    delete();
                    break;
                case EXIT:
                    return;
                default:
                    break;
            }
        }
    }


    private MenuChoices renderMenu() {

        while (true) {

            ConsoleManager.clear();

            ConsoleManager.writeLine("[L]ist Tasks");
            ConsoleManager.writeLine("[A]dd Task");
            ConsoleManager.writeLine("[E]dit Task");
            ConsoleManager.writeLine("[C]omplete task");
            ConsoleManager.writeLine("Assig[n] a Task");
            ConsoleManager.writeLine("[D]elete Task");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");

            String choice = ConsoleManager.readLine();

            switch (choice.toUpperCase()) {
                case "L":
                    return MenuChoices.LIST;
                case "A":
                    return MenuChoices.ADD;
                case "E":
                    return MenuChoices.EDIT;
                case "C":
                    return MenuChoices.COMPLETE;
                case "N":
                    return MenuChoices.ASSIGN;
                case "D":
                    return MenuChoices.DELETE;
                case "X":
                    return MenuChoices.EXIT;
                default:
                    ConsoleManager.clear();
                    ConsoleManager.writeLine("Invalid choice!");
                    ConsoleManager.writeLine("Press [Enter] to continue");
                    ConsoleManager.readLine();
                    break;
            }
        }
    }

    private void add() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add Task####");

        Task task = new Task();

        ConsoleManager.write("Title: ");

        task.setToDoListId(parent.getId());
        task.setTitle(ConsoleManager.readLine());

        ConsoleManager.write("Description: ");
        task.setDescription(ConsoleManager.readLine());

        task.setDateCreated(LocalDateTime.now());
        task.setIsComplete(false);
        task.setDateCreated(LocalDateTime.now());
        task.setCreatorId(this.loggedUserId);
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);
        task.setAssignUsersIds("not assigned");

        TaskRepository taskRepoTxt = new TaskRepositoryTxtImpl("tasks.txt");
        TaskRepository taskRepoJson = new TaskRepositoryJsonImpl("tasks.json");
        TaskRepository taskRepoXml = new TaskRepositoryXmlImpl();

        taskRepoXml.add(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item added successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void list() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####List All Tasks####");

        TaskRepositoryTxtImpl taskRepoTxt = new TaskRepositoryTxtImpl("tasks.txt");
        TaskRepository taskRepoJson = new TaskRepositoryJsonImpl("tasks.json");
        TaskRepository taskRepoXml = new TaskRepositoryXmlImpl();

        List<Task> tasks = taskRepoXml.getAll(parent.getId());

        for (Task task : tasks) {

            ConsoleManager.write("ID: ");
            ConsoleManager.writeLine(task.getId());
            ConsoleManager.write("Title: ");
            ConsoleManager.writeLine(task.getTitle());
            ConsoleManager.write("ToDoList ID: ");
            ConsoleManager.writeLine(task.getToDoListId());
            ConsoleManager.write("Description:");
            ConsoleManager.writeLine(task.getDescription());
            ConsoleManager.write("Complete:");
            ConsoleManager.writeLine(task.getIsComplete());
            ConsoleManager.write("Created: ");
            ConsoleManager.writeLine(task.getDateCreated());
            ConsoleManager.write("Creator ID: ");
            ConsoleManager.writeLine(task.getCreatorId());
            ConsoleManager.write("Last User made changes: ");
            ConsoleManager.writeLine(task.getModifierId());
            ConsoleManager.write("Date of last change: ");
            ConsoleManager.writeLine(task.getDateModified());
            ConsoleManager.writeLine("---------------------------------");
        }

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void assign() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Assign Task####");

        TaskRepository taskRepoTxt = new TaskRepositoryTxtImpl("tasks.txt");
        TaskRepository taskRepoJson = new TaskRepositoryJsonImpl("tasks.json");
        TaskRepository taskRepoXml = new TaskRepositoryXmlImpl();

        List<Task> tasks = taskRepoXml.getAll(this.loggedUserId);

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Tasks: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        Task task = taskRepoXml.getById(id);

        ConsoleManager.clear();

        UserRepository userRepoTxt = new UserRepositoryTxtImpl("users.txt");
        UserRepository userRepoJson = new UserRepositoryJsonImpl("users.json");
        UserRepository userRepoXml = new UserRepositoryXmlImpl();

        List<User> users = userRepoXml.getAll();

        for (User user : users) {

            ConsoleManager.write(user.getUsername() + " ( " + user.getId() + " )\n");

        }

        ConsoleManager.write("Assign to: ");
        String assignId = ConsoleManager.readLine();

        boolean isShared = isToDoListSharedWithUser(assignId);

        if (isShared) {
            if (!"not assigned".equalsIgnoreCase(task.getAssignUsersIds())) {
                String ids = String.join(",", task.getAssignUsersIds(), assignId);
                task.setAssignUsersIds(ids);
            } else {
                task.setAssignUsersIds(assignId);
            }
        } else {
            ConsoleManager.writeLine("The User doesn't have access to the List task belongs to.");
            ConsoleManager.writeLine("Press [Enter] to continue");
            ConsoleManager.readLine();
            return;
        }

        taskRepoXml.edit(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();

    }

    private void edit() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Task####");

        TaskRepository taskRepoTxt = new TaskRepositoryTxtImpl("tasks.txt");
        TaskRepository taskRepoJson = new TaskRepositoryJsonImpl("tasks.json");
        TaskRepository taskRepoXml = new TaskRepositoryXmlImpl();

        List<Task> tasks = taskRepoXml.getAll();

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        Task task = taskRepoXml.getById(id);

        ConsoleManager.write("Title ( " + task.getTitle() + " ): ");
        task.setTitle(ConsoleManager.readLine());

        ConsoleManager.write("Description ( " + task.getDescription() + " ): ");
        task.setDescription(ConsoleManager.readLine());

        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        taskRepoXml.edit(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void complete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Complete Task####");

        TaskRepository taskRepoTxt = new TaskRepositoryTxtImpl("tasks.txt");
        TaskRepository taskRepoJson = new TaskRepositoryJsonImpl("tasks.json");
        TaskRepository taskRepoXml = new TaskRepositoryXmlImpl();

        List<Task> tasks = taskRepoXml.getAll();

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        Task task = taskRepoXml.getById(id);

        ConsoleManager.write("Title ( " + task.getIsComplete() + " ): ");
        task.setIsComplete(Boolean.parseBoolean(ConsoleManager.readLine()));
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        taskRepoXml.edit(task);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();

    }

    private void delete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Task####");

        TaskRepository taskRepoTxt = new TaskRepositoryTxtImpl("tasks.txt");
        TaskRepository taskRepoJson = new TaskRepositoryJsonImpl("tasks.json");
        TaskRepository taskRepoXml = new TaskRepositoryXmlImpl();

        List<Task> tasks = taskRepoXml.getAll();

        for (Task task : tasks) {

            ConsoleManager.write(task.getTitle() + " ( " + task.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        Task task = taskRepoXml.getById(id);
        taskRepoXml.delete(task, this.loggedUserId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item deleted successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private boolean isToDoListSharedWithUser(String userId) {
        String[] ids = parent.getSharedUsersIds().split(",");
        for (String s : ids) {
            if (userId.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }
}
