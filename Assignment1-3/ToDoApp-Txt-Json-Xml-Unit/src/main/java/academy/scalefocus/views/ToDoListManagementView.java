package academy.scalefocus.views;


import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.ToDoListRepository;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.json.ToDoListRepositoryJsonImpl;
import academy.scalefocus.repositories.json.UserRepositoryJsonImpl;
import academy.scalefocus.repositories.txt.ToDoListRepositoryTxtImpl;
import academy.scalefocus.repositories.xml.ToDoListRepositoryXmlImpl;
import academy.scalefocus.repositories.xml.UserRepositoryXmlImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class ToDoListManagementView {
    private int loggedUserId;

    public ToDoListManagementView() {
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
    }

    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            switch (choice) {

                case LIST:
                    list();
                    break;
                case ADD:
                    add();
                    break;
                case EDIT:
                    edit();
                    break;
                case DELETE:
                    delete();
                    break;
                case SHARE:
                    share();
                    break;
                case VIEW:
                    view();
                    break;
                case EXIT:
                    return;
                default:
                    break;
            }
        }
    }

    private MenuChoices renderMenu() {

        while (true) {

            ConsoleManager.clear();

            ConsoleManager.writeLine("[L]ist ToDoLists");
            ConsoleManager.writeLine("[A]dd ToDoLIst");
            ConsoleManager.writeLine("[E]dit ToDoList");
            ConsoleManager.writeLine("[D]elete ToDoList");
            ConsoleManager.writeLine("[S]hare ToDoList");
            ConsoleManager.writeLine("[V]iew ToDoList");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");

            String choice = ConsoleManager.readLine();

            switch (choice.toUpperCase()) {
                case "L":
                    return MenuChoices.LIST;
                case "A":
                    return MenuChoices.ADD;
                case "E":
                    return MenuChoices.EDIT;
                case "D":
                    return MenuChoices.DELETE;
                case "S":
                    return MenuChoices.SHARE;
                case "V":
                    return MenuChoices.VIEW;
                case "X":
                    return MenuChoices.EXIT;
                default:
                    ConsoleManager.clear();
                    ConsoleManager.writeLine("Invalid choice!");
                    ConsoleManager.writeLine("Press [Enter] to continue");
                    ConsoleManager.readLine();
                    break;
            }
        }
    }

    private void add() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add ToDoList####");

        ToDoList toDoList = new ToDoList();

        ConsoleManager.write("Title: ");

        toDoList.setCreatorId(this.loggedUserId);
        toDoList.setTitle(ConsoleManager.readLine());
        toDoList.setDateCreated(LocalDateTime.now());
        toDoList.setDateModified(LocalDateTime.now());
        toDoList.setModifierId(this.loggedUserId);
        toDoList.setSharedUsersIds("not shared");


        ToDoListRepository toDoRepoTxt = new ToDoListRepositoryTxtImpl("todoLists.txt");
        ToDoListRepository toDoRepoJson = new ToDoListRepositoryJsonImpl("toDoLists.json");
        ToDoListRepository toDoRepoXml = new ToDoListRepositoryXmlImpl();

        toDoRepoXml.add(toDoList);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item added successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void list() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####List ToDoLists####");

        ToDoListRepository toDoRepoTxt = new ToDoListRepositoryTxtImpl("todoLists.txt");
        ToDoListRepository toDoRepoJson = new ToDoListRepositoryJsonImpl("toDoLists.json");
        ToDoListRepository toDoRepoXml = new ToDoListRepositoryXmlImpl();

        List<ToDoList> lists = toDoRepoXml.getAll(this.loggedUserId);

        if(lists.isEmpty()) {
            ConsoleManager.writeLine("You don't have ToDoLists");
            ConsoleManager.writeLine("Press [Enter] to continue");
            ConsoleManager.readLine();
            return;
        }

        for (ToDoList list : lists) {

            ConsoleManager.write("ID: ");
            ConsoleManager.writeLine(list.getId());
            ConsoleManager.write("Title: ");
            ConsoleManager.writeLine(list.getTitle());
            ConsoleManager.write("Created: ");
            ConsoleManager.writeLine(list.getDateCreated());
            ConsoleManager.write("Creator ID: ");
            ConsoleManager.writeLine(list.getCreatorId());
            ConsoleManager.write("Date of last change: ");
            ConsoleManager.writeLine(list.getDateCreated());
            ConsoleManager.write("Modifier ID: ");
            ConsoleManager.writeLine(list.getModifierId());
            ConsoleManager.write("Shared IDs: ");
            ConsoleManager.writeLine(list.getSharedUsersIds());
            ConsoleManager.writeLine("---------------------------------");
        }

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void edit() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit ToDoList####");

        ToDoListRepository toDoRepoTxt = new ToDoListRepositoryTxtImpl("todoLists.txt");
        ToDoListRepository toDoRepoJson = new ToDoListRepositoryJsonImpl("toDoLists.json");
        ToDoListRepository toDoRepoXml = new ToDoListRepositoryXmlImpl();

        List<ToDoList> lists = toDoRepoXml.getAll();

        for (ToDoList toDoList : lists) {

            ConsoleManager.write(toDoList.getTitle() + " ( " + toDoList.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        ToDoList list = toDoRepoXml.getById(id);

        ConsoleManager.write("Title ( " + list.getTitle() + " ): ");list.setTitle(ConsoleManager.readLine());

        list.setDateModified(LocalDateTime.now());
        list.setModifierId(this.loggedUserId);

        toDoRepoXml.edit(list);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void delete() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete ToDoList####");

        ToDoListRepository toDoRepoTxt = new ToDoListRepositoryTxtImpl("todoLists.txt");
        ToDoListRepository toDoRepoJson = new ToDoListRepositoryJsonImpl("toDoLists.json");
        ToDoListRepository toDoRepoXml = new ToDoListRepositoryXmlImpl();

        List<ToDoList> lists = toDoRepoXml.getAll();

        for (ToDoList list : lists) {

            ConsoleManager.write(list.getTitle() + " ( " + list.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        ToDoList list = toDoRepoXml.getById(id);
        toDoRepoXml.delete(list, this.loggedUserId);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item deleted successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private void share() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Share ToDoList####");

        ToDoListRepository toDoRepoTxt = new ToDoListRepositoryTxtImpl("todoLists.txt");
        ToDoListRepository toDoRepoJson = new ToDoListRepositoryJsonImpl("toDoLists.json");
        ToDoListRepository toDoRepoXml = new ToDoListRepositoryXmlImpl();

        List<ToDoList> lists = toDoRepoXml.getAll(this.loggedUserId);

        for (ToDoList list : lists) {

            ConsoleManager.write(list.getTitle() + " ( " + list.getId() + " )\n");

        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        ToDoList toDoList = toDoRepoXml.getById(id);

        ConsoleManager.clear();

        UserRepository userRepoJson = new UserRepositoryJsonImpl("users.json");
        UserRepository userRepoXml = new UserRepositoryXmlImpl();

        List<User> users = userRepoXml.getAll();

        for (User user : users) {

            ConsoleManager.write(user.getUsername() + " ( " + user.getId() + " )\n");

        }

        ConsoleManager.write("Share to: ");
        String shareId = ConsoleManager.readLine();

        if (!"not shared".equalsIgnoreCase(toDoList.getSharedUsersIds())) {
            String ids = String.join(",",toDoList.getSharedUsersIds(), shareId);
            toDoList.setSharedUsersIds(ids);
        } else {
            toDoList.setSharedUsersIds(shareId);
        }
        toDoRepoXml.edit(toDoList);

        ConsoleManager.clear();
        ConsoleManager.writeLine("Item updated successfully");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }
    private void view() {

        ConsoleManager.clear();
        ConsoleManager.writeLine("####View ToDoList####");

        ToDoListRepository toDoRepoTxt = new ToDoListRepositoryTxtImpl("todoLists.txt");
        ToDoListRepository toDoRepoJson = new ToDoListRepositoryJsonImpl("toDoLists.json");
        ToDoListRepository toDoRepoXml = new ToDoListRepositoryXmlImpl();

        List<ToDoList> toDoLists = toDoRepoXml.getAll(this.loggedUserId);

        for (ToDoList toDoList : toDoLists) {
            ConsoleManager.write(toDoList.getTitle() + " ( " + toDoList.getId() + " )\n");
        }

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of ToDoList: ");
        int id = Integer.parseInt(ConsoleManager.readLine());

        ToDoList toDoList = toDoRepoXml.getById(id);

        ConsoleManager.clear();

        TaskManagementView taskView = new TaskManagementView(toDoList);
        taskView.run();
    }
}
