package academy.scalefocus.repositories.xml;

import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.exceptions.DataAccessException;
import academy.scalefocus.exceptions.IncorrectFileException;
import academy.scalefocus.repositories.ToDoListRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ToDoListRepositoryXmlImpl implements ToDoListRepository {
    public static final String DEFAULT_SHARED = "not shared";
    private static final String TO_DO_LIST_FILE_NAME = "toDoLists.xml";

    private final ObjectMapper objectMapper;
    private final ObjectWriter objectWriter;
    private Path path;


    public ToDoListRepositoryXmlImpl() {
        this.objectMapper = new XmlMapper().registerModule(new JavaTimeModule());
        this.objectWriter = this.objectMapper.writerWithDefaultPrettyPrinter();
        this.objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        this.path = Paths.get(TO_DO_LIST_FILE_NAME);

        try {
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
        } catch (IOException e) {
            throw new DataAccessException(e);
        }
    }

    public int getNextId() {
        int nextId = 0;
        List<ToDoList> lists;

        try {
            if (path.toFile().length() == 0) {
                return nextId + 1;
            }

            lists = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }

        for (ToDoList list : lists) {

            if (nextId < list.getId()) {
                nextId = list.getId();
            }
        }
        return nextId + 1;
    }

    public ToDoList getById(int id) {
        List<ToDoList> lists;

        try {
            lists = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }

        for (ToDoList list : lists) {
            if (list.getId() == id) {
                return list;
            }
        }
        return null;
    }

    public List<ToDoList> getAll() {
        List<ToDoList> lists;

        try {
            if (path.toFile().length() == 0) {
                return new ArrayList<>();
            }

            lists = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
        return lists;
    }

    public void add(ToDoList toDoList) {
        List<ToDoList> lists = getAll();
        toDoList.setId(getNextId());
        lists.add(toDoList);

        try {
            this.objectWriter.writeValue(path.toFile(), lists);
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }

    }


    public void edit(ToDoList toDoList) {
        List<ToDoList> lists;

        try {
            lists = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });

            deleteToDoListFromList(lists, toDoList);
            lists.add(toDoList);

            this.objectWriter.writeValue(path.toFile(), lists);
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
    }

    private void deleteToDoListFromList(List<ToDoList> allToDoLists, ToDoList toDoList) {
        for (ToDoList toDo : allToDoLists) {
            if (toDo.getId() == toDoList.getId()) {
                allToDoLists.remove(toDo);
                break;
            }
        }
    }

    private void removeShareToDoListFromList(ToDoList toDoList, int loggedUserId) {
        List<String> ids = new ArrayList<>(Arrays.asList(toDoList.getSharedUsersIds().split(",")));
        for (String id : ids) {
            int shareId = Integer.parseInt(id);
            if (shareId == loggedUserId) {
                ids.remove(id);
                break;
            }
        }
        if (!ids.isEmpty()) {
            String shareIds = String.join(",", ids);
            toDoList.setSharedUsersIds(shareIds);
        } else {
            toDoList.setSharedUsersIds(DEFAULT_SHARED);
        }
    }

    public void delete(ToDoList toDoList, int loggedUserId) {
        List<ToDoList> lists;

        try {
            lists = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });

            if (toDoList.getCreatorId() == loggedUserId) {
                deleteToDoListFromList(lists, toDoList);
            } else {
                removeShareToDoListFromList(toDoList, loggedUserId);
                deleteToDoListFromList(lists, toDoList);
                lists.add(toDoList);
            }

            this.objectWriter.writeValue(path.toFile(), lists);
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
    }

    public List<ToDoList> getAll(int parentId) {
        List<ToDoList> lists;
        List<ToDoList> result = new ArrayList<>();

        try {
            lists = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
        for (ToDoList list : lists) {
            if (list.getCreatorId() == parentId) {
                result.add(list);
            }
            if (!DEFAULT_SHARED.equalsIgnoreCase(list.getSharedUsersIds())) {
                String[] ids = list.getSharedUsersIds().split(",");
                for (String id : ids) {
                    int shareId = Integer.parseInt(id);
                    if (shareId == parentId) {
                        result.add(list);
                    }
                }
            }
        }
        return result;
    }
}
