package academy.scalefocus.repositories;

import academy.scalefocus.entities.ToDoList;

import java.util.List;

public interface ToDoListRepository {

    int getNextId();

    ToDoList getById(int id);

    List<ToDoList> getAll();

    void add(ToDoList toDoList);

    void edit(ToDoList toDoList);

    void delete(ToDoList toDoList, int loggedUserId);

    List<ToDoList> getAll(int parentId);
}
