package academy.scalefocus.repositories.txt;

import academy.scalefocus.entities.Task;
import academy.scalefocus.exceptions.DataAccessException;
import academy.scalefocus.repositories.TaskRepository;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConfigurationManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskRepositoryTxtImpl implements TaskRepository {

    private String fileName;

    public TaskRepositoryTxtImpl(String fileName) {
        this.fileName = fileName;

        try {
            Path path = Paths.get(this.fileName);
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    private Task createTask(BufferedReader bufferedReader, String value) throws IOException {
        Task task = new Task();
        task.setId(Integer.parseInt(value));

        task.setTitle(bufferedReader.readLine());
        task.setToDoListId(Integer.parseInt(bufferedReader.readLine()));
        task.setDescription(bufferedReader.readLine());
        task.setIsComplete(Boolean.parseBoolean(bufferedReader.readLine()));
        task.setDateCreated(LocalDateTime.parse(bufferedReader.readLine()));
        task.setCreatorId(Integer.parseInt(bufferedReader.readLine()));
        task.setDateModified(LocalDateTime.parse(bufferedReader.readLine()));
        task.setModifierId(Integer.parseInt(bufferedReader.readLine()));
        task.setAssignUsersIds(bufferedReader.readLine());
        return task;
    }

    @Override
    public int getNextId() {
        int nextId = 0;
        List<Task> tasks = getAll();

        for (Task task : tasks) {
            if (nextId < task.getId()) {
                nextId = task.getId();
            }
        }
        return nextId + 1;
    }

    @Override
    public Task getById(int id) {

        Task result = null;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                Task task = createTask(bufferedReader, value);

                if (task.getId() == id) {
                    result = task;
                    break;
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

    @Override
    public List<Task> getAll() {

        List<Task> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                Task task = createTask(bufferedReader, value);

                result.add(task);
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

    @Override
    public void add(Task task) {

        List<Task> tasks = getAll();

        if (tasks.isEmpty()) {
            task.setId(task.getId() + 1);
        } else {
            task.setId(getNextId());
        }
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(this.fileName, true))) {

            printWriter.println(task.getId());

            printWriter.println(task.getTitle());
            printWriter.println(task.getToDoListId());
            printWriter.println(task.getDescription());
            printWriter.println(task.getIsComplete());
            printWriter.println(task.getDateCreated());
            printWriter.println(task.getCreatorId());
            printWriter.println(task.getDateModified());
            printWriter.println(task.getModifierId());
            printWriter.println("not assigned");
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void edit(Task task) {

        String tempFileName = ConfigurationManager.tempFilePrefix() + this.fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
             PrintWriter printWriter = new PrintWriter(new FileWriter(tempFileName, true))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                Task tempTask = createTask(bufferedReader, value);

                if (tempTask.getId() != task.getId()) {

                    printWriter.println(tempTask.getId());

                    printWriter.println(tempTask.getTitle());
                    printWriter.println(tempTask.getToDoListId());
                    printWriter.println(tempTask.getDescription());
                    printWriter.println(tempTask.getIsComplete());
                    printWriter.println(tempTask.getDateCreated());
                    printWriter.println(tempTask.getCreatorId());
                    printWriter.println(tempTask.getDateModified());
                    printWriter.println(tempTask.getModifierId());
                    printWriter.println(tempTask.getAssignUsersIds());
                } else {

                    printWriter.println(task.getId());

                    printWriter.println(task.getTitle());
                    printWriter.println(task.getToDoListId());
                    printWriter.println(task.getDescription());
                    printWriter.println(task.getIsComplete());
                    printWriter.println(task.getDateCreated());
                    printWriter.println(task.getCreatorId());
                    printWriter.println(task.getDateModified());
                    printWriter.println(task.getModifierId());
                    printWriter.println(task.getAssignUsersIds());
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        File original = new File(this.fileName);
        File tempFile = new File(tempFileName);

        original.delete();
        tempFile.renameTo(original);
        tempFile.delete();
    }

    @Override
    public void delete(Task task, int loggedUserId) {

        String tempFileName = ConfigurationManager.tempFilePrefix() + this.fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
             PrintWriter printWriter = new PrintWriter(new FileWriter(tempFileName, true))) {

            String value = "";
            while ((value = bufferedReader.readLine()) != null) {

                Task tempTask = createTask(bufferedReader, value);

                if (tempTask.getId() != task.getId()) {

                    printWriter.println(tempTask.getId());

                    printWriter.println(tempTask.getTitle());
                    printWriter.println(tempTask.getToDoListId());
                    printWriter.println(tempTask.getDescription());
                    printWriter.println(tempTask.getIsComplete());
                    printWriter.println(tempTask.getDateCreated());
                    printWriter.println(tempTask.getCreatorId());
                    printWriter.println(tempTask.getDateModified());
                    printWriter.println(tempTask.getModifierId());
                } else {
                    if (tempTask.getCreatorId() != loggedUserId) {
                        List<String> ids = new ArrayList<>(Arrays.asList(tempTask.getAssignUsersIds().split(",")));
                        for (String id : ids) {
                            int shareId = Integer.parseInt(id);
                            if (shareId == AuthenticationService.getInstance().getLoggedUser().getId()) {
                                ids.remove(id);
                                break;
                            }
                        }

                        printWriter.println(tempTask.getId());

                        printWriter.println(tempTask.getTitle());
                        printWriter.println(tempTask.getDateCreated());
                        printWriter.println(tempTask.getCreatorId());
                        printWriter.println(tempTask.getDateModified());
                        printWriter.println(tempTask.getModifierId());

                        if (ids.isEmpty()) {
                            String shareIds = String.join(",", ids);
                            tempTask.setAssignUsersIds(shareIds);
                            printWriter.println(tempTask.getAssignUsersIds());
                        } else {
                            printWriter.println("not assigned");
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        File original = new File(this.fileName);
        File tempFile = new File(tempFileName);

        original.delete();
        tempFile.renameTo(original);
        tempFile.delete();
    }

    @Override
    public List<Task> getAll(int parentId) {

        List<Task> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                Task task = createTask(bufferedReader, value);

                if (task.getToDoListId()== parentId)
                    result.add(task);
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }
}
