package academy.scalefocus.repositories.txt;


import academy.scalefocus.entities.User;
import academy.scalefocus.exceptions.DataAccessException;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.tools.ConfigurationManager;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTxtImpl implements UserRepository {
    private String fileName;

    public UserRepositoryTxtImpl(String fileName) {
        this.fileName = fileName;
        try {
            Path path = Paths.get(fileName);
            if (!Files.exists(path)) {
                Files.createFile(path);

                User user = new User();
                user.setUsername("admin");
                user.setPassword("adminpass");
                user.setFirstName("Administrator");
                user.setLastName("Administrator");
                user.setIsAdmin(true);
                user.setDateCreated(LocalDateTime.now());
                user.setCreatorId(1);
                user.setModifierId(1);
                user.setDateModified(LocalDateTime.now());

                add(user);
            }
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    private User createUser(BufferedReader bufferedReader, String value) {
        User user = new User();
        try {
            user.setId(Integer.parseInt(value));

            user.setUsername(bufferedReader.readLine());
            user.setPassword(bufferedReader.readLine());
            user.setFirstName(bufferedReader.readLine());
            user.setLastName(bufferedReader.readLine());
            user.setIsAdmin(Boolean.parseBoolean(bufferedReader.readLine()));
            user.setDateCreated(LocalDateTime.parse(bufferedReader.readLine()));
            user.setCreatorId(Integer.parseInt(bufferedReader.readLine()));
            user.setModifierId(Integer.parseInt(bufferedReader.readLine()));
            user.setDateModified(LocalDateTime.parse(bufferedReader.readLine()));
        } catch (IOException e) {
            throw new DataAccessException(e);
        }
        return user;
    }

    public int getNextId() {

        int nextId = 0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                User user = createUser(bufferedReader, value);

                if (nextId < user.getId())
                    nextId = user.getId();
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return nextId + 1;
    }

    public User getById(int id) {

        User result = null;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                User user = createUser(bufferedReader, value);

                if (user.getId() == id) {
                    result = user;
                    break;
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

    public List<User> getAll() {
        List<User> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                User user = createUser(bufferedReader, value);

                result.add(user);
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

    public void add(User user) {
        user.setId(getNextId());

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(this.fileName, true))) {

            printWriter.println(user.getId());

            printWriter.println(user.getUsername());
            printWriter.println(user.getPassword());
            printWriter.println(user.getFirstName());
            printWriter.println(user.getLastName());
            printWriter.println(user.getIsAdmin());
            printWriter.println(user.getDateCreated());
            printWriter.println(user.getCreatorId());
            printWriter.println(user.getModifierId());
            printWriter.println(user.getDateModified());
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }
    }

    public void edit(User user) {

        String tempFileName = ConfigurationManager.tempFilePrefix() + this.fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
             PrintWriter printWriter = new PrintWriter(new FileWriter(tempFileName, true))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                User tempUser = createUser(bufferedReader, value);

                if (tempUser.getId() != user.getId()) {

                    printWriter.println(tempUser.getId());

                    printWriter.println(tempUser.getUsername());
                    printWriter.println(tempUser.getPassword());
                    printWriter.println(tempUser.getFirstName());
                    printWriter.println(tempUser.getLastName());
                    printWriter.println(tempUser.getIsAdmin());
                    printWriter.println(tempUser.getDateCreated());
                    printWriter.println(tempUser.getCreatorId());
                    printWriter.println(tempUser.getModifierId());
                    printWriter.println(tempUser.getDateModified());
                } else {

                    printWriter.println(user.getId());

                    printWriter.println(user.getUsername());
                    printWriter.println(user.getPassword());
                    printWriter.println(user.getFirstName());
                    printWriter.println(user.getLastName());
                    printWriter.println(user.getIsAdmin());
                    printWriter.println(user.getDateCreated());
                    printWriter.println(user.getCreatorId());
                    printWriter.println(user.getModifierId());
                    printWriter.println(user.getDateModified());
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        File original = new File(this.fileName);
        File tempFile = new File(tempFileName);

        original.delete();
        tempFile.renameTo(original);
        tempFile.delete();
    }

    public User getByUserNameAndPassword(String username, String password) {

        User result = null;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                User user = createUser(bufferedReader, value);

                if (user.getUsername().compareTo(username) == 0 && user.getPassword().compareTo(password) == 0) {
                    result = user;
                    break;
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

    public void delete(User user) {

        String tempFileName = ConfigurationManager.tempFilePrefix() + this.fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
             PrintWriter printWriter = new PrintWriter(new FileWriter(tempFileName, true))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                User tempUser = createUser(bufferedReader, value);

                if (tempUser.getId() != user.getId()) {

                    printWriter.println(tempUser.getId());

                    printWriter.println(tempUser.getUsername());
                    printWriter.println(tempUser.getPassword());
                    printWriter.println(tempUser.getFirstName());
                    printWriter.println(tempUser.getLastName());
                    printWriter.println(tempUser.getIsAdmin());
                    printWriter.println(tempUser.getDateCreated());
                    printWriter.println(tempUser.getCreatorId());
                    printWriter.println(tempUser.getModifierId());
                    printWriter.println(tempUser.getDateModified());
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        File original = new File(this.fileName);
        File tempFile = new File(tempFileName);

        original.delete();
        tempFile.renameTo(original);
        tempFile.delete();
    }
}
