package academy.scalefocus.repositories;


import academy.scalefocus.entities.User;

import java.util.List;

public interface UserRepository {


    List<User> getAll();

    User getById(int id);

    void add(User user);

    void edit(User user);

    void delete(User user);

    User getByUserNameAndPassword(String userName, String password);

}
