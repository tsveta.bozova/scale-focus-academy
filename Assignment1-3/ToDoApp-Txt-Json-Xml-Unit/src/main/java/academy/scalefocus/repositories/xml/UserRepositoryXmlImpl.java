package academy.scalefocus.repositories.xml;

import academy.scalefocus.entities.User;
import academy.scalefocus.exceptions.DataAccessException;
import academy.scalefocus.exceptions.IncorrectFileException;
import academy.scalefocus.repositories.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryXmlImpl implements UserRepository {
    private static final String USERS_FILE_NAME = "users.xml";

    private final ObjectMapper objectMapper;
    private final ObjectWriter objectWriter;

    private Path path;


    public UserRepositoryXmlImpl () {

        this.objectMapper = new XmlMapper().registerModule(new JavaTimeModule());
        this.objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        this.objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        try {
            this.path = Paths.get(USERS_FILE_NAME);
            if (!Files.exists(path)) {
                Files.createFile(path);

                User user = new User();
                user.setId(1);
                user.setUsername("admin");
                user.setPassword("adminpass");
                user.setFirstName("Administrator");
                user.setLastName("Administrator");
                user.setIsAdmin(true);
                user.setDateCreated(LocalDateTime.now());
                user.setCreatorId(1);
                user.setModifierId(1);
                user.setDateModified(LocalDateTime.now());

                List<User> users = new ArrayList<>();
                users.add(user);

                this.objectWriter.writeValue(this.path.toFile(), users);

            }
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }


    public int getNextId() {
        int nextId = 0;
        List<User> users;
        try {
            users = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (Exception e) {
            throw new IncorrectFileException(e);
        }

        for (User user : users) {
            if (nextId < user.getId())
                nextId = user.getId();
        }
        return nextId + 1;
    }

    @Override
    public List<User> getAll() {
        List<User> users;
        try {
            users = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (Exception e) {
            throw new IncorrectFileException(e);
        }
        return users;
    }

    @Override
    public User getById(int id) {
        User result = null;
        List<User> users = getAll();

        for (User user : users) {
            if (user.getId() == id) {
                result = user;
            }
        }

        return result;
    }

    @Override
    public void add(User user){
        user.setId(getNextId());
        List<User> users = getAll();
        users.add(user);

        try {
            this.objectWriter.writeValue(path.toFile(), users);
        } catch (Exception e) {
            throw new IncorrectFileException(e);
        }
    }

    @Override
    public void edit(User user) {
        List<User> allUsers = getAll();
        deleteUserFromList(allUsers, user);
        allUsers.add(user);

        try {
            this.objectWriter.writeValue(path.toFile(), allUsers);
        } catch (Exception e) {
            throw new IncorrectFileException(e);
        }
    }

    @Override
    public void delete(User user) {
        List<User> allUsers = getAll();
        deleteUserFromList(allUsers, user);

        try {
            this.objectWriter.writeValue(path.toFile(), allUsers);
        } catch (Exception e) {
            throw new IncorrectFileException(e);
        }
    }

    private void deleteUserFromList(List<User> allUsers, User user) {
        for (User u : allUsers) {
            if (u.getId() == user.getId()) {
                allUsers.remove(u);
                break;
            }
        }
    }

    @Override
    public User getByUserNameAndPassword(String userName, String password)  {
        User result = null;
        List<User> users;

        try {
            users = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (Exception e) {
            throw new IncorrectFileException(e);
        }

        for (User user : users) {
            if (user.getUsername().equals(userName) && user.getPassword().equals(password)) {
                result = user;
            }
        }
        return result;
    }
}
