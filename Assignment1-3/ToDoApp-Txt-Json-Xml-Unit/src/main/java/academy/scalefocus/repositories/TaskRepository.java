package academy.scalefocus.repositories;

import academy.scalefocus.entities.Task;

import java.util.List;

public interface TaskRepository {

    int getNextId();

    Task getById(int id);

    List<Task> getAll();

    void add(Task task);

    void edit(Task task);

    void delete(Task task, int loggedUserId);

    List<Task> getAll(int parentId);
}
