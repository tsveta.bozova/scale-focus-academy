package academy.scalefocus.repositories.xml;

import academy.scalefocus.entities.Task;
import academy.scalefocus.exceptions.DataAccessException;
import academy.scalefocus.exceptions.IncorrectFileException;
import academy.scalefocus.repositories.TaskRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskRepositoryXmlImpl implements TaskRepository {
    private static final String TASK_FILE_NAME = "tasks.xml";
    private static final String DEFAULT_SHARED = "not assigned";

    private final ObjectMapper objectMapper;
    private final ObjectWriter objectWriter;
    private Path path;

    public TaskRepositoryXmlImpl() {
        this.objectMapper = new XmlMapper().registerModule(new JavaTimeModule());
        this.objectWriter = this.objectMapper.writerWithDefaultPrettyPrinter();
        this.path = Paths.get(TASK_FILE_NAME);

        try {
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
        } catch (IOException e) {
            throw new DataAccessException(e);
        }
    }


    @Override
    public int getNextId() {
        int nextId = 0;
        List<Task> tasks;
        try {
            if (path.toFile().length() == 0) {
                return nextId + 1;
            }

            tasks = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }

        for (Task task : tasks) {

            if (nextId < task.getId()) {
                nextId = task.getId();
            }
        }
        return nextId + 1;
    }

    @Override
    public Task getById(int id) {
        List<Task> tasks;

        try {
            tasks = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }

        for (Task task : tasks) {
            if (task.getId() == id) {
                return task;
            }
        }
        return null;
    }

    @Override
    public List<Task> getAll() {
        List<Task> tasks;
        try {
            if (this.path.toFile().length() == 0) {
                return new ArrayList<>();
            }

            tasks = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
        return tasks;
    }

    @Override
    public void add(Task task) {
        List<Task> tasks = getAll();
        task.setId(getNextId());
        tasks.add(task);

        try {
            this.objectWriter.writeValue(path.toFile(), tasks);
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
    }

    @Override
    public void edit(Task task) {
        List<Task> tasks;
        try {
            tasks = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });

            deleteTaskFromList(tasks, task);
            tasks.add(task);

            this.objectWriter.writeValue(path.toFile(), tasks);
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
    }

    private void deleteTaskFromList(List<Task> allTasks, Task task) {
        for (Task t : allTasks) {
            if (t.getId() == task.getId()) {
                allTasks.remove(t);
                break;
            }
        }
    }

    @Override
    public void delete(Task task, int loggedUserId) {
        List<Task> tasks;
        try {
            tasks = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });

            if (task.getCreatorId() == loggedUserId) {
                deleteTaskFromList(tasks, task);
            } else {
                removeAssignTaskFromList(task, loggedUserId);
                deleteTaskFromList(tasks, task);
                tasks.add(task);
            }

            this.objectWriter.writeValue(path.toFile(), tasks);
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
    }

    private void removeAssignTaskFromList(Task task, int loggedUserId) {
        List<String> ids = new ArrayList<>(Arrays.asList(task.getAssignUsersIds().split(",")));
        for (String id : ids) {
            int shareId = Integer.parseInt(id);
            if (shareId == loggedUserId) {
                ids.remove(id);
                break;
            }
        }
        if (!ids.isEmpty()) {
            String shareIds = String.join(",", ids);
            task.setAssignUsersIds(shareIds);
        } else {
            task.setAssignUsersIds(DEFAULT_SHARED);
        }
    }

    @Override
    public List<Task> getAll(int parentId) {
        List<Task> tasks;
        List<Task> result = new ArrayList<>();
        try {
            tasks = this.objectMapper.readValue(this.path.toFile(), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IncorrectFileException(e);
        }
        for (Task task : tasks) {
            if (task.getToDoListId() == parentId) {
                result.add(task);
            }
        }
        return result;
    }
}
