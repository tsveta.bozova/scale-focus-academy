package academy.scalefocus.repositories.txt;

import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.exceptions.DataAccessException;
import academy.scalefocus.repositories.ToDoListRepository;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConfigurationManager;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ToDoListRepositoryTxtImpl implements ToDoListRepository {
    public static final String DEFAULT_SHARED = "not shared";
    private String fileName;


    public ToDoListRepositoryTxtImpl(String fileName) {
        this.fileName = fileName;

        try {
            Path path = Paths.get(this.fileName);
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }


    @Override
    public int getNextId() {

        int nextId = 0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                ToDoList toDoList = createToDoList(bufferedReader, value);

                if (nextId < toDoList.getId())
                    nextId = toDoList.getId();
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return nextId + 1;
    }

    private ToDoList createToDoList(BufferedReader bufferedReader, String value) {
        ToDoList toDoList = new ToDoList();
        try {
            toDoList.setId(Integer.parseInt(value));

            toDoList.setCreatorId(Integer.parseInt(bufferedReader.readLine()));
            toDoList.setTitle(bufferedReader.readLine());
            toDoList.setDateCreated(LocalDateTime.parse(bufferedReader.readLine()));
            toDoList.setDateModified(LocalDateTime.parse(bufferedReader.readLine()));
            toDoList.setModifierId(Integer.parseInt(bufferedReader.readLine()));
            toDoList.setSharedUsersIds(bufferedReader.readLine());
        } catch (IOException e) {
            throw new DataAccessException(e);
        }
        return toDoList;
    }


    @Override
    public ToDoList getById(int id) {
        List<ToDoList> lists = getAll();
        for (ToDoList list : lists) {
            if (list.getId() == id) {
                return list;
            }
        }

        return null;
    }

    @Override
    public List<ToDoList> getAll() {

        List<ToDoList> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {
            String value;

            while ((value = bufferedReader.readLine()) != null) {
                ToDoList toDoList = createToDoList(bufferedReader, value);
                result.add(toDoList);
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

    @Override
    public void add(ToDoList toDoList) {
        if (getAll().isEmpty()) {
            toDoList.setId(toDoList.getId() + 1);
        } else {
            toDoList.setId(getNextId());
        }

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(this.fileName, true))) {

            printToDoList(toDoList, printWriter);
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }
    }

    private void printToDoList(ToDoList toDoList, PrintWriter printWriter) {
        printWriter.println(toDoList.getId());
        printWriter.println(toDoList.getCreatorId());
        printWriter.println(toDoList.getTitle());
        printWriter.println(toDoList.getDateCreated());
        printWriter.println(toDoList.getDateModified());
        printWriter.println(toDoList.getModifierId());
        printWriter.println(toDoList.getSharedUsersIds());
    }

    @Override
    public void edit(ToDoList toDoList) {

        String tempFileName = ConfigurationManager.tempFilePrefix() + this.fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
             PrintWriter printWriter = new PrintWriter(new FileWriter(tempFileName, true))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                ToDoList tempToDoList = createToDoList(bufferedReader, value);

                if (tempToDoList.getId() != toDoList.getId()) {
                    printToDoList(tempToDoList, printWriter);
                } else {
                    printToDoList(toDoList, printWriter);
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        File original = new File(this.fileName);
        File tempFile = new File(tempFileName);

        original.delete();
        tempFile.renameTo(original);
        tempFile.delete();
    }

    @Override
    public void delete(ToDoList toDoList, int loggedUserId) {

        String tempFileName = ConfigurationManager.tempFilePrefix() + this.fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
             PrintWriter printWriter = new PrintWriter(new FileWriter(tempFileName, true))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                ToDoList tempToDoList = createToDoList(bufferedReader, value);

                if (tempToDoList.getId() != toDoList.getId()) {
                    printToDoList(tempToDoList, printWriter);
                } else {
                    if (tempToDoList.getCreatorId() != loggedUserId) {
                        List<String> ids = new ArrayList<>(Arrays.asList(tempToDoList.getSharedUsersIds().split(",")));
                        for (String id : ids) {
                            int shareId = Integer.parseInt(id);
                            if (shareId == AuthenticationService.getInstance().getLoggedUser().getId()) {
                                ids.remove(id);
                                break;
                            }
                        }

                        printWriter.println(tempToDoList.getId());

                        printWriter.println(tempToDoList.getTitle());
                        printWriter.println(tempToDoList.getDateCreated());
                        printWriter.println(tempToDoList.getCreatorId());
                        printWriter.println(tempToDoList.getDateModified());
                        printWriter.println(tempToDoList.getModifierId());

                        if (!ids.isEmpty()) {
                            String shareIds = String.join(",", ids);
                            tempToDoList.setSharedUsersIds(shareIds);
                            printWriter.println(tempToDoList.getSharedUsersIds());
                        } else {
                            printWriter.println(DEFAULT_SHARED);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        File original = new File(this.fileName);
        File tempFile = new File(tempFileName);

        original.delete();
        tempFile.renameTo(original);
        tempFile.delete();
    }

    @Override
    public List<ToDoList> getAll(int parentId) {

        List<ToDoList> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {

            String value;
            while ((value = bufferedReader.readLine()) != null) {

                ToDoList toDoList = createToDoList(bufferedReader, value);


                if (toDoList.getCreatorId() == parentId) {
                    result.add(toDoList);
                }

                if (!DEFAULT_SHARED.equalsIgnoreCase(toDoList.getSharedUsersIds())) {
                    String[] ids = toDoList.getSharedUsersIds().split(",");
                    for (String id : ids) {
                        int shareId = Integer.parseInt(id);
                        if (shareId == parentId) {
                            result.add(toDoList);
                        }
                    }
                }

            }
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        }

        return result;
    }

}
