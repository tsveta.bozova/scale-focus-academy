package academy.scalefocus.tools;

public class ConfigurationManager {
    public static String getDefaultFileEncoding() {

        return "utf-8";
    }

    public static String tempFilePrefix() {

        return "temp_";
    }
}
