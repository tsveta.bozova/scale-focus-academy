package academy.scalefocus.repositories;

import academy.scalefocus.entities.Task;
import academy.scalefocus.repositories.txt.TaskRepositoryTxtImpl;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskRepositoryJsonTest {
    private static final int USER_ID = 1;
    private static final int TASK_ID = 1;
    private static final int TODO_LIST_ID = 1;
    private static final String TEST_FILE_NAME = "tasks_test.json";

    private TaskRepository repo;
    private Task task;

    @BeforeAll
    void beforeClass() throws IOException {
        Path file = Paths.get(TEST_FILE_NAME);

        if (Files.exists(file)) {
            Files.delete(file);
        }

        this.repo = new TaskRepositoryTxtImpl(TEST_FILE_NAME);
    }

    @BeforeEach
    public void setUp() {
        this.task = new Task();

        this.task.setTitle("todoApp");
        this.task.setToDoListId(TODO_LIST_ID);
        this.task.setDescription("to create");
        this.task.setIsComplete(false);
        this.task.setDateCreated(LocalDateTime.now());
        this.task.setCreatorId(USER_ID);
        this.task.setDateModified(LocalDateTime.now());
        this.task.setModifierId(USER_ID);
        this.task.setAssignUsersIds("not shared");

        this.repo.add(this.task);
    }

    @AfterEach
    public void erase() {
        this.repo.delete(this.task, USER_ID);
    }

    @Test
    public void addTaskCorrectly() {
        Task retrieveTask = this.repo.getById(this.task.getId());

        assertNotNull(retrieveTask);
    }

    @Test
    public void editTaskCorrectly() {
        String updatedTitle = "todo";
        this.task.setTitle(updatedTitle);

        this.repo.edit(this.task);

        Task updatedEntity = this.repo.getById(this.task.getId());

        assertEquals(updatedTitle, updatedEntity.getTitle());
    }

    @Test
    public void getTaskByIdCorrectly() {
        Task task = this.repo.getById(TASK_ID);

        assertEquals(USER_ID, task.getCreatorId());
        assertEquals(TASK_ID, task.getId());
    }

    @Test
    public void getAllByParentIdCorrectly() {
        List<Task> all = this.repo.getAll(USER_ID);

        assertEquals(1, all.size());
        assertEquals(USER_ID, all.get(0).getCreatorId());
    }

    @Test
    public void deleteTaskCorrectly() {
        Task task = this.repo.getById(TASK_ID);
        List<Task> allTasks = this.repo.getAll();
        int initialCount = allTasks.size();

        this.repo.delete(task, USER_ID);

        allTasks = this.repo.getAll();
        assertEquals(initialCount - 1, allTasks.size());
    }

    @Test
    public void getAllCorrectly() {
        List<Task> all = this.repo.getAll();

        assertEquals(1, all.size());
    }

    @AfterAll
    void afterAll() throws IOException {
        Path file = Paths.get(TEST_FILE_NAME);
        if (Files.exists(file)) {
            Files.delete(file);
        }
    }
}
