package academy.scalefocus.repositories;


import academy.scalefocus.entities.ToDoList;
import academy.scalefocus.repositories.txt.ToDoListRepositoryTxtImpl;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ToDoListRepositoryTxtShould {
    private static final int USER_ID = 1;
    private static final int TODO_LIST_ID = 1;
    private static final String TEST_FILE_NAME = "todoLists_test.txt";

    private ToDoListRepository repo;
    private ToDoList toDoList;

    @BeforeAll
    void beforeClass() throws IOException {
        Path file = Paths.get(TEST_FILE_NAME);

        if (Files.exists(file)) {
            Files.delete(file);
        }

        this.repo = new ToDoListRepositoryTxtImpl(TEST_FILE_NAME);
    }

    @BeforeEach
    public void setUp() {
        this.toDoList = new ToDoList();

        this.toDoList.setCreatorId(USER_ID);
        this.toDoList.setTitle("My lists");
        this.toDoList.setDateCreated(LocalDateTime.now());
        this.toDoList.setDateModified(LocalDateTime.now());
        this.toDoList.setModifierId(USER_ID);
        this.toDoList.setSharedUsersIds("not shared");

        this.repo.add(toDoList);
    }

    @AfterEach
    public void erase() {
        this.repo.delete(toDoList, USER_ID);
    }

    @Test
    public void addToDoListCorrectly() {
        ToDoList retrieveToDoList = this.repo.getById(this.toDoList.getId());

        assertNotNull(retrieveToDoList);
    }

    @Test
    public void editToDoCorrectly() {
        String updatedTitle = "My tasks";
        this.toDoList.setTitle(updatedTitle);

        this.repo.edit(this.toDoList);

        ToDoList updatedEntity = this.repo.getById(this.toDoList.getId());

        assertEquals(updatedTitle, updatedEntity.getTitle());
    }

    @Test
    public void getToDoListByIdCorrectly() {
        ToDoList toDoList = this.repo.getById(TODO_LIST_ID);

        assertEquals(USER_ID, toDoList.getCreatorId());
        assertEquals(TODO_LIST_ID, toDoList.getId());
    }

    @Test
    public void getAllByParentIdCorrectly() {
        List<ToDoList> all = this.repo.getAll(USER_ID);

        assertEquals(1, all.size());
        assertEquals(USER_ID, all.get(0).getCreatorId());
    }

    @Test
    public void deleteToDoListCorrectly() {
        ToDoList toDoList = this.repo.getById(TODO_LIST_ID);
        List<ToDoList> allToDoList = this.repo.getAll();
        int initialCount = allToDoList.size();

        this.repo.delete(toDoList, USER_ID);

        allToDoList = this.repo.getAll();
        assertEquals(initialCount - 1, allToDoList.size());
    }

    @Test
    public void getAllCorrectly() {
        List<ToDoList> all = this.repo.getAll();

        assertEquals(1, all.size());
    }

    @AfterAll
    void afterAll() throws IOException {
        Path file = Paths.get(TEST_FILE_NAME);
        if (Files.exists(file)) {
            Files.delete(file);
        }
    }
}