package academy.scalefocus.repositories;

import academy.scalefocus.entities.User;
import academy.scalefocus.repositories.txt.UserRepositoryTxtImpl;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserRepositoryTxtShould {
    private static final int USER_ID = 1;
    private static final String TEST_FILE_NAME = "users_test.txt";
    private UserRepository repo;
    private User user;

    @BeforeAll
    void beforeAll() throws IOException {
        Path file = Paths.get(TEST_FILE_NAME);

        if (Files.exists(file)) {
            Files.delete(file);
        }

        this.repo = new UserRepositoryTxtImpl(TEST_FILE_NAME);
    }

    @BeforeEach
    public void setUp() {
        this.user = new User();

        this.user.setUsername("admin");
        this.user.setPassword("adminpass");
        this.user.setFirstName("Administrator");
        this.user.setLastName("Administrator");
        this.user.setIsAdmin(true);
        this.user.setDateCreated(LocalDateTime.now());
        this.user.setCreatorId(USER_ID);
        this.user.setModifierId(USER_ID);
        this.user.setDateModified(LocalDateTime.now());

        this.repo.add(this.user);
    }

    @AfterEach
    public void terminate() {
        this.repo.delete(this.user);
    }

    @Test
    public void addUserCorrectly() {
        User retrieveUser = this.repo.getById(USER_ID);

        assertNotNull(retrieveUser);
    }

    @Test
    public void editUserCorrectly() {
        User editUser = this.repo.getByUserNameAndPassword(this.user.getUsername(), this.user.getPassword());

        String updatedFirstName = "Updated First Name";
        String updatedLastName = "Updated Last Name";
        editUser.setFirstName(updatedFirstName);
        editUser.setLastName(updatedLastName);

        this.repo.edit(editUser);

        User updatedEntity = this.repo.getById(editUser.getId());
        boolean isUpdated = updatedEntity.getFirstName().equals(updatedFirstName) ||
                updatedEntity.getLastName().equals(updatedLastName);


        assertTrue(isUpdated);
    }

    @Test
    public void getByIdCorrectly() {
        User retrieveUser = this.repo.getById(USER_ID);

        assertEquals(this.user.getUsername(), retrieveUser.getUsername());
        assertEquals(this.user.getPassword(), retrieveUser.getPassword());
    }

    @Test
    public void getUserByUsernameAndPasswordCorrectly() {
        User userByUsernameAndPassword = this.repo.getByUserNameAndPassword(this.user.getUsername(), this.user.getPassword());

        assertNotEquals(null, userByUsernameAndPassword);
    }

    @Test
    public void deleteUserCorrectly() {
        User retrieveUser = this.repo.getById(USER_ID);

        List<User> allUsers = this.repo.getAll();
        int initialCount = allUsers.size();
        this.repo.delete(retrieveUser);
        allUsers = this.repo.getAll();

        assertEquals(initialCount - 1, allUsers.size());
    }

    @Test
    public void getAllCorrectly() {
        List<User> allUsers = this.repo.getAll();

        assertEquals(1, allUsers.size());
    }

    @AfterAll
    void afterAll() throws IOException {
        Path file = Paths.get(TEST_FILE_NAME);
        if (Files.exists(file)) {
            Files.delete(file);
        }
    }
}