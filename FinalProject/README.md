# Workforce Management API

## Summary
The Workforce Management API handles different types of leave requests, submitted by workers in a given company. 
This project uses Spring JPA, Hibernate, Spring Web, Spring Boot, Spring Security and Docker.

## How to build

1.Clone the repository;

2.Unzip and cd to project root folder (```~/team-two-final-project/workforce-management```);

3.Make sure you have [docker](https://hub.docker.com/editions/community/docker-ce-desktop-windows/) and [docker-composer](https://docs.docker.com/compose/install/) installed;

4.Run the command

```
docker-compose up
```
or
```
docker-compose up -d
```
to run it as a daemon.

By default the docker compose file is set up without a mounted volume for data persistence of the Oracle DB container. If you wish to change this behaviour add a line to ```docker-compose.yml``` with ```/opt/oracle/oradata``` as shown bellow before building the docker containers with ```docker-compose up```:

```bash
...
volumes:
  - ./src/main/resources:/opt/oracle/scripts/setup
  - ./{PATH}/{TO}/{LOCAL}/{FOLDER}:/opt/oracle/oradata
  pm-spring-boot:
...
```

The first time you build the containers, the Oracle DB will be automatically populated with data from the database.sql file.
A default user with an admin role for the app is created with ```username: admin``` and ```password: adminpass```.

## Used docker images

- Oracle 19.3.0 EE DB - **omgthehorror/oracle-db:19.3.0-ee**
- Workforce Management API - **omgthehorror/wmapi-server:wmapi-server**

## Functionality

### Login

- A user must use **valid** credentials (email and password) inside the body (in json format). After a successful authentication he receives an authentication token. This token **must** be used in the Authorization header for each request the user sends. **Each token expires one hour after creation.**  

### Users Management

#### View all users' data

- A logged user with administrative rights can view every user's data.

#### View a specific user's data

- A logged user with administrative rights can view a specific person's data

#### Create a user 

- A logged user with administrative rights can create users.

#### Edit a user 

- A logged user with administrative rights can edit users.

#### Delete a user

- A logged user with administrative rights can delete other users.
- **The default admin cannot be deleted** 

### Teams Management

#### View all teams' data
- A logged user with administrative rights can view every team's data.

#### View a specific team's data
- A logged user with administrative rights can view a specific team's data

#### Create a team
- A logged user with administrative rights can create a team.

#### Edit a team
- A logged user with administrative rights can edit a team.

#### Delete a team
- A logged user with administrative rights can delete a team.

#### Assign a user to a team
- A logged user with administrative rights can assign a user to a specific team.
- **A user can be assigned to a team only once.**

#### Remove a user from a team
- A logged user with administrative rights can remove a user from a specific team.
- **The user who is being removed must be a part of the team to begin with.**

#### Assign a team leader
- A logged user with administrative right can set a user to the position of team leader for a specific team. 
- **The default admin cannot be a team leader.** 
- **A user cannot be a team leader unless he is a part of the team.** 