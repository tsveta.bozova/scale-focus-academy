package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.entities.TimeOff;

public interface SickLeaveService {
    TimeOff create(TimeOffRequestDTO timeOffRequestDTO);
}
