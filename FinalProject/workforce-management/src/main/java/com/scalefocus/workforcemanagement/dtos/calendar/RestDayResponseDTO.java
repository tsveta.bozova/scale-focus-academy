package com.scalefocus.workforcemanagement.dtos.calendar;

import com.scalefocus.workforcemanagement.dtos.calendar.enums.RestDayType;

import java.time.LocalDate;

public class RestDayResponseDTO {
    private LocalDate date;
    private RestDayType dayType;

    public RestDayResponseDTO(LocalDate date, RestDayType dayTime) {
        this.date = date;
        this.dayType = dayTime;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public RestDayType getDayType() {
        return dayType;
    }

    public void setDayType(RestDayType dayTime) {
        this.dayType = dayTime;
    }
}
