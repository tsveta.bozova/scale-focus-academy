package com.scalefocus.workforcemanagement.jobs;

import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import com.scalefocus.workforcemanagement.services.TimeOffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CronJob {
    private final UserRepository userRepository;
    private final TimeOffRepository timeOffRepository;
    private final TimeOffService timeOffService;

    @Autowired
    public CronJob(UserRepository userRepository,
                   TimeOffRepository timeOffRepository,
                   TimeOffService timeOffService) {
        this.userRepository = userRepository;
        this.timeOffRepository = timeOffRepository;
        this.timeOffService = timeOffService;
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void deleteOldUserData() {
        List<User> userList = userRepository.findAll()
                .stream()
                .filter(User::isUserGdprDeleted)
                .filter(user -> (user.getDateUpdated().compareTo(Instant.now().minus(Period.ofDays(183)))) <= 0)
                .collect(Collectors.toList());
        userRepository.deleteAll(userList);
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void deleteOldCanceledRequests() {
        List<TimeOff> timeOffList = timeOffRepository.findAll()
                .stream()
                .filter(timeOff -> timeOff.getStatus().equals(TimeOffStatus.CANCELLED))
                .filter(timeOff -> (timeOff.getDateUpdated().compareTo(Instant.now().minus(Period.ofDays(183)))) <= 0)
                .collect(Collectors.toList());
        timeOffRepository.deleteAll(timeOffList);
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void autoApproveTimeOff() {
        List<TimeOff> timeOffToApprove = timeOffRepository.findAll()
                .stream()
                .filter(timeOff -> timeOff.getTimeOffStart().equals(LocalDate.now()) &&
                        timeOff.getStatus() == TimeOffStatus.AWAITING)
                .collect(Collectors.toList());
        timeOffService.autoApproveTimeOffRequest(timeOffToApprove);
    }
}
