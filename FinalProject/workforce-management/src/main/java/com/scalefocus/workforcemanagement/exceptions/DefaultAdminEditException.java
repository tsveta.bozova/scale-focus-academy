package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Default admin cannot be edited")
public class DefaultAdminEditException extends RuntimeException {
    public DefaultAdminEditException(String message) {
        super(message);
    }
}
