package com.scalefocus.workforcemanagement.dtos.timeoff;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.validators.annotations.LeaveType;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class TimeOffRequestDTO {

    @LeaveType(enumClass = TimeOffType.class)
    private String type;

    @FutureOrPresent(message = "Please enter a valid start date.")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    private LocalDate endDate;
    private Integer numberOfDays;

    @Size(max = 300, message = "Time off request description must be maximum 300 characters")
    private String reason;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Integer getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @AssertTrue(message = "Invalid time off period")
    public boolean isValidPeriod() {
        if (!isPeriodDataValid()) {
            return false;
        }

        if (this.type.equals("PAID") || this.type.equals("UNPAID")) {
            return isPaidUnpaidPeriodValid();
        } else if (this.type.equals("SICK_LEAVE")) {
            return isSickLeavePeriodValid();
        } else {
            return false;
        }
    }

    private boolean isPeriodDataValid() {
        return this.startDate != null
                && this.startDate.getYear() == LocalDate.now().getYear()
                && (this.endDate != null || this.numberOfDays != null);
    }

    private boolean isSickLeavePeriodValid() {
        return this.endDate != null
                && this.endDate.getYear() == LocalDate.now().getYear()
                && !this.startDate.isBefore(LocalDate.now())
                && !this.startDate.isAfter(this.endDate)
                && this.numberOfDays == null;
    }

    private boolean isPaidUnpaidPeriodValid() {
        return this.numberOfDays != null
                && this.numberOfDays >= 1
                && this.endDate == null;
    }
}
