package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class LockedTimeOffRequestException extends RuntimeException {
    public LockedTimeOffRequestException(String message) {
        super(message);
    }
}
