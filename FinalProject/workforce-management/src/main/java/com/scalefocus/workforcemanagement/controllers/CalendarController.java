package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.calendar.RestDayResponseDTO;
import com.scalefocus.workforcemanagement.services.CalendarService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@Validated
@RestController
public class CalendarController {
    private final CalendarService calendarService;

    @Autowired
    public CalendarController(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @GetMapping("/calendar/{monthNumber}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    List<RestDayResponseDTO> getAllRestDays(@PathVariable("monthNumber")
                                      @Min(value = 1, message = "Month number must be minimum 1")
                                      @Max(value = 12, message = "Month number must be maximum 12")
                                      int monthNumber){
        return calendarService.getAllNonWorkingDays(monthNumber);
    }
}
