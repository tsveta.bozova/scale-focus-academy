package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffResponseDTO;
import com.scalefocus.workforcemanagement.services.TimeOffService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
public class TimeOffController {
    private final TimeOffService timeOffService;

    @Autowired
    public TimeOffController(TimeOffService timeOffService) {
        this.timeOffService = timeOffService;
    }

    @GetMapping("/time-offs")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public List<TimeOffResponseDTO> getAll() {
        return timeOffService.getAll();
    }

    @GetMapping("/time-offs/{timeOffId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TimeOffResponseDTO getById(
            @PathVariable("timeOffId") @NotNull @Min(value = 1, message = "Invalid timeOff id") Long timeOffId) {
        return timeOffService.getById(timeOffId);
    }

    @PostMapping("/time-offs")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    @ResponseStatus(HttpStatus.CREATED)
    public TimeOffResponseDTO createTimeOff(@Valid @RequestBody TimeOffRequestDTO timeOffRequestDTO) {
        return timeOffService.create(timeOffRequestDTO);
    }

    @PutMapping("/time-offs/cancel/{timeOffId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TimeOffResponseDTO cancelTimeOff(
            @PathVariable("timeOffId") @NotNull @Min(value = 1, message = "Invalid timeOff id") Long timeOffId) {
        return timeOffService.cancel(timeOffId);
    }

    @PutMapping("/time-offs/{timeOffId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TimeOffResponseDTO updateTimeOff(
            @PathVariable("timeOffId") @NotNull @Min(value = 1, message = "Invalid time off id") Long timeOffId,
            @Valid @RequestBody TimeOffRequestDTO timeOffRequestDTO) {
        return timeOffService.update(timeOffRequestDTO, timeOffId);
    }

    @DeleteMapping("/time-offs/{timeOffId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public void deleteTimeOff(
            @PathVariable("timeOffId") @NotNull @Min(value = 1, message = "Invalid timeOff id") Long timeOffId) {
        timeOffService.delete(timeOffId);
    }

    @PutMapping("/time-offs/approve/{timeOffId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TimeOffResponseDTO approveTimeOff(@PathVariable("timeOffId")
                                             @NotNull @Min(value = 1, message = "Invalid time-off ID")
                                                     Long timeOffId) {
        return timeOffService.approve(timeOffId);
    }

    @PutMapping("/time-offs/reject/{timeOffId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TimeOffResponseDTO rejectTimeOff(
            @PathVariable("timeOffId") @NotNull @Min(value = 1, message = "Invalid timeOff id") Long timeOffId) {
        return timeOffService.reject(timeOffId);
    }
}
