package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Default admin user cannot be part of team")
public class DefaultAdminLeaderAssignmentException extends RuntimeException {
    public DefaultAdminLeaderAssignmentException(String message) {
        super(message);
    }
}
