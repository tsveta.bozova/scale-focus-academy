package com.scalefocus.workforcemanagement.entities;

import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TIME_OFF_REQUESTS", schema = "WMAPI")
public class TimeOff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TIME_OFF_ID")
    private Long id;

    @Column(name = "TIME_OFF_TYPE")
    private TimeOffType type;

    @Column(name = "TIME_OFF_START")
    private LocalDate timeOffStart;

    @Column(name = "TIME_OFF_END")
    private LocalDate timeOffEnd;

    @Column(name = "TIME_OFF_REASON", length = 300)
    private String reason;

    @Column(name = "TIME_OFF_STATUS", columnDefinition = "NUMBER(1,0) DEFAULT 0")
    private TimeOffStatus status = TimeOffStatus.AWAITING;

    @Column(name = "TIME_OFF_DATE_CREATED")
    private Instant dateCreated;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "TIME_OFF_CREATOR_ID")
    private User creator;

    @Column(name = "TIME_OFF_LAST_CHANGED")
    private Instant dateUpdated;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "TIME_OFF_LAST_CHANGE_USER_ID")
    private User editor;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
    @JoinTable(
            name = "APPROVALS",
            schema = "WMAPI",
            joinColumns = {@JoinColumn(name = "APPROVAL_TIME_OFF_ID")},
            inverseJoinColumns = {@JoinColumn(name = "APPROVER_ID")}
    )
    private List<User> approversList = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TimeOffType getType() {
        return type;
    }

    public void setType(TimeOffType type) {
        this.type = type;
    }

    public LocalDate getTimeOffStart() {
        return timeOffStart;
    }

    public void setTimeOffStart(LocalDate timeOffStart) {
        this.timeOffStart = timeOffStart;
    }

    public LocalDate getTimeOffEnd() {
        return timeOffEnd;
    }

    public void setTimeOffEnd(LocalDate timeOffEnd) {
        this.timeOffEnd = timeOffEnd;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public TimeOffStatus getStatus() {
        return status;
    }

    public void setStatus(TimeOffStatus status) {
        this.status = status;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @PrePersist
    public void setDateCreated() {
        this.dateCreated = Instant.now();
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<User> getApproversList() {
        return approversList;
    }

    public void setApproversList(List<User> approversList) {
        this.approversList = approversList;
    }
}
