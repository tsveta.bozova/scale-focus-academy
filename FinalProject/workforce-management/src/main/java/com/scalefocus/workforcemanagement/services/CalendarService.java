package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.calendar.RestDayResponseDTO;

import java.util.List;

public interface CalendarService {

    List<RestDayResponseDTO> getAllNonWorkingDays (int month);
}
