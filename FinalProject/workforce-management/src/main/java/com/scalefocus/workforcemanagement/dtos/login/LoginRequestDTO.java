package com.scalefocus.workforcemanagement.dtos.login;

import com.scalefocus.workforcemanagement.validators.annotations.CustomEmail;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

public class LoginRequestDTO {

    @Size(min = 5, max = 254, message = "Email must be between 5 and 254 characters")
    @CustomEmail(message = "Please enter a valid email. Email must be between 5 and 254 characters")
    private String email;

    @Size(min = 4, max = 120, message = "Password must be between 4 and 120 characters")
    @NotBlank(message = "Please enter a valid password. Password must be between 4 and 120 characters")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginRequestDTO that = (LoginRequestDTO) o;
        return Objects.equals(email, that.email) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }
}
