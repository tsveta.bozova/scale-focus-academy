package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserAlreadyExistsInTeamException extends RuntimeException {
    public UserAlreadyExistsInTeamException(String message) {
        super(message);
    }
}
