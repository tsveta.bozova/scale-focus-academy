package com.scalefocus.workforcemanagement.repositories;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Component
public class UserRepositoryImpl implements CustomUserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void resetAllRemainingDaysOff() {
        Query resAllDaysToDefaultQuery = entityManager
                .createNativeQuery("update WMAPI.USERS set PAID_DAYS = 20, UNPAID_DAYS = 90, SICK_LEAVE = 40");
        resAllDaysToDefaultQuery.executeUpdate();
    }
}
