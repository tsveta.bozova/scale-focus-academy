package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.DuplicateTimeOffDatesException;
import com.scalefocus.workforcemanagement.exceptions.NotEnoughAvailableDaysException;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.services.AuthenticationService;
import com.scalefocus.workforcemanagement.services.SickLeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class SickLeaveServiceImpl implements SickLeaveService {

    private final TimeOffRepository timeOffRepository;
    private final AuthenticationService authenticationService;
    private final OfficialHolidayClient officialHolidayClient;

    @Autowired
    public SickLeaveServiceImpl(TimeOffRepository timeOffRepository,
                                AuthenticationService authenticationService,
                                OfficialHolidayClient officialHolidayClient) {
        this.timeOffRepository = timeOffRepository;
        this.authenticationService = authenticationService;
        this.officialHolidayClient = officialHolidayClient;
    }

    @Override
    public TimeOff create(TimeOffRequestDTO sickLeaveRequestDTO) {
        User creator = authenticationService.getLoggedInUser();
        TimeOff newSickLeave = sickLeaveRequestToSickLeave(sickLeaveRequestDTO);
        newSickLeave.setCreator(creator);
        newSickLeave.setEditor(creator);
        newSickLeave.setDateUpdated(Instant.now());

        checkForEnoughAvailableDays(creator, newSickLeave);
        checkForOverlappingExistingTimeOff(newSickLeave);

        creator.setSickLeaveDays(creator.getSickLeaveDays() - getWorkingDaysInRequest(newSickLeave));
        newSickLeave.setStatus(TimeOffStatus.APPROVED);

        return newSickLeave;
    }

    private TimeOff sickLeaveRequestToSickLeave(TimeOffRequestDTO sickLeaveRequestDTO) {
        TimeOff sickLeave = new TimeOff();
        sickLeave.setTimeOffStart(sickLeaveRequestDTO.getStartDate());
        sickLeave.setReason(sickLeaveRequestDTO.getReason());
        sickLeave.setType(TimeOffType.valueOf(sickLeaveRequestDTO.getType()));
        sickLeave.setTimeOffEnd(sickLeaveRequestDTO.getEndDate());

        return sickLeave;
    }

    private void checkForEnoughAvailableDays(User loggedInUser, TimeOff newSickLeave) {
        int workingDaysInRequest = getWorkingDaysInRequest(newSickLeave);
        if (loggedInUser.getSickLeaveDays() < workingDaysInRequest) {
            throw new NotEnoughAvailableDaysException("Not enough available sick leave days");
        }
    }

    private int getWorkingDaysInRequest(TimeOff sickLeave) {
        return getNumberOfDaysInRequest(sickLeave)
                - getNumberOfWeekendDaysInRequest(sickLeave)
                - getNumberOfHolidaysInRequest(sickLeave);
    }

    private int getNumberOfDaysInRequest(TimeOff sickLeave) {
        return (int) sickLeave.getTimeOffStart().until(sickLeave.getTimeOffEnd().plusDays(1), ChronoUnit.DAYS);
    }

    private int getNumberOfWeekendDaysInRequest(TimeOff newSickLeave) {
        return (int) newSickLeave.getTimeOffStart()
                .datesUntil(newSickLeave.getTimeOffEnd().plusDays(1))
                .filter(this::isWeekend)
                .count();
    }

    private boolean isWeekend(LocalDate date) {
        return date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    private int getNumberOfHolidaysInRequest(TimeOff newSickLeave) {
        List<LocalDate> allHolidays = officialHolidayClient.getOfficialHolidays();
        return (int) allHolidays.stream()
                .filter(holiday -> isDayInRequest(holiday, newSickLeave))
                .count();
    }

    private boolean isDayInRequest(LocalDate dateToCheck, TimeOff newSickLeave) {
        return !(dateToCheck.isBefore(newSickLeave.getTimeOffStart()) ||
                dateToCheck.isAfter(newSickLeave.getTimeOffEnd()));
    }

    private void checkForOverlappingExistingTimeOff(TimeOff newSickLeave) {
        Long creatorId = newSickLeave.getCreator().getId();
        boolean hasOverlappingDays = timeOffRepository.findAllByCreatorId(creatorId).stream()
                .filter(this::isAwaitingOrApproved)
                .anyMatch(existingTimeOff -> hasOverlappingPeriods(existingTimeOff, newSickLeave));

        if (hasOverlappingDays) {
            throw new DuplicateTimeOffDatesException("Requested period has overlapping days with existing time off");
        }
    }

    private boolean isAwaitingOrApproved(TimeOff timeOff) {
        return timeOff.getStatus().equals(TimeOffStatus.APPROVED) || timeOff.getStatus().equals(TimeOffStatus.AWAITING);
    }

    private boolean hasOverlappingPeriods(TimeOff existingTimeOff, TimeOff newSickLeave) {
        LocalDate newTimeOffStart = newSickLeave.getTimeOffStart();
        LocalDate newTimeOffEnd = newSickLeave.getTimeOffEnd();
        return isDayInRequest(newTimeOffStart, existingTimeOff)
                || isDayInRequest(newTimeOffEnd, existingTimeOff);
    }
}
