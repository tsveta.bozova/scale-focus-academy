package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.dtos.team.TeamRequestDTO;
import com.scalefocus.workforcemanagement.dtos.team.TeamResponseDTO;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.*;
import com.scalefocus.workforcemanagement.repositories.TeamRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import com.scalefocus.workforcemanagement.services.AuthenticationService;
import com.scalefocus.workforcemanagement.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {

    private final UserRepository userRepository;
    private final TeamRepository teamRepository;
    private final AuthenticationService authenticationService;

    @Autowired
    public TeamServiceImpl(UserRepository userRepository,
                           TeamRepository teamRepository,
                           AuthenticationService authenticationService) {
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
        this.authenticationService = authenticationService;
    }

    @Override
    public List<TeamResponseDTO> getAll() {
        return teamRepository.findAll().stream()
                .map(TeamResponseDTO::responseFromTeam)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public TeamResponseDTO create(TeamRequestDTO teamRequestDTO) {
        if (teamRepository.findByTitle(teamRequestDTO.getTitle()).isPresent()) {
            throw new TeamTitleTakenException("Team title is taken");
        }

        User loggedUser = authenticationService.getLoggedInUser();
        Team newTeam = teamRequestDtoToTeam(teamRequestDTO);
        newTeam.setCreator(loggedUser);
        newTeam.setEditor(loggedUser);
        newTeam.setDateUpdated(Instant.now());

        return TeamResponseDTO.responseFromTeam(teamRepository.save(newTeam));
    }

    @Override
    public TeamResponseDTO getById(Long teamId) {
        Team team = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamNotFoundException("Team not found"));
        return TeamResponseDTO.responseFromTeam(team);
    }

    @Override
    @Transactional
    public TeamResponseDTO setTeamLeader(Long teamId, Long userId) {
        Team team = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamNotFoundException("Team not found"));
        User teamLeader = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        checkUserIsGdprDeleted(teamLeader);
        checkIfNewTeamLeaderIsDefaultAdmin(teamLeader);
        checkIfUserIsTeamMember(team, userId);

        User loggedUser = authenticationService.getLoggedInUser();
        team.setTeamLeader(teamLeader);
        team.setEditor(loggedUser);
        team.setDateUpdated(Instant.now());
        teamRepository.save(team);

        return TeamResponseDTO.responseFromTeam(team);
    }

    private Team teamRequestDtoToTeam(TeamRequestDTO teamRequestDTO) {
        Team team = new Team();

        team.setTitle(teamRequestDTO.getTitle());
        team.setDescription(teamRequestDTO.getDescription());

        if (teamRequestDTO.getTeamLeaderId() != null) {
            User teamLeader = userRepository.findById(teamRequestDTO.getTeamLeaderId()).orElseThrow(
                    () -> new UserNotFoundException("User not found"));

            checkUserIsGdprDeleted(teamLeader);
            checkIfNewTeamLeaderIsDefaultAdmin(teamLeader);
            checkIfUserIsTeamMember(team, teamLeader.getId());

            team.setTeamLeader(teamLeader);
        }

        return team;
    }

    @Override
    @Transactional
    public TeamResponseDTO removeUserFromTeam(Long teamId, Long userId) {
        Team teamToEdit = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamNotFoundException("Team not found"));
        User userToRemove = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        checkIfUserIsTeamMember(teamToEdit, userToRemove.getId());

        if (teamToEdit.getTeamLeader().getId().equals(userId)) {
            teamToEdit.setTeamLeader(null);
        }

        User loggedUser = authenticationService.getLoggedInUser();

        teamToEdit.getTeamMembers().remove(userToRemove);
        userToRemove.getTeamList().remove(teamToEdit);

        teamToEdit.setDateUpdated(Instant.now());
        teamToEdit.setEditor(loggedUser);

        return TeamResponseDTO.responseFromTeam(teamRepository.save(teamToEdit));
    }

    @Override
    @Transactional
    public TeamResponseDTO updateTeam(Long teamId, TeamRequestDTO teamRequestDTO) {
        Team teamToUpdate = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamNotFoundException("Team not found"));

        checkIfTeamTitleExists(teamToUpdate, teamRequestDTO);

        teamToUpdate.setTitle(teamRequestDTO.getTitle());
        teamToUpdate.setDescription(teamRequestDTO.getDescription());

        updateTeamLeader(teamRequestDTO, teamToUpdate);

        User loggedUser = authenticationService.getLoggedInUser();
        teamToUpdate.setEditor(loggedUser);
        teamToUpdate.setDateUpdated(Instant.now());
        teamRepository.save(teamToUpdate);
        return TeamResponseDTO.responseFromTeam(teamToUpdate);
    }

    private void checkIfTeamTitleExists(Team teamToUpdate, TeamRequestDTO teamRequestDTO) {
        if (isNewTeamNameDifferent(teamToUpdate, teamRequestDTO) && isTeamNameRegistered(teamRequestDTO)) {
            throw new TeamTitleTakenException("Team title is taken");
        }
    }

    private boolean isNewTeamNameDifferent(Team teamToEdit, TeamRequestDTO teamRequestDTO) {
        return !teamToEdit.getTitle().equalsIgnoreCase(teamRequestDTO.getTitle());
    }

    private boolean isTeamNameRegistered(TeamRequestDTO teamRequestDTO) {
        return teamRepository.findByTitle(teamRequestDTO.getTitle()).isPresent();
    }

    private void updateTeamLeader(TeamRequestDTO teamRequestDTO, Team teamToUpdate) {
        if (teamRequestDTO.getTeamLeaderId() != null) {
            User teamLeader = userRepository.findById(teamRequestDTO.getTeamLeaderId())
                    .orElseThrow(() -> new UserNotFoundException("User not found"));
            checkUserIsGdprDeleted(teamLeader);
            checkIfNewTeamLeaderIsDefaultAdmin(teamLeader);
            checkIfUserIsTeamMember(teamToUpdate, teamRequestDTO.getTeamLeaderId());

            teamToUpdate.setTeamLeader(teamLeader);
        }
    }

    private void checkIfUserIsTeamMember(Team team, Long userId) {
        if (!getTeamMembersIds(team).contains(userId)) {
            throw new UserNotInTeamException("User is not team member");
        }
    }

    private List<Long> getTeamMembersIds(Team team) {
        List<Long> teamMemberIds = new ArrayList<>();

        team.getTeamMembers()
                .stream()
                .filter(member -> !(member.isUserGdprDeleted()))
                .forEach(member -> teamMemberIds.add(member.getId()));

        return teamMemberIds;
    }

    private void checkIfNewTeamLeaderIsDefaultAdmin(User teamLeader) {
        if (teamLeader.getEmail().equals("admin")) {
            throw new DefaultAdminLeaderAssignmentException("Default admin user cannot be team leader");
        }
    }

    @Override
    @Transactional
    public void deleteTeam(Long teamId) {
        teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamNotFoundException("Team not found"));

        teamRepository.deleteById(teamId);
    }

    @Override
    @Transactional
    public TeamResponseDTO assignUserToTeam(Long teamId, Long userId) {
        User loggedUser = authenticationService.getLoggedInUser();
        Team teamToEdit = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamNotFoundException("Team not found"));

        if (userId == 1) {
            throw new DefaultAdminLeaderAssignmentException("Default admin user cannot be team member");
        }

        User userToAdd = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        checkUserIsGdprDeleted(userToAdd);
        if (teamToEdit.getTeamMembers().contains(userToAdd)) {
            throw new UserAlreadyExistsInTeamException("User already assigned to team");
        }

        teamToEdit.getTeamMembers().add(userToAdd);
        userToAdd.getTeamList().add(teamToEdit);
        teamToEdit.setDateUpdated(Instant.now());
        teamToEdit.setEditor(loggedUser);

        return TeamResponseDTO.responseFromTeam(teamRepository.save(teamToEdit));
    }

    private void checkUserIsGdprDeleted(User userToAdd) {
        if (userToAdd.isUserGdprDeleted()) {
            throw new UserNotFoundException("User not found");
        }
    }
}
