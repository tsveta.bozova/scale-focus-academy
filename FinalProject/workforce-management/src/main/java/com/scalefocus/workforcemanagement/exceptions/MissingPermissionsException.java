package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class MissingPermissionsException extends RuntimeException {
    public MissingPermissionsException(String message) {
        super(message);
    }
}
