package com.scalefocus.workforcemanagement.repositories;

public interface CustomUserRepository {
    void resetAllRemainingDaysOff();
}
