package com.scalefocus.workforcemanagement.jobs;

import com.scalefocus.workforcemanagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class YearlyJobs {
    private final UserService userService;

    @Autowired
    public YearlyJobs(UserService userService) {
        this.userService = userService;
    }

    @Scheduled(cron = "0 1 0 1 1 *")
    public void resetDayOffLimit(){
        userService.resetDayOffLimit();
    }
}
