package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.entities.User;

public interface AuthenticationService {
    User getLoggedInUser();
}
