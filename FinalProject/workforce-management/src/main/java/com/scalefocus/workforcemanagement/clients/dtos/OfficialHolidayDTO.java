package com.scalefocus.workforcemanagement.clients.dtos;

import java.time.LocalDate;

public class OfficialHolidayDTO {
    private LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
