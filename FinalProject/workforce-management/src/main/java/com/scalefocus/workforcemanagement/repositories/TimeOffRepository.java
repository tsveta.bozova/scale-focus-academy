package com.scalefocus.workforcemanagement.repositories;

import com.scalefocus.workforcemanagement.entities.TimeOff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TimeOffRepository extends JpaRepository<TimeOff, Long> {
    List<TimeOff> findAllByCreatorId(Long creatorId);
}
