package com.scalefocus.workforcemanagement.dtos.timeoff;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.TimeOff;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

public class TimeOffResponseDTO {

    private Long id;
    private TimeOffType type;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private LocalDate endDate;

    private String reason;

    private TimeOffStatus status;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private Instant dateCreated;
    private Long creatorId;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private Instant dateUpdated;
    private Long editorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TimeOffType getType() {
        return type;
    }

    public void setType(TimeOffType type) {
        this.type = type;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public TimeOffStatus getStatus() {
        return status;
    }

    public void setStatus(TimeOffStatus status) {
        this.status = status;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Long getEditorId() {
        return editorId;
    }

    public void setEditorId(Long editorId) {
        this.editorId = editorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeOffResponseDTO that = (TimeOffResponseDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(type, that.type) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(status, that.status) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(dateUpdated, that.dateUpdated) &&
                Objects.equals(editorId, that.editorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, startDate, endDate, status, reason, dateCreated, creatorId, dateUpdated, editorId);
    }

    public static TimeOffResponseDTO responseFromTimeOff(TimeOff timeOff) {
        TimeOffResponseDTO response = new TimeOffResponseDTO();
        response.setId(timeOff.getId());
        response.setType(timeOff.getType());
        response.setStartDate(timeOff.getTimeOffStart());
        response.setEndDate(timeOff.getTimeOffEnd());
        response.setReason(timeOff.getReason());
        response.setStatus(timeOff.getStatus());
        response.setDateCreated(timeOff.getDateCreated());
        response.setCreatorId(timeOff.getCreator().getId());
        response.setDateUpdated(timeOff.getDateUpdated());
        response.setEditorId(timeOff.getEditor().getId());
        return response;
    }
}
