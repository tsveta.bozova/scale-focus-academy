package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.team.TeamRequestDTO;
import com.scalefocus.workforcemanagement.dtos.team.TeamResponseDTO;
import com.scalefocus.workforcemanagement.services.TeamService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@PreAuthorize("hasRole('ADMIN')")
public class TeamController {
    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @GetMapping("/teams")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public List<TeamResponseDTO> getAll() {
        return teamService.getAll();
    }

    @PostMapping("/teams")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TeamResponseDTO createTeam(@Valid @RequestBody TeamRequestDTO teamRequestDTO) {
        return teamService.create(teamRequestDTO);
    }

    @GetMapping("/teams/{teamId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TeamResponseDTO getById(
            @PathVariable("teamId") @NotNull @Min(value = 1, message = "Invalid team id") Long userId) {
        return teamService.getById(userId);
    }

    @PutMapping("/teams/{teamId}/team-leader/{userId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TeamResponseDTO setTeamLeader(
            @PathVariable("teamId") @NotNull @Min(value = 1, message = "Invalid team id") Long teamId,
            @PathVariable("userId") @NotNull @Min(value = 1, message = "Invalid user id") Long userId) {

        return teamService.setTeamLeader(teamId, userId);
    }

    @DeleteMapping("/teams/{teamId}/users/{userId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TeamResponseDTO removeUserFromTeam(
            @PathVariable("teamId") @NotNull @Min(value = 1, message = "Invalid team id") Long teamId,
            @PathVariable("userId") @NotNull @Min(value = 1, message = "Invalid user id") Long userId) {
        return teamService.removeUserFromTeam(teamId, userId);
    }

    @PutMapping("/teams/{teamId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TeamResponseDTO updateTeam(
            @PathVariable("teamId") @NotNull @Min(value = 1, message = "Invalid team id") Long teamId,
            @Valid @RequestBody TeamRequestDTO teamRequestDTO) {
        return teamService.updateTeam(teamId, teamRequestDTO);
    }

    @DeleteMapping("/teams/{teamId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public void deleteTeam(@PathVariable("teamId") @NotNull @Min(value = 1, message = "Invalid team id") Long teamId) {
        teamService.deleteTeam(teamId);
    }

    @PutMapping("/teams/{teamId}/users/{userId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public TeamResponseDTO assignUserToTeam(
            @PathVariable("teamId") @NotNull @Min(value = 1, message = "Invalid team id") Long teamId,
            @PathVariable("userId") @NotNull @Min(value = 1, message = "Invalid user id") Long userId) {
        return teamService.assignUserToTeam(teamId, userId);
    }

}
