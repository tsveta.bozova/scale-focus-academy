package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.user.UserRequestDTO;
import com.scalefocus.workforcemanagement.dtos.user.UserResponseDTO;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.DefaultAdminEditException;
import com.scalefocus.workforcemanagement.exceptions.UserNotFoundException;
import com.scalefocus.workforcemanagement.exceptions.UsernameTakenException;
import com.scalefocus.workforcemanagement.repositories.TeamRepository;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepositoryImpl;
import com.scalefocus.workforcemanagement.services.AuthenticationService;
import com.scalefocus.workforcemanagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private static final long DEFAULT_ADMIN_ID = 1;

    private final UserRepository userRepository;
    private final TeamRepository teamRepository;
    private final TimeOffRepository timeOffRepository;
    private final UserRepositoryImpl customUserRepository;
    private final AuthenticationService authenticationService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           TeamRepository teamRepository,
                           TimeOffRepository timeOffRepository,
                           UserRepositoryImpl customUserRepository,
                           AuthenticationService authenticationService,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
        this.timeOffRepository = timeOffRepository;
        this.customUserRepository = customUserRepository;
        this.authenticationService = authenticationService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public List<UserResponseDTO> getAll() {
        return userRepository.findAll()
                .stream()
                .filter(user -> !user.isUserGdprDeleted())
                .map(UserResponseDTO::responseFromUser)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponseDTO getById(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));
        checkUserIsGdprDeleted(user);
        return UserResponseDTO.responseFromUser(user);
    }

    @Override
    @Transactional
    public UserResponseDTO create(UserRequestDTO userRequestDTO) {
        User loggedUser = authenticationService.getLoggedInUser();

        if (isNewEmailRegistered(userRequestDTO)) {
            throw new UsernameTakenException("Email is already registered");
        }

        User newUser = userRequestDtoToUser(userRequestDTO);
        newUser.setCreator(loggedUser);
        newUser.setEditor(loggedUser);
        newUser.setDateUpdated(Instant.now());

        String rawPassword = newUser.getPassword();
        String encryptedPassword = bCryptPasswordEncoder.encode(rawPassword);
        newUser.setPassword(encryptedPassword);

        User savedUser = userRepository.save(newUser);
        return UserResponseDTO.responseFromUser(savedUser);
    }

    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    @Transactional
    public void gdprDelete(Long userId) {
        User userToDelete = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        checkIfDefaultAdmin(userToDelete);

        User loggedUser = authenticationService.getLoggedInUser();
        userToDelete.setEmail(null);
        userToDelete.setPassword(null);
        userToDelete.setFirstName(null);
        userToDelete.setLastName(null);
        gdprDeleteTeamLeader(userToDelete, loggedUser);

        cancelFutureTimeOffs(loggedUser, userToDelete);

        userToDelete.setDateUpdated(Instant.now());
        userToDelete.setEditor(loggedUser);
        userRepository.save(userToDelete);
    }

    private void gdprDeleteTeamLeader(User userToDelete, User loggedUser) {
        userToDelete.getTeamList()
                .forEach(team -> {
                    if (team.getTeamLeader() != null && team.getTeamLeader().equals(userToDelete)) {
                        team.setTeamLeader(null);
                        team.setDateUpdated(Instant.now());
                        team.setEditor(loggedUser);
                        teamRepository.save(team);
                    }
                });
    }

    private void cancelFutureTimeOffs(User loggedUser, User userToDelete) {
        getAllFutureTimeOffsFromUser(userToDelete)
                .forEach(
                        timeOff -> {
                            timeOff.setStatus(TimeOffStatus.CANCELLED);
                            timeOff.setDateUpdated(Instant.now());
                            timeOff.setEditor(loggedUser);
                            timeOffRepository.save(timeOff);
                        });
    }

    private List<TimeOff> getAllFutureTimeOffsFromUser(User user) {
        return timeOffRepository
                .findAll()
                .stream()
                .filter(timeOff -> timeOff.getCreator().getId().equals(user.getId()))
                .filter(this::isTimeOffStatusAwaitingOrApproved)
                .filter(timeOff -> timeOff.getTimeOffStart().isAfter(LocalDate.now()))
                .collect(Collectors.toList());
    }

    private boolean isTimeOffStatusAwaitingOrApproved(TimeOff timeOff) {
        return timeOff.getStatus().equals(TimeOffStatus.AWAITING)
                || timeOff.getStatus().equals(TimeOffStatus.APPROVED);
    }

    @Override
    @Transactional
    public UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long userId) {
        User loggedUser = authenticationService.getLoggedInUser();
        User userToUpdate = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        checkIfDefaultAdmin(userToUpdate);
        checkUserIsGdprDeleted(userToUpdate);
        checkIfEmailExists(userRequestDTO, userToUpdate);

        String password = bCryptPasswordEncoder.encode(userRequestDTO.getPassword());

        userToUpdate.setEmail(userRequestDTO.getEmail());
        userToUpdate.setPassword(password);
        userToUpdate.setFirstName(userRequestDTO.getFirstName());
        userToUpdate.setLastName(userRequestDTO.getLastName());
        userToUpdate.setAdmin(userRequestDTO.getAdmin());
        userToUpdate.setEditor(loggedUser);
        userToUpdate.setDateUpdated(Instant.now());

        userRepository.save(userToUpdate);
        return UserResponseDTO.responseFromUser(userToUpdate);
    }

    private void checkIfEmailExists(UserRequestDTO userRequestDTO, User userToUpdate) {
        if (isNewEmailDifferent(userRequestDTO, userToUpdate) && isNewEmailRegistered(userRequestDTO)) {
            throw new UsernameTakenException("Email is already registered");
        }
    }

    private boolean isNewEmailRegistered(UserRequestDTO userRequestDTO) {
        return userRepository.findByEmail(userRequestDTO.getEmail()).isPresent();
    }

    private boolean isNewEmailDifferent(UserRequestDTO userRequestDTO, User userToUpdate) {
        return !userToUpdate.getEmail().equalsIgnoreCase(userRequestDTO.getEmail());
    }

    private void checkUserIsGdprDeleted(User userToUpdate) {
        if (userToUpdate.isUserGdprDeleted()) {
            throw new UserNotFoundException("User not found");
        }
    }

    private void checkIfDefaultAdmin(User user) {
        if (user.getId().equals(DEFAULT_ADMIN_ID)) {
            throw new DefaultAdminEditException("Cannot edit default admin");
        }
    }

    private User userRequestDtoToUser(UserRequestDTO userRequestDTO) {
        User user = new User();
        user.setEmail(userRequestDTO.getEmail());
        user.setPassword(userRequestDTO.getPassword());
        user.setFirstName(userRequestDTO.getFirstName());
        user.setLastName(userRequestDTO.getLastName());
        user.setAdmin(userRequestDTO.getAdmin());

        return user;
    }

    @Transactional
    public void resetDayOffLimit() {
        customUserRepository.resetAllRemainingDaysOff();
    }
}
