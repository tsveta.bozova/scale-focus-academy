package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.entities.TimeOff;

public interface VacationService {
    TimeOff create(TimeOffRequestDTO timeOffRequestDTO);

    TimeOff update(TimeOffRequestDTO timeOffRequestDTO, Long timeOffId);

    void checkCanBeDeleted(Long timeOffId);

    TimeOff approve(Long timeOffId);

    TimeOff reject(Long timeOffId);
}
