package com.scalefocus.workforcemanagement.dtos.team;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

public class TeamRequestDTO {

    @Size(min = 3, max = 120, message = "Team title must be between 3 and 120 characters")
    @NotBlank(message = "Please enter a valid team title. Team title must be between 3 and 120 characters")
    private String title;

    @Size(max = 300, message = "Team description must be maximum 120 characters")
    private String description;

    private Long teamLeaderId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTeamLeaderId() {
        return teamLeaderId;
    }

    public void setTeamLeaderId(Long teamLeaderId) {
        this.teamLeaderId = teamLeaderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamRequestDTO that = (TeamRequestDTO) o;
        return teamLeaderId.equals(that.teamLeaderId) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description, teamLeaderId);
    }
}
