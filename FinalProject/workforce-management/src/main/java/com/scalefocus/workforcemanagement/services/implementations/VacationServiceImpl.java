package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.*;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import com.scalefocus.workforcemanagement.services.AuthenticationService;
import com.scalefocus.workforcemanagement.services.VacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VacationServiceImpl implements VacationService {

    private final UserRepository userRepository;
    private final TimeOffRepository timeOffRepository;
    private final AuthenticationService authenticationService;
    private final OfficialHolidayClient officialHolidayClient;

    @Autowired
    public VacationServiceImpl(TimeOffRepository timeOffRepository,
                               AuthenticationService authenticationService,
                               OfficialHolidayClient officialHolidayClient,
                               UserRepository userRepository) {
        this.userRepository = userRepository;
        this.timeOffRepository = timeOffRepository;
        this.authenticationService = authenticationService;
        this.officialHolidayClient = officialHolidayClient;
    }

    @Override
    public TimeOff create(TimeOffRequestDTO timeOffRequestDTO) {
        User creator = authenticationService.getLoggedInUser();
        TimeOff newTimeOff = timeOffRequestToTimeOff(timeOffRequestDTO);
        newTimeOff.setCreator(creator);
        newTimeOff.setEditor(creator);
        newTimeOff.setDateUpdated(Instant.now());

        shiftEndDate(newTimeOff, timeOffRequestDTO.getNumberOfDays());

        checkForEnoughAvailableDays(creator, newTimeOff);
        List<TimeOff> allTimeOffRequests = timeOffRepository.findAllByCreatorId(creator.getId());
        checkForOverlappingExistingTimeOff(allTimeOffRequests, newTimeOff);

        List<User> allUsersTeamLeadersInOffice = getUsersTeamLeadersInOffice(creator);
        if (allUsersTeamLeadersInOffice.isEmpty()) {
            removeApprovedRequestDaysFromUser(newTimeOff, creator);
            newTimeOff.setStatus(TimeOffStatus.APPROVED);
        }

        return newTimeOff;
    }

    private TimeOff timeOffRequestToTimeOff(TimeOffRequestDTO timeOffRequestDTO) {
        TimeOff timeOff = new TimeOff();
        timeOff.setTimeOffStart(timeOffRequestDTO.getStartDate());
        timeOff.setReason(timeOffRequestDTO.getReason());
        timeOff.setType(TimeOffType.valueOf(timeOffRequestDTO.getType()));
        timeOff.setTimeOffEnd(timeOffRequestDTO.getStartDate());

        return timeOff;
    }

    @Override
    public TimeOff update(TimeOffRequestDTO timeOffRequestDTO, Long timeOffId) {
        User editor = authenticationService.getLoggedInUser();
        TimeOff timeOffToUpdate = timeOffRepository.findById(timeOffId)
                .orElseThrow(() -> new TimeOffNotFoundException("Time off not found"));

        checkHasAccessToEdit(timeOffToUpdate, editor);
        checkCanBeEdited(timeOffToUpdate);

        timeOffToUpdate.setTimeOffStart(timeOffRequestDTO.getStartDate());
        timeOffToUpdate.setReason(timeOffRequestDTO.getReason());
        if (!timeOffToUpdate.getType().toString().equals(timeOffRequestDTO.getType())) {
            throw new InvalidTimeOffTypeException("Cannot edit time off type");
        }
        timeOffToUpdate.setType(TimeOffType.valueOf(timeOffRequestDTO.getType()));
        timeOffToUpdate.setTimeOffEnd(timeOffRequestDTO.getStartDate());

        shiftEndDate(timeOffToUpdate, timeOffRequestDTO.getNumberOfDays());

        List<TimeOff> timeOffRequests = timeOffRepository.findAllByCreatorId(timeOffToUpdate.getCreator().getId());
        timeOffRequests.remove(timeOffToUpdate);
        checkForOverlappingExistingTimeOff(timeOffRequests, timeOffToUpdate);
        checkForEnoughAvailableDays(editor, timeOffToUpdate);

        timeOffToUpdate.setDateUpdated(Instant.now());
        timeOffToUpdate.setEditor(editor);

        List<User> allUsersTeamLeadersInOffice = getUsersTeamLeadersInOffice(editor);
        if (allUsersTeamLeadersInOffice.isEmpty()) {
            removeApprovedRequestDaysFromUser(timeOffToUpdate, editor);
            timeOffToUpdate.setStatus(TimeOffStatus.APPROVED);
        }

        return timeOffToUpdate;
    }

    private void shiftEndDate(TimeOff newTimeOff, int numberOfRequestDays) {
        List<LocalDate> allHolidays = officialHolidayClient.getOfficialHolidays();
        while (isNonWorkingDay(newTimeOff.getTimeOffEnd(), allHolidays) || numberOfRequestDays > 1) {
            if (!isNonWorkingDay(newTimeOff.getTimeOffEnd(), allHolidays)) {
                numberOfRequestDays--;
            }
            newTimeOff.setTimeOffEnd(newTimeOff.getTimeOffEnd().plusDays(1));
        }
    }

    private boolean isNonWorkingDay(LocalDate day, List<LocalDate> allHolidays) {
        return day.getDayOfWeek().equals(DayOfWeek.SATURDAY)
                || day.getDayOfWeek().equals(DayOfWeek.SUNDAY)
                || allHolidays.contains(day);
    }

    private void checkForEnoughAvailableDays(User loggedInUser, TimeOff newTimeOff) {
        int workingDaysInRequest = getWorkingDaysInRequest(newTimeOff);
        if ((newTimeOff.getType() == TimeOffType.PAID
                && hasNotEnoughPaidDays(newTimeOff, workingDaysInRequest, loggedInUser))
                || (newTimeOff.getType() == TimeOffType.UNPAID
                && hasNotEnoughUnpaidDays(newTimeOff, workingDaysInRequest, loggedInUser))) {
            throw new NotEnoughAvailableDaysException("Not enough available days");
        }
    }

    private int getWorkingDaysInRequest(TimeOff newTimeOff) {
        return getNumberOfDaysInRequest(newTimeOff)
                - getNumberOfWeekendDaysInRequest(newTimeOff)
                - getNumberOfHolidaysInRequest(newTimeOff);
    }

    private int getNumberOfHolidaysInRequest(TimeOff newTimeOff) {
        List<LocalDate> allHolidays = officialHolidayClient.getOfficialHolidays();
        return (int) allHolidays.stream()
                .filter(holiday -> isDayInRequest(holiday, newTimeOff))
                .count();
    }

    private boolean isDayInRequest(LocalDate dateToCheck, TimeOff newTimeOff) {
        return !(dateToCheck.isBefore(newTimeOff.getTimeOffStart()) ||
                dateToCheck.isAfter(newTimeOff.getTimeOffEnd()));
    }

    private int getNumberOfWeekendDaysInRequest(TimeOff newTimeOff) {
        return (int) newTimeOff.getTimeOffStart()
                .datesUntil(newTimeOff.getTimeOffEnd().plusDays(1))
                .filter(this::isWeekend)
                .count();
    }

    private boolean isWeekend(LocalDate date) {
        return date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    private boolean hasNotEnoughPaidDays(TimeOff newTimeOff, int workingDaysInRequest, User loggedInUser) {
        return loggedInUser.getPaidDays() < workingDaysInRequest + getNumberOfPendingRequestDays(newTimeOff);
    }

    private boolean hasNotEnoughUnpaidDays(TimeOff newTimeOff, int workingDaysInRequest, User loggedInUser) {
        return loggedInUser.getUnpaidDays() < workingDaysInRequest + getNumberOfPendingRequestDays(newTimeOff);
    }

    private int getNumberOfPendingRequestDays(TimeOff newTimeOff) {
        return timeOffRepository.findAllByCreatorId(newTimeOff.getCreator().getId())
                .stream()
                .filter(timeOff -> timeOff.getId() != null && !timeOff.getId().equals(newTimeOff.getId()))
                .filter(timeOff -> timeOff.getType().equals(newTimeOff.getType())
                        && timeOff.getStatus().equals(TimeOffStatus.AWAITING))
                .mapToInt(this::getWorkingDaysInRequest)
                .sum();
    }

    private int getNumberOfDaysInRequest(TimeOff timeOff) {
        return (int) timeOff.getTimeOffStart().until(timeOff.getTimeOffEnd().plusDays(1), ChronoUnit.DAYS);
    }

    @Override
    public void checkCanBeDeleted(Long timeOffId) {
        TimeOff timeOffToDelete = timeOffRepository.findById(timeOffId)
                .orElseThrow(() -> new TimeOffNotFoundException("Time off not found"));

        User loggedInUser = authenticationService.getLoggedInUser();
        checkHasAccessToEdit(timeOffToDelete, loggedInUser);
        checkCanBeEdited(timeOffToDelete);
    }

    private void checkHasAccessToEdit(TimeOff timeOff, User loggedInUser) {
        if (!loggedInUser.getAdmin() && !timeOff.getCreator().getId().equals(loggedInUser.getId())) {
            throw new MissingPermissionsException("No permissions to edit request");
        }
    }

    private void checkCanBeEdited(TimeOff timeOff) {
        if (timeOff.getStatus() != TimeOffStatus.AWAITING || !timeOff.getApproversList().isEmpty()) {
            throw new ResourceLockedException("Request is locked");
        }
    }

    @Override
    public TimeOff approve(Long timeOffId) {
        TimeOff timeOffToApprove = timeOffRepository.findById(timeOffId)
                .orElseThrow(() -> new TimeOffNotFoundException("TimeOff request not found"));
        User approver = authenticationService.getLoggedInUser();

        checkIfTeamLeaderOfCreator(timeOffToApprove, approver);
        checkIsAwaiting(timeOffToApprove);
        checkHasAlreadyApproved(timeOffToApprove, approver);

        List<User> approversList = new ArrayList<>(timeOffToApprove.getApproversList());
        approversList.add(approver);
        timeOffToApprove.setApproversList(approversList);
        if (isLastApprover(timeOffToApprove)) {
            removeApprovedRequestDaysFromUser(timeOffToApprove, timeOffToApprove.getCreator());
            timeOffToApprove.setStatus(TimeOffStatus.APPROVED);
            timeOffToApprove.setDateUpdated(Instant.now());
            timeOffToApprove.setEditor(approver);
        }

        return timeOffToApprove;
    }

    private void checkHasAlreadyApproved(TimeOff timeOffToApprove, User approver) {
        if (timeOffToApprove.getApproversList().contains(approver)) {
            throw new DuplicateApprovalException("user id:" + approver.getId() +
                    ", time off id:" + timeOffToApprove.getId());
        }
    }

    @Override
    public TimeOff reject(Long timeOffId) {
        User loggedInUser = authenticationService.getLoggedInUser();
        TimeOff timeOffToReject = timeOffRepository.findById(timeOffId)
                .orElseThrow(() -> new TimeOffNotFoundException("TimeOff request not found"));

        checkIfTeamLeaderOfCreator(timeOffToReject, loggedInUser);
        checkIsAwaiting(timeOffToReject);

        timeOffToReject.setStatus(TimeOffStatus.REJECTED);
        timeOffToReject.getApproversList().clear();
        timeOffToReject.setEditor(loggedInUser);
        timeOffToReject.setDateUpdated(Instant.now());

        return timeOffToReject;
    }

    private void checkIfTeamLeaderOfCreator(TimeOff timeOffToReject, User loggedInUser) {
        List<Long> userIdsLedByLoggedUser = getUserIdsLedByUser(loggedInUser);
        if (!userIdsLedByLoggedUser.contains(timeOffToReject.getCreator().getId())) {
            throw new MissingPermissionsException("No permission to reject this request");
        }
    }

    private void checkIsAwaiting(TimeOff timeOff) {
        if (timeOff.getStatus() != TimeOffStatus.AWAITING) {
            throw new LockedTimeOffRequestException("Time off cannot be updated");
        }
    }

    private boolean isLastApprover(TimeOff timeOffToApprove) {
        List<User> leadersToApprove = getUsersTeamLeadersInOffice(timeOffToApprove.getCreator());
        return leadersToApprove.size() <= timeOffToApprove.getApproversList().size();
    }

    private List<Long> getUserIdsLedByUser(User user) {
        return getTeamsLedByUser(user)
                .stream()
                .flatMap(teamLedByUser -> teamLedByUser.getTeamMembers()
                        .stream()
                        .filter(member -> !member.getId().equals(user.getId()))
                        .map(User::getId))
                .distinct()
                .collect(Collectors.toList());
    }

    private List<Team> getTeamsLedByUser(User teamLeader) {
        return teamLeader.getTeamList()
                .stream()
                .filter(team -> team.getTeamLeader().getId().equals(teamLeader.getId()))
                .collect(Collectors.toList());
    }

    private void removeApprovedRequestDaysFromUser(TimeOff newTimeOff, User loggedInUser) {
        if (newTimeOff.getType() == TimeOffType.PAID) {
            loggedInUser.setPaidDays(loggedInUser.getPaidDays() - getWorkingDaysInRequest(newTimeOff));
        } else if (newTimeOff.getType() == TimeOffType.UNPAID) {
            loggedInUser.setUnpaidDays(loggedInUser.getUnpaidDays() - getWorkingDaysInRequest(newTimeOff));
        }
    }

    private List<Long> getIdsOfUserTeamLeaders(User loggedInUser) {
        return loggedInUser.getTeamList().stream()
                .filter(team -> team.getTeamLeader() != null &&
                        !team.getTeamLeader().getId().equals(loggedInUser.getId()))
                .map(team -> team.getTeamLeader().getId())
                .distinct()
                .collect(Collectors.toList());
    }

    private boolean isTeamLeaderInOffice(Long teamLeaderId) {
        return timeOffRepository.findAll()
                .stream()
                .filter(timeOff -> timeOff.getCreator().getId().equals(teamLeaderId))
                .filter(timeOff -> timeOff.getStatus().equals(TimeOffStatus.APPROVED))
                .noneMatch(timeOff -> isDayInRequest(LocalDate.now(), timeOff));
    }

    private List<User> getUsersTeamLeadersInOffice(User loggedInUser) {
        List<Long> teamLeaders = getIdsOfUserTeamLeaders(loggedInUser).stream()
                .filter(this::isTeamLeaderInOffice)
                .collect(Collectors.toList());
        return userRepository.findAllById(teamLeaders);
    }

    private void checkForOverlappingExistingTimeOff(List<TimeOff> timeOffRequests, TimeOff newTimeOff) {
        boolean hasOverlappingDays = timeOffRequests.stream()
                .filter(timeOff -> timeOff.getStatus().equals(TimeOffStatus.APPROVED) ||
                        timeOff.getStatus().equals(TimeOffStatus.AWAITING))
                .anyMatch(existingTimeOff -> hasOverlappingPeriods(existingTimeOff, newTimeOff));
        if (hasOverlappingDays) {
            throw new DuplicateTimeOffDatesException("Requested period has overlapping days with existing time off");
        }
    }

    private boolean hasOverlappingPeriods(TimeOff existingTimeOff, TimeOff newTimeOff) {
        LocalDate newTimeOffStart = newTimeOff.getTimeOffStart();
        LocalDate newTimeOffEnd = newTimeOff.getTimeOffEnd();
        return isDayInRequest(newTimeOffStart, existingTimeOff)
                || isDayInRequest(newTimeOffEnd, existingTimeOff);
    }
}
