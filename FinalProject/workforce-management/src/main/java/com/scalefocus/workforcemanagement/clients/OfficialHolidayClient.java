package com.scalefocus.workforcemanagement.clients;

import com.scalefocus.workforcemanagement.clients.dtos.OfficialHolidayDTO;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OfficialHolidayClient {
    private static final String URL = "https://date.nager.at/api/v2/publicholidays/2020/BG";

    RestTemplate restTemplate = new RestTemplate();

    ResponseEntity<List<OfficialHolidayDTO>> responseEntity = restTemplate.exchange(URL, HttpMethod.GET, null,
            new ParameterizedTypeReference<>() {
            });

    List<OfficialHolidayDTO> holidays = responseEntity.getBody();

    @Cacheable("holidays")
    public List<LocalDate> getOfficialHolidays() {
        for (OfficialHolidayDTO holiday : holidays) {
            if (holiday.getDate().getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                holiday.setDate(holiday.getDate().plusDays(2));
            } else if (holiday.getDate().getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                holiday.setDate(holiday.getDate().plusDays(1));
            }
        }
        return holidays
                .stream()
                .map(holiday -> LocalDate.from(holiday.getDate()))
                .distinct()
                .collect(Collectors.toList());
    }
}
