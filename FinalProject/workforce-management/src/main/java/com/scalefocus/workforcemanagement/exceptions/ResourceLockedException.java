package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.LOCKED)
public class ResourceLockedException extends RuntimeException {

    public ResourceLockedException(String message) {
        super(message);
    }
}
