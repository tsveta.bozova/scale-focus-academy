package com.scalefocus.workforcemanagement.dtos.team;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.User;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TeamResponseDTO {

    private Long id;
    private String title;
    private String description;
    private Long teamLeaderId;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private Instant dateCreated;
    private Long creatorId;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private Instant dateUpdated;
    private Long editorId;
    private List<Long> teamMemberIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTeamLeaderId() {
        return teamLeaderId;
    }

    public void setTeamLeaderId(Long teamLeaderId) {
        this.teamLeaderId = teamLeaderId;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Long getEditorId() {
        return editorId;
    }

    public void setEditorId(Long editorId) {
        this.editorId = editorId;
    }

    public List<Long> getTeamMemberIds() {
        return teamMemberIds;
    }

    public void setTeamMemberIds(List<Long> teamMemberIds) {
        this.teamMemberIds = teamMemberIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamResponseDTO that = (TeamResponseDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(teamLeaderId, that.teamLeaderId) &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(dateUpdated, that.dateUpdated) &&
                Objects.equals(editorId, that.editorId) &&
                Objects.equals(teamMemberIds, that.teamMemberIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, teamLeaderId, dateCreated, creatorId, dateUpdated, editorId, teamMemberIds);
    }

    public static TeamResponseDTO responseFromTeam(Team team) {
        TeamResponseDTO teamResponseDTO = new TeamResponseDTO();

        teamResponseDTO.setId(team.getId());
        teamResponseDTO.setDescription(team.getDescription());
        teamResponseDTO.setTitle(team.getTitle());

        if (team.getTeamLeader() != null) {
            teamResponseDTO.setTeamLeaderId(team.getTeamLeader().getId());
        }
        teamResponseDTO.setDateCreated(team.getDateCreated());
        teamResponseDTO.setCreatorId(team.getCreator().getId());
        teamResponseDTO.setDateUpdated(team.getDateUpdated());
        teamResponseDTO.setEditorId(team.getEditor().getId());
        teamResponseDTO.setTeamMemberIds(getTeamMembersIds(team));

        return teamResponseDTO;
    }

    private static List<Long> getTeamMembersIds(Team team) {
        return team.getTeamMembers().stream()
                .filter(member -> !(member.isUserGdprDeleted()))
                .map(User::getId)
                .collect(Collectors.toList());
    }
}
