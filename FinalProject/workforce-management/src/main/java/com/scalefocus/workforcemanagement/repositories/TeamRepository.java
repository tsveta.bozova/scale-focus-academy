package com.scalefocus.workforcemanagement.repositories;

import com.scalefocus.workforcemanagement.entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team, Long> {
    Optional<Team> findByTitle(String teamTitle);
}
