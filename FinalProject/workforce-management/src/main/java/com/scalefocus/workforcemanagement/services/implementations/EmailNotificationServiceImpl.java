package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class EmailNotificationServiceImpl implements NotificationService {
    private final JavaMailSender emailSender;
    private final SpringTemplateEngine thymeleafTemplateEngine;

    @Autowired
    public EmailNotificationServiceImpl(JavaMailSender mailSender, SpringTemplateEngine thymeleafTemplateEngine) {
        this.emailSender = mailSender;
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
    }

    @Override
    public void sendApproverNotification(User approver, TimeOff timeOff) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(approver.getEmail());
        email.setSubject("New " + timeOff.getType() + " leave request with ID " + timeOff.getId());
        email.setText("New " + timeOff.getType() +
                " Leave From " + timeOff.getTimeOffStart() +
                " Until " + timeOff.getTimeOffEnd() +
                " submitted by " + timeOff.getCreator().getFirstName() + " " +
                timeOff.getCreator().getLastName());
        emailSender.send(email);
    }

    @Override
    public void sendSickLeaveNotifications(User member, TimeOff timeOff) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(member.getEmail());
        email.setSubject("New Sick Leave Request ID:" + timeOff.getId());
        email.setText("New Sick Leave From " +
                timeOff.getTimeOffStart() + " Until " + timeOff.getTimeOffEnd() +
                ", submitted by " + timeOff.getCreator().getFirstName()
                + " " + timeOff.getCreator().getLastName());
        emailSender.send(email);
    }

    @Override
    public void sendApprovedNotification(TimeOff timeOff) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(timeOff.getCreator().getEmail());
        email.setSubject("Your " + timeOff.getType() + " leave request has been approved");
        email.setText("Approved " + timeOff.getType() +
                " Leave From " + timeOff.getTimeOffStart() +
                " Until " + timeOff.getTimeOffEnd() +
                " ID " + timeOff.getId());
        emailSender.send(email);
    }

    @Override
    public void sendRejectedNotification(TimeOff timeOff) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(timeOff.getCreator().getEmail());
        email.setSubject(timeOff.getType() + " leave ID:" + timeOff.getId() + " has been rejected");
        email.setText("Rejected " + timeOff.getType() +
                " Leave From " + timeOff.getTimeOffStart() +
                " Until " + timeOff.getTimeOffEnd() +
                " ID " + timeOff.getId() +
                ", submitted by " + timeOff.getCreator().getFirstName() + " " + timeOff.getCreator().getLastName());
        emailSender.send(email);
    }

    @Override
    public void sendEmail(User approver, TimeOff timeOff) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setTo(approver.getEmail());
        helper.setSubject("New " + timeOff.getType() + " leave request with ID " + timeOff.getId());

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("timeOffType", timeOff.getType());
        templateModel.put("startDate", timeOff.getTimeOffStart());
        templateModel.put("endDate", timeOff.getTimeOffEnd());
        templateModel.put("fName", timeOff.getCreator().getFirstName());
        templateModel.put("lName", timeOff.getCreator().getLastName());

        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(templateModel);
        String htmlBody = thymeleafTemplateEngine.process("template-thymeleaf.html", thymeleafContext);

        helper.setText(htmlBody, true);

        emailSender.send(message);
    }

}
