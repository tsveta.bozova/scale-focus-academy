package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Duplicate time off periods")
public class DuplicateTimeOffDatesException extends RuntimeException {
    public DuplicateTimeOffDatesException(String message) {
        super(message);
    }
}