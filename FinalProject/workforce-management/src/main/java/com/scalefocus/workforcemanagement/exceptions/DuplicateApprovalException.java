package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Cannot approve a request more than once")
public class DuplicateApprovalException extends RuntimeException {
    public DuplicateApprovalException(String message) {
        super(message);
    }
}