package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Time off request not found")
public class TimeOffNotFoundException extends RuntimeException {
    public TimeOffNotFoundException(String message) {
        super(message);
    }
}
