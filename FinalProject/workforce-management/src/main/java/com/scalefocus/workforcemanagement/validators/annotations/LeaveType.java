package com.scalefocus.workforcemanagement.validators.annotations;

import com.scalefocus.workforcemanagement.validators.LeaveTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = LeaveTypeValidator.class)
public @interface LeaveType {
    Class<? extends Enum<?>> enumClass();

    String message() default "Time off request must be PAID, UNPAID or SICK_LEAVE";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}