package com.scalefocus.workforcemanagement.dtos.timeoff.enums;

public enum TimeOffStatus {
    AWAITING,
    APPROVED,
    REJECTED,
    CANCELLED
}
