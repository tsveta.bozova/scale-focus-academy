package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.user.UserRequestDTO;
import com.scalefocus.workforcemanagement.dtos.user.UserResponseDTO;

import java.util.List;

public interface UserService {

    List<UserResponseDTO> getAll();

    UserResponseDTO getById(Long userId);

    UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long userId);

    UserResponseDTO create(UserRequestDTO userRequestDTO);

    void deleteUser(Long userId);

    void gdprDelete(Long userId);

    void resetDayOffLimit();
}
