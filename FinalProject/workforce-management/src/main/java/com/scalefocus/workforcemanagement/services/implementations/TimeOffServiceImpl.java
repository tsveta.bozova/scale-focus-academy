package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffResponseDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.*;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import com.scalefocus.workforcemanagement.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TimeOffServiceImpl implements TimeOffService {
    private final UserRepository userRepository;
    private final TimeOffRepository timeOffRepository;
    private final VacationService vacationService;
    private final SickLeaveService sickLeaveService;
    private final NotificationService notificationService;
    private final AuthenticationService authenticationService;
    private final OfficialHolidayClient officialHolidayClient;

    @Autowired
    public TimeOffServiceImpl(UserRepository userRepository,
                              TimeOffRepository timeOffRepository,
                              VacationService vacationService,
                              SickLeaveService sickLeaveService,
                              NotificationService notificationService,
                              AuthenticationService authenticationService,
                              OfficialHolidayClient officialHolidayClient) {
        this.userRepository = userRepository;
        this.timeOffRepository = timeOffRepository;
        this.vacationService = vacationService;
        this.sickLeaveService = sickLeaveService;
        this.notificationService = notificationService;
        this.authenticationService = authenticationService;
        this.officialHolidayClient = officialHolidayClient;
    }

    @Override
    public List<TimeOffResponseDTO> getAll() {
        User loggedInUser = authenticationService.getLoggedInUser();
        List<TimeOff> allTimeOffRequests = timeOffRepository.findAll();

        if (loggedInUser.getAdmin()) {
            return allTimeOffRequests.stream()
                    .map(TimeOffResponseDTO::responseFromTimeOff)
                    .collect(Collectors.toList());
        }
        return allTimeOffRequests.stream()
                .filter(timeOff -> isAccessibleByUser(timeOff, loggedInUser))
                .map(TimeOffResponseDTO::responseFromTimeOff)
                .collect(Collectors.toList());
    }

    @Override
    public TimeOffResponseDTO getById(Long timeOffId) {
        User loggedInUser = authenticationService.getLoggedInUser();
        TimeOff timeOffById = timeOffRepository.findById(timeOffId)
                .orElseThrow(() -> new TimeOffNotFoundException("TimeOff request not found"));

        if (!loggedInUser.getAdmin() && !isAccessibleByUser(timeOffById, loggedInUser)) {
            throw new MissingPermissionsException("No permission to view this request");
        } else {
            return TimeOffResponseDTO.responseFromTimeOff(timeOffById);
        }
    }

    private boolean isAccessibleByUser(TimeOff timeOff, User loggedInUser) {
        List<Long> userIdsLedByUser = getUserIdsLedByUser(loggedInUser);
        return timeOff.getCreator().getId().equals(loggedInUser.getId())
                || userIdsLedByUser.contains(timeOff.getCreator().getId());
    }

    private List<Long> getUserIdsLedByUser(User teamLeader) {
        return getTeamsLedByUser(teamLeader)
                .stream()
                .flatMap(team -> getTeamMembersExceptUser(team, teamLeader))
                .distinct()
                .collect(Collectors.toList());
    }

    private List<Team> getTeamsLedByUser(User teamLeader) {
        return teamLeader.getTeamList()
                .stream()
                .filter(team -> team.getTeamLeader().getId().equals(teamLeader.getId()))
                .collect(Collectors.toList());
    }

    private Stream<Long> getTeamMembersExceptUser(Team team, User user) {
        return team.getTeamMembers()
                .stream()
                .filter(teamMember -> !teamMember.getId().equals(user.getId()))
                .map(User::getId);
    }

    @Override
    @Transactional
    public TimeOffResponseDTO create(TimeOffRequestDTO timeOffRequestDTO) {
        if (authenticationService.getLoggedInUser().getId().equals(1L)) {
            throw new DefaultAdminTimeOffException("Default admin user cannot create time off requests");
        }
        TimeOff newTimeOff;
        String timeOffRequestType = timeOffRequestDTO.getType();

        if (timeOffRequestType.equalsIgnoreCase("PAID")
                || timeOffRequestType.equalsIgnoreCase("UNPAID")) {
            newTimeOff = vacationService.create(timeOffRequestDTO);
            newTimeOff = timeOffRepository.save(newTimeOff);
            sendVacationNotification(newTimeOff);
        } else if (timeOffRequestType.equalsIgnoreCase("SICK_LEAVE")) {
            newTimeOff = sickLeaveService.create(timeOffRequestDTO);
            newTimeOff = timeOffRepository.save(newTimeOff);
            sendSickLeaveNotification(newTimeOff);
        } else {
            throw new InvalidTimeOffTypeException("Invalid time off type");
        }

        return TimeOffResponseDTO.responseFromTimeOff(newTimeOff);
    }

    private void sendSickLeaveNotification(TimeOff newSickLeave) {
        User creator = newSickLeave.getCreator();
        List<User> teamMembers = new ArrayList<>(getTeamMembers(creator));
        teamMembers.add(creator);
        teamMembers
                .stream()
                .filter(teamMember -> !teamMember.getEmail().equals("admin"))
                .forEach(teamMember -> {
                    try {
                        notificationService.sendEmail(teamMember, newSickLeave);
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                });
    }

    private List<User> getTeamMembers(User creator) {
        List<Long> membersIds = creator.getTeamList().stream()
                .flatMap(this::getTeamMembersIds)
                .distinct()
                .filter(id -> !id.equals(creator.getId()))
                .collect(Collectors.toList());
        return userRepository.findAllById(membersIds);
    }

    private Stream<Long> getTeamMembersIds(Team team) {
        return team.getTeamMembers()
                .stream()
                .filter(user -> !user.isUserGdprDeleted())
                .map(User::getId);
    }

    @Override
    @Transactional
    public TimeOffResponseDTO cancel(Long timeOffId) {
        TimeOff timeOffToCancel = timeOffRepository.findById(timeOffId)
                .orElseThrow(() -> new TimeOffNotFoundException("TimeOff request not found"));
        User loggedInUser = authenticationService.getLoggedInUser();

        checkHasPermissionToCancelRequest(loggedInUser, timeOffToCancel);
        checkHasRequestBegun(timeOffToCancel);

        restoreRequestDaysToUser(timeOffToCancel);

        timeOffToCancel.setStatus(TimeOffStatus.CANCELLED);
        timeOffToCancel.setEditor(loggedInUser);
        timeOffToCancel.setDateUpdated(Instant.now());

        return TimeOffResponseDTO.responseFromTimeOff(timeOffRepository.save(timeOffToCancel));
    }

    private void checkHasPermissionToCancelRequest(User loggedInUser, TimeOff timeOffToCancel) {
        if (!loggedInUser.getAdmin() && !timeOffToCancel.getCreator().getId().equals(loggedInUser.getId())) {
            throw new MissingPermissionsException("No permission to cancel this request");
        }
    }

    private void checkHasRequestBegun(TimeOff timeOff) {
        if (timeOff.getTimeOffStart().isBefore(LocalDate.now())) {
            throw new LockedTimeOffRequestException("Cannot cancel ongoing or past requests");
        }
    }

    private void restoreRequestDaysToUser(TimeOff timeOffToCancel) {
        User timeOffCreator = timeOffToCancel.getCreator();
        int daysToReturn = getWorkingDaysInRequest(timeOffToCancel);

        if (timeOffToCancel.getType().equals(TimeOffType.PAID) && isTimeOffApproved(timeOffToCancel)) {
            timeOffCreator.setPaidDays(timeOffCreator.getPaidDays() + daysToReturn);
        } else if (timeOffToCancel.getType().equals(TimeOffType.UNPAID) && isTimeOffApproved(timeOffToCancel)) {
            timeOffCreator.setUnpaidDays(timeOffCreator.getUnpaidDays() + daysToReturn);
        } else if (timeOffToCancel.getType().equals(TimeOffType.SICK_LEAVE)) {
            timeOffCreator.setSickLeaveDays(timeOffCreator.getSickLeaveDays() + daysToReturn);
        }
    }

    private int getWorkingDaysInRequest(TimeOff newSickLeave) {
        return getNumberOfDaysInRequest(newSickLeave)
                - getNumberOfWeekendDaysInRequest(newSickLeave)
                - getNumberOfHolidaysInRequest(newSickLeave);
    }

    private int getNumberOfDaysInRequest(TimeOff timeOff) {
        return (int) timeOff.getTimeOffStart().until(timeOff.getTimeOffEnd().plusDays(1), ChronoUnit.DAYS);
    }

    private int getNumberOfWeekendDaysInRequest(TimeOff newSickLeave) {
        return (int) newSickLeave.getTimeOffStart()
                .datesUntil(newSickLeave.getTimeOffEnd().plusDays(1))
                .filter(this::isWeekend)
                .count();
    }

    private boolean isWeekend(LocalDate date) {
        return date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    private int getNumberOfHolidaysInRequest(TimeOff newSickLeave) {
        List<LocalDate> allHolidays = officialHolidayClient.getOfficialHolidays();
        return (int) allHolidays.stream()
                .filter(holiday -> isDayInRequest(holiday, newSickLeave))
                .count();
    }

    private boolean isDayInRequest(LocalDate dateToCheck, TimeOff newSickLeave) {
        return !(dateToCheck.isBefore(newSickLeave.getTimeOffStart()) ||
                dateToCheck.isAfter(newSickLeave.getTimeOffEnd()));
    }

    private boolean isTimeOffApproved(TimeOff timeOff) {
        return timeOff.getStatus().equals(TimeOffStatus.APPROVED);
    }

    @Override
    @Transactional
    public TimeOffResponseDTO update(TimeOffRequestDTO timeOffRequestDTO, Long timeOffId) {
        TimeOff timeOffToUpdate = vacationService.update(timeOffRequestDTO, timeOffId);
        timeOffToUpdate = timeOffRepository.save(timeOffToUpdate);
        sendVacationNotification(timeOffToUpdate);

        return TimeOffResponseDTO.responseFromTimeOff(timeOffToUpdate);
    }

    private void sendVacationNotification(TimeOff newVacation) {
        List<User> allUsersTeamLeadersInOffice = getUsersTeamLeadersInOffice(newVacation.getCreator());
        if (allUsersTeamLeadersInOffice.isEmpty()) {
            notificationService.sendApprovedNotification(newVacation);
        } else {
            allUsersTeamLeadersInOffice
                    .forEach(teamLeader -> notificationService.sendApproverNotification(teamLeader, newVacation));
        }
    }

    private List<User> getUsersTeamLeadersInOffice(User user) {
        List<Long> teamLeaders = getIdsOfUserTeamLeaders(user).stream()
                .filter(this::isTeamLeaderInOffice)
                .collect(Collectors.toList());
        return userRepository.findAllById(teamLeaders);
    }

    private List<Long> getIdsOfUserTeamLeaders(User user) {
        return user.getTeamList().stream()
                .filter(team -> team.getTeamLeader() != null &&
                        !team.getTeamLeader().getId().equals(user.getId()))
                .map(team -> team.getTeamLeader().getId())
                .distinct()
                .collect(Collectors.toList());
    }

    private boolean isTeamLeaderInOffice(Long teamLeaderId) {
        return timeOffRepository.findAll()
                .stream()
                .filter(timeOff -> timeOff.getCreator().getId().equals(teamLeaderId))
                .filter(timeOff -> timeOff.getStatus().equals(TimeOffStatus.APPROVED))
                .noneMatch(timeOff -> isDayInRequest(LocalDate.now(), timeOff));
    }

    @Override
    @Transactional
    public void delete(Long timeOffId) {
        vacationService.checkCanBeDeleted(timeOffId);
        timeOffRepository.deleteById(timeOffId);
    }

    @Override
    @Transactional
    public TimeOffResponseDTO approve(Long timeOffId) {
        TimeOff timeOffToApprove = vacationService.approve(timeOffId);
        timeOffToApprove = timeOffRepository.save(timeOffToApprove);
        if (isLastApprover(timeOffToApprove)) {
            notificationService.sendApprovedNotification(timeOffToApprove);
        }
        return TimeOffResponseDTO.responseFromTimeOff(timeOffToApprove);
    }

    private boolean isLastApprover(TimeOff timeOffToApprove) {
        List<User> leadersToApprove = getUsersTeamLeadersInOffice(timeOffToApprove.getCreator());
        return leadersToApprove.size() <= timeOffToApprove.getApproversList().size();
    }

    @Override
    @Transactional
    public TimeOffResponseDTO reject(Long timeOffId) {
        TimeOff timeOffToReject = vacationService.reject(timeOffId);
        timeOffToReject = timeOffRepository.save(timeOffToReject);

        sendRejectNotification(timeOffToReject);

        return TimeOffResponseDTO.responseFromTimeOff(timeOffToReject);
    }

    private void sendRejectNotification(TimeOff timeOffToReject) {
        List<User> allIdsOfUserTeamLeadersInOffice =
                new ArrayList<>(getUsersTeamLeadersInOffice(timeOffToReject.getCreator()));
        allIdsOfUserTeamLeadersInOffice.add(timeOffToReject.getCreator());
        allIdsOfUserTeamLeadersInOffice
                .forEach(teamLeader ->
                        notificationService.sendRejectedNotification(timeOffToReject));
    }

    @Override
    @Transactional
    public void autoApproveTimeOffRequest(List<TimeOff> timeOffToApproveList) {
        for (TimeOff timeOffToApprove : timeOffToApproveList) {
            User timeOffCreator = timeOffToApprove.getCreator();
            removeApprovedRequestDaysFromUser(timeOffToApprove, timeOffCreator);
            timeOffToApprove.setStatus(TimeOffStatus.APPROVED);
            timeOffToApprove = timeOffRepository.save(timeOffToApprove);

            notificationService.sendApprovedNotification(timeOffToApprove);
        }
    }

    private void removeApprovedRequestDaysFromUser(TimeOff newTimeOff, User loggedInUser) {
        if (newTimeOff.getType() == TimeOffType.PAID) {
            loggedInUser.setPaidDays(loggedInUser.getPaidDays() - getWorkingDaysInRequest(newTimeOff));
        } else if (newTimeOff.getType() == TimeOffType.UNPAID) {
            loggedInUser.setUnpaidDays(loggedInUser.getUnpaidDays() - getWorkingDaysInRequest(newTimeOff));
        }
    }
}
