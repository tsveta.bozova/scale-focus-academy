package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.user.UserRequestDTO;
import com.scalefocus.workforcemanagement.dtos.user.UserResponseDTO;
import com.scalefocus.workforcemanagement.services.UserService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@PreAuthorize("hasRole('ADMIN')")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public List<UserResponseDTO> getAll() {
        List<UserResponseDTO> userList = userService.getAll();
        return userList;
    }

    @GetMapping("/users/{userId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public UserResponseDTO getById(
            @PathVariable("userId") @NotNull @Min(value = 1, message = "Invalid user id") Long userId) {
        return userService.getById(userId);
    }

    @PutMapping("/users/{userId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public UserResponseDTO updateUser(
            @PathVariable("userId") @NotNull @Min(value = 1, message = "Invalid user id") Long userId,
            @Valid @RequestBody UserRequestDTO userRequestDTO) {
        return userService.updateUser(userRequestDTO, userId);
    }

    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public UserResponseDTO createUser(@Valid @RequestBody UserRequestDTO userRequestDTO) {
        UserResponseDTO user = userService.create(userRequestDTO);
        return user;
    }

    @DeleteMapping("/users/{userId}")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header",
            dataTypeClass = String.class, example = "Bearer access_token")
    public void deleteUser(@PathVariable("userId") @NotNull @Min(value = 1, message = "Invalid user id") Long userId) {
        userService.gdprDelete(userId);
    }
}
