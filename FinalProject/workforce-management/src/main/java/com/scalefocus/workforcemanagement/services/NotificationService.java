package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;

import javax.mail.MessagingException;

public interface NotificationService {

    void sendApproverNotification(User approver, TimeOff timeOff);

    void sendSickLeaveNotifications(User member, TimeOff timeOff);

    void sendApprovedNotification(TimeOff timeOff);

    void sendRejectedNotification(TimeOff timeOff);

    void sendEmail(User user, TimeOff timeOff) throws MessagingException;
}
