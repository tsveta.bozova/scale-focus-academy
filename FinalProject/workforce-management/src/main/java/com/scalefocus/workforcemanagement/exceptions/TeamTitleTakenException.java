package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class TeamTitleTakenException extends RuntimeException {
    public TeamTitleTakenException(String message) {
        super(message);
    }
}
