package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidTimeOffTypeException extends RuntimeException {
    public InvalidTimeOffTypeException(String message) {
        super(message);
    }
}