package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.team.TeamRequestDTO;
import com.scalefocus.workforcemanagement.dtos.team.TeamResponseDTO;

import java.util.List;

public interface TeamService {
    List<TeamResponseDTO> getAll();

    TeamResponseDTO create(TeamRequestDTO teamRequestDTO);

    TeamResponseDTO getById(Long teamId);

    TeamResponseDTO setTeamLeader(Long teamId, Long userId);

    TeamResponseDTO removeUserFromTeam(Long teamId, Long userId);

    TeamResponseDTO updateTeam(Long teamId, TeamRequestDTO teamRequestDTO);

    void deleteTeam(Long teamId);

    TeamResponseDTO assignUserToTeam(Long teamId, Long userId);
}
