package com.scalefocus.workforcemanagement.services.implementations;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.calendar.RestDayResponseDTO;
import com.scalefocus.workforcemanagement.dtos.calendar.enums.RestDayType;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.services.AuthenticationService;
import com.scalefocus.workforcemanagement.services.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CalendarServiceImpl implements CalendarService {

    private final TimeOffRepository timeOffRepository;
    private final OfficialHolidayClient officialHolidayClient;
    private final AuthenticationService authenticationService;

    @Autowired
    public CalendarServiceImpl(TimeOffRepository timeOffRepository,
                               OfficialHolidayClient officialHolidayClient,
                               AuthenticationService authenticationService) {
        this.timeOffRepository = timeOffRepository;
        this.officialHolidayClient = officialHolidayClient;
        this.authenticationService = authenticationService;
    }

    @Override
    public List<RestDayResponseDTO> getAllNonWorkingDays(int month) {
        User loggedUser = authenticationService.getLoggedInUser();
        List<RestDayResponseDTO> monthlyRestDaysResponseDTO = new ArrayList<>();
        List<LocalDate> officialHolidaysForMonth = getAllOfficialHolidaysForMonth(month);
        List<TimeOff> timeOffsInMonth = timeOffRepository.findAll().stream()
                .filter(timeOff -> timeOff.getStatus() == TimeOffStatus.APPROVED)
                .filter(timeOff -> getTimeOffToDurationInDates(timeOff, officialHolidaysForMonth)
                        .stream().anyMatch(date -> date.getMonthValue() == month))
                .filter(timeOff -> timeOff.getCreator().getId().equals(loggedUser.getId()))
                .collect(Collectors.toList());
        List<LocalDate> weekEnds = getWeekEndsForMonth(month);

        officialHolidaysForMonth.forEach(date -> monthlyRestDaysResponseDTO.add(new RestDayResponseDTO(date, RestDayType.OFFICIAL_HOLIDAY)));

        timeOffsInMonth.forEach(timeOff -> getTimeOffToDurationInDates(timeOff, officialHolidaysForMonth)
                .stream()
                .filter(date -> date.getMonthValue() == month)
                .forEach(date -> monthlyRestDaysResponseDTO.add(new RestDayResponseDTO(date,
                        timeOffTypeToRestDayType(timeOff.getType())
                ))));

        weekEnds.forEach(date -> monthlyRestDaysResponseDTO.add(new RestDayResponseDTO(date, RestDayType.WEEKEND)));
        return monthlyRestDaysResponseDTO;
    }

    private List<LocalDate> getAllOfficialHolidaysForMonth(int month) {
        return officialHolidayClient.getOfficialHolidays().stream()
                .filter(date -> date.getMonthValue() == month)
                .collect(Collectors.toList());
    }

    private List<LocalDate> getTimeOffToDurationInDates(TimeOff timeOff, List<LocalDate> officialHolidays) {
        List<LocalDate> timeOfFDates = new ArrayList<>();
        if (timeOff.getTimeOffStart().equals(timeOff.getTimeOffEnd())) {
            timeOfFDates.add(timeOff.getTimeOffStart());
            return timeOfFDates;
        }

        LocalDate dateToCheck = timeOff.getTimeOffStart();
        LocalDate endDate = timeOff.getTimeOffEnd().plusDays(1);
        int stopCount = 1;
        int yearLength = LocalDate.now().lengthOfYear();
        while (!dateToCheck.equals(endDate) && stopCount <= yearLength) {
            if (dateToCheck.getDayOfWeek() != DayOfWeek.SATURDAY &&
                    dateToCheck.getDayOfWeek() != DayOfWeek.SUNDAY &&
                    !officialHolidays.contains(dateToCheck)) {
                timeOfFDates.add(dateToCheck);
            }
            dateToCheck = dateToCheck.plusDays(1);
            stopCount++;
        }

        return timeOfFDates;
    }

    private RestDayType timeOffTypeToRestDayType(TimeOffType timeOffType) {
        return RestDayType.valueOf(timeOffType.toString());
    }

    public List<LocalDate> getWeekEndsForMonth(int month) {
        int year = LocalDate.now().getYear();
        LocalDate currentDate = LocalDate.of(year, month, 1);
        List<LocalDate> weekEndDays = new ArrayList<>();
        while (currentDate.getMonthValue() == month) {
            if (currentDate.getDayOfWeek() == DayOfWeek.SATURDAY ||
                    currentDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
                weekEndDays.add(currentDate);
            }
            currentDate = currentDate.plusDays(1);
        }
        return weekEndDays;
    }
}
