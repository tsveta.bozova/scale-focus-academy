package com.scalefocus.workforcemanagement.validators;

import com.scalefocus.workforcemanagement.validators.annotations.CustomEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomEmailValidator implements ConstraintValidator<CustomEmail, String> {

    static final Pattern emailPattern =
            Pattern.compile("^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        try {
            if (value.isEmpty() || value.trim().isBlank()) {
                return false;
            } else {
                Matcher matcher = emailPattern.matcher(value);

                return value.equals("admin") || matcher.matches();
            }
        } catch (Exception ex) {
            return false;
        }
    }
}
