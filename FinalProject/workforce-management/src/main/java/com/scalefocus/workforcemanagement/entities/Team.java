package com.scalefocus.workforcemanagement.entities;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TEAMS", schema = "WMAPI")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEAM_ID")
    private Long id;

    @Column(name = "TEAM_TITLE", length = 120)
    private String title;

    @Column(name = "TEAM_DESCRIPTION", length = 300)
    private String description;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "TEAM_LEADER")
    private User teamLeader;

    @Column(name = "TEAM_DATE_CREATED")
    private Instant dateCreated;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "TEAM_CREATOR_ID")
    private User creator;

    @Column(name = "TEAM_DATE_LAST_CHANGED")
    private Instant dateUpdated;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "TEAM_LAST_CHANGE_USER_ID")
    private User editor;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE})
    @JoinTable(
            name = "MEMBERS",
            schema = "WMAPI",
            joinColumns = {@JoinColumn(name = "MEMBER_TEAM_ID")},
            inverseJoinColumns = {@JoinColumn(name = "MEMBER_USER_ID")}
    )
    private List<User> teamMembers = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(User teamLeader) {
        this.teamLeader = teamLeader;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @PrePersist
    public void setDateCreated() {
        this.dateCreated = Instant.now();
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<User> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(List<User> teamMembers) {
        this.teamMembers = teamMembers;
    }
}
