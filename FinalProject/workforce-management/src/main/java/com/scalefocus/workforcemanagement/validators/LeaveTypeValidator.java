package com.scalefocus.workforcemanagement.validators;

import com.scalefocus.workforcemanagement.validators.annotations.LeaveType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LeaveTypeValidator implements ConstraintValidator<LeaveType, CharSequence> {
    private List<String> acceptedValues;

    @Override
    public void initialize(LeaveType annotation) {
        acceptedValues = Stream.of(annotation.enumClass().getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return acceptedValues.contains(value.toString());
    }
}
