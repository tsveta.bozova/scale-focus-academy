package com.scalefocus.workforcemanagement.exceptions.handlers;

import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.List;

public class ApiError {
    private Instant timestamp;
    private HttpStatus httpStatus;
    private int status;
    private List<String> message;

    public ApiError(HttpStatus httpStatus, List<String> message, Instant timestamp, int status) {
        super();
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> errors) {
        this.message = errors;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
}
