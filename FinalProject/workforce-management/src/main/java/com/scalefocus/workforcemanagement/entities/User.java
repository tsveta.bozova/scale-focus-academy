package com.scalefocus.workforcemanagement.entities;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "USERS", schema = "WMAPI")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "USERNAME", length = 254)
    private String email;

    @Column(name = "PASSWORD", length = 120)
    private String password;

    @Column(name = "F_NAME", length = 35)
    private String firstName;

    @Column(name = "L_NAME", length = 35)
    private String lastName;

    @Column(name = "IS_ADMIN")
    private boolean admin;

    @Column(name = "PAID_DAYS", columnDefinition = "NUMBER DEFAULT 20")
    private int paidDays = 20;

    @Column(name = "UNPAID_DAYS", columnDefinition = "NUMBER DEFAULT 90")
    private int unpaidDays = 90;

    @Column(name = "SICK_LEAVE", columnDefinition = "NUMBER DEFAULT 40")
    private int sickLeaveDays = 40;

    @Column(name = "USER_DATE_CREATED")
    private Instant dateCreated;

    @ManyToOne
    @JoinColumn(name = "USER_CREATOR_ID")
    private User creator;

    @Column(name = "USER_DATE_LAST_CHANGED")
    private Instant dateUpdated;

    @ManyToOne
    @JoinColumn(name = "USER_LAST_CHANGE_USER_ID")
    private User editor;

    @ManyToMany(mappedBy = "teamMembers")
    private List<Team> teamList = new ArrayList<>();

    @ManyToMany(mappedBy = "approversList")
    private List<TimeOff> approvedTimeOffs = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @PrePersist
    public void setDateCreated() {
        this.dateCreated = Instant.now();
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
    }

    public List<TimeOff> getApprovedTimeOffs() {
        return approvedTimeOffs;
    }

    public void setApprovedTimeOffs(List<TimeOff> approvedTimeOffs) {
        this.approvedTimeOffs = approvedTimeOffs;
    }

    public int getPaidDays() {
        return paidDays;
    }

    public void setPaidDays(int paidDays) {
        this.paidDays = paidDays;
    }

    public int getUnpaidDays() {
        return unpaidDays;
    }

    public void setUnpaidDays(int unpaidDays) {
        this.unpaidDays = unpaidDays;
    }

    public int getSickLeaveDays() {
        return sickLeaveDays;
    }

    public void setSickLeaveDays(int sickLeaveDays) {
        this.sickLeaveDays = sickLeaveDays;
    }

    public boolean isUserGdprDeleted() {
        return this.email == null && this.password == null
                && this.firstName == null && this.lastName == null;
    }
}
