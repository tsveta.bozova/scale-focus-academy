package com.scalefocus.workforcemanagement.dtos.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.scalefocus.workforcemanagement.entities.User;

import java.time.Instant;
import java.util.Objects;

public class UserResponseDTO {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private boolean admin;
    private int paidDays;
    private int unpaidDays;
    private int sickLeaveDays;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private Instant dateCreated;
    private Long creatorId;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Europe/Sofia")
    private Instant dateUpdated;
    private Long editorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Long getEditorId() {
        return editorId;
    }

    public void setEditorId(Long editorId) {
        this.editorId = editorId;
    }

    public int getPaidDays() {
        return paidDays;
    }

    public void setPaidDays(int paidDays) {
        this.paidDays = paidDays;
    }

    public int getUnpaidDays() {
        return unpaidDays;
    }

    public void setUnpaidDays(int unpaidDays) {
        this.unpaidDays = unpaidDays;
    }

    public int getSickLeaveDays() {
        return sickLeaveDays;
    }

    public void setSickLeaveDays(int sickLeaveDays) {
        this.sickLeaveDays = sickLeaveDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponseDTO that = (UserResponseDTO) o;
        return admin == that.admin &&
                Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(dateUpdated, that.dateUpdated) &&
                Objects.equals(editorId, that.editorId) &&
                Objects.equals(paidDays, that.paidDays) &&
                Objects.equals(unpaidDays, that.unpaidDays) &&
                Objects.equals(sickLeaveDays, that.sickLeaveDays);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, firstName, lastName, admin, paidDays, unpaidDays, sickLeaveDays, dateCreated,
                creatorId, dateUpdated, editorId);
    }

    public static UserResponseDTO responseFromUser(User user) {
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(user.getId());
        userResponseDTO.setEmail(user.getEmail());
        userResponseDTO.setFirstName(user.getFirstName());
        userResponseDTO.setLastName(user.getLastName());
        userResponseDTO.setAdmin(user.getAdmin());
        userResponseDTO.setPaidDays(user.getPaidDays());
        userResponseDTO.setUnpaidDays(user.getUnpaidDays());
        userResponseDTO.setSickLeaveDays(user.getSickLeaveDays());
        userResponseDTO.setDateCreated(user.getDateCreated());
        userResponseDTO.setCreatorId(user.getCreator().getId());
        userResponseDTO.setDateUpdated(user.getDateUpdated());
        userResponseDTO.setEditorId(user.getEditor().getId());

        return userResponseDTO;
    }
}
