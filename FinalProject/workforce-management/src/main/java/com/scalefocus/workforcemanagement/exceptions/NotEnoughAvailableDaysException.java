package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NotEnoughAvailableDaysException extends RuntimeException {
    public NotEnoughAvailableDaysException(String message) {
        super(message);
    }
}
