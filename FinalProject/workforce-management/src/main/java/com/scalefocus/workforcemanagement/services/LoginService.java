package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.login.LoginRequestDTO;
import com.scalefocus.workforcemanagement.dtos.login.LoginResponseDTO;

public interface LoginService {
    LoginResponseDTO login(LoginRequestDTO loginRequestDTO);
}
