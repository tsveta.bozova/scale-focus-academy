package com.scalefocus.workforcemanagement.dtos.calendar.enums;

public enum RestDayType {
    PAID,
    UNPAID,
    SICK_LEAVE,
    OFFICIAL_HOLIDAY,
    WEEKEND
}
