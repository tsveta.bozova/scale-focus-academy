package com.scalefocus.workforcemanagement.dtos.user;

import com.scalefocus.workforcemanagement.validators.annotations.CustomEmail;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

public class UserRequestDTO {

    @Size(min = 5, max = 254, message = "Email must be between 5 and 254 characters")
    @CustomEmail(message = "Please enter a valid email. Email must be between 5 and 254 characters")
    private String email;

    @Size(min = 4, max = 120, message = "Password must be between 4 and 120 characters")
    @NotBlank(message = "Please enter a valid password. Password must be between 4 and 120 characters")
    private String password;

    @Size(min = 3, max = 35, message = "First name must be between 3 and 35 characters")
    @NotBlank(message = "Please enter a valid first name. First name must be between 3 and 35 characters")
    private String firstName;

    @Size(min = 3, max = 35, message = "Last name must be between 3 and 35 characters")
    @NotBlank(message = "Please enter a valid last name. Last name must be between 3 and 35 characters")
    private String lastName;

    private boolean admin;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRequestDTO that = (UserRequestDTO) o;
        return admin == that.admin &&
                Objects.equals(email, that.email) &&
                Objects.equals(password, that.password) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password, firstName, lastName, admin);
    }
}
