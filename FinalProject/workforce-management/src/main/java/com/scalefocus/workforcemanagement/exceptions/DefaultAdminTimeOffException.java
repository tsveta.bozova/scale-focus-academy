package com.scalefocus.workforcemanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Default admin user cannot create time off requests")
public class DefaultAdminTimeOffException extends RuntimeException {
    public DefaultAdminTimeOffException(String message) {
        super(message);
    }
}