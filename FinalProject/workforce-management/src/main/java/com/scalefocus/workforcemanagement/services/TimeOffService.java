package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffResponseDTO;
import com.scalefocus.workforcemanagement.entities.TimeOff;

import java.util.List;

public interface TimeOffService {
    List<TimeOffResponseDTO> getAll();

    TimeOffResponseDTO getById(Long timeOffId);

    TimeOffResponseDTO create(TimeOffRequestDTO timeOffRequestDTO);

    TimeOffResponseDTO cancel(Long timeOffId);

    TimeOffResponseDTO update(TimeOffRequestDTO timeOffRequestDTO, Long timeOffId);

    void delete(Long timeOffId);

    TimeOffResponseDTO approve(Long timeOffId);

    TimeOffResponseDTO reject(Long timeOffId);

    void autoApproveTimeOffRequest(List<TimeOff> timeOffToApproveList);
}
