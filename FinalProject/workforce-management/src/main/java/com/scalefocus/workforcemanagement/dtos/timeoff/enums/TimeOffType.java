package com.scalefocus.workforcemanagement.dtos.timeoff.enums;

public enum TimeOffType {
    PAID,
    UNPAID,
    SICK_LEAVE
}
