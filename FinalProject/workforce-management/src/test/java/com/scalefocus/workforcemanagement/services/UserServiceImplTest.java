package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.dtos.user.UserRequestDTO;
import com.scalefocus.workforcemanagement.dtos.user.UserResponseDTO;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.DefaultAdminEditException;
import com.scalefocus.workforcemanagement.repositories.TeamRepository;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class UserServiceImplTest {
    private static final String TIME_SUFFIX = "T14:38:40.108Z";
    private static final String DEFAULT_ADMIN_EDIT_EXCEPTION = "Cannot edit default admin";

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private TimeOffRepository timeOffRepository;

    @Autowired
    private UserService userService;

    @MockBean
    private TeamRepository teamRepository;
    @Test
    void whenValidInput_getAll_thenReturnsListOfUserResponseDTO() {
        List<User> expectedList = new ArrayList<>();
        expectedList.add(createTestUser());

        when(userRepository.findAll())
                .thenReturn(expectedList);

        List<UserResponseDTO> actualList = userService.getAll();

        assertNotNull(actualList, "No users found");
        assertEquals(expectedList.size(), actualList.size(), "Not all users are returned");

        verify(userRepository)
                .findAll();
    }

    @Test
    void whenValidInput_getById_thenReturnsUserResponseDTO() {
        User testUserById = createTestUser();

        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(testUserById));

        UserResponseDTO actualUser = userService.getById(1L);
        UserResponseDTO responseUser = createResponse(testUserById);

        assertAll(
                () -> assertNotNull(actualUser),
                () -> assertEquals(responseUser, actualUser)
        );

        verify(userRepository)
                .findById(1L);
    }

    @Test
    void whenValidInput_create_thenCreatesNewUser() {
        User loggedUser = createTestUser();
        User user = createTestUser();
        UserRequestDTO userRequestDTO = createUserUpdateRequestDTO();

        when(userRepository.save(any(User.class)))
                .thenReturn(user);
        when(authenticationService.getLoggedInUser())
                .thenReturn(loggedUser);

        UserResponseDTO responseUser = createResponse(user);
        UserResponseDTO actualUserResponse = userService.create(userRequestDTO);

        assertAll(
                () -> assertNotNull(actualUserResponse),
                () -> assertEquals(responseUser, actualUserResponse)
        );

        verify(userRepository)
                .save(any(User.class));
    }

    @Test
    void whenValidInput_updateUser_thenUpdatesUser() {
        UserRequestDTO userRequestDTO = createUserUpdateRequestDTO();
        User expectedUser = createTestUser();
        expectedUser.setFirstName("Administrator");
        expectedUser.setLastName("Administrator");

        User loggedUser = createTestUser();

        when(userRepository.save(any(User.class)))
                .thenReturn(expectedUser);
        when(userRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(expectedUser));
        when(authenticationService.getLoggedInUser())
                .thenReturn(loggedUser);

        UserResponseDTO actualUser = userService.updateUser(userRequestDTO, 2L);
        UserResponseDTO responseUser = createResponse(expectedUser);

        assertAll(
                () -> assertNotNull(actualUser),
                () -> assertEquals(responseUser, actualUser)
        );

        verify(userRepository)
                .save(any(User.class));
        verify(userRepository)
                .findById(any(Long.class));
    }

    @Test
    void whenValidInput_deleteUser_thenDeleteUser() {
        User testUserToDelete = createTestUser();

        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(testUserToDelete));

        userService.deleteUser(1L);

        verify(userRepository)
                .deleteById(1L);
    }

    @Test
    void whenDefaultAdmin_updateUser_thenDefaultAdminEditException() {
        String message = "";
        UserRequestDTO userRequestDTO = createUserUpdateRequestDTO();
        User expectedUser = createTestUser();
        expectedUser.setId(1L);
        expectedUser.setFirstName("Administrator");
        expectedUser.setLastName("Administrator");

        User loggedUser = createTestUser();

        when(userRepository.save(any(User.class)))
                .thenReturn(expectedUser);
        when(userRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(expectedUser));
        when(authenticationService.getLoggedInUser())
                .thenReturn(loggedUser);

        try {
            userService.updateUser(userRequestDTO, 1L);
        } catch (DefaultAdminEditException e) {
            message = e.getMessage();
        }

        assertEquals(DEFAULT_ADMIN_EDIT_EXCEPTION, message);

        verify(userRepository)
                .findById(any(Long.class));
    }


    @Test
    void whenValidInput_gdprDelete_thenUpdatePersonalData() {
        User userToDelete = createTestUser();
        List<TimeOff> userTimeOffs = new ArrayList<>();
        TimeOff timeOff = createTestTimeOff();

        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(userToDelete));
        when(timeOffRepository.findAll())
                .thenReturn(userTimeOffs);
        when(timeOffRepository.save(any(TimeOff.class)))
                .thenReturn(timeOff);
        when(userRepository.save(any(User.class)))
                .thenReturn(userToDelete);

        userService.gdprDelete(2L);

        assertNull(userToDelete.getEmail());

        verify(userRepository)
                .save(userToDelete);
    }

    @Test
    void whenValidInput_gdprDelete_thenTeamLeaderSetNull() {
        User userToDelete = createTestUser();
        List<TimeOff> userTimeOffs = new ArrayList<>();
        TimeOff timeOff = createTestTimeOff();
        Team testTeam = createTestTeam();
        testTeam.setTeamLeader(userToDelete);
        userToDelete.getTeamList().add(testTeam);

        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(userToDelete));
        when(timeOffRepository.findAll())
                .thenReturn(userTimeOffs);
        when(timeOffRepository.save(any(TimeOff.class)))
                .thenReturn(timeOff);
        when(userRepository.save(any(User.class)))
                .thenReturn(userToDelete);
        when(teamRepository.save(any()))
                .thenReturn(testTeam);

        userService.gdprDelete(2L);

        assertNull(testTeam.getTeamLeader());

        verify(userRepository)
                .save(userToDelete);
    }

    private TimeOff createTestTimeOff() {
        TimeOff timeOff = new TimeOff();
        timeOff.setId(1L);
        timeOff.setType(TimeOffType.PAID);
        timeOff.setTimeOffStart(LocalDate.now().plusDays(1));
        timeOff.setTimeOffEnd(LocalDate.now().plusDays(1));
        timeOff.setReason("Test Reason");
        timeOff.setStatus(TimeOffStatus.AWAITING);
        timeOff.setCreator(createTestUser());
        timeOff.setDateUpdated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        timeOff.setEditor(createTestUser());
        return timeOff;
    }


    private User createTestUser() {
        User testUser = new User();

        testUser.setId(2L);
        testUser.setEmail("admin");
        testUser.setPassword("adminpass");
        testUser.setFirstName("Admin");
        testUser.setLastName("Admin");
        testUser.setAdmin(true);
        testUser.setPaidDays(20);
        testUser.setUnpaidDays(90);
        testUser.setSickLeaveDays(40);
        testUser.setCreator(testUser);
        testUser.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        testUser.setEditor(testUser);

        return testUser;
    }

    private UserResponseDTO createResponse(User expectedUser) {
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(expectedUser.getId());
        userResponseDTO.setEmail(expectedUser.getEmail());
        userResponseDTO.setFirstName(expectedUser.getFirstName());
        userResponseDTO.setLastName(expectedUser.getLastName());
        userResponseDTO.setAdmin(expectedUser.getAdmin());
        userResponseDTO.setPaidDays(expectedUser.getPaidDays());
        userResponseDTO.setUnpaidDays(expectedUser.getUnpaidDays());
        userResponseDTO.setSickLeaveDays(expectedUser.getSickLeaveDays());
        userResponseDTO.setDateCreated(expectedUser.getDateCreated());
        userResponseDTO.setCreatorId(expectedUser.getCreator().getId());
        userResponseDTO.setDateUpdated(expectedUser.getDateUpdated());
        userResponseDTO.setEditorId(expectedUser.getEditor().getId());
        return userResponseDTO;
    }

    private UserRequestDTO createUserUpdateRequestDTO() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();

        userRequestDTO.setEmail("admin");
        userRequestDTO.setPassword("adminpass");
        userRequestDTO.setFirstName("Administrator");
        userRequestDTO.setLastName("Administrator");
        userRequestDTO.setAdmin(true);

        return userRequestDTO;
    }

    private Team createTestTeam() {
        Team testTeam = new Team();

        testTeam.setId((long) 1);
        testTeam.setTitle("team1");
        testTeam.setDescription("team1description");
        testTeam.setTeamLeader(createTestUser());
        testTeam.setCreator(createTestUser());
        testTeam.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        testTeam.setEditor(createTestUser());
        testTeam.setTeamMembers(new ArrayList<>());

        return testTeam;
    }
}