package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffResponseDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.DefaultAdminTimeOffException;
import com.scalefocus.workforcemanagement.exceptions.LockedTimeOffRequestException;
import com.scalefocus.workforcemanagement.exceptions.MissingPermissionsException;
import com.scalefocus.workforcemanagement.repositories.TeamRepository;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class TimeOffServiceImplTest {
    private static final String LOCKED_TIME_OFF_EXCEPTION_MESSAGE = "Cannot cancel ongoing or past requests";
    private static final String MISSING_PERMISSION_EXCEPTION_MESSAGE = "No permission to cancel this request";
    private static final String DEFAULT_ADMIN_TIME_OFF_EXCEPTION_MESSAGE = "Default admin user cannot create time off requests";

    private static final String TIME_SUFFIX = "T14:38:40.108Z";

    @MockBean
    private TimeOffRepository timeOffRepository;

    @MockBean
    private NotificationService notificationService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private TeamRepository teamRepository;

    @MockBean
    private VacationService vacationService;

    @Autowired
    private TimeOffService timeOffService;


    private User firstAdminUser;
    private User secondNonAdminUser;
    private User thirdNonAdminUser;
    private TimeOff expectedTimeOff;
    private Team testTeam;
    private String message;

    @BeforeEach
    void setUp() {
        firstAdminUser = createFirstAdminUser();
        secondNonAdminUser = createSecondNonAdminUser();
        thirdNonAdminUser = createThirdNonAdminUser();
        expectedTimeOff = createTestTimeOff();
        testTeam = createTeam();
    }

    @Test
    void whenDefaultAdmin_create_thenDefaultAdminTimeOffException() {
        String message = "";
        TimeOffRequestDTO timeOffRequestDTO = new TimeOffRequestDTO();
        timeOffRequestDTO.setType("PAID");
        timeOffRequestDTO.setStartDate(LocalDate.now().plusDays(1));
        timeOffRequestDTO.setEndDate(null);
        timeOffRequestDTO.setNumberOfDays(5);
        timeOffRequestDTO.setReason("Test Reason");

        when(authenticationService.getLoggedInUser())
                .thenReturn(firstAdminUser);

        try {
            timeOffService.create(timeOffRequestDTO);
        } catch (DefaultAdminTimeOffException e) {
            message = e.getMessage();
        }

        assertEquals(DEFAULT_ADMIN_TIME_OFF_EXCEPTION_MESSAGE, message);
    }

    @Test
    void whenValidInput_getAll_thenReturnsListOfTimeOffResponseDTO() {
        List<TimeOff> expectedList = new ArrayList<>();
        expectedList.add(expectedTimeOff);
        testTeam.getTeamMembers().add(firstAdminUser);
        testTeam.getTeamMembers().add(secondNonAdminUser);
        testTeam.getTeamMembers().add(secondNonAdminUser);
        testTeam.setTeamLeader(secondNonAdminUser);
        List<Team> teamList = new ArrayList<>();
        teamList.add(testTeam);

        when(timeOffRepository.findAll())
                .thenReturn(expectedList);
        when(authenticationService.getLoggedInUser())
                .thenReturn(firstAdminUser);
        when(teamRepository.findAll())
                .thenReturn(teamList);

        List<TimeOffResponseDTO> actualList = timeOffService.getAll();

        assertNotNull(actualList, "No time off requests found");
        assertEquals(expectedList.size(), actualList.size(), "Not all time off requests are returned");

        verify(timeOffRepository)
                .findAll();
    }

    @Test
    void whenValidInput_getById_thenReturnsTimeOffResponseDTO() {
        testTeam.getTeamMembers().add(firstAdminUser);
        testTeam.getTeamMembers().add(secondNonAdminUser);
        testTeam.getTeamMembers().add(thirdNonAdminUser);
        testTeam.setTeamLeader(secondNonAdminUser);
        List<Team> teamList = new ArrayList<>();
        teamList.add(testTeam);

        when(timeOffRepository.findById(1L))
                .thenReturn(java.util.Optional.of(expectedTimeOff));
        when(authenticationService.getLoggedInUser())
                .thenReturn(firstAdminUser);
        when(teamRepository.findAll())
                .thenReturn(teamList);

        TimeOffResponseDTO timeOffResponse = createResponse(expectedTimeOff);
        TimeOffResponseDTO actualTimeOff = timeOffService.getById(1L);

        assertNotNull(actualTimeOff, "The timeOff request was not found");
        assertEquals(timeOffResponse, actualTimeOff);

        verify(timeOffRepository)
                .findById(1L);
    }

    @Test
    void whenValidInput_withAdmin_cancel_thenReturnsTimeOffResponseDTO() {
        when(authenticationService.getLoggedInUser())
                .thenReturn(firstAdminUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(timeOffRepository.save(any()))
                .thenReturn(expectedTimeOff);

        TimeOffResponseDTO actualTimeOff = timeOffService.cancel(1L);
        TimeOffResponseDTO timeOffResponse = createResponse(expectedTimeOff);

        assertNotNull(actualTimeOff, "The timeOff request was not found");
        assertEquals(timeOffResponse, actualTimeOff);

        verify(timeOffRepository)
                .save(expectedTimeOff);
    }

    @Test
    void whenInvalidInput_cancel_thenThrowsLockedTimeOffRequestException() {
        expectedTimeOff.setTimeOffStart(LocalDate.parse("2020-06-08"));

        when(authenticationService.getLoggedInUser())
                .thenReturn(firstAdminUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));

        try {
            timeOffService.cancel(1L);
        } catch (LockedTimeOffRequestException e) {
            message = e.getMessage();
        }

        assertEquals(LOCKED_TIME_OFF_EXCEPTION_MESSAGE, message);
    }

    @Test
    void whenValidInput_withUserWithoutAccess_cancel_thenThrowsLockedTimeOffRequestException() {

        when(authenticationService.getLoggedInUser())
                .thenReturn(thirdNonAdminUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));

        try {
            timeOffService.cancel(1L);
        } catch (MissingPermissionsException e) {
            message = e.getMessage();
        }

        assertEquals(MISSING_PERMISSION_EXCEPTION_MESSAGE, message);
    }


    private TimeOff createTestTimeOff() {
        TimeOff timeOff = new TimeOff();

        timeOff.setId(1L);
        timeOff.setType(TimeOffType.PAID);
        timeOff.setTimeOffStart(LocalDate.now().plusDays(1));
        timeOff.setTimeOffEnd(LocalDate.now().plusDays(6));
        timeOff.setReason("Test Reason");
        timeOff.setStatus(TimeOffStatus.AWAITING);
        timeOff.setCreator(secondNonAdminUser);
        timeOff.setDateUpdated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        timeOff.setEditor(secondNonAdminUser);

        return timeOff;
    }

    private User createFirstAdminUser() {
        User user = new User();

        user.setId(1L);
        user.setEmail("admin");
        user.setFirstName("Admin");
        user.setLastName("Admin");
        user.setAdmin(true);
        user.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        user.setEditor(user);
        user.setCreator(user);

        return user;
    }

    private User createSecondNonAdminUser() {
        User user = new User();

        user.setId(2L);
        user.setEmail("cveta");
        user.setFirstName("Cveta");
        user.setLastName("Cveta");
        user.setAdmin(false);

        return user;
    }

    private User createThirdNonAdminUser() {
        User user = new User();

        user.setId(3L);
        user.setEmail("georgi");
        user.setFirstName("Georgi");
        user.setLastName("Georgi");
        user.setAdmin(false);

        return user;
    }

    private Team createTeam() {
        Team team = new Team();

        team.setTitle("Test team");
        team.setDescription("Team to test with");

        return team;
    }

    private TimeOffResponseDTO createResponse(TimeOff timeOff) {
        TimeOffResponseDTO dto = new TimeOffResponseDTO();
        dto.setId(timeOff.getId());
        dto.setType(timeOff.getType());
        dto.setStartDate(timeOff.getTimeOffStart());
        dto.setEndDate(timeOff.getTimeOffEnd());
        dto.setReason(timeOff.getReason());
        dto.setStatus(timeOff.getStatus());
        dto.setDateCreated(timeOff.getDateCreated());
        dto.setCreatorId(timeOff.getCreator().getId());
        dto.setDateUpdated(timeOff.getDateUpdated());
        dto.setEditorId(timeOff.getEditor().getId());
        return dto;
    }

    @Test
    void whenValidInput_withTeamLeader_updateTimeOff_thenReturnsTimeOffResponseDTO() {
        TimeOff timeOff = createTestTimeOff();
        User loggedUser = createFirstAdminUser();
        User teamLeader = createSecondNonAdminUser();
        loggedUser.setPaidDays(20);
        List<User> teamLeaders = new ArrayList<>();
        teamLeaders.add(teamLeader);

        TimeOffRequestDTO timeOffRequestDTO = new TimeOffRequestDTO();
        timeOffRequestDTO.setReason("Test Reason");
        timeOffRequestDTO.setType(TimeOffType.PAID.toString());
        timeOffRequestDTO.setStartDate(LocalDate.now().plusDays(1));
        timeOffRequestDTO.setEndDate(LocalDate.now().plusDays(10));


        when(vacationService.update(timeOffRequestDTO, 1L))
                .thenReturn(timeOff);
        doNothing().when(notificationService)
                .sendApproverNotification(loggedUser, timeOff);
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(timeOff))
                .thenReturn(timeOff);

        TimeOffResponseDTO actualTimeOff = timeOffService.update(timeOffRequestDTO, 1L);
        TimeOffResponseDTO responseTimeOff = createResponse(timeOff);

        assertEquals(responseTimeOff, actualTimeOff);

        verify(notificationService)
                .sendApproverNotification(teamLeader, timeOff);
    }

    @Test
    void whenValidInput_withoutTeamLeader_updateTimeOff_thenReturnsTimeOffResponseDTO() {
        TimeOff timeOff = createTestTimeOff();
        User loggedUser = createFirstAdminUser();
        loggedUser.setPaidDays(20);
        List<User> teamLeaders = new ArrayList<>();

        TimeOffRequestDTO timeOffRequestDTO = new TimeOffRequestDTO();
        timeOffRequestDTO.setReason("Test Reason");
        timeOffRequestDTO.setType(TimeOffType.PAID.toString());
        timeOffRequestDTO.setStartDate(LocalDate.now().plusDays(1));
        timeOffRequestDTO.setEndDate(LocalDate.now().plusDays(10));


        when(vacationService.update(timeOffRequestDTO, 1L))
                .thenReturn(timeOff);
        doNothing().when(notificationService)
                .sendApprovedNotification(timeOff);
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(timeOff))
                .thenReturn(timeOff);

        TimeOffResponseDTO actualTimeOff = timeOffService.update(timeOffRequestDTO, 1L);
        TimeOffResponseDTO responseTimeOff = createResponse(timeOff);

        assertEquals(responseTimeOff, actualTimeOff);

        verify(notificationService)
                .sendApprovedNotification(timeOff);
    }

    @Test
    void whenValidInput_approveTimeOff_thenApproveRequest() {
        TimeOff expectedTimeOff = createTestTimeOff();
        User timeOffCreator = createFirstAdminUser();
        User loggedUser = new User();
        expectedTimeOff.setCreator(timeOffCreator);
        expectedTimeOff.setEditor(timeOffCreator);
        loggedUser.setId(2L);
        loggedUser.setEmail("cvetabozova@gmail.com");
        Team testTeam = new Team();
        testTeam.getTeamMembers().add(timeOffCreator);
        testTeam.getTeamMembers().add(loggedUser);
        testTeam.setTeamLeader(loggedUser);
        List<Team> teamList = new ArrayList<>();
        testTeam.setTeamLeader(loggedUser);
        teamList.add(testTeam);
        loggedUser.setTeamList(teamList);
        timeOffCreator.setTeamList(teamList);

        when(vacationService.approve(anyLong()))
                .thenReturn(expectedTimeOff);
        when(timeOffRepository.save(any(TimeOff.class)))
                .thenReturn(expectedTimeOff);
        doNothing().when(notificationService)
                .sendApprovedNotification(expectedTimeOff);

        TimeOffResponseDTO actualTimeOff = timeOffService.approve(1L);
        TimeOffResponseDTO responseTimeOff = createResponse(expectedTimeOff);

        assertEquals(responseTimeOff, actualTimeOff);

        verify(notificationService)
                .sendApprovedNotification(expectedTimeOff);
    }

    @Test
    void whenValidInput_rejectTimeOff_thenReturnsCancelledTimeOffResponseDTO() {
        TimeOff expectedTimeOff = createTestTimeOff();
        User timeOffCreator = createFirstAdminUser();
        User loggedUser = new User();
        expectedTimeOff.setCreator(timeOffCreator);
        expectedTimeOff.setEditor(timeOffCreator);
        loggedUser.setId(2L);
        loggedUser.setEmail("cvetabozova@gmail.com");
        Team testTeam = new Team();
        testTeam.getTeamMembers().add(timeOffCreator);
        testTeam.getTeamMembers().add(loggedUser);
        List<Team> teamList = new ArrayList<>();
        teamList.add(testTeam);
        timeOffCreator.setTeamList(teamList);
        List<User> teamLeaders = new ArrayList<>();
        teamLeaders.add(loggedUser);

        when(vacationService.reject(anyLong()))
                .thenReturn(expectedTimeOff);
        when(timeOffRepository.save(any(TimeOff.class)))
                .thenReturn(expectedTimeOff);
        doNothing().when(notificationService)
                .sendRejectedNotification(expectedTimeOff);
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);

        TimeOffResponseDTO actualTimeOff = timeOffService.reject(1L);
        TimeOffResponseDTO responseTimeOff = createResponse(expectedTimeOff);

        assertEquals(responseTimeOff, actualTimeOff);

        verify(notificationService, times(2))
                .sendRejectedNotification(expectedTimeOff);
    }

    @Test
    void whenValidInput_delete_thenDeletesTimeOff() {
        doNothing().when(vacationService)
                .checkCanBeDeleted(1L);

        timeOffService.delete(1L);

        verify(timeOffRepository)
                .deleteById(1L);
    }
}