package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.team.TeamRequestDTO;
import com.scalefocus.workforcemanagement.dtos.team.TeamResponseDTO;
import com.scalefocus.workforcemanagement.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@WithMockUser(roles = "ADMIN")
class TeamControllerTest {

    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN = "token";

    private static final String GET_ALL_RESPONSE = "[{\"id\":1,\"title\":\"team1\",\"description\":\"team1description\",\n" +
            "           \"teamLeaderId\":1,\"dateCreated\":\"2003-05-03\",\n" +
            "          \"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"teamMemberIds\":[1,2]}]";

    private static final String CREATE_BODY = "{\n" +
            "   \"title\": \"team1\",\n" +
            "   \"description\": \"team1description\",\n" +
            "    \"teamLeaderId\": 1,\n" +
            "    \"teamMemberIds\": [1,2]\n" +
            "}";
    private static final String CREATE_BODY_RESPONSE = "{\n" +
            "    \"id\": 1,\n" +
            "    \"title\": \"team1\",\n" +
            "    \"description\": team1description,\n" +
            "    \"teamLeaderId\": 1,\n" +
            "    \"dateCreated\": \"2003-05-03\",\n" +
            "    \"creatorId\": 1,\n" +
            "    \"dateUpdated\": \"2003-05-03\",\n" +
            "    \"editorId\": 1,\n" +
            "    \"teamMemberIds\": [1,2]\n" +
            "}";

    private static final String SET_TEAM_LEADER_RESPONSE = "{\n" +
            "    \"id\": 1,\n" +
            "    \"title\": \"team1\",\n" +
            "    \"description\": team1description,\n" +
            "    \"teamLeaderId\": 2,\n" +
            "    \"dateCreated\": \"2003-05-03\",\n" +
            "    \"creatorId\": 1,\n" +
            "    \"dateUpdated\": \"2003-05-03\",\n" +
            "    \"editorId\": 1,\n" +
            "    \"teamMemberIds\": [1,2]\n" +
            "}";

    private static final String GET_BY_ID_RESPONSE = "{\"id\":1,\"title\":\"team1\",\"description\":\"team1description\",\n" +
            "           \"teamLeaderId\":1,\"dateCreated\":\"2003-05-03\",\n" +
            "          \"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"teamMemberIds\":[1,2]}";

    private static final String REMOVE_USER_FROM_TEAM_RESPONSE = "{\"id\":1,\"title\":\"team1\",\"description\":\"team1description\",\n" +
            "           \"teamLeaderId\":1,\"dateCreated\":\"2003-05-03\",\n" +
            "          \"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"teamMemberIds\":[2]}";


    private static final String UPDATED_REQUEST_BODY = "{\"title\":\"team1\",\"description\":\"team1description\",\"teamLeaderId\":1}";

    private static final String GET_UPDATED_RESPONSE = "{\"id\":1,\"title\":\"team1\",\"description\":\"team1description\"," +
            "\"teamLeaderId\":1,\"dateCreated\":\"2003-05-03\",\"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"teamMemberIds\":[1,2]}";

    private static final String ASSIGN_USER_TO_TEAM_RESPONSE = "{\"id\":1,\"title\":\"team1\",\"description\":\"team1description\",\n" +
            "           \"teamLeaderId\":1,\"dateCreated\":\"2003-05-03\",\n" +
            "          \"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"teamMemberIds\":[1,2,3]}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void whenValidInput_withAdminRole_getAll_thenReturnsListOfTeamResponseDTO() {

        List<TeamResponseDTO> teamList = new ArrayList<>();
        teamList.add((createTestResponseDto()));
        when(teamService.getAll()).thenReturn(teamList);

        try {
            mockMvc.perform(get("/teams")
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(GET_ALL_RESPONSE));
            verify(teamService, times(1)).getAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    void whenValidInput_withAdminRole_createTeam_thenReturns201() {
        TeamResponseDTO teamResponseDTO = createTestResponseDto();

        when(teamService.create(any(TeamRequestDTO.class)))
                .thenReturn(teamResponseDTO);

        try {
            mockMvc.perform(post("/teams")
                    .content(CREATE_BODY)
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(CREATE_BODY_RESPONSE));
            verify(teamService, times(1)).create(any(TeamRequestDTO.class));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    private TeamResponseDTO createTestResponseDto() {
        TeamResponseDTO teamResponseDTO = new TeamResponseDTO();

        List<Long> teamMembersIds = new ArrayList<>();
        teamMembersIds.add(1L);
        teamMembersIds.add((long) 2);

        teamResponseDTO.setId((long) 1);
        teamResponseDTO.setTitle("team1");
        teamResponseDTO.setDescription("team1description");
        teamResponseDTO.setTeamLeaderId((long) 1);
        teamResponseDTO.setDateCreated(Instant.parse("2003-05-03T00:00:00.00Z"));
        teamResponseDTO.setCreatorId((long) 1);
        teamResponseDTO.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        teamResponseDTO.setEditorId((long) 1);
        teamResponseDTO.setTeamMemberIds(teamMembersIds);

        return teamResponseDTO;
    }

    private TeamResponseDTO createAssignTestResponseDTO() {
        TeamResponseDTO testResponse = createTestResponseDto();
        testResponse.getTeamMemberIds().add((long) 3);
        return testResponse;
    }

    @Test
    void whenValidInput_withAdminRole_getById_thenReturnsTeamResponseDTO() {
        TeamResponseDTO responseDTO = createTestResponseDto();

        when(teamService.getById(1L)).thenReturn(responseDTO);

        try {
            mockMvc.perform(get("/teams/{teamId}", 1L)
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(GET_BY_ID_RESPONSE));
            verify(teamService, times(1)).getById(1L);
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Test
    void whenValidInput_withAdminRole_setTeamLeader_thenReturns200() throws Exception {

        TeamResponseDTO teamResponseDTO = createTestResponseDto();
        teamResponseDTO.setTeamLeaderId(2L);

        when(teamService.setTeamLeader(anyLong(), anyLong()))
                .thenReturn(teamResponseDTO);

        mockMvc.perform(put("/teams/{teamId}/team-leader/{userId}", 1L, 2L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(SET_TEAM_LEADER_RESPONSE));

        verify(teamService)
                .setTeamLeader(1L, 2L);
    }

    @Test
    void whenValidInput_withAdminRole_removeUserFromTeam_thenReturnsTeamResponseDTO() {

        TeamResponseDTO responseDTO = createTestResponseDto();
        responseDTO.getTeamMemberIds().remove(1L);

        when(teamService.removeUserFromTeam(1L, 1L)).thenReturn(responseDTO);

        try {
            mockMvc.perform(delete("/teams/{teamId}/users/{userId}", 1L, 1L)
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(REMOVE_USER_FROM_TEAM_RESPONSE));
            verify(teamService, times(1)).removeUserFromTeam(1L, 1L);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    void whenValidInput_withAdminRole_updateTeam_thenReturns200() throws Exception {
        TeamRequestDTO teamRequestDTO = new TeamRequestDTO();
        teamRequestDTO.setTitle("team1");
        teamRequestDTO.setDescription("team1description");
        teamRequestDTO.setTeamLeaderId((long) 1);

        TeamResponseDTO teamResponseDTO = createTestResponseDto();

        when(teamService.updateTeam(anyLong(), any(TeamRequestDTO.class)))
                .thenReturn(teamResponseDTO);

        mockMvc.perform(put("/teams/{teamId}", 1L)
                .header(AUTHORIZATION, TOKEN)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_UPDATED_RESPONSE));

        verify(teamService, times(1))
                .updateTeam(1L, teamRequestDTO);
    }

    @Test
    void whenValidInput_withAdminRole_deleteTeam_thenReturns200() throws Exception {

        doNothing().when(teamService)
                .deleteTeam(1L);

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/teams/{teamId}", 1L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk());

        verify(teamService, times(1))
                .deleteTeam(1L);
    }

    @Test
    void whenValidInput_withAdminRole_assignUserToTeam_thenReturnsTeamResponseDTO() {

        TeamResponseDTO responseDtoAfterAssigning = createAssignTestResponseDTO();

        when(teamService.assignUserToTeam(1L, 3L)).thenReturn(responseDtoAfterAssigning);

        try {
            mockMvc.perform(put("/teams/{teamId}/users/{userId}", 1L, 3L)
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(ASSIGN_USER_TO_TEAM_RESPONSE));
            verify(teamService, times(1)).assignUserToTeam(1L, 3L);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

}
