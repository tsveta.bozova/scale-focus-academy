package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffResponseDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.services.TimeOffService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class TimeOffControllerTest {
    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN = "token";
    private static final String TIME_SUFFIX = "T14:38:40.108Z";

    private static final String GET_ALL_RESPONSE = "[{\"id\":1,\"type\":\"PAID\",\"startDate\":\"" + LocalDate.now().plusDays(1) + "\"," +
            "\"endDate\":\"" + LocalDate.now().plusDays(6) + "\",\"reason\":\"Test Reason\",\"status\":\"AWAITING\"," +
            "\"dateCreated\":\"2020-05-31\",\"creatorId\":1," +
            "\"dateUpdated\":\"2020-05-31\",\"editorId\":1}]";

    private static final String GET_BY_ID_RESPONSE = "{\"id\":1,\"type\":\"PAID\",\"startDate\":\"" + LocalDate.now().plusDays(1) + "\"," +
            "\"endDate\":\"" + LocalDate.now().plusDays(6) + "\",\"reason\":\"Test Reason\",\"status\":\"AWAITING\"," +
            "\"dateCreated\":\"2020-05-31\",\"creatorId\":1," +
            "\"dateUpdated\":\"2020-05-31\",\"editorId\":1}";

    private static final String UPDATE_REQUEST_BODY = "{ \"type\" : \"PAID\", \"startDate\" : \"" + LocalDate.now().plusDays(1) + "\", \"endDate\" : null, \"numberOfDays\" : 5, \"reason\" : \"summer vacation\" }";

    private static final String UPDATE_RESPONSE_BODY = "{\"id\":1,\"type\":\"PAID\",\"startDate\":\"" + LocalDate.now().plusDays(1) + "\"," +
            "\"endDate\":\"" + LocalDate.now().plusDays(6) + "\",\"reason\":\"Test Reason\"," +
            "\"status\":\"AWAITING\",\"dateCreated\":\"2020-05-31\"," +
            "\"creatorId\":1,\"dateUpdated\":\"2020-05-31\",\"editorId\":1}";


    private static final String CANCEL_TIME_OFF_RESPONSE = "{\"id\":1,\"type\":\"PAID\",\"startDate\":\"" + LocalDate.now().plusDays(1) + "\"," +
            "\"endDate\":\"" + LocalDate.now().plusDays(6) + "\",\"reason\":\"Test Reason\",\"status\":\"CANCELLED\"," +
            "\"dateCreated\":\"2020-05-31\",\"creatorId\":1," +
            "\"dateUpdated\":\"2020-05-31\",\"editorId\":1}";

    private static final String REJECT_TIME_OFF_RESPONSE = "{\"id\":1,\"type\":\"PAID\",\"startDate\":\"" + LocalDate.now().plusDays(1) + "\"," +
            "\"endDate\":\"" + LocalDate.now().plusDays(6) + "\",\"reason\":\"Test Reason\",\"status\":\"CANCELLED\"," +
            "\"dateCreated\":\"2020-05-31\",\"creatorId\":1," +
            "\"dateUpdated\":\"2020-05-31\",\"editorId\":1}";

    private static final String APPROVE_TIME_OFF_RESPONSE = "{\"id\":1,\"type\":\"PAID\",\"startDate\":\"" + LocalDate.now().plusDays(1) + "\"," +
            "\"endDate\":\"" + LocalDate.now().plusDays(6) + "\",\"reason\":\"Test Reason\",\"status\":\"APPROVED\"," +
            "\"dateCreated\":\"2020-05-31\",\"creatorId\":1," +
            "\"dateUpdated\":\"2020-05-31\",\"editorId\":1}";


    private static final String CREATE_BODY = "{ \"type\" : \"PAID\", \"startDate\" : \"" + LocalDate.now().plusDays(1) + "\", \"endDate\" : null, \"numberOfDays\" : 5, \"reason\" : \"summer vacation\" }";

    private static final String CREATE_BODY_RESPONSE = "{\n" +
            "    \"id\": 1,\n" +
            "    \"type\": \"PAID\",\n" +
            "    \"startDate\": \"" + LocalDate.now().plusDays(1) + "\",\n" +
            "    \"endDate\": \"" + LocalDate.now().plusDays(6) + "\",\n" +
            "    \"reason\": \"Test Reason\",\n" +
            "    \"status\": \"AWAITING\",\n" +
            "    \"dateCreated\": \"2020-05-31\",\n" +
            "    \"creatorId\": 1,\n" +
            "    \"dateUpdated\": \"2020-05-31\",\n" +
            "    \"editorId\": 1\n" +
            "}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TimeOffService timeOffService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void whenValidInput_withAdminRole_getAll_thenReturnsListOfTimeOffResponseDTO() throws Exception {
        List<TimeOffResponseDTO> timeOffList = new ArrayList<>();
        timeOffList.add(createTestResponseDto());

        when(timeOffService.getAll())
                .thenReturn(timeOffList);

        mockMvc.perform(get("/time-offs")
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_ALL_RESPONSE));

        verify(timeOffService)
                .getAll();
    }

    @Test
    void whenValidInput_withAdminRole_getById_thenReturnsTimeOffResponseDTO() throws Exception {
        TimeOffResponseDTO timeOffResponseDTO = createTestResponseDto();

        when(timeOffService.getById(1L))
                .thenReturn(timeOffResponseDTO);

        mockMvc.perform(get("/time-offs/{timeOffId}", 1L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_BY_ID_RESPONSE));

        verify(timeOffService)
                .getById(1L);
    }

    @Test
    void whenValidInput_withAdminRole_updatePaidTimeOff_thenReturnsTimeOffResponseDTO() throws Exception {
        TimeOffResponseDTO timeOffResponseDTO = createTestResponseDto();

        when(timeOffService.update(any(TimeOffRequestDTO.class), anyLong()))
                .thenReturn(timeOffResponseDTO);

        mockMvc.perform(put("/time-offs/{timeOffId}", 1L)
                .header(AUTHORIZATION, TOKEN)
                .content(UPDATE_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UPDATE_RESPONSE_BODY));

        verify(timeOffService)
                .update(any(TimeOffRequestDTO.class), anyLong());
    }

    @Test
    void whenValidInput_withAdminRole_cancelTimeOffRequest_thenReturns200() throws Exception {
        TimeOffResponseDTO timeOffResponseDTO = createTestResponseDto();
        timeOffResponseDTO.setStatus(TimeOffStatus.CANCELLED);

        when(timeOffService.cancel(anyLong()))
                .thenReturn(timeOffResponseDTO);

        mockMvc.perform(put("/time-offs/cancel/{timeOffId}", 1L)
                .header(AUTHORIZATION, TOKEN)
                .content(GET_BY_ID_RESPONSE)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CANCEL_TIME_OFF_RESPONSE));

        verify(timeOffService)
                .cancel(1L);
    }

    @Test
    void whenValidInput_approveTimeOffRequest_thenReturns200() throws Exception {
        TimeOffResponseDTO timeOffResponseDTO = createTestResponseDto();
        timeOffResponseDTO.setStatus(TimeOffStatus.APPROVED);

        when(timeOffService.approve(anyLong()))
                .thenReturn(timeOffResponseDTO);

        mockMvc.perform(put("/time-offs/approve/{timeOffId}", 1L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(APPROVE_TIME_OFF_RESPONSE));

        verify(timeOffService)
                .approve(1L);
    }

    @Test
    void whenValidInput_withAdminRole_rejectTimeOffRequest_thenReturns200() throws Exception {
        TimeOffResponseDTO timeOffResponseDTO = createTestResponseDto();
        timeOffResponseDTO.setStatus(TimeOffStatus.CANCELLED);

        when(timeOffService.reject(anyLong()))
                .thenReturn(timeOffResponseDTO);

        mockMvc.perform(put("/time-offs/reject/{timeOffId}", 1L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(REJECT_TIME_OFF_RESPONSE));

        verify(timeOffService)
                .reject(1L);
    }

    @Test
    void whenValidInput_createTimeOff_thenReturns201() throws Exception {
        TimeOffResponseDTO timeOffResponseDTO = createTestResponseDto();

        when(timeOffService.create(any(TimeOffRequestDTO.class)))
                .thenReturn(timeOffResponseDTO);

        mockMvc.perform(post("/time-offs")
                .header(AUTHORIZATION, TOKEN)
                .content(CREATE_BODY)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(CREATE_BODY_RESPONSE));

        verify(timeOffService)
                .create(any(TimeOffRequestDTO.class));
    }

    @Test
    void whenValidInput_delete_thenReturns200() throws Exception {
        doNothing().when(timeOffService)
                .delete(1L);

        mockMvc.perform(delete("/time-offs/{timeOffId}", 1L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk());

        verify(timeOffService)
                .delete(1L);
    }

    private TimeOffResponseDTO createTestResponseDto() {
        TimeOffResponseDTO dto = new TimeOffResponseDTO();
        dto.setId(1L);
        dto.setType(TimeOffType.PAID);
        dto.setStartDate(LocalDate.now().plusDays(1));
        dto.setEndDate(LocalDate.now().plusDays(6));
        dto.setReason("Test Reason");
        dto.setStatus(TimeOffStatus.AWAITING);
        dto.setDateCreated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        dto.setCreatorId(1L);
        dto.setDateUpdated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        dto.setEditorId(1L);
        return dto;
    }
}
