package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.login.LoginRequestDTO;
import com.scalefocus.workforcemanagement.dtos.login.LoginResponseDTO;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import com.scalefocus.workforcemanagement.services.implementations.JWTTokenServiceImpl;
import com.scalefocus.workforcemanagement.services.implementations.LoginServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class LoginServiceImplTest {
    private static final String TOKEN = "token";

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private LoginServiceImpl loginService;

    @MockBean
    private JWTTokenServiceImpl tokenService;

    @Test
    @DisplayName("Should return registered user if exists.")
    void login() {
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setEmail("admin");
        loginRequestDTO.setPassword("adminpass");

        User expectedUser = new User();
        expectedUser.setEmail("admin");
        expectedUser.setPassword("adminpass");

        when(userRepository.findByEmail(any(String.class)))
                .thenReturn(java.util.Optional.of(expectedUser));
        when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);
        when(tokenService.generate(any(User.class)))
                .thenReturn(TOKEN);

        LoginResponseDTO responseUser = new LoginResponseDTO();
        responseUser.setToken(TOKEN);

        LoginResponseDTO actualUser = loginService.login(loginRequestDTO);

        assertAll(
                () -> assertNotNull(actualUser),
                () -> assertEquals(responseUser, actualUser)
        );

        verify(userRepository)
                .findByEmail(anyString());
    }
}
