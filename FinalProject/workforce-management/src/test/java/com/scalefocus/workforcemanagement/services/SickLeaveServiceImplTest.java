package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.NotEnoughAvailableDaysException;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class SickLeaveServiceImplTest {
    private static final String TIME_SUFFIX = "T14:38:40.108Z";
    private static final String NOT_ENOUGH_AVAILABLE_DAYS_MESSAGE = "Not enough available sick leave days";

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private TimeOffRepository timeOffRepository;

    @MockBean
    private OfficialHolidayClient officialHolidayClient;

    @Autowired
    private SickLeaveService sickLeaveService;

    private User creatorUser;
    private TimeOff sickLeave;
    private TimeOffRequestDTO requestDTO;

    @BeforeEach
    void setUp() {
        creatorUser = createLoggedUser();
        sickLeave = createTestTimeOff();
        requestDTO = createTimeOffRequest();
    }

    @Test
    void whenValidInput_create_theReturnTimeOff() {
        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());

        sickLeave = sickLeaveService.create(requestDTO);

        assertEquals(sickLeave.getStatus(), TimeOffStatus.APPROVED);
    }

    @Test
    void whenValidInputNoAvailableDays_create_theRNotEnoughAvailableDaysException() {
        String message = "";
        creatorUser.setSickLeaveDays(1);

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        try {
            sickLeaveService.create(requestDTO);
        } catch (NotEnoughAvailableDaysException e) {
            message = e.getMessage();
        }

        assertEquals(NOT_ENOUGH_AVAILABLE_DAYS_MESSAGE, message);
    }

    private TimeOff createTestTimeOff() {
        TimeOff timeOff = new TimeOff();

        timeOff.setId(1L);
        timeOff.setType(TimeOffType.PAID);
        timeOff.setTimeOffStart(LocalDate.now().plusDays(1));
        timeOff.setTimeOffEnd(LocalDate.now().plusDays(6));
        timeOff.setReason("Test Reason");
        timeOff.setStatus(TimeOffStatus.AWAITING);
        timeOff.setCreator(creatorUser);
        timeOff.setDateUpdated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        timeOff.setEditor(creatorUser);

        return timeOff;
    }

    private User createLoggedUser() {
        User user = new User();

        user.setId(1L);
        user.setEmail("cveta");
        user.setFirstName("Cveta");
        user.setLastName("Cveta");
        user.setAdmin(true);
        user.setSickLeaveDays(40);
        user.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        user.setEditor(user);
        user.setCreator(user);

        return user;
    }

    private TimeOffRequestDTO createTimeOffRequest() {
        TimeOffRequestDTO timeOffRequestDTO = new TimeOffRequestDTO();

        timeOffRequestDTO.setType("SICK_LEAVE");
        timeOffRequestDTO.setStartDate(LocalDate.now().plusDays(1));
        timeOffRequestDTO.setEndDate(LocalDate.now().plusDays(6));
        timeOffRequestDTO.setNumberOfDays(null);
        timeOffRequestDTO.setReason("Sick");

        return timeOffRequestDTO;
    }
}