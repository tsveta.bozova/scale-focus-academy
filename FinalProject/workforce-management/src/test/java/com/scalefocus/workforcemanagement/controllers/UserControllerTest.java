package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.user.UserRequestDTO;
import com.scalefocus.workforcemanagement.dtos.user.UserResponseDTO;
import com.scalefocus.workforcemanagement.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@WithMockUser(roles = "ADMIN")
class UserControllerTest {

    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN = "token";

    private static final String GET_ALL_RESPONSE = "[{\"id\":1,\"email\":\"admin\",\"firstName\":\"Admin\"," +
            "\"lastName\":\"Admin\",\"dateCreated\":\"2003-05-03\"," +
            "\"paidDays\":20,\"unpaidDays\":90," +
            "\"sickLeaveDays\":40," +
            "\"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"admin\":true}]";
    private static final String GET_BY_ID_RESPONSE = "{\"id\":1,\"email\":\"admin\",\"firstName\":\"Admin\"," +
            "\"lastName\":\"Admin\",\"dateCreated\":\"2003-05-03\"," +
            "\"paidDays\":20,\"unpaidDays\":90," +
            "\"sickLeaveDays\":40," +
            "\"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"admin\":true}";
    private static final String GET_UPDATED_RESPONSE = "{\"id\":1,\"email\":\"admin\"," +
            "\"firstName\":\"Admin\",\"lastName\":\"Admin\",\"dateCreated\":\"2003-05-03\"," +
            "\"paidDays\":20,\"unpaidDays\":90," +
            "\"sickLeaveDays\":40," +
            "\"creatorId\":1,\"dateUpdated\":\"2003-05-03\",\"editorId\":1,\"admin\":true}";
    private static final String UPDATED_REQUEST_BODY = "{\"email\" : \"admin\", \"password\" : \"adminpass\", " +
            "\"firstName\" : \"Admin\", \"lastName\" : \"Admin\",\"admin\" : true}";
    private static final String CREATE_BODY = "{\"email\": \"admin\",\"password\": \"123456\"," +
            "\"firstName\": \"Admin\",\"lastName\": \"Admin\",\"admin\": true}";
    private static final String CREATE_BODY_RESPONSE = "{\"id\":1,\"email\":\"admin\",\"firstName\":\"Admin\"," +
            "\"lastName\":\"Admin\",\"dateCreated\":\"2003-05-03\",\"creatorId\":1,\"dateUpdated\":\"2003-05-03\"," +
            "\"paidDays\":20,\"unpaidDays\":90," +
            "\"sickLeaveDays\":40," +
            "\"editorId\":1,\"admin\":true}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void whenValidInput_withAdminRole_getAll_thenReturnsListOfUserResponseDTO() {

        List<UserResponseDTO> userList = new ArrayList<>();
        userList.add((createTestResponseDto()));
        when(userService.getAll()).thenReturn(userList);

        try {
            mockMvc.perform(get("/users")
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(GET_ALL_RESPONSE));
            verify(userService, times(1)).getAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    void whenValidInput_withAdminRole_getById_thenReturnsUserResponseDTO() {


        UserResponseDTO getByIdUserResponse = createTestResponseDto();
        when(userService.getById(anyLong())).thenReturn(getByIdUserResponse);

        try {
            mockMvc.perform(get("/users/{userId}", 1L)
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(GET_BY_ID_RESPONSE));
            verify(userService, times(1)).getById(1L);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    void whenValidInput_withAdminRole_createUser_thenReturns201() {
        UserResponseDTO userResponseDTO = createTestResponseDto();

        when(userService.create(any(UserRequestDTO.class))).thenReturn(userResponseDTO);

        try {
            mockMvc.perform(post("/users")
                    .content(CREATE_BODY)
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(CREATE_BODY_RESPONSE));
            verify(userService, times(1)).create(any(UserRequestDTO.class));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    void whenValidInput_withAdminRole_updateUser_thenReturns200() throws Exception {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setEmail("admin");
        userRequestDTO.setPassword("adminpass");
        userRequestDTO.setFirstName("Admin");
        userRequestDTO.setLastName("Admin");
        userRequestDTO.setAdmin(true);

        UserResponseDTO userResponseDTO = createTestResponseDto();

        when(userService.updateUser(userRequestDTO, 1L))
                .thenReturn(userResponseDTO);

        mockMvc.perform(put("/users/{userId}", 1L)
                .header(AUTHORIZATION, TOKEN)
                .content(UPDATED_REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(GET_UPDATED_RESPONSE));

        verify(userService)
                .updateUser(userRequestDTO, 1L);
    }

    @Test
    void whenValidInput_withAdminRole_gdprDelete_thenReturns200() throws Exception {

        doNothing().when(userService)
                .gdprDelete(1L);

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/{userId}", 1L)
                .header(AUTHORIZATION, TOKEN))
                .andExpect(status().isOk());

        verify(userService)
                .gdprDelete(1L);
    }

    private UserResponseDTO createTestResponseDto() {
        UserResponseDTO userResponseDTO = new UserResponseDTO();

        userResponseDTO.setId((long) 1);
        userResponseDTO.setEmail("admin");
        userResponseDTO.setFirstName("Admin");
        userResponseDTO.setLastName("Admin");
        userResponseDTO.setAdmin(true);
        userResponseDTO.setPaidDays(20);
        userResponseDTO.setUnpaidDays(90);
        userResponseDTO.setSickLeaveDays(40);
        userResponseDTO.setDateCreated(Instant.parse("2003-05-03T00:00:00.00Z"));
        userResponseDTO.setCreatorId((long) 1);
        userResponseDTO.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        userResponseDTO.setEditorId((long) 1);

        return userResponseDTO;
    }
}