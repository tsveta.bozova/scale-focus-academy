package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.calendar.RestDayResponseDTO;
import com.scalefocus.workforcemanagement.dtos.calendar.enums.RestDayType;
import com.scalefocus.workforcemanagement.services.CalendarService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class CalendarControllerTest {
    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN = "token";
    private static final String GET_ALL = "[{\"date\":\"2020-05-31\",\"dayType\":\"PAID\"" +
                                                "},{\"date\":\"2020-06-01\"," +
                                            "\"dayType\":\"PAID\"}]";


    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CalendarController calendarController;
    @MockBean
    private CalendarService calendarService;


    @Test
    void  whenValidInput_getAllRestDays_thenReturnsAllRestDaysDTO() {
        RestDayResponseDTO dayStart = new RestDayResponseDTO(LocalDate.parse("2020-05-31"), RestDayType.PAID);
        RestDayResponseDTO dayEnd = new RestDayResponseDTO(LocalDate.parse("2020-06-01"), RestDayType.PAID);
        List<RestDayResponseDTO> monthlyRestDaysResponseDTO = new ArrayList<>();
        monthlyRestDaysResponseDTO.add(dayStart);
        monthlyRestDaysResponseDTO.add(dayEnd);

        when(calendarService.getAllNonWorkingDays(11)).thenReturn(monthlyRestDaysResponseDTO);

        try{
            mockMvc.perform(get("/calendar/{monthNumber}", 11L)
                    .header(AUTHORIZATION, TOKEN))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(GET_ALL));
            verify(calendarService, times(1)).getAllNonWorkingDays(11);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}