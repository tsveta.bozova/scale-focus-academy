package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.calendar.RestDayResponseDTO;
import com.scalefocus.workforcemanagement.dtos.calendar.enums.RestDayType;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.services.implementations.CalendarServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class CalendarServiceImplTest {

    private static final String TIME_SUFFIX = "T14:38:40.108Z";

    @MockBean
    private OfficialHolidayClient officialHolidayClient;

    @MockBean
    private TimeOffRepository timeOffRepository;

    @MockBean
    private AuthenticationService authenticationService;

    @Autowired
    private CalendarServiceImpl calendarServiceImpl;

    @Test
    void whenValidInput_getAllRestDays_thenReturnsAllRestDaysDTO() {
        User loggedInUser = createTestAdminUser();
        List<TimeOff> expectedList = new ArrayList<>();
        expectedList.add(createTestTimeOff());

        when(timeOffRepository.findAll())
                .thenReturn(expectedList);
        when(authenticationService.getLoggedInUser())
                .thenReturn(loggedInUser);
        CalendarServiceImpl calendarService1 = Mockito.spy(calendarServiceImpl);
        Mockito.doReturn(new ArrayList<LocalDate>())
                .when(calendarService1).getWeekEndsForMonth(6);

        List<RestDayResponseDTO> monthlyRestDaysResponseDTO = calendarService1.getAllNonWorkingDays(6);

        assertNotNull(monthlyRestDaysResponseDTO, "No vacation days found");
        assertEquals(1, monthlyRestDaysResponseDTO.size(), "Not all rest days are returned");
        assertEquals(LocalDate.parse("2020-06-01"), monthlyRestDaysResponseDTO.get(0).getDate(), "Returned date does not match");
        assertEquals(RestDayType.PAID, monthlyRestDaysResponseDTO.get(0).getDayType(), "Returned type does not match");
    }

    private TimeOff createTestTimeOff() {
        TimeOff timeOff = new TimeOff();
        timeOff.setId(1L);
        timeOff.setType(TimeOffType.PAID);
        timeOff.setTimeOffStart(LocalDate.parse("2020-05-31"));
        timeOff.setTimeOffEnd(LocalDate.parse("2020-06-01"));
        timeOff.setReason("Test Reason");
        timeOff.setStatus(TimeOffStatus.APPROVED);
        timeOff.setCreator(createTestAdminUser());
        timeOff.setDateUpdated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        timeOff.setEditor(createTestAdminUser());
        return timeOff;
    }

    private User createTestAdminUser() {
        User teamCreator = new User();

        teamCreator.setId(1L);
        teamCreator.setEmail("admin");
        teamCreator.setFirstName("Admin");
        teamCreator.setLastName("Admin");
        teamCreator.setAdmin(true);
        teamCreator.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        teamCreator.setEditor(teamCreator);
        teamCreator.setCreator(teamCreator);

        return teamCreator;
    }
}