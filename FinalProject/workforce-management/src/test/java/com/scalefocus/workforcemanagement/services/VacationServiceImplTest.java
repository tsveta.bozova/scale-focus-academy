package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.clients.OfficialHolidayClient;
import com.scalefocus.workforcemanagement.dtos.timeoff.TimeOffRequestDTO;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffStatus;
import com.scalefocus.workforcemanagement.dtos.timeoff.enums.TimeOffType;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.TimeOff;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.*;
import com.scalefocus.workforcemanagement.repositories.TimeOffRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class VacationServiceImplTest {
    private static final String TIME_SUFFIX = "T14:38:40.108Z";
    private static final String NOT_ENOUGH_AVAILABLE_DAYS_MESSAGE = "Not enough available days";
    private static final String MISSING_PERMISSION_EXCEPTION_MESSAGE = "No permissions to edit request";
    private static final String RESOURCE_LOCKED_EXCEPTION_MESSAGE = "Request is locked";
    private static final String INVALID_TIME_OFF_TYPE_EXCEPTION_MESSAGE = "Cannot edit time off type";
    private static final String DUPLICATE_TIME_OFF_DATES_EXCEPTION_MESSAGE = "Requested period has overlapping days with existing time off";

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private TimeOffRepository timeOffRepository;

    @MockBean
    private OfficialHolidayClient officialHolidayClient;

    @Autowired
    private VacationService vacationService;


    @MockBean
    private UserRepository userRepository;

    private TimeOff expectedTimeOff;
    private User creatorUser;
    private TimeOffRequestDTO timeOffRequestDTO;
    private String message;

    @BeforeEach
    void setUp() {
        creatorUser = createLoggedUser();
        timeOffRequestDTO = createTimeOffRequest();
        expectedTimeOff = createTestTimeOff();
    }

    @Test
    void whenValidInput_NoTeamLeaders_create_thenCreate_autoApprove() {
        List<TimeOff> timeOffs = new ArrayList<>();
        List<LocalDate> officialHolidays = new ArrayList<>();
        officialHolidays.add(LocalDate.now().plusDays(5));

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findAll())
                .thenReturn(timeOffs);
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(officialHolidays);

        expectedTimeOff = vacationService.create(timeOffRequestDTO);

        assertNotNull(expectedTimeOff);
        assertEquals(expectedTimeOff.getStatus(), TimeOffStatus.APPROVED);
    }

    @Test
    void whenValidInput_NoEnoughAvailableDays_create_thenThrowsNotEnoughAvailableDaysException() {
        List<TimeOff> timeOffs = new ArrayList<>();
        List<LocalDate> officialHolidays = new ArrayList<>();
        officialHolidays.add(LocalDate.now().plusDays(5));
        creatorUser.setPaidDays(3);

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findAll())
                .thenReturn(timeOffs);
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(officialHolidays);

        try {
            expectedTimeOff = vacationService.create(timeOffRequestDTO);
        } catch (NotEnoughAvailableDaysException e) {
            message = e.getMessage();
        }

        assertEquals(NOT_ENOUGH_AVAILABLE_DAYS_MESSAGE, message);
    }

    @Test
    void whenValidInput_withTeamLeader_create_thenCreate () {
        List<User> allUsersTeamLeadersInOffice = new ArrayList<>();
        List<TimeOff> timeOffs = new ArrayList<>();
        List<LocalDate> officialHolidays = new ArrayList<>();
        Team team = new Team();
        User teamLeader = createTeamLeader();
        team.setTeamLeader(teamLeader);
        team.getTeamMembers().add(creatorUser);
        allUsersTeamLeadersInOffice.add(teamLeader);
        creatorUser.getTeamList().add(team);

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findAll())
                .thenReturn(timeOffs);
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(officialHolidays);
        when(userRepository.findAllById(anyList()))
                .thenReturn(allUsersTeamLeadersInOffice);

        expectedTimeOff = vacationService.create(timeOffRequestDTO);

        assertNotNull(expectedTimeOff);
        assertEquals(expectedTimeOff.getStatus(), TimeOffStatus.AWAITING);
    }

    @Test
    void whenValidInput_update_thenReturnUpdatedTimeOff() {
        List<User> teamLeaders = new ArrayList<>();

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(expectedTimeOff))
                .thenReturn(expectedTimeOff);

        TimeOff actualTimeOff = vacationService.update(timeOffRequestDTO, 1L);

        assertEquals(expectedTimeOff, actualTimeOff);
    }

    @Test
    void whenInvalidInput_update_thenResourceLockedException() {
        List<User> teamLeaders = new ArrayList<>();
        expectedTimeOff.setStatus(TimeOffStatus.APPROVED);

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(expectedTimeOff))
                .thenReturn(expectedTimeOff);

        try {
            vacationService.update(timeOffRequestDTO, 1L);
        } catch (ResourceLockedException e) {
            message = e.getMessage();
        }

        assertEquals(RESOURCE_LOCKED_EXCEPTION_MESSAGE, message);
    }

    @Test
    void whenInvalidInput_update_thenInvalidTimeOffException() {
        List<User> teamLeaders = new ArrayList<>();
        timeOffRequestDTO.setType("UNPAID");

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(expectedTimeOff))
                .thenReturn(expectedTimeOff);

        try {
            vacationService.update(timeOffRequestDTO, 1L);
        } catch (InvalidTimeOffTypeException e) {
            message = e.getMessage();
        }

        assertEquals(INVALID_TIME_OFF_TYPE_EXCEPTION_MESSAGE, message);
    }

    @Test
    void whenInvalidInput_update_thenDuplicateTimeOffDatesException() {
        List<User> teamLeaders = new ArrayList<>();
        List<TimeOff> timeOffs = new ArrayList<>();
        TimeOff secondTimeOff = createTestTimeOff();
        secondTimeOff.setId(2L);
        timeOffs.add(secondTimeOff);

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(expectedTimeOff))
                .thenReturn(expectedTimeOff);
        when(timeOffRepository.findAllByCreatorId(anyLong()))
                .thenReturn(timeOffs);

        try {
            vacationService.update(timeOffRequestDTO, 1L);
        } catch (DuplicateTimeOffDatesException e) {
            message = e.getMessage();
        }

        assertEquals(DUPLICATE_TIME_OFF_DATES_EXCEPTION_MESSAGE, message);
    }

    @Test
    void whenValidInput_withTeamLeader_update_thenReturnUpdatedTimeOff() {
        List<User> teamLeaders = new ArrayList<>();
        teamLeaders.add(createTeamLeader());

        when(authenticationService.getLoggedInUser())
                .thenReturn(creatorUser);
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(expectedTimeOff))
                .thenReturn(expectedTimeOff);

        TimeOff actualTimeOff = vacationService.update(timeOffRequestDTO, 1L);

        assertEquals(expectedTimeOff, actualTimeOff);
    }

    @Test
    void whenUserWithoutAccess_update_thenMissingPermissionException() {
        List<User> teamLeaders = new ArrayList<>();

        when(authenticationService.getLoggedInUser())
                .thenReturn(createTeamLeader());
        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(officialHolidayClient.getOfficialHolidays())
                .thenReturn(new ArrayList<>());
        when(userRepository.findAllById(anyList()))
                .thenReturn(teamLeaders);
        when(timeOffRepository.save(expectedTimeOff))
                .thenReturn(expectedTimeOff);

        try {
            vacationService.update(timeOffRequestDTO, 1L);
        } catch (MissingPermissionsException e) {
            message = e.getMessage();
        }

        assertEquals(MISSING_PERMISSION_EXCEPTION_MESSAGE, message);
    }

    @Test
    void whenTeamLeader_approve_thenReturnTimeOff() {
        Team team = new Team();
        team.getTeamMembers().add(creatorUser);
        User teamLeader = createTeamLeader();
        team.setTeamLeader(teamLeader);
        teamLeader.getTeamList().add(team);
        creatorUser.getTeamList().add(team);
        expectedTimeOff.setApproversList(new ArrayList<>());

        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(authenticationService.getLoggedInUser())
                .thenReturn(teamLeader);

        TimeOff actualTimeOff = vacationService.approve(1L);

        assertAll(
                () -> assertEquals(expectedTimeOff.getApproversList().size(), actualTimeOff.getApproversList().size()),
                () -> assertEquals(expectedTimeOff.getStatus(), actualTimeOff.getStatus())
        );
    }

    @Test
    void whenTeamLeader_reject_thenReturnTimeOff() {
        Team team = new Team();
        team.getTeamMembers().add(creatorUser);
        User teamLeader = createTeamLeader();
        team.setTeamLeader(teamLeader);
        teamLeader.getTeamList().add(team);
        creatorUser.getTeamList().add(team);
        expectedTimeOff.setApproversList(new ArrayList<>());

        when(timeOffRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.ofNullable(expectedTimeOff));
        when(authenticationService.getLoggedInUser())
                .thenReturn(teamLeader);

        TimeOff actualTimeOff = vacationService.reject(1L);

        assertEquals(expectedTimeOff.getStatus(), actualTimeOff.getStatus());
    }

    private TimeOff createTestTimeOff() {
        TimeOff timeOff = new TimeOff();

        timeOff.setId(1L);
        timeOff.setType(TimeOffType.PAID);
        timeOff.setTimeOffStart(LocalDate.now().plusDays(1));
        timeOff.setTimeOffEnd(LocalDate.now().plusDays(6));
        timeOff.setReason("Test Reason");
        timeOff.setStatus(TimeOffStatus.AWAITING);
        timeOff.setCreator(creatorUser);
        timeOff.setDateUpdated(Instant.parse("2020-05-31" + TIME_SUFFIX));
        timeOff.setEditor(creatorUser);

        return timeOff;
    }

    private User createLoggedUser() {
        User user = new User();

        user.setId(1L);
        user.setEmail("cveta");
        user.setFirstName("Cveta");
        user.setLastName("Cveta");
        user.setAdmin(true);
        user.setPaidDays(20);
        user.setUnpaidDays(90);
        user.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        user.setEditor(user);
        user.setCreator(user);

        return user;
    }

    private TimeOffRequestDTO createTimeOffRequest() {
        TimeOffRequestDTO timeOffRequestDTO = new TimeOffRequestDTO();

        timeOffRequestDTO.setType("PAID");
        timeOffRequestDTO.setStartDate(LocalDate.now().plusDays(1));
        timeOffRequestDTO.setEndDate(LocalDate.now().plusDays(6));
        timeOffRequestDTO.setNumberOfDays(5);
        timeOffRequestDTO.setReason("Test Reason");

        return timeOffRequestDTO;
    }
    private User createTeamLeader() {
        User user = new User();

        user.setId(2L);
        user.setEmail("georgi");
        user.setFirstName("Cveta");
        user.setLastName("Cveta");
        user.setAdmin(false);

        return user;
    }
}