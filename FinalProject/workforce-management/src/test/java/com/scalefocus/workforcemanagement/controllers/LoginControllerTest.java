package com.scalefocus.workforcemanagement.controllers;

import com.scalefocus.workforcemanagement.dtos.login.LoginRequestDTO;
import com.scalefocus.workforcemanagement.dtos.login.LoginResponseDTO;
import com.scalefocus.workforcemanagement.services.LoginService;
import com.scalefocus.workforcemanagement.services.implementations.JWTTokenServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(LoginController.class)
public class LoginControllerTest {
    private static final String AUTHORIZATION = "Authorization";
    private static final String USERNAME_PASSWORD = "admin,adminpass";
    private static final String TOKEN = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNTkwNTI4NDY2fQ.JIoAKJlbxgwZF9GlqJ7WOliuD3_8JQ8BN1zuLig7-0A";
    private static final String LOGIN_RESPONSE = "{}";
    private static final String REQUEST_BODY = "{\"email\": \"admin\",\"password\": \"adminpass\"}";

    private MockMvc mockMvc;

    @MockBean
    private LoginService loginService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private JWTTokenServiceImpl tokenService;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void whenValidInput_login_thenReturnsToken() throws Exception {

        LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
        loginResponseDTO.setToken(TOKEN);
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setEmail("admin");
        loginRequestDTO.setPassword("adminpass");
        when(loginService.login(any())).thenReturn(loginResponseDTO);

        mockMvc.perform(post("/login")
                .header(AUTHORIZATION, USERNAME_PASSWORD)
                .content(REQUEST_BODY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(LOGIN_RESPONSE));
        verify(loginService).login(loginRequestDTO);
    }
}
