package com.scalefocus.workforcemanagement.services;

import com.scalefocus.workforcemanagement.dtos.team.TeamRequestDTO;
import com.scalefocus.workforcemanagement.dtos.team.TeamResponseDTO;
import com.scalefocus.workforcemanagement.entities.Team;
import com.scalefocus.workforcemanagement.entities.User;
import com.scalefocus.workforcemanagement.exceptions.TeamTitleTakenException;
import com.scalefocus.workforcemanagement.repositories.TeamRepository;
import com.scalefocus.workforcemanagement.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TeamServiceImplTest {
    private static final String TEAM_TITLE_TAKEN_EXCEPTION_MESSAGE = "Team title is taken";

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private TeamRepository teamRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private TeamService teamService;

    @Test
    void whenValidInput_getAll_thenReturnsListOfTeamResponseDTO() {
        List<Team> expectedList = new ArrayList<>();
        expectedList.add(createTestTeam());

        when(teamRepository.findAll())
                .thenReturn(expectedList);

        List<TeamResponseDTO> actualList = teamService.getAll();

        assertNotNull(actualList, "No teams found");
        assertEquals(expectedList.size(), actualList.size(), "Not all teams are returned");

        verify(teamRepository)
                .findAll();
    }

    @Test
    void whenValidInput_create_thenCreatesNewTeam() {
        User loggedUser = createTestTeamCreator();
        Team team = createTestTeam();
        team.setTeamLeader(loggedUser);
        team.getTeamMembers().add(loggedUser);
        TeamRequestDTO teamRequestDTO = createTeamUpdateRequestDTO();

        when(teamRepository.save(any(Team.class)))
                .thenReturn(team);
        when(authenticationService.getLoggedInUser())
                .thenReturn(loggedUser);
        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(loggedUser));
        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(loggedUser));

        TeamResponseDTO actualTeamResponse = teamService.create(teamRequestDTO);
        TeamResponseDTO responseTeam = createResponse(team);

        assertAll(
                () -> assertNotNull(actualTeamResponse, "No team found"),
                () -> assertEquals(responseTeam, actualTeamResponse)
        );

        verify(teamRepository)
                .save(any(Team.class));
    }

    @Test
    void whenInValidInput_create_thenTeamTitleTakenException() {
        String message = "";
        Team team = createTestTeam();
        TeamRequestDTO teamRequestDTO = createTeamUpdateRequestDTO();

        when(teamRepository.findByTitle(anyString()))
                .thenReturn(Optional.of(team));

        try {
            teamService.create(teamRequestDTO);
        } catch (TeamTitleTakenException e) {
            message = e.getMessage();
        }

        assertEquals(TEAM_TITLE_TAKEN_EXCEPTION_MESSAGE, message);
    }

    private Team createTestTeam() {
        Team testTeam = new Team();

        testTeam.setId(1L);
        testTeam.setTitle("team1");
        testTeam.setDescription("team1description");
        testTeam.setTeamLeader(createTestTeamCreator());
        testTeam.setCreator(createTestTeamCreator());
        testTeam.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        testTeam.setEditor(createTestTeamCreator());

        return testTeam;
    }

    private User createTestTeamCreator() {
        User teamCreator = new User();

        teamCreator.setId(2L);
        teamCreator.setEmail("Test User");
        teamCreator.setFirstName("Admin");
        teamCreator.setLastName("Admin");
        teamCreator.setAdmin(true);
        teamCreator.setDateUpdated(Instant.parse("2003-05-03T00:00:00.00Z"));
        teamCreator.setEditor(teamCreator);
        teamCreator.setCreator(teamCreator);

        return teamCreator;
    }

    private TeamRequestDTO createTeamUpdateRequestDTO() {
        TeamRequestDTO teamRequestDTO = new TeamRequestDTO();

        teamRequestDTO.setTitle("team1");
        teamRequestDTO.setDescription("team1description");

        return teamRequestDTO;
    }

    @Test
    void whenValidInput_getById_thenReturnsTeamResponseDTO() {
        Team expectedTeam = createTestTeam();

        when(teamRepository.findById(anyLong()))
                .thenReturn(java.util.Optional.of(expectedTeam));

        TeamResponseDTO responseTeam = createResponse(expectedTeam);
        TeamResponseDTO actualTeam = teamService.getById(1L);


        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(responseTeam.getTeamLeaderId(), actualTeam.getTeamLeaderId())
        );

        verify(teamRepository)
                .findById(1L);
    }

    @Test
    void whenValidInput_setTeamLeader_thenUpdatesTeam() {
        Team expectedTeam = createTestTeam();
        List<User> expectedTeamMembers = new ArrayList<>();
        expectedTeamMembers.add(createTestTeamCreator());
        expectedTeam.setTeamMembers(expectedTeamMembers);
        User testTeamLeader = createTestTeamCreator();
        testTeamLeader.setEmail("admin@admin.com");
        expectedTeam.setTeamLeader(testTeamLeader);

        when(teamRepository.save(any(Team.class)))
                .thenReturn(expectedTeam);
        when(teamRepository.findById(any(Long.class)))
                .thenReturn(java.util.Optional.of(expectedTeam));
        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(testTeamLeader));
        when(authenticationService.getLoggedInUser())
                .thenReturn(testTeamLeader);

        TeamResponseDTO responseTeam = createResponse(expectedTeam);

        TeamResponseDTO actualTeam = teamService.setTeamLeader(1L, 2L);
        actualTeam.setDateUpdated(responseTeam.getDateUpdated());

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(responseTeam.getTeamLeaderId(), actualTeam.getTeamLeaderId())
        );

        verify(teamRepository)
                .save(any(Team.class));
        verify(teamRepository)
                .findById(any(Long.class));
    }

    private TeamResponseDTO createResponse(Team expectedTeam) {
        TeamResponseDTO teamResponseDTO = new TeamResponseDTO();
        teamResponseDTO.setId(expectedTeam.getId());
        teamResponseDTO.setTitle(expectedTeam.getTitle());
        teamResponseDTO.setDescription(expectedTeam.getDescription());
        teamResponseDTO.setTeamLeaderId(expectedTeam.getTeamLeader().getId());
        teamResponseDTO.setCreatorId(expectedTeam.getCreator().getId());
        teamResponseDTO.setDateCreated(expectedTeam.getDateCreated());
        teamResponseDTO.setEditorId(expectedTeam.getEditor().getId());
        teamResponseDTO.setDateUpdated(expectedTeam.getDateUpdated());
        List<Long> teamMemberIds = new ArrayList<>();
        expectedTeam.getTeamMembers()
                .forEach(user -> teamMemberIds.add(user.getId()));
        teamResponseDTO.setTeamMemberIds(teamMemberIds);
        return teamResponseDTO;
    }

    @Test
    void whenValidInput_removeUserFromTeam_thenReturnsTeamResponseDTO() {
        Team expectedTeam = createTestTeam();
        User userInTeam = createTestTeamCreator();
        expectedTeam.getTeamMembers().add(userInTeam);

        when(authenticationService.getLoggedInUser())
                .thenReturn(userInTeam);
        when(teamRepository.findById(1L))
                .thenReturn(java.util.Optional.of(expectedTeam));
        when(userRepository.findById((1L)))
                .thenReturn(java.util.Optional.of(userInTeam));
        when(teamRepository.save(any(Team.class)))
                .thenReturn(expectedTeam);

        TeamResponseDTO actualTeam = teamService.removeUserFromTeam(1L, 1L);
        TeamResponseDTO responseTeam = createResponse(expectedTeam);

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(responseTeam, actualTeam)
        );

        verify(teamRepository)
                .save(any(Team.class));
    }

    @Test
    void whenValidInput_updateTeam_thenUpdatesTeam() {
        TeamRequestDTO teamRequestDTO = createTeamUpdateRequestDTO();
        teamRequestDTO.setTeamLeaderId(2L);
        Team testTeam = createTestTeam();
        User loggedUser = createTestTeamCreator();
        testTeam.getTeamMembers().add(loggedUser);

        when(teamRepository.save(any(Team.class)))
                .thenReturn(testTeam);
        when(authenticationService.getLoggedInUser())
                .thenReturn(loggedUser);
        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(loggedUser));
        when(teamRepository.findById(anyLong()))
                .thenReturn(Optional.of(testTeam));

        TeamResponseDTO responseTeam = createResponse(testTeam);

        TeamResponseDTO actualTeam = teamService.updateTeam(1L, teamRequestDTO);
        actualTeam.setDateUpdated(responseTeam.getDateUpdated());

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(responseTeam, actualTeam)
        );

        verify(teamRepository, times(1))
                .save(testTeam);
    }

    @Test
    void whenValidInput_deleteTeam_thenDeletesTeam() {
        Team testTeam = createTestTeam();

        when(teamRepository.findById(anyLong()))
                .thenReturn(Optional.of(testTeam));

        teamService.deleteTeam(1L);

        verify(teamRepository)
                .deleteById(1L);
    }

    @Test
    void whenValidInput_assignUserToTeam_thenReturnsTeamResponseDTO() {
        Team expectedTeam = createTestTeam();
        User userInTeam = createTestTeamCreator();
        userInTeam.setId(2L);

        when(authenticationService.getLoggedInUser())
                .thenReturn(userInTeam);
        when(teamRepository.findById(1L))
                .thenReturn(java.util.Optional.of(expectedTeam));
        when(userRepository.findById(2L))
                .thenReturn(java.util.Optional.of(userInTeam));
        when(teamRepository.save(any(Team.class)))
                .thenReturn(expectedTeam);

        TeamResponseDTO actualTeam = teamService.assignUserToTeam(1L, 2L);
        TeamResponseDTO responseTeam = createResponse(expectedTeam);

        assertAll(
                () -> assertNotNull(actualTeam),
                () -> assertEquals(responseTeam, actualTeam)
        );

        verify(teamRepository)
                .save(any(Team.class));
    }
}
