
CREATE TABLE users (
        id               INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
        username              VARCHAR(255) NOT NULL,
        password              VARCHAR(255) NOT NULL,
        first_name            VARCHAR(255) NOT NULL,
        last_name             VARCHAR(255) NOT NULL,
        is_admin              CHAR(1)        NOT NULL,
        date_created          TIMESTAMP    NOT NULL,
        creator_id            INT          NOT NULL,
        modifier_id           INT          NOT NULL,
        date_modified         TIMESTAMP    NOT NULL,
		
		CONSTRAINT user_creator_fk
		FOREIGN KEY (creator_id)
		REFERENCES users(id),
		
		CONSTRAINT user_modifier_fk
		FOREIGN KEY (modifier_id)
		REFERENCES users(id),
        
        CONSTRAINT username_k UNIQUE (username)
    );
    
CREATE TABLE projects (
        id                 INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
        title                 VARCHAR(255) NOT NULL,
        description           VARCHAR(1000) NOT NULL,
        date_created          TIMESTAMP         NOT NULL,
        creator_id            INT          NOT NULL,
        date_modified         TIMESTAMP         NOT NULL,
        modifier_id           INT          NOT NULL,
		
		CONSTRAINT project_creator_fk
		FOREIGN KEY (creator_id)
        REFERENCES users (id),
		
		CONSTRAINT project_modifier_fk
		FOREIGN KEY (modifier_id)
		REFERENCES users(id)
    ); 
    
CREATE TABLE tasks (
        id                 INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
        project_id            INT          NOT NULL,
        assignee_id           INT          NOT NULL,
        title                 VARCHAR(255) NOT NULL,
        description           VARCHAR(255) NOT NULL,
        status                VARCHAR(20)      NOT NULL,
        date_created          TIMESTAMP         NOT NULL,
        creator_id            INT          NOT NULL,
        date_modified         TIMESTAMP         NOT NULL,
        modifier_id           INT          NOT NULL,
		
        CONSTRAINT task_creator_fk
		FOREIGN KEY (creator_id)
        REFERENCES users (id),
		
        CONSTRAINT project_fk
		FOREIGN KEY (project_id)
        REFERENCES projects (id) ON DELETE CASCADE,

		CONSTRAINT task_modifier_fk
		FOREIGN KEY (modifier_id)
		REFERENCES users(id),
        
        CONSTRAINT assignee_fk
        FOREIGN KEY (assignee_id)
        REFERENCES users(id)
        
    );
    
CREATE TABLE teams (
    id                 INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
     title                 VARCHAR(255) NOT NULL,
     date_created          TIMESTAMP    NOT NULL,
     creator_id            INT          NOT NULL,
     date_modified         TIMESTAMP    NOT NULL,
     modifier_id           INT          NOT NULL,  
     
     CONSTRAINT team_creator_fk
     FOREIGN KEY (creator_id)
     REFERENCES users(id),
     
     CONSTRAINT team_modifier_fk
     FOREIGN KEY (modifier_id)
     REFERENCES users(id)
     
     );
     
CREATE TABLE team_member (
	team_id INT NOT NULL,
	user_id INT,
	
	CONSTRAINT team_fK
	FOREIGN KEY (team_id)
	REFERENCES teams(id) ON DELETE CASCADE,
	
	CONSTRAINT team_member_fk
	FOREIGN KEY (user_id)
	REFERENCES users(id)ON DELETE CASCADE,
    
    CONSTRAINT team_members_k UNIQUE (team_id, user_id)
); 

CREATE TABLE teams_projects (
	team_id INT NOT NULL,
	project_id INT,
	
	CONSTRAINT team_on_project_fk
	FOREIGN KEY (team_id)
	REFERENCES teams(id) ON DELETE CASCADE,
	
	CONSTRAINT project_with_team_fk
	FOREIGN KEY (project_id)
	REFERENCES projects(id) ON DELETE CASCADE,
    
    CONSTRAINT team_project_k UNIQUE (team_id, project_id)
);

CREATE TABLE worklogs (
	id                 INT     GENERATED ALWAYS
                                    AS IDENTITY
                                    (START WITH 1
                                    INCREMENT BY 1
                                    MINVALUE 1
                                    MAXVALUE 2147483645
                                    NOCYCLE
                                    NOCACHE
                                    ORDER) PRIMARY KEY NOT NULL,
	task_id			  INT NOT NULL,
	user_id 		  INT NOT NULL,
	hours_spent       INT NOT NULL,
	date_worked       DATE NOT NULL,
	
	CONSTRAINT task_fk
	FOREIGN KEY (task_id)
	REFERENCES tasks(id) ON DELETE CASCADE,
	
	CONSTRAINT logging_user_id
	FOREIGN KEY (user_id)
	REFERENCES users(id)
	
);
     
     
     
     