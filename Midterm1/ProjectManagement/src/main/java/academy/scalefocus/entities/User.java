package academy.scalefocus.entities;

import java.time.LocalDateTime;

public class User {
    private int id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private boolean isAdmin;
    private LocalDateTime dateCreated;
    private int creatorId;
    private LocalDateTime dateModified;
    private int modifierId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean admin) {
        isAdmin = admin;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }

    public int getModifierId() {
        return modifierId;
    }

    public void setModifierId(int modifierId) {
        this.modifierId = modifierId;
    }

    public String getIsAdminAsString() {
        return this.isAdmin ? "1" : "0";
    }

    public void setIsAdminFromString(String isAdminString) {
        this.isAdmin = "1".equals(isAdminString);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(this.id).append(System.lineSeparator()).append("Username: ").append(this.username)
                .append(System.lineSeparator()).append("Password: ").append(this.password).append(System.lineSeparator())
                .append("First Name: ").append(this.firstName).append(System.lineSeparator()).append("Last Name: ")
                .append(this.lastName).append(System.lineSeparator()).append("Is Admin: ").append(this.isAdmin)
                .append(System.lineSeparator()).append("Date of creation: ").append(this.dateCreated).append(System.lineSeparator())
                .append("Creator ID: ").append(this.creatorId).append(System.lineSeparator()).append("Modifier ID: ").append(this.modifierId)
                .append(System.lineSeparator()).append("Date of last change: ").append(this.dateModified).append(System.lineSeparator())
                .append("---------------------------------");
        return sb.toString();
    }
}
