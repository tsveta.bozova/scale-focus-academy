package academy.scalefocus.entities;

import java.time.LocalDateTime;

public class Project {
    private int id;
    private String title;
    private String description;
    private LocalDateTime dateCreated;
    private int creatorId;
    private LocalDateTime dateModified;
    private int modifierId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }

    public int getModifierId() {
        return modifierId;
    }

    public void setModifierId(int modifierId) {
        this.modifierId = modifierId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(this.id).append(System.lineSeparator()).append("Title: ").append(this.title).append(System.lineSeparator())
                .append("Description: ").append(this.description).append(System.lineSeparator()).append("Date of creation: ")
                .append(this.dateCreated).append(System.lineSeparator()).append("Creator ID: ").append(this.creatorId)
                .append(System.lineSeparator()).append("Date of last change: ").append(this.dateModified).append(System.lineSeparator())
                .append("Modifier ID: ").append(this.modifierId).append(System.lineSeparator()).append("---------------------------------");
        return sb.toString();
    }
}

