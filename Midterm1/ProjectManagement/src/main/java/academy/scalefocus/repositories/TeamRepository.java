package academy.scalefocus.repositories;

import academy.scalefocus.entities.Team;

import java.util.List;

public interface TeamRepository {

    Team getById(int id);

    void add(Team team);

    void edit(Team team, int teamId);

    void delete(int teamId);

    List<Team> getAllTeams();

    void assignUser(int userId, int teamId);
}
