package academy.scalefocus.repositories;

import academy.scalefocus.tools.ConsoleManager;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.entities.Team;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamRepositoryImpl implements TeamRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(TeamRepositoryImpl.class);
    Connection connection = DatabaseService.getConnection();


    private Team createTeam(ResultSet resultSet) throws SQLException {
        Team team = new Team();

        team.setId(resultSet.getInt("id"));
        team.setTitle(resultSet.getString("title"));
        team.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        team.setCreatorId(resultSet.getInt("creator_id"));
        team.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());
        team.setModifierId(resultSet.getInt("modifier_id"));

        return team;
    }

    @Override
    public Team getById(int teamId) {
        Team team = new Team();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM teams WHERE" +
                " id=?")) {

            preparedStatement.setInt(1, teamId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    team = createTeam(resultSet);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return team;
    }

    @Override
    public void add(Team team) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO teams " +
                "(title, date_created, creator_id, date_modified, modifier_id) " +
                "VALUES (?,?,?,?,?)")) {

            preparedStatement.setString(1, team.getTitle());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(team.getDateCreated()));
            preparedStatement.setInt(3, team.getCreatorId());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(team.getDateModified()));
            preparedStatement.setInt(5, team.getModifierId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

    }

    @Override
    public void edit(Team team, int teamId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE teams SET title=?, " +
                     "date_modified=?, modifier_id=? " +
                     "WHERE id=?")) {

            preparedStatement.setString(1, team.getTitle());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(team.getDateModified()));
            preparedStatement.setInt(3, team.getModifierId());
            preparedStatement.setInt(4, teamId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(int teamId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM teams WHERE id=?");) {

            preparedStatement.setInt(1, teamId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public List<Team> getAllTeams() {
        List<Team> teams = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM teams");
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                Team team = createTeam(resultSet);
                teams.add(team);
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return teams;
    }

    @Override
    public void assignUser(int userId, int teamId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO team_member " +
                     "(team_id, user_id) VALUES (?,?)")) {

            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, userId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }
}
