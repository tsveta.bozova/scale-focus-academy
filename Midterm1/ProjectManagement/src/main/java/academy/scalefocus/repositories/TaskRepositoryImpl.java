package academy.scalefocus.repositories;

import academy.scalefocus.tools.ConsoleManager;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.enums.TaskStatus;
import academy.scalefocus.entities.Task;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryImpl implements TaskRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(TaskRepositoryImpl.class);
    Connection connection = DatabaseService.getConnection();

    private Task createTask(ResultSet resultSet) throws SQLException {
        Task task = new Task();

        task.setId(resultSet.getInt("id"));
        task.setProjectId(resultSet.getInt("project_id"));
        task.setAssigneeId(resultSet.getInt("assignee_id"));
        task.setTitle(resultSet.getString("title"));
        task.setDescription(resultSet.getString("description"));
        task.setStatus(Enum.valueOf(TaskStatus.class, resultSet.getString("status").toUpperCase()));
        task.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        task.setCreatorId(resultSet.getInt("creator_id"));
        task.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());
        task.setModifierId(resultSet.getInt("modifier_id"));

        return task;
    }

    @Override
    public Task getById(int taskId) {
        Task task = new Task();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM tasks WHERE" +
                     " id=?")) {

            preparedStatement.setInt(1, taskId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    task = createTask(resultSet);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return task;
    }

    @Override
    public void add(Task task) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO tasks " +
                     "(project_id, assignee_id, title, description, status, date_created, creator_id, date_modified, modifier_id) " +
                     "VALUES (?,?,?,?,?,?,?,?,?)")) {

            preparedStatement.setInt(1, task.getProjectId());
            preparedStatement.setInt(2, task.getAssigneeId());
            preparedStatement.setString(3, task.getTitle());
            preparedStatement.setString(4, task.getDescription());
            preparedStatement.setString(5, task.getStatus().toString().toLowerCase());
            preparedStatement.setTimestamp(6, Timestamp.valueOf(task.getDateCreated()));
            preparedStatement.setInt(7, task.getCreatorId());
            preparedStatement.setTimestamp(8, Timestamp.valueOf(task.getDateModified()));
            preparedStatement.setInt(9, task.getModifierId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void edit(Task task) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE tasks SET assignee_id=?, title=?, " +
                     "description=?, status=?, date_modified=?, modifier_id=? " +
                     "WHERE id=?")) {

            preparedStatement.setInt(1, task.getAssigneeId());
            preparedStatement.setString(2, task.getTitle());
            preparedStatement.setString(3, task.getDescription());
            preparedStatement.setString(4, task.getStatus().name().toLowerCase());
            preparedStatement.setTimestamp(5, Timestamp.valueOf(task.getDateModified()));
            preparedStatement.setInt(6, task.getModifierId());
            preparedStatement.setInt(7, task.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(Task task, int loggedUserId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM tasks WHERE id=?")) {

            preparedStatement.setInt(1, task.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public List<Task> getAll(int parentProjectId) {
        List<Task> tasks = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM tasks WHERE" +
                     " project_id=?")) {

            preparedStatement.setInt(1, parentProjectId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Task task = createTask(resultSet);
                    tasks.add(task);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
        return tasks;
    }

    @Override
    public void assign(int userId, int taskId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE tasks SET assignee_id=? WHERE id=?")) {

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, taskId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void changeStatus(String newStatus, int taskId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE tasks SET status=? WHERE id=?")) {

            preparedStatement.setString(1, newStatus);
            preparedStatement.setInt(2, taskId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }
}
