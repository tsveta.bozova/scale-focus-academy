package academy.scalefocus.repositories;

import academy.scalefocus.entities.WorkLog;

import java.util.List;

public interface WorkLogRepository {

    WorkLog getById(int workLogId);

    void add(WorkLog workLog);

    void edit(WorkLog workLog);

    void delete(int workLogId);

    List<WorkLog> getAllWorkLogsByCreator(int userId);

    List<WorkLog> getAllWorkLogs(int parentTaskId);
}
