package academy.scalefocus.repositories;

import academy.scalefocus.entities.Task;

import java.util.List;

public interface TaskRepository {

    Task getById(int id);

    void add(Task task);

    void edit(Task task);

    void delete(Task task, int loggedUserId);

    List<Task> getAll(int parentProjectId);

    void assign(int userId, int taskId);

    void changeStatus(String newStatus, int taskId);
}
