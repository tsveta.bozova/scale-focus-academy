package academy.scalefocus.repositories;

import academy.scalefocus.entities.User;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.tools.ConsoleManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(UserRepositoryImpl.class);
    Connection connection = DatabaseService.getConnection();

    public UserRepositoryImpl() {
        User user = new User();

        user.setUsername("admin");
        user.setPassword("adminpass");
        user.setFirstName("Administrator");
        user.setLastName("Administrator");
        user.setIsAdmin(true);
        user.setDateCreated(LocalDateTime.now());
        user.setCreatorId(1);
        user.setModifierId(1);
        user.setDateModified(LocalDateTime.now());

        List<User> users = getAllUsers();

        if (users != null && users.isEmpty()) {
            add(user);
        }
    }

    private User createUser(ResultSet resultSet) throws SQLException {
        User user = new User();

        user.setId(resultSet.getInt("id"));
        user.setUsername(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setIsAdminFromString(resultSet.getString("is_admin"));
        user.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        user.setCreatorId(resultSet.getInt("creator_id"));
        user.setModifierId(resultSet.getInt("modifier_id"));
        user.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());

        return user;
    }

    @Override
    public List<User> getAllRelatedUsers(int projectId) {
        List<User> users = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users u " +
                "INNER JOIN team_member tm ON u.id = tm.user_id INNER JOIN teams_projects tp ON tm.team_id = tp.team_id " +
                "WHERE project_id=?")) {
            preparedStatement.setInt(1, projectId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    User user = createUser(resultSet);
                    users.add(user);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return users;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            users = new ArrayList<>();
            while (resultSet.next()) {
                User user = createUser(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return users;
    }

    @Override
    public User getById(int userId) {
        User user = new User();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE" +
                " id=?")) {

            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    user = createUser(resultSet);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return user;
    }

    @Override
    public void add(User user) {
        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users " +
                "(username, password, first_name, last_name, is_admin, date_created, creator_id, modifier_id, date_modified) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getIsAdminAsString());
            preparedStatement.setTimestamp(6, Timestamp.valueOf(user.getDateCreated()));
            preparedStatement.setInt(7, user.getCreatorId());
            preparedStatement.setInt(8, user.getModifierId());
            preparedStatement.setTimestamp(9, Timestamp.valueOf(user.getDateModified()));

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void edit(User user, int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET username=?, " +
                "password=?, first_name=?, last_name=?, is_admin=?, modifier_id=?, date_modified=? " +
                "WHERE id=?")) {

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getIsAdminAsString());
            preparedStatement.setInt(6, user.getModifierId());
            preparedStatement.setTimestamp(7, Timestamp.valueOf(user.getDateModified()));
            preparedStatement.setInt(8, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(int userId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE projects SET creator_id=?, " +
                " modifier_id=? WHERE creator_id=? OR modifier_id=?")) {
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, 1);
            preparedStatement.setInt(3, userId);
            preparedStatement.setInt(4, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE tasks SET creator_id=?, " +
                " modifier_id=?, assignee_id=? WHERE creator_id=? OR modifier_id=? OR assignee_id=?")) {
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, 1);
            preparedStatement.setInt(3, 1);
            preparedStatement.setInt(4, userId);
            preparedStatement.setInt(5, userId);
            preparedStatement.setInt(6, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE teams SET creator_id=?, " +
                " modifier_id=? WHERE creator_id=? OR modifier_id=?")) {
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, 1);
            preparedStatement.setInt(3, userId);
            preparedStatement.setInt(4, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE worklogs SET user_id=?" +
                " WHERE user_id=?")) {
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET modifier_id=?" +
                " WHERE modifier_id=?")) {
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM users WHERE id=?")) {

            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public User getByUserNameAndPassword(String userName, String password) {
        User user = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE" +
                " username=? AND password=?")) {

            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, password);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    user = createUser(resultSet);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return user;
    }
}
