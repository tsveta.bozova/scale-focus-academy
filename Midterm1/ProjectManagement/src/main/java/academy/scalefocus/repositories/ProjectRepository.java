package academy.scalefocus.repositories;

import academy.scalefocus.entities.Project;

import java.util.List;

public interface ProjectRepository {

    Project getById(int projectId);

    List<Project> getAllProjects();

    void add(Project project);

    void edit(Project project, int projectId);

    void delete(int projectId);

    List<Project> getAllRelatedProjects(int userId);

    List<Project> getAllProjectsByCreator(int userId);

    void assignToTeam(int teamId, int projectId);
}
