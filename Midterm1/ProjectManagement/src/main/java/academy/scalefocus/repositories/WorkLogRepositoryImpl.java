package academy.scalefocus.repositories;

import academy.scalefocus.entities.WorkLog;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.tools.ConsoleManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkLogRepositoryImpl implements WorkLogRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(WorkLogRepositoryImpl.class);
    Connection connection = DatabaseService.getConnection();

    @Override
    public WorkLog getById(int workLogId) {
        WorkLog workLog = new WorkLog();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM worklogs WHERE" +
                " id=?")) {

            preparedStatement.setInt(1, workLogId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    workLog = createWorkLog(resultSet);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return workLog;
    }

    @Override
    public void add(WorkLog workLog) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO worklogs " +
                "(task_id, user_id, hours_spent, date_worked) " +
                "VALUES (?,?,?,?)")) {

            preparedStatement.setInt(1, workLog.getTaskId());
            preparedStatement.setInt(2, workLog.getUserId());
            preparedStatement.setInt(3, workLog.getHoursSpent());
            preparedStatement.setDate(4, Date.valueOf(workLog.getDate()));

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void edit(WorkLog workLog) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE worklogs SET task_id=?, user_id=?, " +
                "hours_spent=?, date_worked=? WHERE id=?")) {

            preparedStatement.setInt(1, workLog.getTaskId());
            preparedStatement.setInt(2, workLog.getUserId());
            preparedStatement.setInt(3, workLog.getHoursSpent());
            preparedStatement.setDate(4, Date.valueOf(workLog.getDate()));
            preparedStatement.setInt(5, workLog.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(int workLogId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM worklogs WHERE id=?")) {

            preparedStatement.setInt(1, workLogId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public List<WorkLog> getAllWorkLogsByCreator(int userId) {
        List<WorkLog> workLogs = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM worklogs WHERE user_id=?")) {

            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    WorkLog workLog = createWorkLog(resultSet);
                    workLogs.add(workLog);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return workLogs;
    }

    @Override
    public List<WorkLog> getAllWorkLogs(int parentTaskId) {
        List<WorkLog> workLogs = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM worklogs WHERE task_id=?")) {

            preparedStatement.setInt(1, parentTaskId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    WorkLog workLog = createWorkLog(resultSet);
                    workLogs.add(workLog);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return workLogs;
    }

    private WorkLog createWorkLog(ResultSet resultSet) throws SQLException {
        WorkLog workLog = new WorkLog();

        workLog.setId(resultSet.getInt("id"));
        workLog.setTaskId(resultSet.getInt("task_id"));
        workLog.setUserId(resultSet.getInt("user_id"));
        workLog.setHoursSpent(resultSet.getInt("hours_spent"));
        workLog.setDate(resultSet.getDate("date_worked").toLocalDate());

        return workLog;
    }
}
