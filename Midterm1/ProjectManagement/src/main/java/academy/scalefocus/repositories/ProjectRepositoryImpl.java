package academy.scalefocus.repositories;

import academy.scalefocus.tools.ConsoleManager;
import academy.scalefocus.services.DatabaseService;
import academy.scalefocus.entities.Project;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryImpl implements ProjectRepository {
    private static final String EXCEPTION_CUSTOM_MESSAGE = "Can not execute the query";
    private static final Logger logger = Logger.getLogger(ProjectRepositoryImpl.class);
    private Connection connection = DatabaseService.getConnection();

    private Project createProject(ResultSet resultSet) throws SQLException {
        Project project = new Project();

        project.setId(resultSet.getInt("id"));
        project.setTitle(resultSet.getString("title"));
        project.setDescription(resultSet.getString("description"));
        project.setDateCreated(resultSet.getTimestamp("date_created").toLocalDateTime());
        project.setCreatorId(resultSet.getInt("creator_id"));
        project.setDateModified(resultSet.getTimestamp("date_modified").toLocalDateTime());
        project.setModifierId(resultSet.getInt("modifier_id"));

        return project;
    }

    @Override
    public Project getById(int projectId) {
        Project project = new Project();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM projects WHERE" +
                " id=?")) {

            preparedStatement.setInt(1, projectId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    project = createProject(resultSet);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return project;
    }

    @Override
    public List<Project> getAllProjects() {
        List<Project> projects = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM projects");
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                Project project = createProject(resultSet);
                projects.add(project);
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return projects;
    }

    @Override
    public void add(Project project) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO projects " +
                "(title, description, date_created, creator_id, date_modified, modifier_id) " +
                "VALUES (?,?,?,?,?,?)")) {

            preparedStatement.setString(1, project.getTitle());
            preparedStatement.setString(2, project.getDescription());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(project.getDateCreated()));
            preparedStatement.setInt(4, project.getCreatorId());
            preparedStatement.setTimestamp(5, Timestamp.valueOf(project.getDateModified()));
            preparedStatement.setInt(6, project.getModifierId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

    }

    @Override
    public void edit(Project project, int projectId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE projects SET title=?, " +
                "description=?, date_modified=?, modifier_id=? " +
                "WHERE id=?")) {

            preparedStatement.setString(1, project.getTitle());
            preparedStatement.setString(2, project.getDescription());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(project.getDateModified()));
            preparedStatement.setInt(4, project.getModifierId());
            preparedStatement.setInt(5, projectId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }

    @Override
    public void delete(int projectId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM projects WHERE id=?")) {
            preparedStatement.setInt(1, projectId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            ConsoleManager.writeLine(e.getMessage());
        }
    }

    @Override
    public List<Project> getAllRelatedProjects(int userId) {
        List<Project> projects = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * " +
                "FROM projects p " +
                "INNER JOIN teams_projects tp " +
                "ON p.id = tp.project_id " +
                "INNER JOIN team_member tm " +
                "ON tp.team_id = tm.team_id " +
                "WHERE tm.user_id=? OR p.creator_id=?")) {
            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, 1);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Project project = createProject(resultSet);
                    projects.add(project);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return projects;
    }

    @Override
    public List<Project> getAllProjectsByCreator(int userId) {
        List<Project> projects = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM projects WHERE " +
                "creator_id=?")) {

            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Project project = createProject(resultSet);
                    projects.add(project);
                }
            }
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }

        return projects;
    }

    @Override
    public void assignToTeam(int teamId, int projectId) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO teams_projects " +
                "(team_id, project_id) VALUES (?,?)")) {

            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, projectId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(EXCEPTION_CUSTOM_MESSAGE, e);
            ConsoleManager.readLine();
        }
    }
}
