package academy.scalefocus.repositories;

import academy.scalefocus.entities.User;

import java.util.List;

public interface UserRepository {

    List<User> getAllRelatedUsers(int projectId);

    List<User> getAllUsers();

    User getById(int id);

    void add(User user);

    void edit(User user, int id);

    void delete(int id);

    User getByUserNameAndPassword(String userName, String password);
}
