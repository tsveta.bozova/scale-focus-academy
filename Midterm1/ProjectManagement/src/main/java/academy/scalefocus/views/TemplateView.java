package academy.scalefocus.views;

import academy.scalefocus.enums.MenuChoices;

public abstract class TemplateView {

    public void run() {
        while (true) {
            MenuChoices choice = renderMenu();

            if (choice.equals(MenuChoices.LIST)) {
                list();
            } else if (choice.equals(MenuChoices.CREATE)) {
                add();
            } else if (choice.equals(MenuChoices.EDIT)) {
                edit();
            } else if (choice.equals(MenuChoices.DELETE)) {
                delete();
            } else if (choice.equals(MenuChoices.EXIT)) {
                return;
            } else {
                break;
            }
        }
    }
    protected abstract void list();
    protected abstract void add();
    protected abstract void edit();
    protected abstract void delete();
    protected abstract MenuChoices renderMenu ();
}
