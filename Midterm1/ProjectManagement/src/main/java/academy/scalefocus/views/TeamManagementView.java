package academy.scalefocus.views;

import academy.scalefocus.entities.Team;
import academy.scalefocus.entities.User;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.TeamRepository;
import academy.scalefocus.repositories.TeamRepositoryImpl;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class TeamManagementView extends TemplateView{
    private int loggedUserId;
    private UserRepository userRepo;
    private TeamRepository teamRepo;

    public TeamManagementView() {
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.userRepo = new UserRepositoryImpl();
        this.teamRepo = new TeamRepositoryImpl();
    }

    @Override
    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

             if (choice.equals(MenuChoices.ASSIGN)) {
                 assignUser();
             }
        }
    }

    protected MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printTeamManagementMenu();

            String choice = ConsoleManager.readLine();

            if ("L".equalsIgnoreCase(choice)) {
                return MenuChoices.LIST;
            } else if ("A".equalsIgnoreCase(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equalsIgnoreCase(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equalsIgnoreCase(choice)) {
                return MenuChoices.DELETE;
            } else if ("S".equalsIgnoreCase(choice)) {
                return MenuChoices.ASSIGN;
            } else if ("X".equalsIgnoreCase(choice)) {
                return MenuChoices.EXIT;
            } else {
               ConsoleManager.printInvalidMessage();
            }
        }
    }

    protected void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add Team####");

        Team team = new Team();

        ConsoleManager.write("Title: ");
        team.setTitle(ConsoleManager.readLine());
        team.setDateCreated(LocalDateTime.now());
        team.setCreatorId(this.loggedUserId);
        team.setDateModified(LocalDateTime.now());
        team.setModifierId(this.loggedUserId);

        this.teamRepo.add(team);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void list() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List Teams####");

        List<Team> teams = this.teamRepo.getAllTeams();

        teams.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    protected void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Team####");

        List<Team> teams = this.teamRepo.getAllTeams();

        teams.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Team: ");
        int teamId = Integer.parseInt(ConsoleManager.readLine());

        Team team = this.teamRepo.getById(teamId);

        ConsoleManager.write("Title ( " + team.getTitle() + " ): ");
        team.setTitle(ConsoleManager.readLine());
        team.setModifierId(this.loggedUserId);
        team.setDateModified(LocalDateTime.now());

        this.teamRepo.edit(team, teamId);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Team####");

        List<Team> teams = this.teamRepo.getAllTeams();

        teams.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Team: ");
        int teamId = Integer.parseInt(ConsoleManager.readLine());

        this.teamRepo.delete(teamId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void assignUser() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Assign User####");

        List<Team> teams = this.teamRepo.getAllTeams();

        teams.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Team: ");
        int teamId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        ConsoleManager.write("Assign to: ");
        int userToAssignId = Integer.parseInt(ConsoleManager.readLine());

        this.teamRepo.assignUser(userToAssignId, teamId);

        ConsoleManager.printSuccessfulMessage();
    }
}
