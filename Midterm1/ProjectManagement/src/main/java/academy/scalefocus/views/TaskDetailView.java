package academy.scalefocus.views;

import academy.scalefocus.entities.Task;
import academy.scalefocus.entities.WorkLog;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.WorkLogRepository;
import academy.scalefocus.repositories.WorkLogRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDate;
import java.util.List;

public class TaskDetailView extends TemplateView{
    private Task parentTask;
    private WorkLogRepository workLogRepo;
    private int loggedUserId;

    public TaskDetailView(Task task) {
        this.parentTask = task;
        this.workLogRepo = new WorkLogRepositoryImpl();
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
    }

    @Override
    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

            if (choice.equals(MenuChoices.VIEW)) {
                view();
            }
        }
    }

    protected MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printTaskDetailMenu();

            String choice = ConsoleManager.readLine().toUpperCase();

            if ("A".equals(choice)) {
                return MenuChoices.CREATE;
            } else if ("L".equals(choice)){
                return MenuChoices.LIST;
            }else if ("E".equals(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equals(choice)) {
                return MenuChoices.DELETE;
            } else if ("V".equals(choice)) {
                return MenuChoices.VIEW;
            } else if ("X".equals(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    protected void list () {
        if (this.loggedUserId != parentTask.getAssigneeId()) {
            ConsoleManager.printDeniedMessage();
            return;
        }

        ConsoleManager.clear();
        ConsoleManager.writeLine("####List All Work Logs####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogs(this.parentTask.getId());

        workLogs.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    protected void add() {
        if (this.loggedUserId != parentTask.getAssigneeId()) {
            ConsoleManager.printDeniedMessage();
            return;
        }

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Log Work####");

        WorkLog workLog = new WorkLog();

        workLog.setTaskId(this.parentTask.getId());
        workLog.setUserId(this.loggedUserId);
        ConsoleManager.write("Hours: ");
        workLog.setHoursSpent(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Date: ");
        workLog.setDate(LocalDate.parse(ConsoleManager.readLine()));

        this.workLogRepo.add(workLog);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Log####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogsByCreator(this.loggedUserId);

        workLogs.forEach(w -> ConsoleManager.writeLine("Work Log ( " + w.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Log: ");
        int logId = Integer.parseInt(ConsoleManager.readLine());

        WorkLog workLog = this.workLogRepo.getById(logId);

        ConsoleManager.write("Task ID ( " + workLog.getTaskId() + " ): ");
        workLog.setTaskId(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("User ID ( " + workLog.getUserId() + " ): ");
        workLog.setUserId(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Hours ( " + workLog.getHoursSpent() + " ): ");
        workLog.setHoursSpent(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Date ( " + workLog.getDate() + " ): ");
        workLog.setDate(LocalDate.parse(ConsoleManager.readLine()));

        this.workLogRepo.edit(workLog);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Log####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogsByCreator(this.loggedUserId);

        workLogs.forEach(w -> ConsoleManager.writeLine("Work Log ( " + w.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Log: ");
        int logId = Integer.parseInt(ConsoleManager.readLine());

        this.workLogRepo.delete(logId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void view() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####View All Logs####");

        List<WorkLog> workLogs = this.workLogRepo.getAllWorkLogs(this.parentTask.getId());

        workLogs.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }
}
