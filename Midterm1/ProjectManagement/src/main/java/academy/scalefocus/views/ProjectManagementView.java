package academy.scalefocus.views;

import academy.scalefocus.entities.Project;
import academy.scalefocus.entities.Team;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.*;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class ProjectManagementView extends TemplateView{
    private int loggedUserId;
    private TeamRepository teamRepo;
    private ProjectRepository projectRepo;

    public ProjectManagementView() {
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.teamRepo = new TeamRepositoryImpl();
        this.projectRepo = new ProjectRepositoryImpl();
    }

    @Override
    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

             if (choice.equals(MenuChoices.ASSIGN)) {
                assignToTeam();
            } else if (choice.equals(MenuChoices.VIEW)) {
                view();
            } else if (choice.equals(MenuChoices.BROWSE)) {
                browse();
            }
        }
    }

    protected MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printProjectMenu();

            String choice = ConsoleManager.readLine().toUpperCase();

            if ("L".equals(choice)) {
                return MenuChoices.LIST;
            } else if ("C".equals(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equalsIgnoreCase(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equalsIgnoreCase(choice)) {
                return MenuChoices.DELETE;
            } else if ("A".equals(choice)) {
                return MenuChoices.ASSIGN;
            } else if ("V".equals(choice)) {
                return MenuChoices.VIEW;
            } else if ("B".equals(choice)) {
                return MenuChoices.BROWSE;
            } else if ("X".equalsIgnoreCase(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    protected void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Create Project####");

        Project project = new Project();

        ConsoleManager.write("Title: ");
        project.setTitle(ConsoleManager.readLine());
        ConsoleManager.write("Description: ");
        project.setDescription(ConsoleManager.readLine());
        project.setDateCreated(LocalDateTime.now());
        project.setCreatorId(this.loggedUserId);
        project.setDateModified(LocalDateTime.now());
        project.setModifierId(this.loggedUserId);

        this.projectRepo.add(project);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void list() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List Projects####");

        List<Project> projects = this.projectRepo.getAllRelatedProjects(this.loggedUserId);

        if (isEmpty(projects)) return;

        projects.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    private boolean isEmpty(List<Project> projects) {
        if (projects.isEmpty()) {
            ConsoleManager.writeLine("You don't have Projects");
            ConsoleManager.writeLine("Press [Enter] to continue");
            ConsoleManager.readLine();
            return true;
        }
        return false;
    }

    protected void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Project####");

        List<Project> projects = this.projectRepo.getAllProjectsByCreator(this.loggedUserId);

        if (isEmpty(projects)) return;

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        Project project = this.projectRepo.getById(projectId);

        ConsoleManager.write("Title ( " + project.getTitle() + " ): ");
        project.setTitle(ConsoleManager.readLine());
        ConsoleManager.writeLine("Description ( " + project.getDescription() + " ): ");
        project.setDescription(ConsoleManager.readLine());
        project.setDateModified(LocalDateTime.now());
        project.setModifierId(this.loggedUserId);

        this.projectRepo.edit(project, projectId);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Project####");

        List<Project> projects = this.projectRepo.getAllProjectsByCreator(this.loggedUserId);

        if (isEmpty(projects)) return;

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        this.projectRepo.delete(projectId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void assignToTeam() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Assign Project####");

        List<Project> projects = this.projectRepo.getAllProjectsByCreator(this.loggedUserId);

        if (isEmpty(projects)) return;

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();

        List<Team> teams = this.teamRepo.getAllTeams();

        teams.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.write("Assign to: ");
        int teamToAssignId = Integer.parseInt(ConsoleManager.readLine());

        this.projectRepo.assignToTeam(teamToAssignId, projectId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void view() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####View Project####");

        List<Project> projects = this.projectRepo.getAllRelatedProjects(this.loggedUserId);

        if (isEmpty(projects)) return;

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        Project project = this.projectRepo.getById(projectId);

        ConsoleManager.clear();

        TaskManagementView taskView = new TaskManagementView(project);
        taskView.run();
    }

    private void browse() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Browse Project####");

        List<Project> projects = this.projectRepo.getAllProjects();

        if (isEmpty(projects)) return;

        projects.forEach(p -> ConsoleManager.writeLine(p.getTitle() + " ( " + p.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Project: ");
        int projectId = Integer.parseInt(ConsoleManager.readLine());

        Project project = this.projectRepo.getById(projectId);

        ConsoleManager.clear();

        TemplateView taskView = new TaskManagementView(project);
        taskView.list();
    }
}
