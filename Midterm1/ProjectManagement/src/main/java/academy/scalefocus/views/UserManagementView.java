package academy.scalefocus.views;

import academy.scalefocus.entities.User;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;

public class UserManagementView extends TemplateView{
    private int loggedUserId;
    private UserRepository userRepo;

    public UserManagementView() {
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.userRepo = new UserRepositoryImpl();
    }

    protected MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.printUserManagementMenu();

            String choice = ConsoleManager.readLine();

            if ("L".equalsIgnoreCase(choice)) {
                return MenuChoices.LIST;
            } else if ("A".equalsIgnoreCase(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equalsIgnoreCase(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equalsIgnoreCase(choice)) {
                return MenuChoices.DELETE;
            } else if ("X".equalsIgnoreCase(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    protected void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add User####");

        User user = new User();

        ConsoleManager.write("Username: ");
        user.setUsername(ConsoleManager.readLine());
        ConsoleManager.write("Password: ");
        user.setPassword(ConsoleManager.readLine());
        ConsoleManager.write("First Name: ");
        user.setFirstName(ConsoleManager.readLine());
        ConsoleManager.write("Last Name: ");
        user.setLastName(ConsoleManager.readLine());
        ConsoleManager.write("Is Admin: ");
        user.setIsAdmin(Boolean.parseBoolean(ConsoleManager.readLine()));
        user.setDateCreated(LocalDateTime.now());
        user.setCreatorId(this.loggedUserId);
        user.setModifierId(this.loggedUserId);
        user.setDateModified(LocalDateTime.now());

        this.userRepo.add(user);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void list() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List Users####");

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    protected void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit User####");

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of user: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        User user = this.userRepo.getById(userId);

        ConsoleManager.write("Username ( " + user.getUsername() + " ): ");
        user.setUsername(ConsoleManager.readLine());
        ConsoleManager.write("Password ( " + user.getPassword() + " ) : ");
        user.setPassword(ConsoleManager.readLine());
        ConsoleManager.write("First Name ( " + user.getFirstName() + " ): ");
        user.setFirstName(ConsoleManager.readLine());
        ConsoleManager.write("Last Name: ( " + user.getLastName() + " ): ");
        user.setLastName(ConsoleManager.readLine());
        ConsoleManager.write("Is Admin: ( " + user.getIsAdmin() + " ): ");
        user.setIsAdmin(Boolean.parseBoolean(ConsoleManager.readLine()));

        user.setModifierId(this.loggedUserId);
        user.setDateModified(LocalDateTime.now());

        this.userRepo.edit(user, userId);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete User####");

        List<User> users = this.userRepo.getAllUsers();

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of user: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        this.userRepo.delete(userId);

        ConsoleManager.printSuccessfulMessage();
    }
}
