package academy.scalefocus.views;

import academy.scalefocus.entities.Project;
import academy.scalefocus.entities.Task;
import academy.scalefocus.entities.User;
import academy.scalefocus.enums.MenuChoices;
import academy.scalefocus.enums.TaskStatus;
import academy.scalefocus.repositories.TaskRepository;
import academy.scalefocus.repositories.TaskRepositoryImpl;
import academy.scalefocus.repositories.UserRepository;
import academy.scalefocus.repositories.UserRepositoryImpl;
import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class TaskManagementView extends TemplateView{
    private Project parentProject;
    private int loggedUserId;
    private TaskRepository taskRepo;
    private UserRepository userRepo;

    public TaskManagementView(Project parentProject) {
        this.parentProject = parentProject;
        this.loggedUserId = AuthenticationService.getInstance().getLoggedUser().getId();
        this.taskRepo = new TaskRepositoryImpl();
        this.userRepo = new UserRepositoryImpl();
    }

    @Override
    public void run() {

        while (true) {
            MenuChoices choice = renderMenu();

             if (choice.equals(MenuChoices.VIEW)) {
                view();
            } else if (choice.equals(MenuChoices.ASSIGN)) {
                assign();
            } else if (choice.equals(MenuChoices.STATUS)) {
                changeStatus();
            }
        }
    }

    protected MenuChoices renderMenu() {

        while (true) {
            ConsoleManager.clear();

            if (this.loggedUserId == parentProject.getCreatorId()) {
                ConsoleManager.writeLine("[C]reate Task");
                ConsoleManager.writeLine("[E]dit Task");
                ConsoleManager.writeLine("[D]elete Task");
            }
            ConsoleManager.writeLine("[L]ist Tasks");
            ConsoleManager.writeLine("[Ch]ange assignee");
            ConsoleManager.writeLine("[S]tatus");
            ConsoleManager.writeLine("[V]iew task details");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");

            String choice = ConsoleManager.readLine().toUpperCase();

            if ("L".equals(choice)) {
                return MenuChoices.LIST;
            } else if ("C".equals(choice)) {
                return MenuChoices.CREATE;
            } else if ("E".equals(choice)) {
                return MenuChoices.EDIT;
            } else if ("D".equals(choice)) {
                return MenuChoices.DELETE;
            } else if ("V".equals(choice)) {
                return MenuChoices.VIEW;
            } else if ("CH".equals(choice)) {
                return MenuChoices.ASSIGN;
            } else if ("S".equals(choice)) {
                return MenuChoices.STATUS;
            } else if ("X".equals(choice)) {
                return MenuChoices.EXIT;
            } else {
                ConsoleManager.printInvalidMessage();
            }
        }
    }

    protected void add() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Add Task####");

        List<User> users = this.userRepo.getAllRelatedUsers(this.parentProject.getId());

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        Task task = new Task();

        ConsoleManager.write("Assign to: ");
        task.setAssigneeId(Integer.parseInt(ConsoleManager.readLine()));
        task.setProjectId(this.parentProject.getId());
        ConsoleManager.write("Title: ");
        task.setTitle(ConsoleManager.readLine());
        ConsoleManager.write("Description: ");
        task.setDescription(ConsoleManager.readLine());
        task.setStatus(TaskStatus.PENDING);
        task.setDateCreated(LocalDateTime.now());
        task.setCreatorId(this.loggedUserId);
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        this.taskRepo.add(task);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void list() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####List All Tasks####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(ConsoleManager::writeLine);

        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }


    protected void edit() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Edit Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.write("Assignee ID ( " + task.getAssigneeId() + " ): ");
        task.setAssigneeId(Integer.parseInt(ConsoleManager.readLine()));
        ConsoleManager.write("Title ( " + task.getTitle() + " ): ");
        task.setTitle(ConsoleManager.readLine());
        ConsoleManager.write("Description ( " + task.getDescription() + " ): ");
        task.setDescription(ConsoleManager.readLine());
        ConsoleManager.write("Status ( " + task.getStatus().name() + " ): ");
        task.setStatus(Enum.valueOf(TaskStatus.class, ConsoleManager.readLine().toUpperCase()));
        task.setDateModified(LocalDateTime.now());
        task.setModifierId(this.loggedUserId);

        this.taskRepo.edit(task);

        ConsoleManager.printSuccessfulMessage();
    }

    protected void delete() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Delete Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);
        this.taskRepo.delete(task, this.loggedUserId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void view() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####View Task####");

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        tasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.writeLine();
        ConsoleManager.write("Enter ID of Task: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.clear();

        TemplateView taskDetailView = new TaskDetailView(task);
        taskDetailView.run();
    }

    private void assign() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Change Assignee####");

        ConsoleManager.writeLine();

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        List<Task> assignedTasks = tasks.stream()
                .filter(t -> t.getAssigneeId() == this.loggedUserId)
                .collect(Collectors.toList());

        if (isEmpty(assignedTasks)) return;

        assignedTasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.write("Task ID: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        ConsoleManager.clear();
        ConsoleManager.writeLine("####Users####");

        List<User> users = this.userRepo.getAllRelatedUsers(this.parentProject.getId());

        users.forEach(u -> ConsoleManager.writeLine(u.getUsername() + " ( " + u.getId() + " )"));

        ConsoleManager.write("Assign to: ");
        int userId = Integer.parseInt(ConsoleManager.readLine());

        this.taskRepo.assign(userId, taskId);

        ConsoleManager.printSuccessfulMessage();
    }

    private void changeStatus() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("####Change Status####");

        ConsoleManager.writeLine();

        List<Task> tasks = this.taskRepo.getAll(this.parentProject.getId());

        List<Task> assignedTasks = tasks.stream()
                .filter(t -> t.getAssigneeId() == this.loggedUserId)
                .collect(Collectors.toList());

        if (isEmpty(assignedTasks)) return;

        assignedTasks.forEach(t -> ConsoleManager.writeLine(t.getTitle() + " ( " + t.getId() + " )"));

        ConsoleManager.write("Task ID: ");
        int taskId = Integer.parseInt(ConsoleManager.readLine());

        Task task = this.taskRepo.getById(taskId);

        ConsoleManager.write(task.getTitle() + "( " + task.getStatus() + " ): ");
        String newStatus = ConsoleManager.readLine();

        this.taskRepo.changeStatus(newStatus, taskId);

        ConsoleManager.printSuccessfulMessage();
    }

    private boolean isEmpty(List<Task> assignedTasks) {
        if(assignedTasks.isEmpty()) {
            ConsoleManager.writeLine("You don't have assigned Tasks");
            ConsoleManager.writeLine("Press [Enter] to continue");
            ConsoleManager.readLine();
            return true;
        }
        return false;
    }
}
