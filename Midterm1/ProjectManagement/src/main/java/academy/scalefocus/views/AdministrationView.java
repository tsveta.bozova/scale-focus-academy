package academy.scalefocus.views;


import academy.scalefocus.services.AuthenticationService;
import academy.scalefocus.tools.ConsoleManager;

public class AdministrationView {
    public void run() {

        while (true) {
            ConsoleManager.clear();

            if (AuthenticationService.getInstance().getLoggedUser().getIsAdmin()) {
                ConsoleManager.writeLine("[U]ser Management");
                ConsoleManager.writeLine("[T]eam Management");
            }
            ConsoleManager.writeLine("[P]roject Management");
            ConsoleManager.writeLine("E[x]it");

            ConsoleManager.write(">");
            String choice = ConsoleManager.readLine();

            if ("U".equalsIgnoreCase(choice)) {
                usersManagement();
                break;
            } else if ("P".equalsIgnoreCase(choice)) {
                projectManagement();
                break;
            } else if ("T".equalsIgnoreCase(choice)) {
                teamManagement();
                break;
            } else if ("X".equalsIgnoreCase(choice)) {
                return;
            } else {
                ConsoleManager.printInvalidMessage();
                break;
            }
        }
    }

    private void teamManagement() {
        TemplateView view = new TeamManagementView();
        view.run();
    }

    private void usersManagement() {
        TemplateView view = new UserManagementView();
        view.run();
    }

    private void projectManagement() {
        TemplateView view = new ProjectManagementView();
        view.run();
    }
}
