package academy.scalefocus.tools;


import java.util.Scanner;


public class ConsoleManager {
    private static Scanner scanner = new Scanner(System.in);

    public static void clear() {

        for (int i = 0; i < 5; i++) {

            System.out.println("\n\n\n\n\n\n\n\n\n");
        }
    }

    public static String readLine() {

        return scanner.nextLine();
    }

    public static void writeLine() {

        System.out.println();
    }

    public static <T> void writeLine(T value) {

        System.out.println(value);
    }

    public static <T> void write(T value) {

        System.out.print(value);
    }

    public static void printDeniedMessage() {
        ConsoleManager.clear();
        ConsoleManager.writeLine("Access denied!");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    public static void printSuccessfulMessage () {
        ConsoleManager.clear();
        ConsoleManager.writeLine("Successful operation");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    public static void printInvalidMessage () {
        ConsoleManager.clear();
        ConsoleManager.writeLine("Invalid choice!");
        ConsoleManager.writeLine("Press [Enter] to continue");
        ConsoleManager.readLine();
    }

    public static void printTaskDetailMenu () {
        ConsoleManager.clear();
        ConsoleManager.writeLine("[Li]st Work Logs");
        ConsoleManager.writeLine("[L]og work to a Task");
        ConsoleManager.writeLine("[E]dit work log");
        ConsoleManager.writeLine("[D]elete work log");
        ConsoleManager.writeLine("[V]iew work logs");
        ConsoleManager.writeLine("E[x]it");
        ConsoleManager.write(">");
    }

    public static void printUserManagementMenu () {
        ConsoleManager.clear();
        ConsoleManager.writeLine("[L]ist Users");
        ConsoleManager.writeLine("[A]dd user");
        ConsoleManager.writeLine("[E]dit User");
        ConsoleManager.writeLine("[D]elete User");
        ConsoleManager.writeLine("E[x]it");
        ConsoleManager.write(">");
    }

    public static void printTeamManagementMenu () {
        ConsoleManager.clear();
        ConsoleManager.writeLine("[L]ist Teams");
        ConsoleManager.writeLine("[A]dd Team");
        ConsoleManager.writeLine("[E]dit Team");
        ConsoleManager.writeLine("[D]elete Team");
        ConsoleManager.writeLine("A[s]sign User to a Team");
        ConsoleManager.writeLine("E[x]it");
        ConsoleManager.write(">");
    }

    public static void printProjectMenu () {
        ConsoleManager.clear();
        ConsoleManager.writeLine("[L]ist Project");
        ConsoleManager.writeLine("[C]reate Project");
        ConsoleManager.writeLine("[E]dit Project");
        ConsoleManager.writeLine("[D]elete Project");
        ConsoleManager.writeLine("[A]ssign Project to a Team");
        ConsoleManager.writeLine("[V]iew Project");
        ConsoleManager.writeLine("[B]rowse projects");
        ConsoleManager.writeLine("E[x]it");
        ConsoleManager.write(">");
    }
}
