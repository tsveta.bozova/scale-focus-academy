package academy.scalefocus.enums;

public enum TaskStatus {
    PENDING, IN_PROGRESS, COMPLETED;
}
