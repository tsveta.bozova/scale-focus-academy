package academy.scalefocus.enums;

public enum MenuChoices {
    LIST, BROWSE, CREATE, EDIT, DELETE, STATUS, ASSIGN, VIEW, EXIT
}
